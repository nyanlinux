#include <stdio.h>
#define loop for(;;)
int main(void)
{
	int i;
	int j;
	int n;

	i = 0;
	loop {
		if (i == 11)
			break;

		j = 0;
		loop {
			if (j == 10)
				break;

			n = 10 * i + j;
			if (n > 108)
				break;
			printf("\033[%dm %3d\033[m", n, n);

			++j;
		}
		printf("\n");

		++i;
	}
	return 0;
}
#undef loop
