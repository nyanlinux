#!/bin/sh

# this is for emergency/rare use only, hence the non automated way
#
# The right ways would be:
#   - WSI native browser of openstreetmap tile servers
#   - local C code software renderer with partial ector data
#   - noscript/basic (x)html www site with C coded software renderer and
#     partial vector data or browsing some tile servers (found one but dead)

curl=/nyan/curl/current/bin/curl

# vitrolles: 43.46 5.24861111111
# marseille: 43.296346 5.369889
# aubagne: 43.29083 5.57083
#
# typical: at 43.28 latitude, 0.01 longitude is ~800 meters
# typical: at 5.58 longitude,  0.01 latitude is ~1400 meters
# XXX: broken, "paper_size": "Din A0"
cat >/tmp/map.json <<EOF
{
	"title": "emergency map",
	"bbox_bottom": 43.28,
	"bbox_left": 5.58,
	"bbox_right": 5.60,
	"bbox_top": 43.29
}
EOF

# XXX: broken
#$curl "https://api.get-map.org/apis/paper_formats"
# not tested: "layout" parameter
#$curl "https://api.get-map.org/apis/layouts"
# not tested: "style" parameter for base style
#$curl "https://api.get-map.org/apis/styles"
# not tested: "overlays" parmeter, list of overlays
#$curl "https://api.get-map.org/apis/overlays"

#$curl -X POST "https://api.get-map.org/apis/jobs/" -H "Content-Type: application/json" -d @/tmp/map.json -i -o /tmp/job.txt
# WAIT FOR 15 SECONDS
#$curl "https://api.get-map.org/apis/jobs/71079" -o /tmp/job-results.txt
#$curl -O "https://api.get-map.org/results//071079_2019-07-12_19-57_emergency-map.png"
