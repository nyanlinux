//---------------- global ---------------------------------------
/* API result */
var API_RESULT_SUCCESS = 0;
var API_RESULT_FAIL = 1;

/*login state*/
var LOGIN_STATE_SUCCESS = 5;
var LOGIN_STATE_LOGOUT = 0;
var LOGIN_STATE_PASSWORD_WRONG = 1;
var LOGIN_STATE_SOME_ONE_LOGINED = 2;


var MACRO_INVALID_STR = "-11111";
/*SIM card state*/

var  MACRO_UIM_APP_STATE_UNKNOWN_V01 = 0; 
var  MACRO_UIM_APP_STATE_DETECTED_V01 = 1; 
var  MACRO_UIM_APP_STATE_PIN1_OR_UPIN_REQ_V01 = 2; 
var  MACRO_UIM_APP_STATE_PUK1_OR_PUK_REQ_V01 = 3; 
var  MACRO_UIM_APP_STATE_PERSON_CHECK_REQ_V01 = 4; 
var  MACRO_UIM_APP_STATE_PIN1_PERM_BLOCKED_V01 = 5; 
var  MACRO_UIM_APP_STATE_ILLEGAL_V01 = 6; 
var  MACRO_UIM_APP_STATE_READY_V01 = 7; 
var  MACRO_SIM_CARD_INITING  = 11; 

/*SIM card PIN state*/
var  MACRO_UIM_PIN_STATE_UNKNOWN = 0; 
var  MACRO_UIM_PIN_STATE_ENABLED_NOT_VERIFIED = 1; 
var  MACRO_UIM_PIN_STATE_ENABLED_VERIFIED = 2; 
var  MACRO_UIM_PIN_STATE_DISABLED = 3; 
var  MACRO_UIM_PIN_STATE_BLOCKED = 4; 
var  MACRO_UIM_PIN_STATE_PERMANENTLY_BLOCKED = 5; 

var  MACRO_SIM_CARD_ABSENT           = 0; 
var  MACRO_SIM_CARD_PRESENT          = 1;
var  MACRO_SIM_CARD_NOT_INIT         = 2;
var  MACRO_SIM_CARD_CHV_BLOCKED       = 5;
var  MACRO_SIM_CARD_NOT_READY        = 0;
var  MACRO_SIM_CARD_VALID            = 7;
var  MACRO_SIM_CARD_REINIT           = 7;
var  MACRO_SIM_CARD_ILLEGAL_CARD     = 0; 
var  MACRO_SIM_CARD_INVALID          = 0;
var  MACRO_SIM_CARD_ERROR             = 6;
var  MACRO_CARD_SIM_LOCK_CHECK        = 4;
var  MACRO_SIM_CARD_API_ERROR         = 255;

var GSDI_PIN_NOT_INITIALISED         = 0;
var GSDI_PIN_DISABLED                = 7;
var GSDI_PIN_ENABLED                 = 2;
var GSDI_PIN_BLOCKED                 = 3;
var GSDI_PIN_PERM_BLOCKED            = 5;
var GSDI_MAX_PIN_STATUS              = 7;

var SIMLOCK_PERSO_NONE_REQUIRED = -1;/*no sim lock/sim lock unlock*/
var SIMLOCK_PERSO_NET_PIN_REQUIRED = 0; /*nck*/ 
var SIMLOCK_PERSO_NETSUB_PIN_REQUIRED = 1; /*nsck*/
var SIMLOCK_PERSO_SP_PIN_REQUIRED = 2;   /*spck*/
var SIMLOCK_PERSO_CORP_PIN_REQUIRED = 3; /*cck*/
var SIMLOCK_PERSO_PH_FSIM_PIN_REQUIRED = 4; /*pck*/

var SIMLOCK_PERSO_NET_PUK_REQUIRED = 15;  /*rck*/
var SIMLOCK_PERSO_NETSUB_PUK_REQUIRED =16;
var SIMLOCK_PERSO_SP_PUK_REQUIRED = 17;   
var SIMLOCK_PERSO_CORP_PUK_REQUIRED = 18; 
var SIMLOCK_PERSO_PH_FSIM_PUK_REQUIRED = 19;
var SIMLOCK_PERSO_RCK_FORBID = 30;/*rck forbid*/

var  MACRO__API_ERROR      = 255;
var MACRO_SAVE_PIN_ENABLED = 1;
var MACRO_SAVE_PIN_DISABLED = 0;

/*auto connect even when roaming*/
var MACRO_AUTO_CONNECT_ENABLED = 0;
var MACRO_AUTO_CONNECT_DISABLED = 1;

/*wan status*/
var MACRO_PPP_DISCONNECTED  = 0;
var MACRO_PPP_CONNECTING    = 1;
var MACRO_PPP_CONNECTED     = 2;
var MACRO_PPP_DISCONNECTING = 3;



var PPP_DIAL_AUTO = 0;
var PPP_DIAL_MANUAL = 1;

/*network type*/
var MACRO_NETWORKTYPE_NO_SERVICE = 0;
var MACRO_NETWORKTYPE_GPRS = 1;
var MACRO_NETWORKTYPE_EDGE = 2;
var MACRO_NETWORKTYPE_HSDPA = 3;
var MACRO_NETWORKTYPE_HSUPA = 4;
var MACRO_NETWORKTYPE_UMTS = 5;
var MACRO_NETWORKTYPE_CDMA = 6;
var MACRO_NETWORKTYPE_EV_DO_A = 7;
var MACRO_NETWORKTYPE_EV_DO_B = 8;
var MACRO_NETWORKTYPE_GSM = 9;
var MACRO_NETWORKTYPE_EV_DO_C = 10;
var MACRO_NETWORKTYPE_LTE = 11;
var MACRO_NETWORKTYPE_HSPA_PLUS = 12;
var MACRO_NETWORKTYPE_DC_HSPA_PLUS  = 13;

/*roam*/
var MACRO_ROAM_DISABLE  = 1;
var MACRO_ROAM_ENABLE  = 0;

/*wlan status*/
var MACRO_WLAN_DISABLED = 0;
var MACRO_WLAN_ENABLED = 1;
var MACRO_WLAN_WPS    = 2;

/*signal level*/
var MACRO_EVDO_LEVEL_ZERO = 0;
var MACRO_EVDO_LEVEL_ONE = 1;
var MACRO_EVDO_LEVEL_TWO = 2;
var MACRO_EVDO_LEVEL_THREE = 3;
var MACRO_EVDO_LEVEL_FOUR = 4;
var MACRO_EVDO_LEVEL_FIVE = 5;
var MACRO_EVDO_API_ERROR    = 255;

var MACRO_BATTERY_CHARGING = 0;
var MACRO_BATTERY_COMPLATE = 1;
var MACRO_BATTERY_NOCHARGE = 2;


/*sms status*/
var MACRO_SMS_DISENABLE = 0;
var MACRO_SMS_FULL = 1;
var MACRO_SMS_NOREAD = 2;
var MACRO_SMS_READ= 3;

var SMS_HANDLE_FORWARD = 0;
var SMS_HANDLE_REPLY = 1;
var SMS_HANDLE_READ = 2;
var SMS_HANDLE_DRAFT = 3;
var SMS_HANDLE_SENT = 4;
var SMS_HANDLE_NEW  = 5;

/*security*/
var SECRUTIY_TYPE_DISABLE = 0;
var SECRUTIY_TYPE_WEP = 1;
var SECRUTIY_TYPE_WPA_PSK = 2;
var SECRUTIY_TYPE_WPS2_PSK = 3;
var SECRUTIY_TYPE_WPA_MIXED = 4;

/* network mode*/
var NETWORK_AUTOMATIC = 4;
var NETWORK_GSM_ONLY  = 13;
var NETWORK_WCDMA_ONLY = 14;
/*Select wan mode*/
var SELECT_MODE_AUTOMATIC = 0;
var SELECT_MODE_MANUAL    = 1;
var SELECT_MODE_LIMITED_SRV = 2;

/*not support char*/
var MACRO_SUPPORT_CHAR_MIN = 32;
var MACRO_SUPPORT_CHAR_MAX = 127;
var MACRO_NOT_SUPPORT_CHAR_COMMA = 44;             //,
var MACRO_NOT_SUPPORT_CHAR_QUOTATION_MARK = 34;      //"
var MACRO_NOT_SUPPORT_CHAR_COLON = 58;          //:
var MACRO_NOT_SUPPORT_CHAR_SEMICOLON = 59;          //;
var MACRO_NOT_SUPPORT_BACKSLASH_MARK = 92;         //\
var MACRO_NOT_SUPPORT_CHAR_38 = 38;        //&
var MACRO_NOT_SUPPORT_CHAR_39 = 39;        //'
var MACRO_NOT_SUPPORT_CHAR_42 = 42;         //*
var MACRO_NOT_SUPPORT_CHAR_47 = 47;         ///
var MACRO_NOT_SUPPORT_CHAR_60 = 60;         //<
var MACRO_NOT_SUPPORT_CHAR_62 = 62;         //>
var MACRO_NOT_SUPPORT_CHAR_63 = 63;         //?
var MACRO_NOT_SUPPORT_CHAR_124 = 124;         //|

/*ssid*/
var MACRO_WLAN_SSID_NUMBER_START = 48;
var MACRO_WLAN_SSID_NUMBER_END = 57;
var MACRO_WLAN_SSID_UP_CHAR_START = 65;
var MACRO_WLAN_SSID_UP_CHAR_END = 90;
var MACRO_WLAN_SSID_LOW_CHAR_START = 97;
var MACRO_WLAN_SSID_LOW_CHAR_END = 122;
var MACRO_WLAN_SSID_CHAR_UNDERLINE = 95;
var MACRO_WLAN_SSID_CHAR_DASH = 45;
var MACRO_WLAN_SSID_CHAR_DOT = 46;
var MACRO_WLAN_SSID_CHAR_SPACE = 32;

/*sd card*/
var MACRO_SD_IS_EXIST = 1;
var MACRO_SD_NOT_EXIST = 0;
var MACRO_SD_USB_ONLY = 1;
var MACRO_SD_WEB_ONLY = 0;
var MACRO_SD_SHARE_ENABLE = 1;
var MACRO_SD_SHARE_DISABLE = 0;
var MACRO_SD_ALL = 0;
var MACRO_SD_PATH_DEFINE = 1;
var MACRO_SD_READ_WRITE = 1;
var MACRO_SD_READ_ONLY = 0;

var DEFUALT_PROFILE_VALUE=1;

var intervalgetNetworkList = null;
var intervalgetNetworkRegisterResult=null;
var interGetSendResult=null;

var popUp={
    show:function(options){
        var defaults = {
            type:"alert",
            width:"auto",
            height:"auto",
            title:"",
            msg:"",
            time:2000
        };
        $("body").css({
            background:"#b3b3b3"
        })
        var opts = $.extend(defaults, options);
        if($("#popUpMask").css("display")=="none"){
            if(!(!($.browser.msie&&($.browser.version == "6.0")&&!$.support.style))){
                $("#popUpMask,#popUpWrap").css({
                    height:$("#wrap").height()
                });
            }
            $("#popUpMask,#popUpWrap").css({
                display: "block"
            });
            $("#popUpMask").show("slow").css({
                opacity:0.3
            });
        }
        if(opts.type=="confirm"){
            $("#okBtnWrap,#cancelBtnWrap,#popUpClose").css({"display":"inline-block"});
            $("#popUpClose").unbind("click").bind("click",function(){
                popUp.hide();
            })
        }else if(opts.type=="alert"){
            $("#okBtnWrap,#popUpClose").css({
                display:"inline-block"
            });
            $("#cancelBtnWrap").hide(0);
            $("#popUpClose").unbind().bind("click",function(){
                popUp.hide();
                if ($.isFunction(opts.callback)) {
                    opts.callback.apply();
                }
            })
        }else{
            $("#okBtnWrap,#cancelBtnWrap,#popUpClose").hide(0);
            if(opts.time!=-1){
                setTimeout(function(){
                    popUp.hide();
                },opts.time)
            }
        }
        $("#popUpTitle").html(opts.title);
        $("#popUpContent").html(opts.msg);
        $("#popUpBox").css({
            "top":($("#wrap").height()-50-$("#popUpBox").outerHeight())/2+"px"
        }).fadeIn(200);

        if ($.browser.msie) {
            if (($.browser.version == "6.0" || $.browser.version == "7.0")&& $("#popUpBtnWrap .btnWrap").size()==0){
                $("#popUpBtnWrap .btnNormal").css("border","none");
                $("#popUpBtnWrap .btnNormal").wrap("<div class='btnWrap'></div>");
            }
        }

        $("#btnPopUpOk").unbind("click").bind("click",function(){
            popUp.hide();
            if ($.isFunction(opts.callback)) {
                opts.callback.apply();
            }
        })

        $("#cancelBtnWrap").unbind("click").bind("click",function(){
            popUp.hide();
        })
		$("#btnPopUpOk").focus().blur();
    },

    hide:function(){
        $("#popUpMask,#popUpWrap").fadeOut(0);
        $("#popUpBox").fadeOut(200);
        $("body").css({
            background:"#fff"
        })
    },

    alert:function(msg,callback){
        var option={
            type:"alert",
            msg:msg,
            callback:callback
        }
        popUp.show(option);
    },
    confirm:function(msg,callback){
        var option={
            type:"confirm",
            msg:msg,
            callback:callback
        }
        popUp.show(option);
    },
    prompt:function(msg,time){
        var option={
            type:"msg",
            msg:msg,
            time:time
        }
        popUp.show(option);
    },
    showBox:function(title,content){
        var option={
            type:"openBox",
            msg:content,
            title:title
        }
        popUp.show(option);
    }

}

var page={
    changePage: function(url){
        var that=this;
        $(window).unbind('hashchange', that.initload);
        url = url.replace(/^.*#/, '');
        document.location.hash=url;
        $.ajax({
            type: "GET",
            url: url+"?rand=" + Math.random(),
            dataType: "html",
            beforeSend:function(){
                that.startLoading()
            },
            complete:function(){
                that.stopLoading()
            },
            success: function(data){
                $("#mainBox").html("").empty().html(data); 
                $(window).bind('hashchange', that.initload);
            }
        
        });
    },
    initload:function (){
        var url = document.location.hash;
        url = url.replace(/^.*#/, '');
        if(url!=""){
            page.changePage(url)
        }else{
            page.changePage("connection/connectionStatus.html"); 
        }
    },
    
    reloadMain:function(){
        var url=location.href;
        page.changePage(url)
    },
    
    ajaxlink:function (){
        $('.changePageLink').live('click', function() {
            var url = $(this).attr('href');
            url = url.replace(/^.*#/, '');
            page.changePage(url);
        });
    },

    setCurrentMenu:function (currentMenu){
        if(currentMenu!=null){
            $("#topMenu li").removeClass("current");
            $("#topMenu li").eq(currentMenu-1).addClass("current");
        }
    },
    
    startLoading:function (){
        if(!(!($.browser.msie&&($.browser.version == "6.0")&&!$.support.style))){
            $("#mask,#loading").css({
                height:$("#wrap").height()
            });
        }
        $("#mask,#loading").css({
            display: "block"
        });
        $("#mask").css({
            opacity:0.01
        })
        
    },
    
    stopLoading: function (){
        $("#mask,#loading").css("display","none");
    },
    pageInit: function (){
        $(".btnNormal:enabled").bind("mouseover",function(){
            $(this).addClass("hover");
        })
        $(".btnNormal:enabled").bind("mouseout",function(){
            $(this).removeClass("hover");
        })
        $("button,a").bind('focus',function(){
            if(this.blur){
                this.blur();
            };
        });
        if ($.browser.msie) {
            if ($.browser.version == "6.0" || $.browser.version == "7.0"){
                $("#mainBox .btnNormal").css("border","none");
                $("#mainBox .btnNormal").wrap("<div class='btnWrap'></div>");
            }
        }
        if(pageName!="networkRegist" && intervalgetNetworkRegisterResult != null){
            clearInterval(intervalgetNetworkRegisterResult);
            intervalgetNetworkRegisterResult=null;
        }
        
        if(pageName!="networkSelection" && intervalgetNetworkList!=null){
            clearTimeout(intervalgetNetworkList);
            intervalgetNetworkList=null;
        }
        
        if(pageName!="sendResult" && interGetSendResult != null){
            clearTimeout(interGetSendResult);
            interGetSendResult=null;
        }
    }
    
}

var listenLogout = {
    init: function(){
        var that = this;
        that.start();
        that.listenObj($("#mainBox,#navigation"));
        that.heartbeat();
    },
        
    logoutMethod: null,
        
    start: function(){
        var that=this;
        if(that.logoutMethod != null){
            that.stop();
        }
        that.logoutMethod = setTimeout('indexPage.logout();',360000);
    },
        
    stop: function(){
        var that=this;
        if(that.logoutMethod != null){
            clearTimeout(that.logoutMethod);
            that.logoutMethod = null;
        }
    },
        
    listenObj: function(DomObject){
        var that=this;
        DomObject.live("keydown touchstart",function(){
            that.stop();
        });
    
        DomObject.live('keyup touchend click', function(){
            that.start();
        });
    },
    
    flushed: function(){
        $.ajax({
            type: "GET",
            url: "/heartBeat.asp?rand=" + Math.random()  
        });
    },
    
    heartbeat: function(){
        var that=this;
        setInterval(function(){
            that.flushed();
        }, 10000);
    }
     
};

var indexPage={
    init: function(){
        //listenLogout.flushed();
        $("#tpl").replaceTpl()
        var that=this;
        var $logoutBtn=$("#logoutBtn");
        document.title=titleValue;
        //if(sys.isLogin(sessionId)){
            that.initNav();
            page.initload();
            //listenLogout.init();
            $logoutBtn.removeBtnDisabled()
        /*}else{
           $logoutBtn.setBtnDisabled()
            page.changePage("login.html")
        }*/
        
        page.pageInit()
        page.ajaxlink();
        that.refreshImgStatus();
        that.startRefreshImgStatus();
        $logoutBtn.bind("click",that.logout)
    },
    
    initNav : function(){
        var $nav=$("#topMenu");
        $nav.find(".parent").addClass("hasLogin"); 
        $("#topMenu li").hover(function(){
            $("#topMenu li").removeClass("current");
            $(this).find("dl").stop(true,true).show();
            $(this).addClass("hover");
        },function(){
            $(this).find("dl").stop(true,true).hide();
            $(this).removeClass("hover");
            page.setCurrentMenu(currentMenu);
        })
        $("#topMenu li").bind("touchstart",function(){
            $("#topMenu li").removeClass("current hover");
            $("#topMenu").find("dl").stop(true,true).hide();
            $(this).find("dl").stop(true,true).show();
            $(this).addClass("hover"); 
        })

        $("#topMenu").find(".parent").click(function(){
            $("#topMenu li").removeClass("current hover")
            $("#topMenu").find("dl").stop(true,true).hide();
            $(this).parents("li").addClass("hover");
            $(this).next("dl").stop(true, true).show();
        })
        
        $("#mainBox").bind("touchstart click",function(){
            $("#topMenu li").removeClass("current hover");
            $("#topMenu").find("dl").stop(true,true).hide();
            page.setCurrentMenu(currentMenu);
        })
        $nav.find(".subMenu").find("a").live("click",function(){
            var url = $(this).attr('href');
            url = url.replace(/^.*#/, '');
            if(url!=""){
                page.changePage(url)
            }
            $nav.find(".subMenu").hide();
            $("#topMenu li").addClass("current");
            $("#topMenu li").removeClass("hover");
        })
    },


    logout:function () {
        Logout(function (result) {
            if (result == API_RESULT_SUCCESS) {
                location.reload();
            }
        })
    },
    
    showSignalState:function(iLen){
        var $signalImg = $("#signalImg");
        var value = /^[0,1,2,3,4,5]$/;
        iLen=parseInt(iLen);
        iLen=value.test(iLen)?iLen:0;
        $signalImg.css("background-position","center -"+iLen*26+"px");

    },

    showWanState:function (iwanState){
        var $wanImg = $("#wanImg");
        var value = /^[0,2]$/;
        iwanState=parseInt(iwanState);
        iwanState=value.test(iwanState)?iwanState:0;
        $wanImg.css("background-position","center -"+iwanState*26+"px");
    },

    showSmsState:function(ismsState){
        var $smsImg = $("#smsImg");
        var value = /^[0,1,2,3]$/;
        ismsState=parseInt(ismsState);
        ismsState=value.test(ismsState)?ismsState:0;
        $smsImg.css("background-position","center -"+ismsState*26+"px");

    },

    showBatteryState:function(ibatteryState,ibatterylvl){
        var $batteryImg=$("#batteryImg");
        $batteryImg.removeClass("charging");
        switch(ibatteryState){
            case MACRO_BATTERY_CHARGING:
                $batteryImg.addClass('charging');
                break;
            case MACRO_BATTERY_COMPLATE:
                if(!$batteryImg.hasClass("level_4")){
                    $batteryImg.addClass("level_4");
                }
                break;
            default:
                var level="level_"+ibatterylvl;
                if(!$batteryImg.hasClass(level)){
                    $batteryImg.addClass(level);
                }
        }
    },

    showWlanState:function(iwlanState){
        var $wlanImg=$("#wlanImg");
        var value = /^[0,1,2]$/;
        iwlanState=parseInt(iwlanState);
        iwlanState=value.test(iwlanState)?iwlanState:0;
        $wlanImg.css("background-position","center -"+iwlanState*26+"px");

    },

	showRoamState:function(isRoam){
        var $roamingImg=$("#roamingImg")
        if(isRoam==MACRO_ROAM_ENABLE){
            $roamingImg.addClass("isRoam");
		$roamingImg.attr("title",sys.getRes("ids_wan_roaming"));

        }else{
            $roamingImg.removeClass("isRoam");
		$roamingImg.attr("title","");
        }
    },
	
    refreshImgStatus:function () {
        var that = this;
        async_getImgInfo(function (data) {
            if (data == null) return;
            var _imgInfo = data;
            that.showSignalState(_imgInfo.signal);
            that.showSmsState(_imgInfo.sms);
            that.showBatteryState(_imgInfo.chg_state,_imgInfo.bat_level);
            if(pageName=="connectionStatus" || pageName=="login"){
                showCardInfo(_imgInfo.sim_state,_imgInfo.pin_state)
            }
        });
        async_getWanInfo(function (data) {
            if (data == null) return;
            that.showWanState(data.wan_state);
			that.showRoamState(data.roam);
            if (pageName == "connectionStatus") {
                showCurrentInfo(data.wan_state, data.download, data.upload, data.dur_time, data.roam);
                showWanInfo(data.wan_ip, data.network_type, data.network_name, data.wan_state,data.usage,data.wan_ip6);
                showWanBox(data.wan_state);
            }
        });
		/*
        async_getWlanSettingInfo(function (data) {
            if (data == null) return;
            that.showWlanState(data.wifi_state);
			
            if (pageName == "login" || pageName == "connectionStatus") {
                showWlanInfo(data.wifi_state, data.ssid, data.security_mode, data.curr_num, data.max_numsta);
            }
			
        });
		*/
    },

    refreshImgInterval:null,
    startRefreshImgStatus:function () {
        var that = this;
        if (that.refreshImgInterval != null) {
            that.stopRefreshImgStatus();
        }
        that.refreshImgInterval=setInterval(function(){
            that.refreshImgStatus();
        },10000);
    },

    stopRefreshImgStatus:function (){
        var that=this;
        if(that.refreshImgInterval != null){
            clearInterval(that.refreshImgInterval);
            that.refreshImgInterval = null;
        }
    }   
}

$(function() {
    if ($.browser.msie) {
        if ($.browser.version == "6.0" || $.browser.version == "7.0"){
            $("#navigation .btnNormal").css("border","none");
            $("#navigation .btnNormal").wrap("<div class='btnWrap'></div>");
        }
    }
})

var pageName;

function showCurrentInfo(wanState, currentDownRate, currentUpRate, connectTime, wanRoam) {
    var wanConnectStateStr;
    var downRateStr = 0;
    var connectTimeStr = 0;
    var roamStr = "";

    switch (wanState) {
        case MACRO_PPP_CONNECTED:
            wanConnectStateStr = sys.getRes("ids_wan_connected");
            downRate = (currentUpRate / (1024 * 1024)).toFixed(1) + " Mbps/" + (currentDownRate / (1024 * 1024)).toFixed(1) + " Mbps";
            connectTimeStr = sys.getTimeDesc(connectTime);
            break;
        case MACRO_PPP_CONNECTING:
            wanConnectStateStr = sys.getRes("ids_wan_connecting");
            break;
        case MACRO_PPP_DISCONNECTING:
            wanConnectStateStr = sys.getRes("ids_wan_disconnecting");
            break;
        default:
            wanConnectStateStr = sys.getRes("ids_wan_disconnect");
            break;
    }

    switch (parseInt(wanRoam)) {
        case MACRO_ROAM_ENABLE:
            roamStr = sys.getRes("ids_sim_roaming_yes");
            break;
        case MACRO_ROAM_DISABLE:
            roamStr = sys.getRes("ids_sim_roaming_no");
            break;
        default:
            roamStr = "NA";
    }


    $("#spnWanConnectState").html(wanConnectStateStr);
    $("#spnDownRate").html(downRateStr);
    $("#spnConnectTime").html(connectTimeStr);
    $("#spnWanRoam").html(roamStr);

}

/*
function showWlanInfo(wlanState, wlanSsid, wlanSecurityType, wlanConnectNumber, wlanMaxUser) {
    var wlanStateStr;
    var wlanSsidStr = wlanSsid;
    var wlanSecurityTypeStr;
    var wlanUserStr = wlanConnectNumber + "/" + wlanMaxUser;

    switch (parseInt(wlanState)) {
        case MACRO_WLAN_ENABLED:
            wlanStateStr = sys.getRes("ids_on");
            break;
        case MACRO_WLAN_DISABLED:
            wlanStateStr = sys.getRes("ids_off");
            break;
        case MACRO_WLAN_WPS:
            wlanStateStr = sys.getRes("ids_wps");
            break;
    }

    switch (wlanSecurityType) {
        case SECRUTIY_TYPE_DISABLE:
            wlanSecurityTypeStr = sys.getRes("ids_wifi_securityDisable");
            break;
        case SECRUTIY_TYPE_WEP:
            wlanSecurityTypeStr = sys.getRes("ids_wifi_wep");
            break;
        case SECRUTIY_TYPE_WPA_PSK:
            wlanSecurityTypeStr = sys.getRes("ids_wifi_wpa");
            break;
        case SECRUTIY_TYPE_WPS2_PSK:
            wlanSecurityTypeStr = sys.getRes("ids_wifi_wpa2");
            break;
        case SECRUTIY_TYPE_WPA_MIXED:
            wlanSecurityTypeStr = sys.getRes("ids_wifi_wpaWpa2Psk");
            break;
        default:
            wlanSecurityTypeStr = sys.getRes('ids_sim_unknown');
    }

    $("#spnWlanState").html(wlanStateStr);
    $("#spnWlanSecurity").html(wlanSecurityTypeStr);
    $("#spnWlanSsid").html(wlanSsidStr);
    $("#spnWlanUser").html(wlanUserStr);
}
*/

function showCardInfo(simState, pinState) {
    var simStateStr;

    switch (simState) {
        case MACRO_UIM_APP_STATE_UNKNOWN_V01:
            simStateStr = sys.getRes('ids_sim_noSimCard');
            break;
        case MACRO_UIM_APP_STATE_PIN1_OR_UPIN_REQ_V01:
            simStateStr = sys.getRes('ids_sim_pinRequired');
            break;
        case MACRO_UIM_APP_STATE_PUK1_OR_PUK_REQ_V01:
            simStateStr = sys.getRes('ids_sim_pukRequired');
            break;
        case MACRO_UIM_APP_STATE_READY_V01:
            simStateStr = sys.getRes('ids_sim_ready');
            break;
        case MACRO_UIM_APP_STATE_DETECTED_V01:
            simStateStr = sys.getRes('ids_sim_initializing');
            break;
        case MACRO_UIM_APP_STATE_PERSON_CHECK_REQ_V01:
            simStateStr = sys.getRes('ids_sim_locked');
            break;
        case MACRO_UIM_APP_STATE_ILLEGAL_V01:
            simStateStr = sys.getRes('ids_sim_invalidSimCard');
            break;
        default:
            simStateStr = sys.getRes('ids_sim_unknown');
    }

    $("#spnSimState").html(simStateStr);
}

function showWanInfo(wanIpAddr, networkType, networkName, wanState,wanUsage,wanIP6) {
    var wanIpaddrStr;
    var wanNetworkNameStr;
    var wanNetworkTypeStr;
    var wanIpV6AddrStr;
    var wanUsageStr=sys.covertNum(wanUsage==null?0:wanUsage);

    wanIpaddrStr = (wanState != MACRO_PPP_CONNECTED) ? "0.0.0.0" : sys.getIpAddr(wanIpAddr);
    wanIpV6AddrStr=(wanState != MACRO_PPP_CONNECTED) ? "0::0" : sys.getIpAddr(wanIP6);

    switch (networkName) {
        case "CHINA UNICOM":
        case "CHN CU":
            wanNetworkNameStr = sys.getRes('ids_network_operatorName');
            break;
        case "":
            wanNetworkNameStr = "NA";
            break;
        default:
            wanNetworkNameStr = networkName;
    }

    switch (networkType) {
        case MACRO_NETWORKTYPE_NO_SERVICE:
            wanNetworkTypeStr = sys.getRes("ids_network_noService");
            break;
        case MACRO_NETWORKTYPE_GSM:
            wanNetworkTypeStr = sys.getRes("ids_network_gsm");
            break;
        case MACRO_NETWORKTYPE_GPRS:
            wanNetworkTypeStr = sys.getRes("ids_network_gprs");
            break;
        case MACRO_NETWORKTYPE_EDGE:
            wanNetworkTypeStr = sys.getRes("ids_network_edge");
            break;
        case MACRO_NETWORKTYPE_UMTS:
            wanNetworkTypeStr = "UMTS";
            break;
        case MACRO_NETWORKTYPE_HSDPA:
            wanNetworkTypeStr = sys.getRes("ids_network_hsdpa");
            break;
        case MACRO_NETWORKTYPE_HSUPA:
            wanNetworkTypeStr = sys.getRes("ids_network_hsupa");
            break;
        case MACRO_NETWORKTYPE_CDMA:
            wanNetworkTypeStr = "CDMA";
            break;
        case MACRO_NETWORKTYPE_LTE:
            wanNetworkTypeStr = "LTE";
            break;
        case MACRO_NETWORKTYPE_HSPA_PLUS:
            wanNetworkTypeStr = "HSPA+";
            break;
        case MACRO_NETWORKTYPE_DC_HSPA_PLUS:
            wanNetworkTypeStr = "DC-HSPA+";
            break;
        case MACRO_NETWORKTYPE_EV_DO_A:
        case MACRO_NETWORKTYPE_EV_DO_B:
        case MACRO_NETWORKTYPE_EV_DO_C:
            wanNetworkTypeStr = "EV-DO";
            break;
        default:
            wanNetworkTypeStr = sys.getRes("ids_network_noService");
    }

    $("#spnWanIpAddr").html(wanIpaddrStr);
    $("#spnWanNetworkName").html(wanNetworkNameStr);
    $("#spnWanNetworkType").html(wanNetworkTypeStr);
    $("#spnWanUsage").html(wanUsageStr);
    $("#spnWanIpV6Addr").html(wanIpV6AddrStr);
}

function getSimCardState(simState, pinState) {
    var state;
    switch (simState) {
        case MACRO_UIM_APP_STATE_UNKNOWN_V01:
            state = "noCard";
            break;
        case MACRO_UIM_APP_STATE_PIN1_OR_UPIN_REQ_V01:
            state = "pinReq";
            break;
        case MACRO_UIM_APP_STATE_PUK1_OR_PUK_REQ_V01:
            state = "pukReq";
            break;
        case MACRO_UIM_APP_STATE_READY_V01:
            state = "normal";
            break;
        case MACRO_UIM_APP_STATE_DETECTED_V01:
            state = "simError";
            break;
        case MACRO_UIM_APP_STATE_PERSON_CHECK_REQ_V01:
            state = "simLock";
            break;
        case MACRO_UIM_APP_STATE_ILLEGAL_V01:
            state = "invalid";
            break;
        default:
            state = "noCard";
    }

    return state;

}

function formatHtmlStr(str) {
    var args = arguments, re = new RegExp("%([1-" + args.length + "])", "g");
    return String(str).replace(re, function ($1, $2) {
            return args[$2];
        }
    );
}

function showSetttingLanguage() {
    var languages = [];
    $.each(sys_language, function(index, val) {
        languages.push(val);
    });
    if (languages.length <= 1) {
        $("#setLanguage").hide();
    }
}

function initSimCard(callback){
     $.getData({
        async:true,
        url:"/goform/getSimcardInfo?rand=" + Math.random(),
        success:function(data) {
            if(data.sim_state == MACRO_SIM_CARD_INITING){
                setTimeout(function(){
                    initSimCard(callback);
                },2000);
            }else{
                callback();
            }
        }
    })
}

function pageInitSimCard(){
    page.startLoading();
    initSimCard(function(){
        $.getData({
            async:true,
            url:"/goform/getSimcardInfo?rand=" + Math.random(),
            success:function(data) {
                if(data.sim_state == MACRO_UIM_APP_STATE_READY_V01){
                    initProfileList(function(){
                        page.stopLoading();
                        page.reloadMain();
                    }) 
                }else{
                    page.stopLoading();
                    page.reloadMain();
                }
            }
        })
    });
}

function initProfileList(callback){
    $.getData({
        async:true,
        url:"/goform/getProfileList?rand=" + Math.random(),
        success:function(data) {
            if(data.state == 0){
                setTimeout(function(){
                    initProfileList(callback);
                },2000);
            }else{
                callback();
            }
        }
    })
}