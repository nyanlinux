var sys = {
    getRes:function (strId) {
        return resourceInfo[strId];
    },

    getIpAddr:function (IP) {
        if (IP == MACRO_INVALID_STR) {
            return "0.0.0.0";
        } else {
            return IP;
        }
    },

    isLogin:function (sessionId) {
        return (sessionId == 1 || sessionId == 0 || sessionId == 2) ? false : true;
    },
    alert:function (strId, callback) {
        var resMsg = resourceInfo[strId];
        /*
        alert((resMsg != null || resMsg != undefined) ? resMsg : strId);
        if (callback != null) callback();
        */
        popUp.alert(callback==null?(resMsg!=undefined?resMsg:strId):resMsg!=undefined?resMsg:strId,callback);
    },
    confirm:function (strId, callback) {
        var resMsg = resourceInfo[strId];
        popUp.confirm(callback==null?(resMsg!=undefined?resMsg:strId):resMsg!=undefined?resMsg:strId,callback);
    },
    prompt:function(strId,time){
        var resMsg = resourceInfo[strId];
        popUp.prompt(time==null?(resMsg!=undefined?resMsg:strId):resMsg!=undefined?resMsg:strId,time)
    },

    getTimeDesc:function (time_sec) {
        var that = this;
        var days = Math.floor(time_sec / 86400);
        var hours = Math.floor((time_sec - days * 86400) / 3600);
        var minutes = Math.floor((time_sec - days * 86400 - hours * 3600) / 60);
        var str = "";
        str += days + " " + ((days <= 1) ? that.getRes("ids_day") : that.getRes("ids_days")) + " ";
        str += hours + " " + ((hours <= 1) ? that.getRes('ids_hour') : that.getRes('ids_hours')) + " ";
        str += ((minutes < 10) ? "0" : "") + minutes + " " + ((minutes <= 1) ? that.getRes("ids_minute") : that.getRes("ids_minutes"));
        return str;
    },

    covertNum:function (number) {
        return number > (1024 * 1024 * 1024)?(number / (1024 * 1024 *1024)).toFixed(2) + " GB":(number > (1024 * 1024) ? (number / (1024 * 1024)).toFixed(2) + " MB" : (number / 1024).toFixed(2) + " KB");
    }
};

(function () {
    var cache = {};
    this.tmpl = function (template_id, data) {
        var fn = cache[template_id];
        this.get = function (dataid) {
            var id = dataid.split(":")[0];
            var res = data[id] ? (dataid.charAt(dataid.length - 1) == ":" ? data[id] + ':' : data[id]) : 'null';
            return res;
        }
        if (!fn) {
            var template = document.getElementById(template_id).innerHTML;
            fn = new Function("data", "var p=[]; p.push('" +
                template
                    .replace(/[\r\t\n]/g, " ")
                    .split("'").join("\\'")
                    .replace(/\${(.+?)}/g, "',this.get(\'$1\'),'")
                    .split("${").join("');")
                    .split("}").join("p.push('")
                + "');return p.join('');");
            cache[template_id] = fn;
        }
        return fn();
    };
})();

(function ($) {
    $.extend({
        postForm:function (options) {
            var defaults = {
                action:"",
                dataType:"json",
                error:function () {
                    sys.alert("ids_dlg_sys_request_error");
                }
            };
            var opts = $.extend(defaults, options);
            $.ajax({
                type:"post",
                url:opts.action + "?rand=" + Math.random(),
                data:opts.data || {some:"a"},
                async:false,
                cache:false,
                dataType:opts.dataType,
                success:function (data) {
                    opts.success(data);
                },
                error:function () {
                    opts.error()
                }
            });

        },

        getData:function (options) {
            var backdata = null;
            var defaults = {
                url:"",
                type:"post",
                data:"d=1",
                async:false,
                dataType:"json",
                success:function (data) {
                    backdata = data;
                }
            };
            var opts = $.extend(defaults, options);
            $.ajax({
                type:opts.type,
                async:opts.async,
                url:opts.url,
                data:opts.data,
                dataType:opts.dataType,
                success:function (data) {
                    opts.success(data)
                },
                error:function () {
                    backdata = null
                }
            });
            return backdata;
        }

    });

    $.fn.extend({
        findSelectRes:function (datas) {
            return this.each(function () {
                var option_HtmlStr = "";
                $.each(datas,function(index,val) {
                    option_HtmlStr += "<option value='" + index + "'>"+val + "</option>"
                });
                $(this).html(option_HtmlStr)
            });
        },
        setBtnDisabled:function(){
            return this.each(function(){
                $(this).attr("disabled",true).addClass("btnDisable").removeClass("hover");
                if ($.browser.msie) {
                    if ($.browser.version == "6.0" || $.browser.version == "7.0"){
                        $(this).parent(".btnWrap").addClass("disable");
                    }
                }
            })
        },
        removeBtnDisabled:function(){
            return this.each(function(){
                $(this).removeAttr("disabled").removeClass("btnDisable");
                if ($.browser.msie) {
                    if ($.browser.version == "6.0" || $.browser.version == "7.0"){
                        $(this).parent(".btnWrap").removeClass("disable");
                    }
                }
            })
        },

        setData:function(data){
            return this.each(function(){
                $(this).html(data[$(this).attr("id")]);
            })
        },

        replaceTpl:function(){
            return this.each(function(){
                $(this).replaceWith(tmpl($(this).attr("id"),resourceInfo));
            })
        },
        selectFocus:function () {
            return this.each(function () {
                $(this).val("").focus();
            })
        },
	showRule:function (ruleStr) {
            var ruleMsg=sys.getRes(ruleStr)
            return this.each(function () {
                var thisParent = $(this).parent("li");
                var ruleBox = thisParent.find(".rule");
                if (ruleBox.size() < 1) {
                    thisParent.append("<p class='rule'>" + ruleMsg + "</p>")
                } else {
                    thisParent.find(".rule").html(ruleMsg);
                }

                //$(this).selectFocus();
                $(this).addClass("errorIpt");
                $(this).bind("focusin",function(){
                    $(this).removeClass("errorIpt");
                    thisParent.find(".rule").remove();
                });
            });
        },
	showWarn:function (ruleStr) {
            var ruleMsg=sys.getRes(ruleStr)
            return this.each(function () {
                var ruleBox = $("#warning").find(".rule");
                if (ruleBox.size() < 1) {
                    $("#warning").addClass("rule").html(ruleMsg).css("display","");
                }
                $(this).addClass("errorIpt");
                $(this).bind("focusin",function(){
                    $(this).removeClass("errorIpt");
                    $("#warning").html("").removeClass("rule");
                });
            });
        }

    });
})(jQuery);
function getUrlPara(paraName) {
    var sUrl = location.href;
    var sReg = "(?:\\?|&){1}" + paraName + "=([^&]*)"
    var re = new RegExp(sReg, "gi");
    re.exec(sUrl);
    return RegExp.$1;
}

function check_password(str) {
    return /^[A-Za-z0-9\-\+\!\^\$\@\#\&\*]{4,16}$/.test(str);
}

function check_ssid(str) {
    return /^[A-Za-z0-9\.\s\-\_]+$/.test(str);
}

function check_profile_password(str) {
    return     !(/[\s'\"\\]/g.test(str))
}

function checkProfileName(name){
    return !(/[:;\,\"\\&%<>?\+]+/g.test(name));
}


function show_sms_time(sms_time) {
    var time_i = parseInt(sms_time);

    var time_base = new Date(1980, 0, 6, 0, 0, 0, 0);

    var time_space = time_base.getTime();

    var all_time_space = new Date(time_i * 1000 + time_space);

    var nowDateTime = new Date(parseInt((all_time_space.getTime())));

    var year = nowDateTime.getFullYear();
    var month = nowDateTime.getMonth() + 1;
    var day = nowDateTime.getDate();

    return year + "-" + month + "-" + day + " " + nowDateTime.toLocaleTimeString();

//return nowDateTime.toLocaleDateString() + " " + nowDateTime.toLocaleTimeString(); 
}
function show_SD_time(sms_time) {
    var time_i = parseInt(sms_time);

    //var time_base = new Date(1970,0,6,0,0,0,0);

    //var time_space = time_base.getTime(); 

    var all_time_space = new Date(time_i * 1000);

    var nowDateTime = new Date(parseInt((all_time_space.getTime())));

    var year = nowDateTime.getFullYear();
    var month = nowDateTime.getMonth() + 1;
    var day = nowDateTime.getDate();

    return year + "-" + month + "-" + day + " " + nowDateTime.toLocaleTimeString();

//return nowDateTime.toLocaleDateString() + " " + nowDateTime.toLocaleTimeString();	 
}

function isASCIIData(str) {
    if (str == null) {
        return true;
    }
    var i = 0;
    var char_i;
    var num_char_i;
    for (i = 0; i < str.length; i++) {
        char_i = str.charAt(i);
        num_char_i = char_i.charCodeAt();
        if (num_char_i > 255) {
            return false;
        }
    }
}

function isHexaDigit(digit) {
    var hexVals = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "a", "b", "c", "d", "e", "f");
    var len = hexVals.length;
    var i = 0;
    var ret = false;

    for (i = 0; i < len; i++) {
        if (digit == hexVals[i]) {
            break;
        }
    }

    if (i < len) {
        ret = true;
    }
    return ret;
}

function isHexaData(data) {
    var len = data.length;
    var i = 0;
    for (i = 0; i < len; i++) {
        if (isHexaDigit(data.charAt(i)) == false) {
            return false;
        }
    }
    return true;
}

function check_input_char(str) {
    var i;
    var char_i;
    var num_char_i;

    if (str == "") {
        return true;
    }

    for (i = 0; i < str.length; i++) {
        char_i = str.charAt(i);
        num_char_i = char_i.charCodeAt();
        if ((num_char_i > MACRO_SUPPORT_CHAR_MAX) || (num_char_i < MACRO_SUPPORT_CHAR_MIN)) {
            return false;
        }
        else if ((MACRO_NOT_SUPPORT_CHAR_COMMA == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_QUOTATION_MARK == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_COLON == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_SEMICOLON == num_char_i)
            || (MACRO_NOT_SUPPORT_BACKSLASH_MARK == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_38 == num_char_i)
            ) {
            return false;
        }
        else {
            continue;
        }
    }
    return true;
}

function is_wep_key(key, key_encrp) {
    var len = key.length;
    if (key_encrp == 0) {
        if (len != 5 && len != 10) {
            return false;
        }
    }

    if (key_encrp == 1) {
        if (len != 13 && len != 26) {
            return false;
        }


    }
    switch (len) {
        case 5:
        case 13:
            if (isASCIIData(key) == false) {
                return false;
            }
            break;
        case 10:
        case 26:
            if (isHexaData(key) == false) {
                return false;
            }
            break;
        default:
            return false;
    }
    return true;
}

function isNumber(str) {
    var i;
    if (str.length <= 0) {
        return false;
    }
    for (i = 0; i < str.length; i++) {
        var c = str.charCodeAt(i);
        if (c < 48 || c > 57) {
            return false;
        }
    }
    return true;
}

function IsSameSubnetAddrs(Ip1, Ip2, mask) {
    var addrParts1 = Ip1.split(".");
    var addrParts2 = Ip2.split(".");
    var maskParts = mask.split(".");
    for (i = 0; i < 4; i++) {
        if (((Number(addrParts1[i])) & (Number(maskParts[i]))) != ((Number(addrParts2[i])) & (Number(maskParts[i])))) {
            return false;
        }
    }
    return true;
}

function getLeftMostZeroBitPos(num) {
    var i = 0;
    var numArr = [128, 64, 32, 16, 8, 4, 2, 1];

    for (i = 0; i < numArr.length; i++)
        if ((num & numArr[i]) == 0)
            return i;

    return numArr.length;
}

function getRightMostOneBitPos(num) {
    var i = 0;
    var numArr = [1, 2, 4, 8, 16, 32, 64, 128];

    for (i = 0; i < numArr.length; i++)
        if (((num & numArr[i]) >> i) == 1)
            return (numArr.length - i - 1);

    return -1;
}

function isValidSubnetMask(mask) {
    var i = 0;
    var num = 0;
    var zeroBitPos = 0, oneBitPos = 0;
    var zeroBitExisted = false;

    if (mask == '0.0.0.0') {
        return false;
    }

    if (mask == '255.255.255.255') {
        return false;
    }

    var maskParts = mask.split('.');
    if (maskParts.length != 4) {
        return false;
    }

    for (i = 0; i < 4; i++) {
        if (isNaN(maskParts[i]) == true) {
            return false;
        }
        if (maskParts[i] == '') {
            return false;
        }
        if (maskParts[i].indexOf(' ') != -1) {
            return false;
        }

        if ((maskParts[i].indexOf('0') == 0) && (maskParts[i].length != 1)) {
            return false;
        }

        num = parseInt(maskParts[i]);
        if (num < 0 || num > 255) {
            return false;
        }
        if (zeroBitExisted == true && num != 0) {
            return false;
        }
        zeroBitPos = getLeftMostZeroBitPos(num);
        oneBitPos = getRightMostOneBitPos(num);
        if (zeroBitPos < oneBitPos) {
            return false;
        }
        if (zeroBitPos < 8) {
            zeroBitExisted = true;
        }
    }

    return true;
}

function isValidIpAddress(address) {
    var addrParts = address.split('.');
    if (addrParts.length != 4) {
        return false;
    }

    for (i = 0; i < 4; i++) {
        if (isNaN(addrParts[i]) == true) {
            return false;
        }

        if (addrParts[i] == '') {
            return false;
        }

        if (addrParts[i].indexOf(' ') != -1) {
            return false;
        }

        if ((addrParts[i].indexOf('0') == 0) && (addrParts[i].length != 1)) {
            return false;
        }
    }

    if ((addrParts[0] <= 0 || addrParts[0] == 127 || addrParts[0] > 223)
        || (addrParts[1] < 0 || addrParts[1] > 255)
        || (addrParts[2] < 0 || addrParts[2] > 255)
        || (addrParts[3] <= 0 || addrParts[3] >= 255)) {
        return false;
    }

    return true;
}
function inet_aton(a) {
    var n;

    n = a.split(/\./);
    if (n.length != 4) {
        return 0;
    }
    return ((n[0] << 24) | (n[1] << 16) | (n[2] << 8) | n[3]);
}

function inet_ntoa(n) {
    var a;

    a = (n >> 24) & 255;
    a += "."
    a += (n >> 16) & 255;
    a += "."
    a += (n >> 8) & 255;
    a += "."
    a += n & 255;

    return a;
}

function is_broadcast_or_network_address(Ip, Netmask) {
    var ip;
    var mask;
    var netaddr;
    var broadaddr;

    ip = inet_aton(Ip);
    mask = inet_aton(Netmask);
    netaddr = ip & mask;
    broadaddr = netaddr | ~mask;

    if (netaddr == ip || ip == broadaddr) {
        return false;
    }

    return true;
}

function check_host_name(name) {
    var doname = /^([\w-]+\.)+(home)$/;
    if (!doname.test(name)) {
        return false;
    }
    return true;
}

function check_input_dirname(str) {
    var i;
    var char_i;
    var num_char_i;

    if (str == "") {
        return true;
    }

    for (i = 0; i < str.length; i++) {
        char_i = str.charAt(i);
        num_char_i = char_i.charCodeAt();
        if ((MACRO_NOT_SUPPORT_CHAR_COMMA == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_QUOTATION_MARK == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_COLON == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_SEMICOLON == num_char_i)
            || (MACRO_NOT_SUPPORT_BACKSLASH_MARK == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_38 == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_42 == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_60 == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_62 == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_63 == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_42 == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_47 == num_char_i)
            || (MACRO_NOT_SUPPORT_CHAR_124 == num_char_i)
            ) {
            return false;
        } else {
            continue;
        }
    }
    return true;
}

var Vendor = (function () {

    function getLoginState() {
        var gData=null;
        gData = $.getData({
            url:"/goform/getLoginState?rand=" + Math.random()
        })
        return gData.loginStatus;
    }

    function Login(params, callback) {
        $.postForm({
            action:"/goform/setLogin",
            data:params,
            success:function (data) {
                callback(data);
            }
        })

    }

    function Logout(callback) {
        $.postForm({
            action:"/goform/setLogout",
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getLanguage() {
        var currentLanguage = "en";
        var data = $.getData({
            url:"/goform/getCurrentLanguage?rand=" + Math.random()
        })
        return currentLanguage = data.language;
    }

    function SetLanguage(strLanguageString, callback) {
        $.postForm({
            action:"/goform/setLanguage",
            data:{
                "language":strLanguageString
            },
            success:function (data) {
                callback(data.error);
            }
        })

    }

    function SetPassword(newPassword, callback) {
        $.postForm({
            action:"/goform/setPassword",
            data:{
                "newPassword":newPassword
            },
            success:function (data) {
                callback(data.error);
            }
        })
    }


    function getWlanSettingInfo() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getWlanInfo?rand=" + Math.random()
        });
        return gData;
    }

    function async_getWlanSettingInfo(callback) {
        $.getData({
            async:true,
            url:"/goform/getWlanInfo?rand=" + Math.random(),
            success:function (data) {
                callback(data)
            }
        })

    }


    function setWlanSettingInfo(params, callback) {
        $.postForm({
            action:"/goform/setWlanInfo",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }


    function getSimcardInfo() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getSimcardInfo?rand=" + Math.random()
        });
        return gData;
    }


    function set_unlockPIN(params, callback) {
        $.postForm({
            action:"/goform/unlockPIN",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function set_unlockPUK(params, callback) {
        $.postForm({
            action:"/goform/unlockPUK",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function set_changePIN(params, callback) {
        $.postForm({
            action:"/goform/changePIN",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    //enable or disable
    function set_switchPIN(params, callback) {
        $.postForm({
            action:"/goform/switchPIN",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function disablePIN(params, callback) {
        $.postForm({
            action:"/goform/switchPIN",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }
	
    function setUnlockSIMLock(params, callback) {
        $.postForm({
            action:"/goform/unlockSIMLock",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getSMSList(type,pageNum){
        var gData=null;
        gData = $.getData({
            data:"key="+type+"&pageNum=" + pageNum,
            url:"/goform/getSMSlist?rand=" + Math.random()
        });
        return {
            "list":gData.data
        }
    }



    function getSMSStoreState() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getSMSStoreState?rand=" + Math.random()
        });
        return gData;
    }

    function getPageInfo(type){
        var gData = null;
        gData = $.getData({
            url:"/goform/getSMSPageInfo?key="+type+"&rand=" + Math.random()
        });
        return gData;
    }


    function getSingleSMS(sms_id) {
        var gData = null;
        gData = $.getData({
            url:"/goform/getSingleSMS?sms_id=" + sms_id + "&rand=" + Math.random()
        });
        return gData;
    }

    function deleteSMS(sms_id, callback) {
        $.postForm({
            action:"/goform/deleteSMS",
            data:{
                "sms_id":sms_id
            },
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function sendSMS(params, callback) {
        $.postForm({
            action:"/goform/sendSMS",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function saveSMS(params, callback) {
        $.postForm({
            action:"/goform/saveSMS",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function setSMSSetting(params, callback) {
        $.postForm({
            action:"/goform/setSMSSetting",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }


    function async_getSendSMSResult(callback) {
        $.getData({
            async:true,
            url:"/goform/getSendSMSResult?rand=" + Math.random(),
            success:function (data) {
                callback(data.send_state)
            }
        })
    }

    function getNetworkSetting() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getNetworkInfo?rand=" + Math.random()
        });
        return gData;
    }

    function setNetworkSetting(params, callback) {
        $.postForm({
            action:"/goform/setNetworkInfo",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function setNetworkSelect(params, callback) {
        $.postForm({
            action:"/goform/setNetworkSelect",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getNetworkList() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getNetworkList?rand=" + Math.random()
        });
        return gData;
    }

    function setNetworkRegister(params, callback) {
        $.postForm({
            action:"/goform/setNetworkRegister",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getNetworkRegisterResult() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getNetworkRegisterResult?rand=" + Math.random()
        });
        return gData.regist_state;
    }

    function getWanInfo() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getWanInfo?rand=" + Math.random()
        });
        return gData;
    }

    function async_getWanInfo(callback) {
        $.getData({
            async:true,
            url:"/goform/getWanInfo?rand=" + Math.random(),
            success:function (data) {
                callback(data)
            }
        })

    }


    function setWanConnect(params, callback) {
        $.postForm({
            action:"/goform/setWanConnect",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function setWanDisconnect(callback) {
        $.postForm({
            action:"/goform/setWanDisconnect",
            success:function (data) {
                callback(data.error);
            }
        })
    }
    
    function setCancelWanConnect(callback){
        $.postForm({
            action:"/goform/setWanConnectCancel",
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getCurrentDialProfile() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getCurrentProfile?rand=" + Math.random()
        });
        return gData.profile_id;
    }

    function getRouterSetting() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getRouterInfo?rand=" + Math.random()
        });
        return gData;
    }

    function setRouterSetting(params, callback) {
        $.postForm({
            action:"/goform/setRouterSetting",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }


    function getSystInfo() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getSysteminfo?rand=" + Math.random()
        });
        return gData;
    }

    function getImgInfo() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getImgInfo?rand=" + Math.random()
        });
        return gData;
    }

    function async_getImgInfo(callback) {
        $.getData({
            async:true,
            url:"/goform/getImgInfo?rand=" + Math.random(),
            success:function (data) {
                callback(data)
            }
        })
    }

    function setWpsPin(params, callback) {
        $.postForm({
            action:"/goform/setWpsPin",
            data:params,
            success:function (data) {
                callback(data);
            }
        })
    }

    function setWpsPbc(callback) {
        $.postForm({
            action:"/goform/setWpsPbc",
            success:function (data) {
                callback(data);
            }
        })
    }

    function getWanConnectMode() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getWanConnectMode?rand=" + Math.random()
        });
        return gData;
    }


    function setWanConnectMode(params, callback) {
        $.postForm({
            action:"/goform/setWanConnectMode",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getRouterInfo() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getRouterInfo?rand=" + Math.random()
        });
        return gData;
    }


    function setRouterInfo(params, callback) {
        $.postForm({
            action:"/goform/setRouterInfo",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getUseHistory() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getUseHistory?rand=" + Math.random()
        });
        return gData;
    }


    function setUseHistoryClear(params, callback) {
        $.postForm({
            action:"/goform/setUseHistoryClear",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getWlanClientInfo() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getWlanClientInfo?rand=" + Math.random()
        });
        return gData.data;
    }

    function getProfileList() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getProfileList?rand=" + Math.random()
        });
        return gData;
    }

    function getMACFilterInfo() {
        var gData = null;
        gData = $.getData({
            url:"/goform/getMACFilterInfo?rand=" + Math.random()
        });
        return gData;

    }

    function setMACFilterInfo(params, callback) {
        $.postForm({
            action:"/goform/setMACFilterInfo",
            data:params,
            success:function (data) {
                callback(data);
            }
        })
    }

    function setProfileSave(params, callback) {
        $.postForm({
            action:"/goform/setProfileSave",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function setProfileDefault(params, callback) {
        $.postForm({
            action:"/goform/setProfileDefault",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function setProfileDelete(params, callback) {
        $.postForm({
            action:"/goform/setProfileDelete",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function setReboot(params, callback) {
        $.postForm({
            action:"/goform/setReboot",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function setReset(params, callback) {
        $.postForm({
            action:"/goform/setReset",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function setBackupSettings(params, callback) {
        $.postForm({
            action:"/goform/setBackupSettings",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function getBackupSettings() {
        $("body").append("<iframe src='/cfgbak/configure.bin' style='display: none'></iframe>");
    }

	
	function getFOTADownloadState(){
        var gData = null;
        gData = $.getData({
            url:"/goform/getFOTADownloadState"
        });
        return gData.state;
    }
    
    function setFOTAStartCheckVersion(callback){
        $.postForm({
            action:"/goform/setFOTAStartCheckVersion",
            success:function (data) {
                callback(data.error);
            }
        })
    }


    function getDeviceNewVersion(){
        var gData = null;
        gData = $.getData({
            url:"/goform/getDeviceNewVersion"
        });
        return gData.is_new_version;
    }


    function getFOTADownloadInfo(){
        var gData = null;
        gData = $.getData({
            url:"/goform/getFOTADownloadInfo"
        });
        return gData;
    }

    function setFOTAStartDownload(callback){
        $.postForm({
            action:"/goform/setFOTAStartDownload",
            success:function (data) {
                callback(data.error);
            }
        })
    }


    function setFOTACancelDownload(callback){
        $.postForm({
            action:"/goform/setFOTACancelDownload",
            success:function (data) {
                callback(data.error);
            }
        })
    }
    
    function setFOTAStartUpdate(callback){
         $.postForm({
         action:"/goform/setFOTAStartUpdate",
         success:function (data) {
         callback(data.error);
         }
         })
    }
    function async_getUSSDSendResult(callback) {
        $.getData({
            async:true,
            url:"/goform/getUSSDSendResult?rand=" + Math.random(),
            success:function (data) {
                callback(data)
            }
        })
    }

    function setUSSDSend(params, callback){
        $.postForm({
            action:"/goform/setUSSDSend",
            data:params,
            success:function (data) {
                callback(data.error);
            }
        })
    }

    function async_getUSSDEnd(callback){
        $.getData({
            async:true,
            url:"/goform/setUSSDEnd?rand=" + Math.random(),
            success:function (data) {
                callback(data)
            }
        })
    }
	
    return {
        getLoginState:getLoginState,
        Login:Login,
        Logout:Logout,
        getLanguage:getLanguage,
        SetLanguage:SetLanguage,
        SetPassword:SetPassword,
        getWlanSettingInfo:getWlanSettingInfo,
        async_getWlanSettingInfo:async_getWlanSettingInfo,
        setWlanSettingInfo:setWlanSettingInfo,
        getSimcardInfo:getSimcardInfo,
        set_unlockPIN:set_unlockPIN,
        set_unlockPUK:set_unlockPUK,
        set_changePIN:set_changePIN,
        set_switchPIN:set_switchPIN,
        disablePIN:disablePIN,
	setUnlockSIMLock:setUnlockSIMLock,
        getSMSList:getSMSList,
        getPageInfo:getPageInfo,
        getSMSStoreState:getSMSStoreState,
        getSingleSMS:getSingleSMS,
        deleteSMS:deleteSMS,
        sendSMS:sendSMS,
        saveSMS:saveSMS,
        setSMSSetting:setSMSSetting,
        async_getSendSMSResult:async_getSendSMSResult,
        getNetworkSetting:getNetworkSetting,
        setNetworkSetting:setNetworkSetting,
        setNetworkSelect:setNetworkSelect,
        getNetworkList:getNetworkList,
        setNetworkRegister:setNetworkRegister,
        getNetworkRegisterResult:getNetworkRegisterResult,
        getWanInfo:getWanInfo,
        async_getWanInfo:async_getWanInfo,
        setWanConnect:setWanConnect,
        setWanDisconnect:setWanDisconnect,
        setCancelWanConnect:setCancelWanConnect,
        getCurrentDialProfile:getCurrentDialProfile,
        getRouterSetting:getRouterSetting,
        setRouterSetting:setRouterSetting,
        getSystInfo:getSystInfo,
        getImgInfo:getImgInfo,
        async_getImgInfo:async_getImgInfo,
        setWpsPin:setWpsPin,
        setWpsPbc:setWpsPbc,
        getWanConnectMode:getWanConnectMode,
        setWanConnectMode:setWanConnectMode,
        getRouterInfo:getRouterInfo,
        setRouterInfo:setRouterInfo,
        getUseHistory:getUseHistory,
        setUseHistoryClear:setUseHistoryClear,
        getWlanClientInfo:getWlanClientInfo,
        getProfileList:getProfileList,
        getMACFilterInfo:getMACFilterInfo,
        setMACFilterInfo:setMACFilterInfo,
        setProfileSave:setProfileSave,
        setProfileDefault:setProfileDefault,
        setProfileDelete:setProfileDelete,
        setReboot:setReboot,
        setReset:setReset,
        setBackupSettings:setBackupSettings,
        getBackupSettings:getBackupSettings,

        getFOTADownloadState:getFOTADownloadState,
        getDeviceNewVersion:getDeviceNewVersion,
        getFOTADownloadInfo:getFOTADownloadInfo,
        setFOTAStartDownload:setFOTAStartDownload,
        setFOTACancelDownload:setFOTACancelDownload,
        setFOTAStartUpdate:setFOTAStartUpdate,
        setFOTAStartCheckVersion:setFOTAStartCheckVersion,
		async_getUSSDSendResult:async_getUSSDSendResult,
        setUSSDSend:setUSSDSend,
        async_getUSSDEnd:async_getUSSDEnd
    }


})();

$.extend(window, Vendor);

