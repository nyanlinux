#!/bin/sh
# ALCATEL ONETOUCH USB (TCT MOBILE LIMITED L850V-2AALFR1-1/07 033)
# This is ethernet above USB via microsoft crap RDNIS closed protocol (was
# reversed engineered in the linux kernel).
# Management interface (javascript web crap) default at 192.168.1.1/24

. /etc/os-release

scratch_dir=/run/user/$(id -u)/onetouch
mkdir -p $scratch_dir
printf "SCRATCH_DIR=$scratch_dir\n"
modem_url='http://192.168.1.1'
curl_verbose='--trace-ascii -'
printf "DISTRO ID IS $ID\n"
if test "$ID" = nyanlinux; then :;
else
	curl=curl
	dd=dd
	od=od
	tr=tr
	cut=cut
fi
printf "SCRATCH_DIR=$scratch_dir\n"

rm $scratch_dir/random
$dd bs=1 count=10 if=/dev/urandom of=$scratch_dir/random
RND=$($od -t 'u1' $scratch_dir/random  | $tr -d ' \n' | $cut --byte=8-)
echo RND=$RND

verb=$(basename $0)
printf "VERB=$verb\n"
	
if test $verb = get; then
	# any upper to lower
	cmd=$(printf "$1" | tr '[A-Z]' '[a-z]')
	if test "$cmd" = loginstate; then
		url_cmd=getLoginState
	elif test "$cmd" = wlaninfo; then
		url_cmd=getWlanInfo
	elif test "$cmd" = simcardinfo; then
		url_cmd=getSimcardInfo
	elif test "$cmd" = networkinfo; then
		url_cmd=getNetworkInfo
	elif test "$cmd" = networklist; then
		url_cmd=getNetworkList
		$curl -o /dev/null \
			$curl_verbose \
			--data 'd=1' \
			"$modem_url/goform/$url_cmd?rand=$RND"
		printf 'DOC:XXXXXXXXX (must be preceded by a "set networkselect" command)\n'
		printf 'DOC:nw_sel_stat \n'
		printf 'DOC:\t0\tnone selected\n'
		printf 'DOC:\t1\tselecting\n'
		printf 'DOC:\t2\tselect success\n'
		printf 'DOC:\t3\tselect failure\n'
		printf 'DOC:rat\n'
		printf 'DOC:\t4\tGSM\n'
		printf 'DOC:\t5\tUMTS\n'
		printf 'DOC:\t8\tLTE\n'
		printf 'DOC:\t9\tTD SCDMA\n'
		printf 'DOC:state\n'
		printf 'DOC:\t1\tavailable\n'
		printf 'DOC:\t2\tcurrent\n'
		printf 'DOC:\t3\tforbidden\n'
		exit
	elif test "$cmd" = waninfo; then
		url_cmd=getWanInfo
	elif test "$cmd" = currentprofile; then
		url_cmd=getCurrentProfile
	elif test "$cmd" = routerinfo; then
		url_cmd=getRouterInfo
	elif test "$cmd" = systeminfo; then
		url_cmd=getSysteminfo
	elif test "$cmd" = imginfo; then
		url_cmd=getImgInfo
	elif test "$cmd" = wanconnectmode; then
		url_cmd=getWanConnectMode
	elif test "$cmd" = routerinfo; then
		url_cmd=getRouterInfo
	elif test "$cmd" = wlanclientinfo; then
		url_cmd=getWlanClientInfo
	elif test "$cmd" = profilelist; then
		url_cmd=getProfileList
	elif test "$cmd" = macfilterinfo; then
		url_cmd=getMACFilterInfo
	elif test "$cmd" = ussdsendresult; then
		url_cmd=getUSSDSendResult
	elif test "$cmd" = ussdend; then
		url_cmd=getUSSDEnd
	elif test "$cmd" = networkregisterresult; then
		url_cmd=getNetworkRegisterResult
	else
		printf "ERROR:GET:missing known command:\n\
loginstate\n\
wlaninfo\n\
simcardinfo\n\
networkinfo\n\
networklist\n\
waninfo\n\
currentprofile\n\
routerinfo\n\
systeminfo\n\
imginfo\n\
wanconnectmode\n\
routerinfo\n\
wlanclientinfo\n\
profilelist\n\
macfilterinfo\n\
ussdsendresult\n\
ussdend\n\
networkregisterresult\n\
"
		exit 1
	fi
	$curl -o /dev/null \
		$curl_verbose \
		"$modem_url/goform/$url_cmd?rand=$RND"
	# inline DOC
	if test "$cmd" = loginstate; then :;
	elif test "$cmd" = wlaninfo; then :;
	elif test "$cmd" = simcardinfo; then :;
		printf 'DOC:sim_state\n'
		printf 'DOC:\t0\tunknown\n'
		printf 'DOC:\t1\tdetected\n'
		printf 'DOC:\t2\tpin1 or upin required\n'
		printf 'DOC:\t3\tpuk1 or puk required\n'
		printf 'DOC:\t4\tperson required\n'
		printf 'DOC:\t5\tpin1 permanently blocked\n'
		printf 'DOC:\t6\tillegal\n'
		printf 'DOC:\t7\tready\n'
		printf 'DOC:\t11\tiniting\n'
		printf 'DOC:pin_state\n'
		printf 'DOC:\t0\tunknown\n'
		printf 'DOC:\t1\tenabled not verified\n'
		printf 'DOC:\t2\tenabled verified\n'
		printf 'DOC:\t3\tdisabled\n'
		printf 'DOC:\t4\tblocked\n'
		printf 'DOC:\t5\tpermanently blocked\n'
		printf 'DOC:sim_lock_state\n'
		printf 'DOC:\t-1\tnone required\n'
		printf 'DOC:\t0\tnet pin required\n'
		printf 'DOC:\t1\tnetsub pin required\n'
		printf 'DOC:\t2\tsp pin required\n'
		printf 'DOC:\t3\tcorp pin required\n'
		printf 'DOC:\t4\tph fsim pin required\n'
		printf 'DOC:\t15\tnet puk required\n'
		printf 'DOC:\t16\tnetsub puk required\n'
		printf 'DOC:\t17\tsp puk required\n'
		printf 'DOC:\t18\tcorp puk required\n'
		printf 'DOC:\t19\tph fsim puk required\n'
		printf 'DOC:\t30\trck forbid\n'
	elif test "$cmd" = networkinfo; then :;
		printf 'DOC:nw_mode\n'
		printf 'DOC:\t28\tauto\n'
		printf 'DOC:\t16\t4gonly\n'
		printf 'DOC:\t8\t3gonly\n'
		printf 'DOC:\t4\t2gonly\n'
		printf 'DOC:nw_sel_mode\n'
		printf 'DOC:\t0 automatic\n'
		printf 'DOC:\t1 manual\n'
	elif test "$cmd" = waninfo; then
		printf 'DOC:wan_state\n'
		printf 'DOC:\t0 PPP_DISCONNECTED\n'
		printf 'DOC:\t1 PPP_CONNECTING\n'
		printf 'DOC:\t2 PPP_CONNECTED\n'
		printf 'DOC:\t3 PPP_DISCONNECTING\n'
		printf 'DOC:network_type\n'
		printf 'DOC:\t0\tNO_SERVICE\n'
		printf 'DOC:\t1\tGPRS\n'
		printf 'DOC:\t2\tEDGE\n'
		printf 'DOC:\t3\tHSDPA\n'
		printf 'DOC:\t4\tHSUPA\n'
		printf 'DOC:\t5\tUMTS\n'
		printf 'DOC:\t6\tCDMA\n'
		printf 'DOC:\t7\tEV_DO_A\n'
		printf 'DOC:\t8\tEV_DO_B\n'
		printf 'DOC:\t9\tGSM\n'
		printf 'DOC:\t10\tEV_DO_C\n'
		printf 'DOC:\t11\tLTE\n'
		printf 'DOC:\t12\tHSPA_PLUS\n'
		printf 'DOC:\t13\tDC_HSPA_PLUS\n'
		printf 'DOC:roam\n'
		printf 'DOC:\t0 ENABLE\n'
		printf 'DOC:\t1 DISABLE\n'
	elif test "$cmd" = currentprofile; then :;
	elif test "$cmd" = routerinfo; then :;
	elif test "$cmd" = systeminfo; then :;
	elif test "$cmd" = imginfo; then :;
	elif test "$cmd" = wanconnectmode; then :;
		printf 'DOC:wan_conn_mode\n'
		printf 'DOC:\t0 manual\n'
		printf 'DOC:\t1 automatic\n'
		printf 'DOC:roam_auto_conn\n'
		printf 'DOC:\t0 IS_AUTOROAM_DISABLE\n'
		printf 'DOC:\t1 IS_AUTOROAM_ENABLE\n'
		printf 'DOC:wan_ip_mode\n'
		printf 'DOC:\t0 ipv4\n'
		printf 'DOC:\t2 ipv6\n'
		printf 'DOC:\t3 ipv4/ipv6\n'
	elif test "$cmd" = routerinfo; then :;
	elif test "$cmd" = wlanclientinfo; then :;
	elif test "$cmd" = profilelist; then :;
		printf 'DOC:profile_type\n'
		printf 'DOC:\t0 builtin\n'
		printf 'DOC:\t1 user defined\n'
		printf 'DOC:profile_protocol\n'
		printf 'DOC:\t0 none\n'
		printf 'DOC:\t1 PAP\n'
		printf 'DOC:\t2 CHAP\n'
		printf 'DOC:\t3 PAP & CHAP\n'
	elif test "$cmd" = macfilterinfo; then :;
	elif test "$cmd" = ussdsendresult; then :;
	elif test "$cmd" = ussdend; then :;
	elif test "$cmd" = networkregisterresult; then
		printf 'DOC:regist_stat\n'
		printf 'DOC:\t0\tfail\n'
		printf 'DOC:\t1\tsuccess\n'
		printf 'DOC:\t2\tregistrating\n'
		printf 'DOC:\tother\tno registration in progress\n'
	fi
	exit
elif test $verb = set; then
	# any upper to lower
	cmd=$(printf "$1" | tr '[A-Z]' '[a-z]')
	if test "$cmd" = profiledefault; then
		profile_id=$2
		if test "x$profile_id" = x; then
			printf 'ERROR:SET:setProfileDefault:missing profile id\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "profile_id=$profile_id" \
			"$modem_url/goform/setProfileDefault?rand=$RND"
		exit
	elif test "$cmd" = wanconnect; then
		profile_id=$2
		if test "x$profile_id" = x; then
			printf 'ERROR:SET:setWanConnect:missing profile id\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "profile_id=$profile_id" \
			"$modem_url/goform/setWanConnect?rand=$RND"
		exit
	elif test "$cmd" = wandisconnect; then
		$curl -o /dev/null \
			$curl_verbose \
			"$modem_url/goform/setWanDisconnect?rand=$RND"
		exit
	elif test "$cmd" = cancelwanconnect; then
		$curl -o /dev/null \
			$curl_verbose \
			"$modem_url/goform/setCancelWanConnect?rand=$RND"
	elif test "$cmd" = profiledelete; then
		profile_id=$2
		if test "x$profile_id" = x; then
			printf 'ERROR:SET:setProfileDelete:missing profile id\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "profile_id=$profile_id" \
			"$modem_url/goform/setProfileDelete?rand=$RND"
		exit
	elif test "$cmd" = profilesave; then
		profile_id=$2
		profile_name=$3
		profile_apn=$4
		profile_protocol=$5
		if test "x$profile_id" = x -o "x$profile_name" = x -o "x$profile_apn" = x -o "x$profile_protocol" = x; then
			printf 'ERROR:SET:setProfileSave:missing parameters\n'
			printf 'DOC:set profilesave profile_id profile_name profile_apn profile_protocol\n'
			printf 'DOC:profile_protocol:\n'
			printf 'DOC:\t0 none\n'
			printf 'DOC:\t1 PAP\n'
			printf 'DOC:\t2 CHAP\n'
			printf 'DOC:\t3 PAP & CHAP\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "profile_id=$profile_id" \
			--data 'key=edit' \
			--data "profile_protocol=$profile_protocol" \
			--data "profile_apn=$profile_apn" \
			--data 'profile_password=' \
			--data 'profile_username=' \
			--data "profile_name=$profile_name" \
			--data 'profile_number=*99#' \
			"$modem_url/goform/setProfileSave?rand=$RND"
		exit
	elif test "$cmd" = profilenew; then
		profile_id=$2
		profile_name=$3
		profile_apn=$4
		profile_protocol=$5
		profile_username=$6
		profile_password=$7
		if test "x$profile_id" = x -o "x$profile_name" = x -o "x$profile_apn" = x -o "x$profile_protocol" = x; then
			printf 'ERROR:SET:setProfileSave:missing parameters\n'
			printf 'DOC:set profilenew profile_id profile_name profile_apn profile_protocol profile_username profile_password\n'
			printf 'DOC:profile_protocol:\n'
			printf 'DOC:\t0 none\n'
			printf 'DOC:\t1 PAP\n'
			printf 'DOC:\t2 CHAP\n'
			printf 'DOC:\t3 PAP & CHAP\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "profile_id=$profile_id" \
			--data 'key=new' \
			--data "profile_protocol=$profile_protocol" \
			--data "profile_apn=$profile_apn" \
			--data "profile_password=$profile_password" \
			--data "profile_username=$profile_username" \
			--data "profile_name=$profile_name" \
			--data 'profile_number=*99#' \
			"$modem_url/goform/setProfileSave?rand=$RND"
		exit
	elif test "$cmd" = reboot; then
		$curl -o /dev/null \
			$curl_verbose \
			"$modem_url/goform/setReboot?rand=$RND"
	elif test "$cmd" = wanconnectmode; then
		mode=$2
		ip_mode=$3
		roam_auto_conn=$4
		if test "x$mode" = x -o "x$ip_mode" = x -o "x$roam_auto_conn" = x; then
			printf 'ERROR:SET:setWanConnectMode:missing mode\n'
			printf 'DOC:set wanconnectmode mode ip_mode roam_auto_conn\n'
			printf 'DOC:mode\n'
			printf 'DOC:0 manual\n'
			printf 'DOC:1 automatic\n'
			printf 'DOC:ip_mode\n'
			printf 'DOC:0 ipv4\n'
			printf 'DOC:2 ipv6\n'
			printf 'DOC:3 ipv4/ipv6\n'
			printf 'DOC:roam_auto_conn\n'
			printf 'DOC:0 IS_AUTOROAM_DISABLE\n'
			printf 'DOC:1 IS_AUTOROAM_ENABLE\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "wan_ip_mode=$ip_mode" \
			--data "wan_conn_mode=$mode" \
			--data "roam_auto_conn=$roam_auto_conn" \
			"$modem_url/goform/setWanConnectMode?rand=$RND"
		exit
	elif test "$cmd" = networkselect; then
		$curl -o /dev/null \
			$curl_verbose \
			"$modem_url/goform/setNetworkSelect?rand=$RND"
		printf 'XXXXXXXXXX: CALL "get networklist" TO GET THE LIST\n'
	elif test "$cmd" = reset; then
		$curl -o /dev/null \
			$curl_verbose \
			"$modem_url/goform/setReset?rand=$RND"
	elif test "$cmd" = networkinfo; then
		nw_mode=$2
		nw_sel_mode=$3
		if test "x$nw_mode" = x -o "x$nw_sel_mode" = x; then
			printf 'ERROR:SET:setNetworkInfo:missing mode\n'
			printf 'DOC:set networkinfo nw_mode nw_sel_mode\n'
			printf 'DOC:nw_mode\n'
			printf 'DOC:\t28\tauto\n'
			printf 'DOC:\t16\t4gonly\n'
			printf 'DOC:\t8\t3gonly\n'
			printf 'DOC:\t4\t2gonly\n'
			printf 'DOC:nw_sel_mode\n'
			printf 'DOC:\t0 automatic\n'
			printf 'DOC:\t1 manual\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "nw_mode=$nw_mode" \
			--data "nw_sel_mode=$nw_sel_mode" \
			"$modem_url/goform/setNetworkInfo?rand=$RND"
		exit
	elif test "$cmd" = networkregister; then
		index=$2
		if test "x$index" = x; then
			printf 'ERROR:SET:setNetworkRegister:missing network index\n'
			printf 'DOC:set networkregister index\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "index=$index" \
			"$modem_url/goform/setNetworkRegister?rand=$RND"
		exit
	elif test "$cmd" = unlockpin; then
		pin=$2
		if test "x$pin" = x; then
			printf 'ERROR:SET:unlockPIN:missing pin\n'
			exit 1
		fi
		$curl -o /dev/null \
			$curl_verbose \
			--data "pin=$pin" \
			"$modem_url/goform/unlockPIN?rand=$RND"
		exit
	elif test "$cmd" = disablewifi; then
		# we use the defaults and modify only wifi_state
		$curl -o /dev/null \
			$curl_verbose \
			--data "wifi_state=0" \
			--data "wlan_mode=0" \
			--data "wmode=2" \
			--data "wmode_5g=5" \
			--data "ssid=MY WIFI" \
			--data "ssid_5g=MYWIFI _5G" \
			--data "hidden_ssid=0" \
			--data "hidden_ssid_5g=0" \
			--data "channel=0" \
			--data "channel_5g=0" \
			--data "max_numsta=10" \
			--data "max_numsta_5g=10" \
			--data "curr_num=0" \
			--data "security_mode=3" \
			--data "wep_sec=0" \
			--data "wep_key=1234567890" \
			--data "wpa_sec=2" \
			--data "wpa_passphrase=MYWIFI2549" \
			--data "wifi_country_code=CN" \
			--data "ap_status=0" \
			"$modem_url/goform/setWlanInfo?rand=$RND"
	else
		printf "ERROR:SET:missing known command:\n\
profiledefault\n\
wanconnect\n\
wandisconnect\n\
wanconnectmode\n\
cancelwanconnect\n\
profiledelete\n\
profilesave\n\
profilenew\n\
reboot\n\
reset\n\
networkselect\n\
networkinfo\n\
networkregister\n\
unlockpin\n\
disablewifi\n\
"
		exit 1
		prin
	fi
else
	printf "ERROR:missing known verb, have '$verb', should be get/set\n"
	exit 1
fi
