# This file is sourced by a login shell.
export MAIL=$HOME/mail
export LANG=POSIX
# ENV defines what is sourced by shell for an interactive behavior.
export ENV=$HOME/.init.sh
export PATH=
