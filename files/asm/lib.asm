	[section .data]
;-------------------------------------------------------------------------------
; qword of 64 bits, means up to 64 chars + 1 terminating char
itoa_buf_sz:	equ 65
itoa_buf:	times itoa_buf_sz db 0
sysc_write:	equ 1
str_sz_max:	equ 1024
;-------------------------------------------------------------------------------
	[section .text]
;==============================================================================
; stolen and adapted from asmutils
;itoa (unsigned long value, char *string, int radix)
;
;print 64 bits number as binary,octal,decimal,or hexadecimal value
;
;<RAX	unsigned long value
;<RDI	char *string
;<RCX	base    (2, 8, 10, 16, or another one)

itoa:
	push rax
	push rcx
	push rdx
	push rdi
	
	call	.printB
	mov	byte [rdi], 0	;terminating null char

	pop rdi
	pop rdx
	pop rcx
	pop rax
	ret

.printB:
	xor	rdx, rdx 
	div	rcx 
	test	rax, rax 
	jz	.print0
	push	rdx
	call	.printB
	pop	rdx
.print0:
	add	dl, '0'
	cmp	dl, '9'
	jle	.print1
	add	dl, 0x27
.print1:
	mov	[rdi], dl
 	inc	rdi
 	ret
;------------------------------------------------------------------------------
;rax n
hextoa:
	push rax
	push rcx
	push rdi
	push rsi
	push rdx

	mov qword [itoa_buf], 0
	mov qword [itoa_buf + 8], 0
	mov qword [itoa_buf + 8 * 2], 0
	mov qword [itoa_buf + 8 * 3], 0
	mov byte [itoa_buf + 8 * 3 + 1], 0

	mov rdi, itoa_buf
	mov rcx, 16		; hex
	call itoa

	pop rdx
	pop rsi
	pop rdi
	pop rcx
	pop rax
	ret

; rax = str ptr
; rbx = buf sta
str_out_stderr:
	push rax
	push rbx
	push rcx
	push rdx
	push rdi
	push rsi

	mov rdi, rax
	mov rbx, rax			; save str ptr in rbx

	cld
	xor al,al			; null character
	mov rcx, str_sz_max
	repne scasb			; the flags are from the cmp operation, not from counter termination condition
	jne .exit

	dec rdi				; rdi to point on the terminating null char

	sub rdi, rbx			; compute str len (namely excluding the terminating null char)
	mov rdx, rdi			; 3rd write arg (byte count)

	mov rax, sysc_write		; 1
	mov rdi, 2			; fd, "std error"
	mov rsi, rbx			; str ptr
	
	push r11
	syscall
	pop r11

.exit:
	pop rsi
	pop rdi
	pop rdx
	pop rcx
	pop rbx
	pop rax
	ret
;==============================================================================
