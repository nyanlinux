/*
 * This assembly source code is intended to be preprocessed:
 *	- don't use # comments
 *      - do use your C compiler front-end with proper specific options or do
 *	  pipe cpp output to as (don't forget to link as an executable then)
 *
 * 	cpp poweroff.S | as -o poweroff.o; ld -o poweroff poweroff.o
 */

/*
 * Magic values required to use _reboot() system call.
 */
#define	LINUX_REBOOT_MAGIC1	0xfee1dead
#define	LINUX_REBOOT_MAGIC2	672274793
#define	LINUX_REBOOT_MAGIC2A	85072278
#define	LINUX_REBOOT_MAGIC2B	369367448
#define	LINUX_REBOOT_MAGIC2C	537993216


/*
 * Commands accepted by the _reboot() system call.
 *
 * RESTART     Restart system using default command and mode.
 * HALT        Stop OS and give system control to ROM monitor, if any.
 * CAD_ON      Ctrl-Alt-Del sequence causes RESTART command.
 * CAD_OFF     Ctrl-Alt-Del sequence sends SIGINT to init task.
 * POWER_OFF   Stop OS and remove all power from system, if possible.
 * RESTART2    Restart system using given command string.
 * SW_SUSPEND  Suspend system using software suspend if compiled in.
 * KEXEC       Restart system using a previously loaded Linux kernel
 */
#define	LINUX_REBOOT_CMD_RESTART	0x01234567
#define	LINUX_REBOOT_CMD_HALT		0xCDEF0123
#define	LINUX_REBOOT_CMD_CAD_ON		0x89ABCDEF
#define	LINUX_REBOOT_CMD_CAD_OFF	0x00000000
#define	LINUX_REBOOT_CMD_POWER_OFF	0x4321FEDC
#define	LINUX_REBOOT_CMD_RESTART2	0xA1B2C3D4
#define	LINUX_REBOOT_CMD_SW_SUSPEND	0xD000FCE2
#define	LINUX_REBOOT_CMD_KEXEC		0x45584543

/* the syscall on x86_64 */
#define __ULINUX_NR_reboot 169

/* _start is the ELF symbol which defines the program entry point */
	.global _start
_start:
	mov $__ULINUX_NR_reboot, %eax
	mov $LINUX_REBOOT_MAGIC1, %rdi
	mov $LINUX_REBOOT_MAGIC2, %rsi
	mov $LINUX_REBOOT_CMD_POWER_OFF, %rdx
	xor %r10,%r10
/* never returning syscall */
	syscall

