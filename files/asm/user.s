		.intel_syntax noprefix
		.intel_mnemonic
		.code64

	.data
shell:		.asciz "/bin/sh" /* should be a static executable */
arg_login:	.asciz "-l"
home:		.asciz "HOME=/home/user"

		.align 8,0
argv:		.8byte 0/*shell*/,0/*arg_login*/,0
envp:		.8byte 0/*home*/,0
groups:		.4byte 10/*input*/,11/*audio*/,12/*video*/

	.text
/* _start is the ELF symbol which defines the program entry point */
	.global _start
_start:
	mov rax,116			/* setgroups syscall */
	mov rdi,3			/* 3 groups */
	lea rsi,qword ptr [rip+groups]
	syscall
	mov rax,119			/* setresgid */
	mov rdi,1000			/* real GID=1000 */
	mov rsi,1000			/* effective GID=1000 */
	mov rdx,1000			/* saved GID=1000 */
	syscall
	mov rax,117			/* setresuid */
	mov rdi,1000			/* real UID=1000 */
	mov rsi,1000			/* effective UID=1000 */
	mov rdx,1000			/* saved UID=1000 */
	syscall
	/* init the envp */
	lea rax,[rip+home]
	mov qword ptr [rip+envp],rax
	/* init the argv */
	lea rdi,byte ptr [rip+shell]	/* execve[0] */
	lea rsi,byte ptr [rip+argv]	/* execve[1] */
	lea rcx,byte ptr [rip+arg_login]
	mov qword ptr [rsi],rdi
	mov qword ptr [rsi+8],rcx
	/* execve syscall */
	lea rdx,byte ptr [rip+envp]	/* execve[2] */
	mov rax,59			/* execve */
	syscall
	/* boom */
	neg rax
	mov rdi,rax
	mov rax,231
	syscall
