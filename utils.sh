find_in_path()
{
	r=
	ifs_prev="$IFS"
	IFS=:
	for d in $PATH
	do
		if test -x $d/$1; then
			IFS="$ifs_prev"
			r=$d/$1
			return 0
		fi
	done
	IFS="$ifs_prev"
	return 1
}

