src_name=ffmpeg
git_url0=https://git.ffmpeg.org/$src_name.git

pkg_dir=/run/pkgs/$src_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p /run/pkgs
cp -r $src_dir $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
/nyan/git/current/bin:\
/nyan/nasm/current/bin:\
$PATH\
"

cd $pkg_dir

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

# we use libressl static libs
sed -i -e '/^enabled openssl/,+9 c\
enabled openssl            && require ssl openssl/ssl.h OPENSSL_init_ssl -lssl -ltls -lcrypto -lpthread' ./configure

# force libdav1d
sed -i -e '/^enabled libdav1d/ c\enabled libdav1d' ./configure

# opus is not supported as a static lib, switch to manual detection
sed -i -e '/require_pkg_config libopus opus opus_multistream.h opus_multistream_surround_encoder_create/ c\        require libopus opus_multistream.h opus_multistream_surround_encoder_create -lopus -lm' ./configure
sed -i -e '/require_pkg_config libopus opus opus_multistream.h opus_multistream_decoder_create/ c\        require libopus opus_multistream.h opus_multistream_decoder_create -lopus -lm' ./configure

# fontconfig without pkgconfig
sed -i -e '/^enabled libfontconfig/ c\
enabled libfontconfig     && require libfontconfig "fontconfig/fontconfig.h" FcInit -lfontconfig' ./configure

# freetype without pkgconfig
sed -i -e '/^enabled libfreetype/ c\
enabled libfreetype       && require libfreetype "ft2build.h FT_FREETYPE_H" FT_Init_FreeType -lfreetype' ./configure

# libxcb without pkgconfig
sed -i -e '/^enabled libxcb/ c\
enabled libxcb && check_lib libxcb xcb/xcb.h xcb_connect -lxcb ||' ./configure

# libxcb-shm without pkgconfig
sed -i -E -e '/^[[:space:]]+enabled libxcb_shm/ c\
    enabled libxcb_shm    && check_lib libxcb_shm xcb/shm.h    xcb_shm_attach -lxcb-shm' ./configure

# libxcb-shape without pkgconfig
sed -i -E -e '/^[[:space:]]+enabled libxcb_shape/ c\
    enabled libxcb_shape  && check_lib libxcb_shape  xcb/shape.h  xcb_shape_get_rectangles -lxcb-shape' ./configure

# libxcb-xfixes without pkgconfig
sed -i -E -e '/^[[:space:]]+enabled libxcb_xfixes/ c\
    enabled libxcb_xfixes && check_lib libxcb_xfixes xcb/xfixes.h xcb_xfixes_get_cursor_image -lxcb-xfixes' ./configure

build_dir=/run/builds/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export PATH="\
$build_dir/bin:\
$PATH"

mkdir $build_dir/bin
cat >$build_dir/bin/gcc <<EOF
#!/bin/sh
exec /opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
	-static-libgcc \
\
\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-I/nyan/libvpx/current/include/ \
	-I/nyan/opus/current/include/opus \
	-I/nyan/dav1d/current/include \
	-I/nyan/libressl/current/include \
	-I/nyan/fontconfig/current/include \
	-I/nyan/freetype/current/include/freetype2 \
	-I/nyan/libxcb/current/include \
\
\
	-O2 -pipe -fPIC \
\
\
	-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
	-Wl,-s \
	-Wl,-rpath-link,\
/nyan/glibc/current/lib:\
/nyan/libpng/current/lib:\
/nyan/libXau/current/lib:\
/nyan/libxcb/current/lib:\
/nyan/freetype/current/lib:\
/nyan/dav1d/current/lib \
\
\
	-L/nyan/libvpx/current/lib \
	-L/nyan/opus/current/lib \
	-L/nyan/dav1d/current/lib \
	-L/nyan/libressl/current/lib \
	-L/nyan/fontconfig/current/lib \
	-L/nyan/freetype/current/lib \
	-L/nyan/libxcb/current/lib \
\
"\$@" \
\
	-ldav1d -lssl -ltls -lcrypto -lpthread
exit 1
EOF
chmod +x $build_dir/bin/gcc

# configure runs some programs
LD_LIBRARY_PATH_SAVED=$LD_LIBRARY_PATH
export "LD_LIBRARY_PATH=\
/nyan/fontconfig/current/lib:\
/nyan/freetype/current/lib:\
/nyan/libpng/current/lib:\
/nyan/dav1d/current/lib:\
$LD_LIBRARY_PATH"

# XXX: added libssl libs next to libdav1d due to twitch breaking tls_libtls ssl backend somehow
$pkg_dir/configure \
	--target_os=linux \
	--arch=x86_64 \
	--enable-pic \
	--prefix=/nyan/ffmpeg/$slot \
	--enable-error-resilience \
	--enable-gpl \
	--enable-version3 \
	--enable-nonfree \
	--enable-static \
	--disable-shared \
	--disable-ffplay \
	--enable-ffprobe \
	--disable-doc \
	--disable-htmlpages \
	--disable-manpages \
	--disable-podpages \
	--disable-txtpages \
	--enable-libdav1d \
	--enable-gmp \
	--enable-openssl \
	--disable-libass \
	--enable-libfontconfig \
	--enable-libfreetype \
	--disable-libfribidi \
	--enable-libopus \
	--enable-libvpx \
	--disable-xlib \
	--enable-libxcb \
	--enable-libxcb-shm \
	--enable-libxcb-xfixes \
	--enable-libxcb-shape \
	--disable-decoder=libvpx_vp8,libvpx_vp9

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH_SAVED
make -j $threads_n
make install

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
