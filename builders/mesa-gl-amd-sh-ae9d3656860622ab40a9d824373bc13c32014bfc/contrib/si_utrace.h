#ifndef SI_UTRACE_H
#define SI_UTRACE_H

#define si_utrace_init(...)
#define si_utrace_fini(...)
#define si_utrace_flush(...)
#define si_driver_ds_init(...)

#endif
