#!/bin/sh
# Canonical specialized build scripts for AMD hardware on gnu/linux distros.
# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the scripts in the contrib directory at the top of mesa source tree
# create somewhere else a build directory, cd into it, and call from there the
# main script.
#===============================================================================
# NOTES
# the shared lib link command has 3 important sections:
#  - between whole-archive and no-whole-archive is for finding symbols which are
#    going to be dynamic.
#  - exclude-libs, remove the symbols of a list of archives/objects from the final
#    shared lib. Since with are using the hidden default visibility, it's for
#    external archives which could be compiled without the hidden visibility.
#  - between as-needed and no-as-needed, will add an explicit shared lib
#    dependencies if some symbols from those shared libs are actually used.
#    (don't know if this is done based on the shared lib dependendy tree)
# 
# the dri platform is actually the os:
#  - drm -> linux
#  - apple -> macos
#  - windows -> microsoft
#
# the EGL platforms are, mainly:
#  - GBM/dri/drm (the one used by the xserver glamor acceleration)
#  - x11/dri3 (used by real egl client application)
#  - wayland/etc
#===============================================================================

#===============================================================================
# build dir, src dir and script dir
build_dir=$(readlink -f .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(readlink -f $(dirname $0)/..)
echo "src_dir=$src_dir"
# script location
script_dir=$(readlink -f $(dirname $0))
echo "script_dir=$script_dir"
echo 
#===============================================================================

 
#===============================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. grep for "-unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===============================================================================


#===============================================================================
# configuration of directories
#-------------------------------------------------------------------------------
if test "${sysconfdir-unset}" = unset; then
sysconfdir='/etc'
fi
#-------------------------------------------------------------------------------
if test "${datadir-unset}" = unset; then
datadir='/usr/share'
fi
#-------------------------------------------------------------------------------
if test "${libdir-unset}" = unset; then
libdir='/usr/lib'
fi
#-------------------------------------------------------------------------------
if test "${dri_driver_search_dir-unset}" = unset; then
dri_driver_search_dir=/usr/lib/dri
fi
#-------------------------------------------------------------------------------
if test "${pipe_search_dir-unset}" = unset; then
pipe_search_dir=/usr/lib/gallium-pipe
fi
#===============================================================================


#===============================================================================
if test "${version-unset}" = unset; then
	if test -f $src_dir/VERSION; then
		version=$(cat $src_dir/VERSION)
	else
		echo 'error:missing version'
		exit 1
	fi
fi
#===============================================================================


#===============================================================================
# lexer and parser
if test "${bison-unset}" = unset; then
bison=/nyan/nyanbison/current/bin/bison
fi
if test "${flex-unset}" = unset; then
flex=/nyan/flex/current/bin/flex
fi
#===============================================================================


#===============================================================================
# python/perl/ruby/javascript/lua/etc whatever...
if test "${python3-unset}" = unset; then
python3=/nyan/python/current/bin/python3
fi

if test "${mako-unset}" = unset; then
mako=/nyan/mako/current
fi

if test "${yaml-unset}" = unset; then
yaml=/nyan/PyYAML/current
fi
#===============================================================================


#===============================================================================
. $script_dir/gcc_binutils.sh
. $script_dir/linux.sh
. $script_dir/syslib.sh
#===============================================================================


#===============================================================================
# configuration of mesa code paths

# enable/disable debug code paths
#debug_cpp_flags_defs='-DDEBUG'
debug_cpp_flags_defs='-DNDEBUG=1'

mesa_cpp_flags_defs="\
$debug_cpp_flags_defs \
-DHAVE_RADEONSI \
-DUSE_LIBGLVND=1 \
-DHAVE_OPENGL=1 \
-DHAVE_OPENGL_ES_1=1 \
-DHAVE_OPENGL_ES_2=1 \
-DENABLE_SHADER_CACHE=1 \
-DHAVE_DRI=1 \
-DHAVE_DRI2=1 \
-DHAVE_DRI3=1 \
-DHAVE_DRI3_MODIFIERS=1 \
-DHAVE_DRI3_EXPLICIT_SYNC=1 \
-DHAVE_DRISW_KMS=1 \
-DGLX_DIRECT_RENDERING=1 \
-DGLX_USE_DRM=1 \
-DPACKAGE_VERSION=\"$version\" \
-DPACKAGE_BUGREPORT=\"https://bugs.freedesktop.org/enter_bug.cgi?product=Mesa\" \
-DHAVE_COMPRESSION \
\
-DVK_USE_PLATFORM_XCB_KHR=1 \
\
-DALLOW_KCMP \
"
#===============================================================================


#===============================================================================
. $script_dir/external_deps.sh
#===============================================================================


################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################


#===============================================================================
rm -Rf $build_dir/to_install
mkdir $build_dir/to_install
mkdir $build_dir/to_install/include
#===============================================================================


#===============================================================================
# the git sha
mkdir $build_dir/src
git_sha1=no_git_sha1_available
if test -d $src_dir/.git; then
	git_sha1=$(git --git-dir=$src_dir/.git rev-parse HEAD)
fi
echo git_sha1=$git_sha1
echo "#define MESA_GIT_SHA1 \"$git_sha1\"" >$build_dir/src/git_sha1.h
#===============================================================================


#===============================================================================
# some code generators
. $script_dir/generators_amd.sh
. $script_dir/generators_compiler.sh
. $script_dir/generators_nir.sh
. $script_dir/generators_spirv.sh
. $script_dir/generators_glsl.sh
. $script_dir/generators_gallium_targets.sh
. $script_dir/generators_gallium_auxiliary.sh
. $script_dir/generators_util.sh
. $script_dir/generators_mapi.sh
. $script_dir/generators_mesa.sh
. $script_dir/generators_egl.sh
wait
#------------------------------------------------------------------------------
. $script_dir/util.sh
#------------------------------------------------------------------------------
. $script_dir/loader.sh
. $script_dir/mapi.sh
. $script_dir/compiler.sh
. $script_dir/mesa.sh
#------------------------------------------------------------------------------
. $script_dir/amd.sh
. $script_dir/gallium.sh
#------------------------------------------------------------------------------
. $script_dir/gbm.sh
. $script_dir/egl.sh
. $script_dir/glx.sh
#===============================================================================
