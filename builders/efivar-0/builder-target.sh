src_name=efivar
git_commit=c7d65f38daacb1d9be13532b5eb91b2444e833d0
git_url0=git://github.com/rhboot/efivar

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

#-------------------------------------------------------------------------------
cd $pkg_dir

git checkout --force $git_commit
git reset --hard

# we use our build script, because the official one is straight from a seriously
# ill brain
cp -f $nyan_root/builders/$pkg_name/efivar.sh ./
#-------------------------------------------------------------------------------

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$sdk_texinfo_path/bin:$sdk_help2man_path/bin:$PATH

build_cc=gcc
build_cflags='-O2 -pipe -fPIC'

host_cc="$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
host_cflags='-O2 -pipe -fPIC'
host_ccld="$host_cc"
host_ar=$target_gnu_triple-ar 

destdir=$target_sysroot/nyan/$src_name/0

. ./efivar.sh

unset build_cc
unset build_cflags
unset host_cc
unset host_cflags
unset host_ccld
unset host_ar
unset destdir

# cleanup and tidying
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/bin/$src_name

rm -Rf $pkg_dir
