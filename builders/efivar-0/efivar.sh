cd ./src

# the following should be set
#build_cc=gcc
#build_cflags='-O2 -pipe -fPIC'
#host_cc=gcc
#host_cflags='-O2 -pipe -fPIC'
#host_ccld=gcc
#host_ar=ar 
#destdir=/usr/local

rm -Rf $destdir/include/efivar
mkdir -p $destdir/include/efivar
mkdir -p $destdir/lib
mkdir -p $destdir/bin

#-------------------------------------------------------------------------------

# code generator building
$build_cc $build_cflags -I./include -D_GNU_SOURCE -DEFIVAR_BUILD_ENVIRONMENT -o makeguids makeguids.c guid.c -ldl

# code generation run
./makeguids guids.txt guids.bin names.bin guid-symbols.c include/efivar/efivar-guids.h

#-------------------------------------------------------------------------------

libefivar_cc="$host_cc $host_cflags -D_GNU_SOURCE -I./include -c -o"
$libefivar_cc dp.o dp.c
$libefivar_cc dp-acpi.o dp-acpi.c
$libefivar_cc dp-hw.o dp-hw.c
$libefivar_cc dp-media.o dp-media.c
$libefivar_cc dp-message.o dp-message.c
$libefivar_cc efivarfs.o efivarfs.c
$libefivar_cc error.o error.c
$libefivar_cc export.o export.c
$libefivar_cc guid.o guid.c
$libefivar_cc guids.o guids.S
$libefivar_cc guid-symbols.o guid-symbols.c
$libefivar_cc lib.o lib.c
$libefivar_cc vars.o vars.c

$host_ar rcs $destdir/lib/libefivar.a dp.o dp-acpi.o dp-hw.o dp-media.o dp-message.o efivarfs.o error.o export.o guid.o guids.o guid-symbols.o lib.o vars.o

#-------------------------------------------------------------------------------

libefiboot_cc="$host_cc $host_cflags -D_GNU_SOURCE -I./include -c -o"
$libefiboot_cc crc32.o crc32.c
$libefiboot_cc creator.o creator.c
$libefiboot_cc disk.o disk.c
$libefiboot_cc gpt.o gpt.c
$libefiboot_cc linux.o linux.c
$libefiboot_cc loadopt.o loadopt.c

$host_ar rcs $destdir/lib/libefiboot.a crc32.o creator.o disk.o gpt.o linux.o loadopt.o

#-------------------------------------------------------------------------------

$host_cc $host_cflags -D_GNU_SOURCE -I./include -c -o efivar.o efivar.c
$host_ccld -o $destdir/bin/efivar efivar.o -L$destdir/lib -lefivar -ldl 

#-------------------------------------------------------------------------------

cp -f 					\
./include/efivar/efivar.h		\
./include/efivar/efiboot.h		\
./include/efivar/efiboot-loadopt.h	\
./include/efivar/efivar-dp.h		\
./include/efivar/efiboot-creator.h	\
./include/efivar/efivar-guids.h		\
$destdir/include/efivar

unset libefivar_cc
unset libefiboot_cc

cd ..
