src_name=libXi
mkdir /nyan/$src_name
version=${pkg_name##*-}
slot=$version
mkdir /nyan/$src_name/$slot
archive_name=$src_name-$version.tar.xz
url0=http://xorg.freedesktop.org/releases/individual/lib/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

mkdir -p $build_dir/bin
cat >$build_dir/bin/cc <<EOF
#!/bin/sh
eval \
/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
\
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
\
-I/nyan/libXau/current/include \
-I/nyan/libxcb/current/include \
-I/nyan/libX11/current/include \
-I/nyan/libXext/current/include \
-I/nyan/libXfixes/current/include \
-I/nyan/xorgproto/current/include \
\
-O2 -pipe -fPIC -static-libgcc -ftls-model=global-dynamic -fpic \
\
-B/nyan/glibc/current/lib \
\
-L/nyan/libXau/current/lib \
-L/nyan/libxcb/current/lib \
-L/nyan/libX11/current/lib \
-L/nyan/libXext/current/lib \
-L/nyan/libXfixes/current/lib \
-L/nyan/glibc/current/lib \
\
\
"\$@"
EOF
chmod +x $build_dir/bin/cc
export PATH_SAVED=$PATH
export PATH="\
$build_dir/bin:\
$PATH\
"

XI2_src_files="\
XIAllowEvents.c \
XIGrabDevice.c \
XIQueryVersion.c \
XIQueryDevice.c \
XISetDevFocus.c \
XIGetDevFocus.c \
XIPassiveGrab.c \
XIProperties.c \
XISelEv.c \
XISetCPtr.c \
XIWarpPointer.c \
XIHierarchy.c \
XIDefineCursor.c \
XIQueryPointer.c \
XIBarrier.c \
"

libXi_src_files="\
XAllowDv.c \
XChDProp.c \
XChgDCtl.c \
XChgFCtl.c \
XChgKbd.c \
XChgKMap.c \
XChgPnt.c \
XChgProp.c \
XCloseDev.c \
XDelDProp.c \
XDevBell.c \
XExtToWire.c \
XGetBMap.c \
XGetCPtr.c \
XGetDCtl.c \
XGetDProp.c \
XGetFCtl.c \
XGetKMap.c \
XGetMMap.c \
XGetProp.c \
XGetVers.c \
XGMotion.c \
XGrabDev.c \
XGrDvBut.c \
XGrDvKey.c \
XGtFocus.c \
XGtSelect.c \
XListDev.c \
XListDProp.c \
XOpenDev.c \
XQueryDv.c \
XSelect.c \
XSetBMap.c \
XSetDVal.c \
XSetMMap.c \
XSetMode.c \
XSndExEv.c \
XStFocus.c \
XUngrDev.c \
XUngrDvB.c \
XUngrDvK.c \
XExtInt.c  \
$XI2_src_files \
"
#---------------------------------------------------------------------------------------------------
# TODO: we are still using the compiler driver, bad
for f in $libXi_src_files
do
	o=$(basename $f .c).o
	os="$os $o"

	printf "CC $f->$o\n"
	cc -c $pkg_dir/src/$f -o $build_dir/$o \
		-I$pkg_dir/include &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
# TODO: we are still using the compiler driver, bad
# no version file and it expects all symbols to end up in dynamic table, aka must not use default
# hidden visibility
printf "CCLD libXi.so.6.1.0\n"
cc -o $build_dir/libXi.so.6.1.0 \
	-shared -Wl,--soname=libXi.so.6 -Wl,-s -Wl,-no-undefined \
	$os \
	-lX11 \
	-lXext
#---------------------------------------------------------------------------------------------------
rm -Rf /nyan/$src_name/$slot/lib
mkdir /nyan/$src_name/$slot/lib

rm -Rf /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/include/X11

cp -r $pkg_dir/include/X11/extensions /nyan/$src_name/$slot/include/X11/extensions
cp $build_dir/libXi.so.6.1.0 /nyan/$src_name/$slot/lib

ln -s libXi.so.6.1.0 /nyan/$src_name/$slot/lib/libXi.so
ln -s libXi.so.6.1.0 /nyan/$src_name/$slot/lib/libXi.so.6
#---------------------------------------------------------------------------------------------------
export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
