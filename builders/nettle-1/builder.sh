src_name=nettle
version=3.4.1
archive_name=$src_name-$version.tar.gz
url0=http://ftpmirror.gnu.org/$src_name/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export 'CC_FOR_BUILD=gcc -O2 -pipe -fPIC -B/nyan/glibc/current/lib'
# need to find gmp
export "CPPFLAGS=-I/nyan/toolchains/current/include"
# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=gcc					\
	-B/nyan/glibc/current/lib		\
	-L/nyan/glibc/current/lib		\
	-L/nyan/toolchains/0/lib		\
	-Wl,-rpath-link,/nyan/glibc/current/lib	\
	-static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--prefix=/nyan/nettle/1		\
	--enable-static			\
	--disable-shared		\
	--disable-documentation		\
	--disable-openssl
unset CFLAGS
unset CC
unset CPPFLAGS
unset CC_FOR_BUILD

make -j $threads_n
make install

# cleanup and tidying
strip -s /nyan/$src_name/1/bin/* || true

rm -Rf $build_dir $src_dir
