src_name=libvpx
version=1.7.0
archive_name=$src_name-$version.tar.gz
url0=https://github.com/webmproject/$src_name/archive/v$version.tar.gz

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

cd $src_dir

# fix usual perl diareha
sed -i 's:#!/usr/bin/env perl:#!/nyan/perl/current/bin/perl:' ./build/make/ads2gas.pl
sed -i 's:#!/usr/bin/env perl:#!/nyan/perl/current/bin/perl:' ./build/make/ads2gas_apple.pl
sed -i 's:#!/usr/bin/env perl:#!/nyan/perl/current/bin/perl:' ./build/make/ads2armasm_ms.pl
sed -i 's:#!/usr/bin/env perl:#!/nyan/perl/current/bin/perl:' ./build/make/rtcd.pl
sed -i 's:#!/usr/bin/env perl:#!/nyan/perl/current/bin/perl:' ./build/make/thumb.pm

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
PATH=/nyan/perl/current/bin:$PATH

export 'CFLAGS=-O2 -pipe -fPIC -static-libgcc'
export 'LDFLAGS=-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc'
$src_dir/configure			\
	--prefix=/nyan/libvpx/0		\
	--target=x86_64-linux-gcc	\
	--disable-debug			\
	--disable-gprof			\
	--disable-gcov			\
	--disable-install-docs		\
	--disable-install-bins		\
	--enable-install-libs		\
	--disable-install-srcs		\
	--disable-examples		\
	--disable-tools			\
	--disable-docs			\
	--disable-unit-tests		\
	--disable-codec-srcs		\
	--disable-debug-libs		\
	--enable-libs			\
	--as=nasm			\
	--enable-vp9-highbitdepth	\
	--disable-internal-stats	\
	--enable-postproc		\
	--enable-vp9-postproc		\
	--enable-multi-res-encoding	\
	--disable-webm-io		\
	--disable-libyuv		\
	--enable-static			\
	--disable-shared		\
	--disable-small			\
	--enable-vp8-encoder		\
	--disable-vp8-decoder		\
	--enable-vp9-encoder		\
	--disable-vp9-decoder
unset LDFLAGS
unset CFLAGS

make -j $threads_n
make install

export PATH=$OLD_PATH

rm -Rf $build_dir $src_dir
