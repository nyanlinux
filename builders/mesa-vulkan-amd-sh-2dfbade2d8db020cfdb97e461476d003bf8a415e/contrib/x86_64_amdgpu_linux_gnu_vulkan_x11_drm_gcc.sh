#!/bin/sh

# Canonical specialized build scripts for AMD hardware on gnu/linux distros.
# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the script at the top of mesa source tree, create somewhere else
# a build directory, cd into it, and call from there this script.
#===============================================================================
# build dir and src dir
build_dir=$(readlink -f .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(readlink -f $(dirname $0)/..)
echo "src_dir=$src_dir"
#===============================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===============================================================================
if test "${sysconfdir-unset}" = unset; then
sysconfdir='/etc'
fi

if test "${datadir-unset}" = unset; then
datadir='/usr/share'
fi
#===============================================================================
# when we perform tasks in //, use "roughly" this maximum value
if test "${tasks_n_max-unset}" = unset; then
tasks_n_max=8
fi
#===============================================================================
if test "${version-unset}" = unset; then
	if test -f $src_dir/VERSION; then
		version=$(cat $src_dir/VERSION)
	else
		version=99.99.99-devel
	fi
fi
#===============================================================================
# python/perl/ruby/javascript/lua/etc whatever...
if test "${python3-unset}" = unset; then
python3=/nyan/python/current/bin/python3
fi

if test "${mako-unset}" = unset; then
mako=/nyan/mako/current
fi

if test "${yaml-unset}" = unset; then
yaml=/nyan/PyYAML/current
fi
#===============================================================================
# all symbols are tagged hidden by default, then only the public symbol
# will be tagged public explicitely in the code (with extensions to C)
# (for the gl gallium dri driver, it's a gnu ld version script)
if test "${cc-unset}" = unset; then
cc="gcc -std=c99 -pipe -fPIC -fpic -O2 -c -static-libgcc -fvisibility=hidden \
-fno-math-errno -fno-trapping-math \
-ftls-model=global-dynamic \
-nostdinc \
-isystem /opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include \
-isystem /opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include-fixed \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include"
fi

if test "${cxx-unset}" = unset; then
cxx="g++ -pipe -fPIC -O2 -fpic -c -static-libgcc -static-libstdc++  \
-fno-math-errno -fno-trapping-math \
-ftls-model=global-dynamic \
-fno-rtti -fvisibility=hidden -std=c++17 \
-nostdinc \
-isystem /opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include \
-isystem /opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include-fixed \
-isystem /opt/toolchains/x64/elf/binutils-gcc/current/include/c++/13.2.0 \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include"
fi

if test "${as-unset}" = unset; then
as=as
fi

if test "${ar-unset}" = unset; then
ar='ar rcs'
fi

# XXX:All definitions are mesa specific, except when explicitely defined
# otherwise
# _GNU_SOURCE:
#	glibc specific to enable all glibc extension:
#	- HAVE_DL_ITERATE_PHDR is a GNU extension
# NDEBUG:
#	turn off the debug code paths
# HAVE_NO_AUTOCONF:
#	enable some definitions which were generated only by the GNU autotools
#	in include/c99_compat.h, many gcc builtins and posix.
#	that some macros defined there are used without the inclusion of
#       c99_compat.h, hence must be manually provided
# USE_GCC_ATOMIC_BUILTINS
#	enable the use of gcc built-ins for atomic operations in
#	src/util/u_atomic.h
# HAVE_DLADDR:
#	glibc dladdr mandatory for disk cache in utils
# HAVE_STRTOF:
#	glibc provides strtof function, used in src/util/strtod.c
# HAVE_TIMESPEC_GET:
#	glibc provides the timespec get function, used in threading
# HAVE_STRTOD_L:
#	glibc provides strtod_l function, used in src/util/strtod.c
# HAVE_DL_ITERATE_PHDR:
#	glibc provides the GNU extension dl_interate_phdr support with the
#	_GNU_SOURCE feature test macro, used only in src/util/build_id.[hc]
# HAVE_DLFCN_H:
#	autoconf macro switch for dlfcn.h, used for shader disk cache 
#	timestamp
# MAJOR_IN_SYSMACROS=1, is now used in vulkan
# HAVE_LINUX_FUTEX_H:
#	enable the linux futex syscall support code (using the glibc syscall
#	function in many src/util headers
# HAVE_DRI3_MODIFIERS:
#	mesa vulkan on x11 depends on dri3, this enable latest dri3 features
# ENABLE_SHADER_CACHE:
#	do enable the shader cache in the src/util/disk_cache.[hc]
# HAVE_FUNC_ATTRIBUTE_CONST=1:
#	enable gcc attribute in src/util/macros.h
# HAVE_FUNC_ATTRIBUTE_MALLOC=1:
#	enable gcc attribute in src/util/macros.h
# HAVE_FUNC_ATTRIBUTE_PURE=1:
#	enable gcc attribute in src/util/macros.h (and intel compiler)
# HAVE_FUNC_ATTRIBUTE_RETURNS_NONNULL=1:
#	enable gcc attribute in src/util/macros.h
# HAVE_FUNC_ATTRIBUTE_WARN_UNUSED_RESULT=1:
#	enable gcc attribute in src/util/macros.h
# HAVE_FUNC_ATTRIBUTE_WEAK=1:
#	enable gcc attribute for atomic in src/util.c and thread emulation
# HAVE_ENDIAN_H=1:
#	autoconf macro for the endian.h header used in src/util/u_endian.h
# HAVE_FLOCK=1:
#	autoconf macro for src/util/disk_cache_os.c
# VK_USE_PLATFORM_XCB_KHR:
#	vulkan macro enabling x11/xcb platform support, linked to
#	VK_USE_PLATFORM_XLIB_KHR in the code
# VK_USE_PLATFORM_XLIB_KHR:
#	vulkan macro enabling x11/xlib platform support, linked to
#	VK_USE_PLATFORM_XCB_KHR in the code
# VK_USE_PLATFORM_XLIB_XRANDR_EXT:
#	xrandr stuff for vulkan
#
# XXX: removed macros which seem not to be related
# - HAVE_X11_PLATFORM: gallium video layer (vl), and egl
# - HAVE_SURFACELESS_PLATFORM: egl, egl dri2
# - HAVE_DRM_PLATFORM: egl, egl dri2
# - HAVE_DRI3: egl dri2, glx, gallium video layer
# - STDC_HEADERS=1 autoconf macro switch for standard C headers, used nowhere
# - HAVE_SYS_TYPES_H=1 autoconf macro switch for sys/types.h header, used
#   nowhere
# - HAVE_SYS_STAT_H=1 autoconf macro switch for sys/stat.h, used nowhere
# - HAVE_STDLIB_H=1 autoconf macro switch for stdlib.h, used nowhere
# - HAVE_STRING_H=1 autoconf macro switch for string.h, used nowhere
# - HAVE_MEMORY_H=1 autoconf macro switch for memory.h, used nowhere
# - HAVE_STRINGS_H=1 autoconf macro switch for strings.h, used nowhere
# - HAVE_INTTYPES_H=1 autoconf macro switch for inttypes.h, used nowhere
# - HAVE_STDINT_H=1 autoconf macro switch for stdint.h, used in the
#   old svga gallium driver
# - HAVE_UNISTD_H=1 autoconf macro switch for unistd.h, used in the intel
#   program lexer
# - HAVE_FUNC_ATTRIBUTE_ALIAS=1, usually enable with the HAVE_NO_AUTOCONF
#   but seems to be used directly in glx and glapi
# - HAVE_FUNC_ATTRIBUTE_VISIBILITY=1, enable the gcc visibilty attribute
#   only in mapi entry headers
# - HAVE_CLOCK_GETTIME=1, autoconf macro switch, used nowhere
# - HAVE_PTHREAD_PRIO_INHERIT=1, m4 defined macro, used nowhere
# - USE_SSE41, enable ssee41 code paths in dri intel 965, and mesa vbo
# - USE_X86_64_ASM, enable x86_64 code paths in varios mesa parts, no vulkan
#   parts
# - HAVE_SYS_SYSCTL_H autoconf macro for sys/sysctl.h header, used in the dri
#   software rasterizer
# - HAVE_MKOSTEMP autoconf macro, used only in egl dri2 wayland platform
# - HAVE_MEMFD_CREATE: enable the linux memfd syscall support code (intel vulkan
#   only) usingt the glibc syscall function

gcc_builtins_cppflags="\
-DHAVE___BUILTIN_BSWAP32=1 \
-DHAVE___BUILTIN_BSWAP64=1 \
-DHAVE___BUILTIN_CLZ=1 \
-DHAVE___BUILTIN_CLZLL=1 \
-DHAVE___BUILTIN_CTZ=1 \
-DHAVE___BUILTIN_EXPECT=1 \
-DHAVE___BUILTIN_FFS=1 \
-DHAVE___BUILTIN_FFSLL=1 \
-DHAVE___BUILTIN_POPCOUNT=1 \
-DHAVE___BUILTIN_POPCOUNTLL=1 \
-DHAVE___BUILTIN_UNREACHABLE=1 \
-DUSE_GCC_ATOMIC_BUILTINS=1 \
"

gcc_attributes_cppflags="\
-DHAVE_FUNC_ATTRIBUTE_CONST=1 \
-DHAVE_FUNC_ATTRIBUTE_FLATTEN=1 \
-DHAVE_FUNC_ATTRIBUTE_MALLOC=1 \
-DHAVE_FUNC_ATTRIBUTE_PURE=1 \
-DHAVE_FUNC_ATTRIBUTE_UNUSED=1 \
-DHAVE_FUNC_ATTRIBUTE_WARN_UNUSED_RESULT=1 \
-DHAVE_FUNC_ATTRIBUTE_WEAK=1 \
\
-DHAVE_FUNC_ATTRIBUTE_FORMAT=1 \
-DHAVE_FUNC_ATTRIBUTE_PACKED=1 \
-DHAVE_FUNC_ATTRIBUTE_RETURNS_NONNULL=1 \
-DHAVE_FUNC_ATTRIBUTE_VISIBILITY=1 \
-DHAVE_FUNC_ATTRIBUTE_ALIAS=1 \
-DHAVE_FUNC_ATTRIBUTE_NORETURN=1 \
-DHAVE_FUNC_ATTRIBUTE_UINT128=1 \
"

linux_glibc_cppflags="\
-D_GNU_SOURCE=1 \
-DHAVE_PTHREAD \
-DHAVE_PTHREAD_SETAFFINITY \
-DHAVE_POSIX_MEMALIGN \
-DHAVE_STRTOF \
-DHAVE_TIMESPEC_GET \
-DHAVE_STRTOD_L \
-DHAVE_DLFCN_H \
-DHAVE_DL_ITERATE_PHDR \
-DHAVE_LINUX_FUTEX_H \
-DHAVE_ENDIAN_H=1 \
-DHAVE_PROGRAM_INVOCATION_NAME=1 \
-DHAVE_DLADDR=1 \
-DHAVE_FLOCK=1 \
-DMAJOR_IN_SYSMACROS=1 \
-DHAVE_TIMESPEC_GET=1 \
-DHAVE_STRUCT_TIMESPEC=1 \
-DHAVE_SECURE_GETENV=1 \
-DALLOW_KCMP \
"
mesa_cppflags="\
-DNDEBUG \
-DHAVE_X11_DRM \
-DHAVE_DRI3_MODIFIERS \
-DHAVE_DRI3_EXPLICIT_SYNC \
-DENABLE_SHADER_CACHE \
-DHAVE_COMPRESSION \
"

mesa_vulkan_cppflags="\
-DVK_USE_PLATFORM_DISPLAY_KHR \
-DVK_USE_PLATFORM_XCB_KHR \
-DVK_USE_PLATFORM_XLIB_KHR \
-DVK_USE_PLATFORM_XLIB_XRANDR_EXT \
\
-DVIDEO_CODEC_AV1DEC=0 \
-DVIDEO_CODEC_H265DEC=0 \
-DVIDEO_CODEC_H264DEC=0 \
-DVIDEO_CODEC_AV1ENC=0 \
-DVIDEO_CODEC_H265ENC=0 \
-DVIDEO_CODEC_H264ENC=0 \
\
-DUSE_VK_COMPILER=1 \
"

cppflags_common="\
$gcc_builtins_cppflags \
$gcc_attributes_cppflags \
$linux_glibc_cppflags \
$mesa_cppflags \
$mesa_vulkan_cppflags \
"
#===============================================================================
# expat
if test "${expat_cppflags-unset}" = unset; then
expat_cppflags='-I/nyan/expat/current/include'
fi
if test "${expat_archives-unset}" = unset; then
expat_archives=libexpat.a
fi
if test "${expat_ldflags-unset}" = unset; then
expat_ldflags="/nyan/expat/current/lib/$expat_archives"
fi
#===============================================================================
if test "${libxau_cppflags-unset}" = unset; then
libxau_cppflags='-I/nyan/libXau/current/include'
fi
#===============================================================================
if test "${libxcb_cppflags-unset}" = unset; then
libxcb_cppflags='-I/nyan/libxcb/current/include'
fi

if test "${libxcb_ldflags-unset}" = unset; then
libxcb_ldflags="\
/nyan/libxcb/current/lib/libxcb-xtest.so \
/nyan/libxcb/current/lib/libxcb-damage.so \
/nyan/libxcb/current/lib/libxcb-xfixes.so \
/nyan/libxcb/current/lib/libxcb-xvmc.so \
/nyan/libxcb/current/lib/libxcb-xinerama.so \
/nyan/libxcb/current/lib/libxcb-dri2.so \
/nyan/libxcb/current/lib/libxcb-composite.so \
/nyan/libxcb/current/lib/libxcb-dpms.so \
/nyan/libxcb/current/lib/libxcb-sync.so \
/nyan/libxcb/current/lib/libxcb-randr.so \
/nyan/libxcb/current/lib/libxcb-res.so \
/nyan/libxcb/current/lib/libxcb.so \
/nyan/libxcb/current/lib/libxcb-screensaver.so \
/nyan/libxcb/current/lib/libxcb-xkb.so \
/nyan/libxcb/current/lib/libxcb-xv.so \
/nyan/libxcb/current/lib/libxcb-render.so \
/nyan/libxcb/current/lib/libxcb-shm.so \
/nyan/libxcb/current/lib/libxcb-dri3.so \
/nyan/libxcb/current/lib/libxcb-record.so \
/nyan/libxcb/current/lib/libxcb-xinput.so \
/nyan/libxcb/current/lib/libxcb-present.so \
/nyan/libxcb/current/lib/libxcb-glx.so \
/nyan/libxcb/current/lib/libxcb-shape.so \
/nyan/libxcb/current/lib/libxcb-xf86dri.so \
"
fi
#===============================================================================
if test "${libxrender_cppflags-unset}" = unset; then
libxrender_cppflags='-I/nyan/libXrender/current/include'
fi

if test "${libxrender_ldflags-unset}" = unset; then
libxrender_ldflags="/nyan/libXrender/current/lib/libXrender.so"
fi
#===============================================================================
if test "${libxrandr_cppflags-unset}" = unset; then
libxrandr_cppflags='-I/nyan/libXrandr/current/include'
fi

if test "${libxrandr_ldflags-unset}" = unset; then
libxrandr_ldflags="/nyan/libXrandr/current/lib/libXrandr.so"
fi
#===============================================================================
if test "${libx11_cppflags-unset}" = unset; then
libx11_cppflags='-I/nyan/libX11/current/include'
fi

if test "${libx11_ldflags-unset}" = unset; then
libx11_ldflags="\
/nyan/libX11/current/lib/libX11.so \
/nyan/libX11/current/lib/libX11-xcb.so \
"
fi
#===============================================================================
if test "${libxshmfence_cppflags-unset}" = unset; then
libxshmfence_cppflags='-I/nyan/libxshmfence/current/include'
fi

if test "${libxshmfence_ldflags-unset}" = unset; then
libxshmfence_ldflags='/nyan/libxshmfence/current/lib/libxshmfence.so'
fi
#===============================================================================
if test "${xorgproto_cppflags-unset}" = unset; then
xorgproto_cppflags='-I/nyan/xorgproto/current/include'
fi
#===============================================================================
if test "${libdrm_cppflags-unset}" = unset; then
libdrm_cppflags='-I/nyan/drm/current/include/libdrm -I/nyan/drm/current/include -DHAVE_LIBDRM'
fi

if test "${libdrm_ldflags-unset}" = unset; then
libdrm_ldflags='/nyan/drm/current/lib/libdrm.so'
fi
#===============================================================================
if test "${libdrm_amdgpu_cppflags-unset}" = unset; then
libdrm_amdgpu_cppflags='-I/nyan/drm/current/include/libdrm'
fi

if test "${libdrm_amdgpu_ldflags-unset}" = unset; then
libdrm_amdgpu_ldflags='/nyan/drm/current/lib/libdrm_amdgpu.so'
fi
#===============================================================================
if test "${linux_drm_cppflags-unset}" = unset; then
linux_drm_cppflags='-I/nyan/glibc/current/include-linux/drm'
fi
#===============================================================================
if test "${libelf_cppflags-unset}" = unset; then
libelf_cppflags="-I/nyan/libelf/current/include/libelf \
-I/nyan/libelf/current/include"
fi

if test "${libelf_archives-unset}" = unset; then
libelf_archives=libelf.a
fi

if test "${libelf_ldflags-unset}" = unset; then
libelf_ldflags="/nyan/libelf/current/lib/$libelf_archives"
fi
#===============================================================================
if test "${zlib_cppflags-unset}" = unset; then
zlib_cppflags='-I/nyan/zlib/current/include -DHAVE_ZLIB'
fi

if test "${zlib_archives-unset}" = unset; then
zlib_archives=libz.a
fi

if test "${zlib_ldflags-unset}" = unset; then
zlib_ldflags="/nyan/zlib/current/lib/$zlib_archives"
fi
#===============================================================================
# build system compiler (the one used to compile the build system python), _not_
# the target compiler if you cross compile
if test "${build_cc-unset}" = unset; then
build_cc="gcc -pipe -O2 -c -I/nyan/glibc/current/include-linux"
fi
if test "${build_ccld-unset}" = unset; then
build_ccld="gcc -pipe -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib"
fi
#===============================================================================
# the kronos registry
vulkan_api_xml=$src_dir/src/vulkan/registry/vk.xml
#===============================================================================
# directory hierarchy
build_dir_leaf_dirs="\
$build_dir/include/drm-api \
$build_dir/src/amd/common/nir \
$build_dir/src/amd/compiler \
$build_dir/src/amd/vulkan/meta \
$build_dir/src/amd/vulkan/nir \
$build_dir/src/amd/vulkan/layers \
$build_dir/src/compiler/nir \
$build_dir/src/compiler/glsl \
$build_dir/src/compiler/spirv \
$build_dir/src/gallium/auxiliary \
$build_dir/src/gallium/include \
$build_dir/src/mesa \
$build_dir/src/util/format \
$build_dir/src/vulkan/runtime \
$build_dir/src/vulkan/util \
$build_dir/src/vulkan/wsi \
"
mkdir -p $build_dir_leaf_dirs
#===============================================================================
# all code generation should happen here (no weird interlocked deps)
. $src_dir/contrib/generators.sh
wait
#===============================================================================
# object put in libvulkan_amd_common
$cc -o $build_dir/xmlconfig.o $src_dir/src/util/xmlconfig.c \
	-DSYSCONFDIR=\"$sysconfdir\" \
	-DDATADIR=\"$datadir\" \
	\
	-I$build_dir/src/util \
	-I$src_dir/src/util \
	-I$build_dir/src \
	-I$src_dir/src \
	-I$build_dir/include \
	-I$src_dir/include \
	$cppflags_common \
	$expat_cppflags &
#===============================================================================
# vulkan util
cppflags="\
$cppflags_common \
-DPACKAGE_VERSION=\"$version\" \
-DVERSION=\"$version\" \
-I$build_dir/src/vulkan/runtime \
-I$src_dir/src/vulkan/runtime \
-I$build_dir/src/vulkan/util \
-I$src_dir/src/vulkan/util \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src/compiler/nir \
-I$src_dir/src/compiler/nir \
-I$build_dir/src/compiler \
-I$src_dir/src/compiler \
-I$build_dir/src/util \
-I$src_dir/src/util \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include \
-I$src_dir/include \
$libxcb_cppflags \
$libx11_cppflags \
$libxrandr_cppflags \
$libxrender_cppflags \
$xorgproto_cppflags \
"
libvulkan_util_files="\
$src_dir/src/vulkan/util/vk_alloc.c \
$build_dir/src/vulkan/util/vk_dispatch_table.c \
$build_dir/src/vulkan/util/vk_enum_to_str.c \
$build_dir/src/vulkan/util/vk_extensions.c \
$src_dir/src/vulkan/util/vk_format.c \
$src_dir/src/vulkan/util/vk_util.c \
"
for f in $libvulkan_util_files
do
	libvulkan_util_obj=$build_dir/$(basename $f .c).o
	libvulkan_util_a="$libvulkan_util_a $libvulkan_util_obj"

	$cc $cppflags $f -o $libvulkan_util_obj &
done
#===============================================================================
# vulkan runtime
cppflags="\
$cppflags_common \
-DPACKAGE_VERSION=\"$version\" \
-DVERSION=\"$version\" \
-I$build_dir/src/vulkan/runtime/bvh \
-I$src_dir/src/vulkan/runtime/bvh \
-I$build_dir/src/vulkan/runtime \
-I$src_dir/src/vulkan/runtime \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src/compiler/nir \
-I$src_dir/src/compiler/nir \
-I$build_dir/src/compiler \
-I$src_dir/src/compiler \
-I$build_dir/src/vulkan/util \
-I$src_dir/src/vulkan/util \
-I$build_dir/src/util \
-I$src_dir/src/util \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include \
-I$src_dir/include \
$libdrm_cppflags \
$libxcb_cppflags \
$libx11_cppflags \
$libxrandr_cppflags \
$libxrender_cppflags \
$xorgproto_cppflags \
"
libvulkan_runtime_files="\
$src_dir/src/vulkan/runtime/vk_blend.c \
$src_dir/src/vulkan/runtime/vk_buffer.c \
$src_dir/src/vulkan/runtime/vk_buffer_view.c \
$src_dir/src/vulkan/runtime/vk_cmd_copy.c \
$src_dir/src/vulkan/runtime/vk_cmd_enqueue.c \
$build_dir/src/vulkan/runtime/vk_cmd_enqueue_entrypoints.c \
$build_dir/src/vulkan/runtime/vk_cmd_queue.c \
$src_dir/src/vulkan/runtime/vk_command_buffer.c \
$src_dir/src/vulkan/runtime/vk_command_pool.c \
$build_dir/src/vulkan/runtime/vk_common_entrypoints.c \
$src_dir/src/vulkan/runtime/vk_debug_report.c \
$src_dir/src/vulkan/runtime/vk_debug_utils.c \
$src_dir/src/vulkan/runtime/vk_deferred_operation.c \
$src_dir/src/vulkan/runtime/vk_device_memory.c \
$src_dir/src/vulkan/runtime/vk_device_generated_commands.c \
$src_dir/src/vulkan/runtime/vk_descriptor_set_layout.c \
$src_dir/src/vulkan/runtime/vk_descriptor_update_template.c \
$src_dir/src/vulkan/runtime/vk_descriptors.c \
$src_dir/src/vulkan/runtime/vk_device.c \
$src_dir/src/vulkan/runtime/vk_drm_syncobj.c \
$build_dir/src/vulkan/runtime/vk_dispatch_trampolines.c \
$src_dir/src/vulkan/runtime/vk_fence.c \
$build_dir/src/vulkan/runtime/vk_format_info.c \
$src_dir/src/vulkan/runtime/vk_framebuffer.c \
$src_dir/src/vulkan/runtime/vk_graphics_state.c \
$src_dir/src/vulkan/runtime/vk_image.c \
$src_dir/src/vulkan/runtime/vk_instance.c \
$src_dir/src/vulkan/runtime/vk_log.c \
$src_dir/src/vulkan/runtime/vk_meta.c \
$src_dir/src/vulkan/runtime/vk_meta_blit_resolve.c \
$src_dir/src/vulkan/runtime/vk_meta_clear.c \
$src_dir/src/vulkan/runtime/vk_meta_copy_fill_update.c \
$src_dir/src/vulkan/runtime/vk_meta_draw_rects.c \
$src_dir/src/vulkan/runtime/vk_meta_object_list.c \
$src_dir/src/vulkan/runtime/vk_nir.c \
$src_dir/src/vulkan/runtime/vk_nir_convert_ycbcr.c \
$src_dir/src/vulkan/runtime/vk_object.c \
$src_dir/src/vulkan/runtime/vk_pipeline.c \
$src_dir/src/vulkan/runtime/vk_pipeline_layout.c \
$src_dir/src/vulkan/runtime/vk_pipeline_cache.c \
$src_dir/src/vulkan/runtime/vk_physical_device.c \
$build_dir/src/vulkan/runtime/vk_physical_device_features.c \
$build_dir/src/vulkan/runtime/vk_physical_device_properties.c \
$build_dir/src/vulkan/runtime/vk_physical_device_spirv_caps.c \
$src_dir/src/vulkan/runtime/vk_query_pool.c \
$src_dir/src/vulkan/runtime/vk_queue.c \
$src_dir/src/vulkan/runtime/vk_render_pass.c \
$src_dir/src/vulkan/runtime/vk_sampler.c \
$src_dir/src/vulkan/runtime/vk_semaphore.c \
$src_dir/src/vulkan/runtime/vk_shader.c \
$src_dir/src/vulkan/runtime/vk_shader_module.c \
$src_dir/src/vulkan/runtime/vk_standard_sample_locations.c \
$src_dir/src/vulkan/runtime/vk_sync.c \
$src_dir/src/vulkan/runtime/vk_sync_binary.c \
$src_dir/src/vulkan/runtime/vk_sync_dummy.c \
$src_dir/src/vulkan/runtime/vk_sync_timeline.c \
$build_dir/src/vulkan/runtime/vk_synchronization_helpers.c \
$src_dir/src/vulkan/runtime/vk_synchronization.c \
$src_dir/src/vulkan/runtime/vk_texcompress_etc2.c \
$src_dir/src/vulkan/runtime/vk_video.c \
$src_dir/src/vulkan/runtime/vk_ycbcr_conversion.c \
"
for f in $libvulkan_runtime_files
do
	libvulkan_runtime_obj=$build_dir/$(basename $f .c).o
	libvulkan_runtime_a="$libvulkan_runtime_a $libvulkan_runtime_obj"

	$cc $cppflags $f -o $libvulkan_runtime_obj &
done
#===============================================================================
# wsi x11 (Window System Interface)
cppflags="\
$cppflags_common \
-I$build_dir/src/vulkan/wsi \
-I$src_dir/src/vulkan/wsi \
-I$build_dir/src/vulkan/runtime \
-I$src_dir/src/vulkan/runtime \
-I$build_dir/src/vulkan/util \
-I$src_dir/src/vulkan/util \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include/drm-uapi \
-I$src_dir/include/drm-uapi \
-I$build_dir/include \
-I$src_dir/include \
$libxcb_cppflags \
$libx11_cppflags \
$libxrandr_cppflags \
$libxrender_cppflags \
$xorgproto_cppflags \
$libxshmfence_cppflags \
$libdrm_cppflags \
"
$cc $cppflags $src_dir/src/vulkan/wsi/wsi_common.c \
	-o $build_dir/wsi_common.o &
$cc $cppflags $src_dir/src/vulkan/wsi/wsi_common_drm.c \
	-o $build_dir/wsi_common_drm.o &
$cc $cppflags $src_dir/src/vulkan/wsi/wsi_common_x11.c \
	-o $build_dir/wsi_common_x11.o &
$cc $cppflags $src_dir/src/vulkan/wsi/wsi_common_display.c \
	-o $build_dir/wsi_common_display.o &
$cc $cppflags $src_dir/src/vulkan/wsi/wsi_common_headless.c \
	-o $build_dir/wsi_common_headless.o &
$cc $cppflags $build_dir/src/vulkan/wsi/wsi_common_entrypoints.c \
	-o $build_dir/wsi_common_entrypoints.o &
libvulkan_wsi_a="\
$build_dir/wsi_common.o \
$build_dir/wsi_common_drm.o \
$build_dir/wsi_common_x11.o \
$build_dir/wsi_common_display.o \
$build_dir/wsi_common_headless.o \
$build_dir/wsi_common_entrypoints.o \
"
#===============================================================================
# amd common archive
cppflags="\
$cppflags_common \
-I$build_dir/src/gallium/auxiliary \
-I$src_dir/src/gallium/auxiliary \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src/amd/common/nir \
-I$src_dir/src/amd/common/nir \
-I$build_dir/src/amd/common \
-I$src_dir/src/amd/common \
-I$build_dir/src/amd \
-I$src_dir/src/amd \
-I$build_dir/src/compiler/nir \
-I$src_dir/src/compiler/nir \
-I$build_dir/src/compiler \
-I$src_dir/src/compiler \
-I$build_dir/src/mesa \
-I$src_dir/src/mesa \
-I$build_dir/src/util \
-I$src_dir/src/util \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include/drm-uapi \
-I$src_dir/include/drm-uapi \
-I$build_dir/include \
-I$src_dir/include \
$libdrm_cppflags \
$libdrm_amdgpu_cppflags \
$linux_drm_cppflags \
$libelf_cppflags \
"
#$src_dir/src/amd/common/ac_surface_meta_address_test.c \
#$src_dir/src/amd/common/ac_surface_modifier_test.c \
#$src_dir/src/amd/common/ac_sqtt.c \
libamd_common_c_files="\
$src_dir/src/amd/common/nir/ac_nir.c \
$src_dir/src/amd/common/nir/ac_nir_create_gs_copy_shader.c \
$src_dir/src/amd/common/nir/ac_nir_cull.c \
$src_dir/src/amd/common/nir/ac_nir_lower_esgs_io_to_mem.c \
$src_dir/src/amd/common/nir/ac_nir_lower_global_access.c \
$src_dir/src/amd/common/nir/ac_nir_lower_image_opcodes_cdna.c \
$src_dir/src/amd/common/nir/ac_nir_lower_intrinsics_to_args.c \
$src_dir/src/amd/common/nir/ac_nir_lower_legacy_gs.c \
$src_dir/src/amd/common/nir/ac_nir_lower_legacy_vs.c \
$src_dir/src/amd/common/nir/ac_nir_lower_mem_access_bit_sizes.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ngg.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ngg_gs.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ngg_mesh.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ps_early.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ps_late.c \
$src_dir/src/amd/common/nir/ac_nir_lower_resinfo.c \
$src_dir/src/amd/common/nir/ac_nir_lower_sin_cos.c \
$src_dir/src/amd/common/nir/ac_nir_lower_taskmesh_io_to_mem.c \
$src_dir/src/amd/common/nir/ac_nir_lower_tess_io_to_mem.c \
$src_dir/src/amd/common/nir/ac_nir_lower_tex.c \
$src_dir/src/amd/common/nir/ac_nir_meta_cs_blit.c \
$src_dir/src/amd/common/nir/ac_nir_meta_cs_clear_copy_buffer.c \
$src_dir/src/amd/common/nir/ac_nir_meta_ps_resolve.c \
$src_dir/src/amd/common/nir/ac_nir_opt_outputs.c \
$src_dir/src/amd/common/nir/ac_nir_opt_pack_half.c \
$src_dir/src/amd/common/nir/ac_nir_opt_shared_append.c \
$src_dir/src/amd/common/nir/ac_nir_prerast_utils.c \
$src_dir/src/amd/common/nir/ac_nir_surface.c \
$src_dir/src/amd/common/ac_surface.c \
$src_dir/src/amd/common/ac_binary.c \
$src_dir/src/amd/common/ac_cmdbuf.c \
$src_dir/src/amd/common/ac_debug.c \
$src_dir/src/amd/common/ac_descriptors.c \
$src_dir/src/amd/common/ac_formats.c \
$src_dir/src/amd/common/ac_gpu_info.c \
$src_dir/src/amd/common/ac_msgpack.c \
$src_dir/src/amd/common/ac_linux_drm.c \
$src_dir/src/amd/common/ac_parse_ib.c \
$src_dir/src/amd/common/ac_perfcounter.c \
$src_dir/src/amd/common/ac_pm4.c \
$src_dir/src/amd/common/ac_shader_args.c \
$src_dir/src/amd/common/ac_shader_util.c \
$src_dir/src/amd/common/ac_shadowed_regs.c \
$src_dir/src/amd/common/ac_rgp.c \
$src_dir/src/amd/common/ac_rgp_elf_object_pack.c \
$src_dir/src/amd/common/ac_rtld.c \
$src_dir/src/amd/common/ac_spm.c \
$src_dir/src/amd/common/ac_vcn_dec.c \
$src_dir/src/amd/common/ac_vcn_enc.c \
$src_dir/src/amd/common/amd_family.c \
$build_dir/src/amd/common/gfx10_format_table.c \
"
for f in $libamd_common_c_files
do
	libamd_common_c_obj=$build_dir/$(basename $f .c).o
	libamd_common_a="$libamd_common_a $libamd_common_c_obj"

	$cc $cppflags $f -o $libamd_common_c_obj &
done
#===============================================================================
# addrlib (similar c++ pile of cr*p than llvm, everything c++ is anyway)
cppflags="\
$cppflags_common \
-DLITTLEENDIAN_CPU \
-I$build_dir/src/amd/addrlib/inc/chip/gfx12 \
-I$src_dir/src/amd/addrlib/inc/chip/gfx12 \
-I$build_dir/src/amd/addrlib/inc/chip/gfx11 \
-I$src_dir/src/amd/addrlib/inc/chip/gfx11 \
-I$build_dir/src/amd/addrlib/inc/chip/gfx10 \
-I$src_dir/src/amd/addrlib/inc/chip/gfx10 \
-I$build_dir/src/amd/addrlib/inc/chip/gfx9 \
-I$src_dir/src/amd/addrlib/inc/chip/gfx9 \
-I$build_dir/src/amd/addrlib/inc/chip/r800 \
-I$src_dir/src/amd/addrlib/inc/chip/r800 \
-I$build_dir/src/amd/addrlib/inc \
-I$src_dir/src/amd/addrlib/inc \
-I$build_dir/src/amd/addrlib/src/core \
-I$src_dir/src/amd/addrlib/src/core \
-I$build_dir/src/amd/addrlib/src/chip/gfx12 \
-I$src_dir/src/amd/addrlib/src/chip/gfx12 \
-I$build_dir/src/amd/addrlib/src/chip/gfx11 \
-I$src_dir/src/amd/addrlib/src/chip/gfx11 \
-I$build_dir/src/amd/addrlib/src/chip/gfx10 \
-I$src_dir/src/amd/addrlib/src/chip/gfx10 \
-I$build_dir/src/amd/addrlib/src/chip/gfx9 \
-I$src_dir/src/amd/addrlib/src/chip/gfx9 \
-I$build_dir/src/amd/addrlib/src/chip/r800 \
-I$src_dir/src/amd/addrlib/src/chip/r800 \
-I$build_dir/src/amd/addrlib/src \
-I$src_dir/src/amd/addrlib/src \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include \
-I$src_dir/include \
"
libamdgpu_addrlib_files="\
$src_dir/src/amd/addrlib/src/addrinterface.cpp \
$src_dir/src/amd/addrlib/src/core/addrelemlib.cpp \
$src_dir/src/amd/addrlib/src/core/addrlib.cpp \
$src_dir/src/amd/addrlib/src/core/addrlib1.cpp \
$src_dir/src/amd/addrlib/src/core/addrlib2.cpp \
$src_dir/src/amd/addrlib/src/core/addrlib3.cpp \
$src_dir/src/amd/addrlib/src/core/addrobject.cpp \
$src_dir/src/amd/addrlib/src/core/addrswizzler.cpp \
$src_dir/src/amd/addrlib/src/core/coord.cpp \
$src_dir/src/amd/addrlib/src/gfx10/gfx10addrlib.cpp \
$src_dir/src/amd/addrlib/src/gfx11/gfx11addrlib.cpp \
$src_dir/src/amd/addrlib/src/gfx12/gfx12addrlib.cpp \
$src_dir/src/amd/addrlib/src/gfx9/gfx9addrlib.cpp \
$src_dir/src/amd/addrlib/src/r800/ciaddrlib.cpp \
$src_dir/src/amd/addrlib/src/r800/egbaddrlib.cpp \
$src_dir/src/amd/addrlib/src/r800/siaddrlib.cpp \
"
for f in $libamdgpu_addrlib_files
do
	libamdgpu_addrlib_obj=$build_dir/$(basename $f .c).o
	libamdgpu_addrlib_a="$libamdgpu_addrlib_a $libamdgpu_addrlib_obj"

	$cxx $cppflags $f -o $libamdgpu_addrlib_obj &
done
#===============================================================================
# libcompiler required by libnir
cppflags="\
$cppflags_common \
-I$build_dir/src/compiler \
-I$src_dir/src/compiler \
-I$build_dir/src/gallium/auxiliary \
-I$src_dir/src/gallium/auxiliary \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src/mesa \
-I$src_dir/src/mesa \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include \
-I$src_dir/include \
"
$cc $cppflags $src_dir/src/compiler/glsl_types.c \
	-o $build_dir/glsl_types.o &
$cc $cppflags $src_dir/src/compiler/shader_enums.c \
	-o $build_dir/shader_enums.o &
$cc $cppflags $build_dir/src/compiler/builtin_types.c \
	-o $build_dir/builtin_types.o &
libcompiler_a="\
$build_dir/glsl_types.o \
$build_dir/shader_enums.o \
$build_dir/builtin_types.o \
"
#===============================================================================
# libnir
cppflags="\
$cppflags_common \
-I$build_dir/src/compiler/spirv \
-I$src_dir/src/compiler/spirv \
-I$build_dir/src/compiler/nir \
-I$src_dir/src/compiler/nir \
-I$build_dir/src/compiler \
-I$src_dir/src/compiler \
-I$build_dir/src/gallium/auxiliary \
-I$src_dir/src/gallium/auxiliary \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src/mesa \
-I$src_dir/src/mesa \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include \
-I$src_dir/include \
$xorgproto_cppflags \
$libxcb_cppflags \
$libx11_cppflags \
$libxrender_cppflags \
$libxrandr_cppflags \
"
libnir_files="\
$src_dir/src/compiler/spirv/gl_spirv.c \
$build_dir/src/compiler/spirv/spirv_info.c \
$src_dir/src/compiler/spirv/spirv_to_nir.c \
$src_dir/src/compiler/spirv/vtn_alu.c \
$src_dir/src/compiler/spirv/vtn_amd.c \
$src_dir/src/compiler/spirv/vtn_bindgen2.c \
$src_dir/src/compiler/spirv/vtn_cfg.c \
$src_dir/src/compiler/spirv/vtn_cmat.c \
$src_dir/src/compiler/spirv/vtn_debug.c \
$build_dir/src/compiler/spirv/vtn_gather_types.c \
$src_dir/src/compiler/spirv/vtn_glsl450.c \
$src_dir/contrib/vtn_opencl.c \
$src_dir/src/compiler/spirv/vtn_structured_cfg.c \
$src_dir/src/compiler/spirv/vtn_subgroup.c \
$src_dir/src/compiler/spirv/vtn_variables.c \
\
$build_dir/src/compiler/nir/nir.c \
$src_dir/src/compiler/nir/nir_builder.c \
$src_dir/src/compiler/nir/nir_builtin_builder.c \
$src_dir/src/compiler/nir/nir_clone.c \
$build_dir/src/compiler/nir/nir_constant_expressions.c \
$src_dir/src/compiler/nir/nir_control_flow.c \
$src_dir/src/compiler/nir/nir_deref.c \
$src_dir/src/compiler/nir/nir_divergence_analysis.c \
$src_dir/src/compiler/nir/nir_dominance.c \
$src_dir/src/compiler/nir/nir_fixup_is_exported.c \
$src_dir/src/compiler/nir/nir_format_convert.c \
$src_dir/src/compiler/nir/nir_from_ssa.c \
$src_dir/src/compiler/nir/nir_gather_info.c \
$src_dir/src/compiler/nir/nir_gather_tcs_info.c \
$src_dir/src/compiler/nir/nir_gather_types.c \
$src_dir/src/compiler/nir/nir_gather_xfb_info.c \
$src_dir/src/compiler/nir/nir_group_loads.c \
$src_dir/src/compiler/nir/nir_gs_count_vertices.c \
$src_dir/src/compiler/nir/nir_functions.c \
$src_dir/src/compiler/nir/nir_inline_uniforms.c \
$src_dir/src/compiler/nir/nir_instr_set.c \
$build_dir/src/compiler/nir/nir_intrinsics.c \
$src_dir/src/compiler/nir/nir_legacy.c \
$src_dir/src/compiler/nir/nir_linking_helpers.c \
$src_dir/src/compiler/nir/nir_liveness.c \
$src_dir/src/compiler/nir/nir_loop_analyze.c \
$src_dir/src/compiler/nir/nir_lower_alpha_test.c \
$src_dir/src/compiler/nir/nir_lower_alu.c \
$src_dir/src/compiler/nir/nir_lower_alu_width.c \
$src_dir/src/compiler/nir/nir_lower_amul.c \
$src_dir/src/compiler/nir/nir_lower_array_deref_of_vec.c \
$src_dir/src/compiler/nir/nir_lower_atomics.c \
$src_dir/src/compiler/nir/nir_lower_atomics_to_ssbo.c \
$src_dir/src/compiler/nir/nir_lower_bitmap.c \
$src_dir/src/compiler/nir/nir_lower_bit_size.c \
$src_dir/src/compiler/nir/nir_lower_blend.c \
$src_dir/src/compiler/nir/nir_lower_bool_to_int32.c \
$src_dir/src/compiler/nir/nir_lower_bool_to_bitsize.c \
$src_dir/src/compiler/nir/nir_lower_calls_to_builtins.c \
$src_dir/src/compiler/nir/nir_lower_clamp_color_outputs.c \
$src_dir/src/compiler/nir/nir_lower_cl_images.c \
$src_dir/src/compiler/nir/nir_lower_clip.c \
$src_dir/src/compiler/nir/nir_lower_clip_cull_distance_arrays.c \
$src_dir/src/compiler/nir/nir_lower_clip_disable.c \
$src_dir/src/compiler/nir/nir_lower_convert_alu_types.c \
$src_dir/src/compiler/nir/nir_lower_const_arrays_to_uniforms.c \
$src_dir/src/compiler/nir/nir_lower_continue_constructs.c \
$src_dir/src/compiler/nir/nir_lower_discard_if.c \
$src_dir/src/compiler/nir/nir_lower_double_ops.c \
$src_dir/src/compiler/nir/nir_lower_drawpixels.c \
$src_dir/src/compiler/nir/nir_lower_fb_read.c \
$src_dir/src/compiler/nir/nir_lower_flatshade.c \
$src_dir/src/compiler/nir/nir_lower_flrp.c \
$src_dir/src/compiler/nir/nir_lower_fp16_conv.c \
$src_dir/src/compiler/nir/nir_lower_frag_coord_to_pixel_coord.c \
$src_dir/src/compiler/nir/nir_lower_fragcolor.c \
$src_dir/src/compiler/nir/nir_lower_fragcoord_wtrans.c \
$src_dir/src/compiler/nir/nir_lower_frexp.c \
$src_dir/src/compiler/nir/nir_lower_global_vars_to_local.c \
$src_dir/src/compiler/nir/nir_lower_gs_intrinsics.c \
$src_dir/src/compiler/nir/nir_lower_goto_ifs.c \
$src_dir/src/compiler/nir/nir_lower_helper_writes.c \
$src_dir/src/compiler/nir/nir_lower_idiv.c \
$src_dir/src/compiler/nir/nir_lower_image.c \
$src_dir/src/compiler/nir/nir_lower_image_atomics_to_global.c \
$src_dir/src/compiler/nir/nir_lower_indirect_derefs.c \
$src_dir/src/compiler/nir/nir_lower_input_attachments.c \
$src_dir/src/compiler/nir/nir_lower_int64.c \
$src_dir/src/compiler/nir/nir_lower_int_to_float.c \
$src_dir/src/compiler/nir/nir_lower_interpolation.c \
$src_dir/src/compiler/nir/nir_lower_io.c \
$src_dir/src/compiler/nir/nir_lower_io_arrays_to_elements.c \
$src_dir/src/compiler/nir/nir_lower_io_to_temporaries.c \
$src_dir/src/compiler/nir/nir_lower_io_to_scalar.c \
$src_dir/src/compiler/nir/nir_lower_io_to_vector.c \
$src_dir/src/compiler/nir/nir_lower_is_helper_invocation.c \
$src_dir/src/compiler/nir/nir_lower_load_const_to_scalar.c \
$src_dir/src/compiler/nir/nir_lower_mediump.c \
$src_dir/src/compiler/nir/nir_lower_mem_access_bit_sizes.c \
$src_dir/src/compiler/nir/nir_lower_memcpy.c \
$src_dir/src/compiler/nir/nir_lower_memory_model.c \
$src_dir/src/compiler/nir/nir_lower_multiview.c \
$src_dir/src/compiler/nir/nir_lower_non_uniform_access.c \
$src_dir/src/compiler/nir/nir_lower_packing.c \
$src_dir/src/compiler/nir/nir_lower_passthrough_edgeflags.c \
$src_dir/src/compiler/nir/nir_lower_patch_vertices.c \
$src_dir/src/compiler/nir/nir_lower_phis_to_scalar.c \
$src_dir/src/compiler/nir/nir_lower_point_size.c \
$src_dir/src/compiler/nir/nir_lower_point_size_mov.c \
$src_dir/src/compiler/nir/nir_lower_point_smooth.c \
$src_dir/src/compiler/nir/nir_lower_poly_line_smooth.c \
$src_dir/src/compiler/nir/nir_lower_pntc_ytransform.c \
$src_dir/src/compiler/nir/nir_lower_readonly_images_to_tex.c \
$src_dir/src/compiler/nir/nir_lower_reg_intrinsics_to_ssa.c \
$src_dir/src/compiler/nir/nir_lower_returns.c \
$src_dir/src/compiler/nir/nir_lower_robust_access.c \
$src_dir/src/compiler/nir/nir_lower_samplers.c \
$src_dir/src/compiler/nir/nir_lower_scratch.c \
$src_dir/src/compiler/nir/nir_lower_scratch_to_var.c \
$src_dir/src/compiler/nir/nir_lower_shader_calls.c \
$src_dir/src/compiler/nir/nir_lower_single_sampled.c \
$src_dir/src/compiler/nir/nir_lower_subgroups.c \
$src_dir/src/compiler/nir/nir_lower_sysvals_to_varyings.c \
$src_dir/src/compiler/nir/nir_lower_system_values.c \
$src_dir/src/compiler/nir/nir_lower_task_shader.c \
$src_dir/src/compiler/nir/nir_lower_terminate_to_demote.c \
$src_dir/src/compiler/nir/nir_lower_tess_coord_z.c \
$src_dir/src/compiler/nir/nir_lower_tex.c \
$src_dir/src/compiler/nir/nir_lower_tex_shadow.c \
$src_dir/src/compiler/nir/nir_lower_texcoord_replace.c \
$src_dir/src/compiler/nir/nir_lower_texcoord_replace_late.c \
$src_dir/src/compiler/nir/nir_lower_two_sided_color.c \
$src_dir/src/compiler/nir/nir_lower_ubo_vec4.c \
$src_dir/src/compiler/nir/nir_lower_undef_to_zero.c \
$src_dir/src/compiler/nir/nir_lower_vars_to_ssa.c \
$src_dir/src/compiler/nir/nir_lower_var_copies.c \
$src_dir/src/compiler/nir/nir_lower_variable_initializers.c \
$src_dir/src/compiler/nir/nir_lower_vec_to_regs.c \
$src_dir/src/compiler/nir/nir_lower_vec3_to_vec4.c \
$src_dir/src/compiler/nir/nir_lower_view_index_to_device_index.c \
$src_dir/src/compiler/nir/nir_lower_viewport_transform.c \
$src_dir/src/compiler/nir/nir_lower_wpos_center.c \
$src_dir/src/compiler/nir/nir_lower_wpos_ytransform.c \
$src_dir/src/compiler/nir/nir_lower_wrmasks.c \
$src_dir/src/compiler/nir/nir_metadata.c \
$src_dir/src/compiler/nir/nir_mod_analysis.c \
$src_dir/src/compiler/nir/nir_move_output_stores_to_end.c \
$src_dir/src/compiler/nir/nir_move_vec_src_uses_to_dest.c \
$src_dir/src/compiler/nir/nir_normalize_cubemap_coords.c \
$build_dir/src/compiler/nir/nir_opcodes.c \
$src_dir/src/compiler/nir/nir_opt_access.c \
$build_dir/src/compiler/nir/nir_opt_algebraic.c \
$src_dir/src/compiler/nir/nir_opt_barriers.c \
$src_dir/src/compiler/nir/nir_opt_call.c \
$src_dir/src/compiler/nir/nir_opt_clip_cull_const.c \
$src_dir/src/compiler/nir/nir_opt_combine_stores.c \
$src_dir/src/compiler/nir/nir_opt_comparison_pre.c \
$src_dir/src/compiler/nir/nir_opt_constant_folding.c \
$src_dir/src/compiler/nir/nir_opt_copy_prop_vars.c \
$src_dir/src/compiler/nir/nir_opt_copy_propagate.c \
$src_dir/src/compiler/nir/nir_opt_cse.c \
$src_dir/src/compiler/nir/nir_opt_dce.c \
$src_dir/src/compiler/nir/nir_opt_dead_cf.c \
$src_dir/src/compiler/nir/nir_opt_dead_write_vars.c \
$src_dir/src/compiler/nir/nir_opt_find_array_copies.c \
$src_dir/src/compiler/nir/nir_opt_frag_coord_to_pixel_coord.c \
$src_dir/src/compiler/nir/nir_opt_fragdepth.c \
$src_dir/src/compiler/nir/nir_opt_gcm.c \
$src_dir/src/compiler/nir/nir_opt_generate_bfi.c \
$src_dir/src/compiler/nir/nir_opt_idiv_const.c \
$src_dir/src/compiler/nir/nir_opt_if.c \
$src_dir/src/compiler/nir/nir_opt_intrinsics.c \
$src_dir/src/compiler/nir/nir_opt_large_constants.c \
$src_dir/src/compiler/nir/nir_opt_licm.c \
$src_dir/src/compiler/nir/nir_opt_load_store_vectorize.c \
$src_dir/src/compiler/nir/nir_opt_loop.c \
$src_dir/src/compiler/nir/nir_opt_loop_unroll.c \
$src_dir/src/compiler/nir/nir_opt_memcpy.c \
$src_dir/src/compiler/nir/nir_opt_move.c \
$src_dir/src/compiler/nir/nir_opt_move_discards_to_top.c \
$src_dir/src/compiler/nir/nir_opt_mqsad.c \
$src_dir/src/compiler/nir/nir_opt_non_uniform_access.c \
$src_dir/src/compiler/nir/nir_opt_offsets.c \
$src_dir/src/compiler/nir/nir_opt_phi_precision.c \
$src_dir/src/compiler/nir/nir_opt_phi_to_bool.c \
$src_dir/src/compiler/nir/nir_opt_peephole_select.c \
$src_dir/src/compiler/nir/nir_opt_preamble.c \
$src_dir/src/compiler/nir/nir_opt_ray_queries.c \
$src_dir/src/compiler/nir/nir_opt_reassociate_bfi.c \
$src_dir/src/compiler/nir/nir_opt_remove_phis.c \
$src_dir/src/compiler/nir/nir_opt_shrink_stores.c \
$src_dir/src/compiler/nir/nir_opt_shrink_vectors.c \
$src_dir/src/compiler/nir/nir_opt_sink.c \
$src_dir/src/compiler/nir/nir_opt_tex_skip_helpers.c \
$src_dir/src/compiler/nir/nir_opt_undef.c \
$src_dir/src/compiler/nir/nir_opt_uniform_atomics.c \
$src_dir/src/compiler/nir/nir_opt_varyings.c \
$src_dir/src/compiler/nir/nir_opt_vectorize.c \
$src_dir/src/compiler/nir/nir_opt_vectorize_io.c \
$src_dir/src/compiler/nir/nir_phi_builder.c \
$src_dir/src/compiler/nir/nir_passthrough_gs.c \
$src_dir/src/compiler/nir/nir_passthrough_tcs.c \
$src_dir/src/compiler/nir/nir_print.c \
$src_dir/src/compiler/nir/nir_propagate_invariant.c \
$src_dir/src/compiler/nir/nir_range_analysis.c \
$src_dir/src/compiler/nir/nir_remove_dead_variables.c \
$src_dir/src/compiler/nir/nir_remove_tex_shadow.c \
$src_dir/src/compiler/nir/nir_repair_ssa.c \
$src_dir/src/compiler/nir/nir_scale_fdiv.c \
$src_dir/src/compiler/nir/nir_schedule.c \
$src_dir/src/compiler/nir/nir_search.c \
$src_dir/src/compiler/nir/nir_serialize.c \
$src_dir/src/compiler/nir/nir_split_64bit_vec3_and_vec4.c \
$src_dir/src/compiler/nir/nir_split_per_member_structs.c \
$src_dir/src/compiler/nir/nir_split_var_copies.c \
$src_dir/src/compiler/nir/nir_split_vars.c \
$src_dir/src/compiler/nir/nir_sweep.c \
$src_dir/src/compiler/nir/nir_to_lcssa.c \
$src_dir/src/compiler/nir/nir_trivialize_registers.c \
$src_dir/src/compiler/nir/nir_use_dominance.c \
$src_dir/src/compiler/nir/nir_validate.c \
$src_dir/src/compiler/nir/nir_worklist.c \
"
for f in $libnir_files
do
	libnir_obj=$build_dir/$(basename $f .c).o
	libnir_a="$libnir_a $libnir_obj"

	$cc $cppflags $f -o $libnir_obj &
done
#===============================================================================
# libaco
# we don't use cppflags_common for aco because the code is not yet
# compatible
cppflags="\
$gcc_builtins_cppflags \
$gcc_attributes_cppflags \
$linux_glibc_cppflags \
$mesa_cppflags \
-I$build_dir/src/amd/compiler \
-I$src_dir/src/amd/compiler \
-I$build_dir/src/amd/vulkan \
-I$src_dir/src/amd/vulkan \
-I$build_dir/src/amd/common/nir \
-I$src_dir/src/amd/common/nir \
-I$build_dir/src/amd/common \
-I$src_dir/src/amd/common \
-I$build_dir/src/amd \
-I$src_dir/src/amd \
-I$build_dir/src/compiler/nir \
-I$src_dir/src/compiler/nir \
-I$build_dir/src/compiler \
-I$src_dir/src/compiler \
-I$build_dir/src/mesa \
-I$src_dir/src/mesa \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include \
-I$src_dir/include \
"
libaco_files="\
$src_dir/src/amd/compiler/aco_form_hard_clauses.cpp \
$src_dir/src/amd/compiler/aco_dead_code_analysis.cpp \
$src_dir/src/amd/compiler/aco_dominance.cpp \
$src_dir/src/amd/compiler/aco_insert_delay_alu.cpp \
$src_dir/src/amd/compiler/aco_instruction_selection.cpp \
$src_dir/src/amd/compiler/aco_instruction_selection_setup.cpp \
$src_dir/src/amd/compiler/aco_interface.cpp \
$src_dir/src/amd/compiler/aco_assembler.cpp \
$src_dir/src/amd/compiler/aco_insert_exec_mask.cpp \
$src_dir/src/amd/compiler/aco_insert_NOPs.cpp \
$src_dir/src/amd/compiler/aco_insert_waitcnt.cpp \
$src_dir/src/amd/compiler/aco_ir.cpp \
$src_dir/src/amd/compiler/aco_reduce_assign.cpp \
$src_dir/src/amd/compiler/aco_reindex_ssa.cpp \
$src_dir/src/amd/compiler/aco_register_allocation.cpp \
$src_dir/src/amd/compiler/aco_live_var_analysis.cpp \
$src_dir/src/amd/compiler/aco_lower_branches.cpp \
$src_dir/src/amd/compiler/aco_lower_phis.cpp \
$src_dir/src/amd/compiler/aco_lower_subdword.cpp \
$src_dir/src/amd/compiler/aco_lower_to_cssa.cpp \
$src_dir/src/amd/compiler/aco_lower_to_hw_instr.cpp \
$build_dir/src/amd/compiler/aco_opcodes.cpp \
$src_dir/src/amd/compiler/aco_optimizer.cpp \
$src_dir/src/amd/compiler/aco_optimizer_postRA.cpp \
$src_dir/src/amd/compiler/aco_opt_value_numbering.cpp \
$src_dir/src/amd/compiler/aco_print_asm.cpp \
$src_dir/src/amd/compiler/aco_print_ir.cpp \
$src_dir/src/amd/compiler/aco_repair_ssa.cpp \
$src_dir/src/amd/compiler/aco_scheduler.cpp \
$src_dir/src/amd/compiler/aco_scheduler_ilp.cpp \
$src_dir/src/amd/compiler/aco_ssa_elimination.cpp \
$src_dir/src/amd/compiler/aco_spill.cpp \
$src_dir/src/amd/compiler/aco_statistics.cpp \
$src_dir/src/amd/compiler/aco_validate.cpp \
"
for f in $libaco_files
do
	libaco_obj=$build_dir/$(basename $f .c).o
	libaco_a="$libaco_a $libaco_obj"

	$cxx $cppflags $f -o $libaco_obj &
done
#===============================================================================
# libmesautils
cppflags="\
$cppflags_common \
-I$build_dir/src/gallium/auxiliary \
-I$src_dir/src/gallium/auxiliary \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src/mesa \
-I$src_dir/src/mesa \
-I$build_dir/src/util/blake3 \
-I$src_dir/src/util/blake3 \
-I$build_dir/src/util/format \
-I$src_dir/src/util/format \
-I$build_dir/src/util \
-I$src_dir/src/util \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include \
-I$src_dir/include \
$zlib_cppflags \
"

libmesautil_files="\
$src_dir/src/util/anon_file.c \
$src_dir/src/util/bitscan.c \
$src_dir/src/util/blake3/blake3.c \
$src_dir/src/util/blake3/blake3_dispatch.c \
$src_dir/src/util/blake3/blake3_portable.c \
$src_dir/src/util/blob.c \
$src_dir/src/util/build_id.c \
$src_dir/src/util/compress.c \
$src_dir/src/util/crc32.c \
$src_dir/src/util/cnd_monotonic.c \
$src_dir/src/util/dag.c \
$src_dir/src/util/disk_cache.c \
$src_dir/src/util/disk_cache_os.c \
$src_dir/src/util/double.c \
$src_dir/src/util/fast_idiv_by_const.c \
$build_dir/src/util/format_srgb.c \
$src_dir/src/util/fossilize_db.c \
$src_dir/src/util/futex.c \
$src_dir/src/util/half_float.c \
$src_dir/src/util/hash_table.c \
$src_dir/src/util/helpers.c \
$src_dir/src/util/log.c \
$src_dir/src/util/mesa-blake3.c \
$src_dir/src/util/mesa-sha1.c \
$build_dir/src/util/mesa_cache_db.c \
$src_dir/src/util/mesa_cache_db_multipart.c \
$src_dir/src/util/memstream.c \
$src_dir/src/util/os_file.c \
$src_dir/src/util/os_memory_fd.c \
$src_dir/src/util/os_misc.c \
$src_dir/src/util/os_socket.c \
$src_dir/src/util/os_time.c \
$src_dir/src/util/parson.c \
$src_dir/src/util/sha1/sha1.c \
$src_dir/src/util/ralloc.c \
$src_dir/src/util/rand_xor.c \
$src_dir/src/util/rb_tree.c \
$src_dir/src/util/register_allocate.c \
$src_dir/src/util/rgtc.c \
$src_dir/src/util/rwlock.c \
$src_dir/src/util/set.c \
$src_dir/src/util/simple_mtx.c \
$src_dir/src/util/slab.c \
$src_dir/src/util/softfloat.c \
$src_dir/src/util/sparse_array.c \
$src_dir/src/util/string_buffer.c \
$src_dir/src/util/strtod.c \
$src_dir/src/util/thread_sched.c \
$src_dir/src/util/u_atomic.c \
$src_dir/src/util/u_call_once.c \
$src_dir/src/util/u_cpu_detect.c \
$src_dir/src/util/u_debug.c \
$src_dir/src/util/u_debug_stack.c \
$src_dir/src/util/u_debug_symbol.c \
$src_dir/src/util/u_dl.c \
$src_dir/src/util/u_dynarray.c \
$build_dir/src/util/u_format_table.c \
$src_dir/src/util/u_hash_table.c \
$src_dir/src/util/u_printf.c \
$src_dir/src/util/format/u_format.c \
$src_dir/src/util/format/u_format_bptc.c \
$src_dir/src/util/format/u_format_etc.c \
$src_dir/src/util/format/u_format_fxt1.c \
$src_dir/src/util/format/u_format_latc.c \
$src_dir/src/util/format/u_format_other.c \
$src_dir/src/util/format/u_format_rgtc.c \
$src_dir/src/util/format/u_format_s3tc.c \
$src_dir/src/util/format/u_format_tests.c \
$src_dir/src/util/format/u_format_yuv.c \
$src_dir/src/util/format/u_format_zs.c \
$src_dir/src/util/u_idalloc.c \
$src_dir/src/util/u_math.c \
$src_dir/src/util/u_mm.c \
$src_dir/src/util/u_process.c \
$src_dir/src/util/u_queue.c \
$src_dir/src/util/u_thread.c \
$src_dir/src/util/u_vector.c \
$src_dir/src/util/u_worklist.c \
$src_dir/src/util/vma.c \
$src_dir/src/util/vl_zscan_data.c \
\
$src_dir/src/c11/impl/time.c \
$src_dir/src/c11/impl/threads_posix.c \
"

for f in $libmesautil_files
do
	libmesautil_obj=$build_dir/$(basename $f .c).o
	libmesautil_a="$libmesautil_a $libmesautil_obj"

	$cc $cppflags $f -o $libmesautil_obj &
done
#-------------------------------------------------------------------------------
# The following is for the ultra complex blake3 hash algorithm.
libmesautil_asm_files="\
$src_dir/src/util/blake3/blake3_sse2_x86-64_unix.S \
$src_dir/src/util/blake3/blake3_sse41_x86-64_unix.S \
$src_dir/src/util/blake3/blake3_avx2_x86-64_unix.S \
$src_dir/src/util/blake3/blake3_avx512_x86-64_unix.S \
"
for f in $libmesautil_asm_files
do
	libmesautil_asm_cpp_s=$build_dir/$(basename $f .c).cpp.s
	libmesautil_asm_obj=$build_dir/$(basename $f .c).o
	libmesautil_a="$libmesautil_a $libmesautil_asm_obj"

	$cc -E $cppflags $f -o $libmesautil_asm_cpp_s
	$as $libmesautil_asm_cpp_s -o $libmesautil_asm_obj &
done
#===============================================================================
# amd vulkan
cppflags="\
$cppflags_common \
-DPACKAGE_VERSION=\"$version\" \
-I$build_dir/src/amd/compiler \
-I$src_dir/src/amd/compiler \
-I$build_dir/src/amd/common/nir \
-I$src_dir/src/amd/common/nir \
-I$build_dir/src/amd/common \
-I$src_dir/src/amd/common \
-I$build_dir/src/amd/vulkan/nir \
-I$src_dir/src/amd/vulkan/nir \
-I$build_dir/src/amd/vulkan/meta \
-I$src_dir/src/amd/vulkan/meta \
-I$build_dir/src/amd/vulkan \
-I$src_dir/src/amd/vulkan \
-I$build_dir/src/amd \
-I$src_dir/src/amd \
-I$build_dir/src/gallium/auxiliary \
-I$src_dir/src/gallium/auxiliary \
-I$build_dir/src/gallium/include \
-I$src_dir/src/gallium/include \
-I$build_dir/src/vulkan/runtime/bvh \
-I$src_dir/src/vulkan/runtime/bvh \
-I$build_dir/src/vulkan/runtime \
-I$src_dir/src/vulkan/runtime \
-I$build_dir/src/vulkan/wsi \
-I$src_dir/src/vulkan/wsi \
-I$build_dir/src/vulkan/util \
-I$src_dir/src/vulkan/util \
-I$build_dir/src/mesa \
-I$src_dir/src/mesa \
-I$build_dir/src/compiler/nir \
-I$src_dir/src/compiler/nir \
-I$build_dir/src/compiler \
-I$src_dir/src/compiler \
-I$build_dir/src/util \
-I$src_dir/src/util \
-I$build_dir/src \
-I$src_dir/src \
-I$build_dir/include \
-I$src_dir/include \
$libdrm_cppflags \
$libdrm_amdgpu_cppflags \
$libx11_cppflags \
$libxrandr_cppflags \
$libxrender_cppflags \
$libxcb_cppflags \
$xorgproto_cppflags \
$libxshmfence_cppflags \
"
#$build_dir/radv_extensions.c
#$build_dir/vk_format_table.c
# XXX: remove rt for now
#$src_dir/src/amd/vulkan/radv_acceleration_structure.c
libvulkan_amd_common_files="\
$src_dir/src/amd/vulkan/radv_android.c \
$src_dir/src/amd/vulkan/radv_buffer.c \
$src_dir/src/amd/vulkan/radv_buffer_view.c \
$src_dir/src/amd/vulkan/radv_cmd_buffer.c \
$src_dir/src/amd/vulkan/radv_cp_dma.c \
$src_dir/src/amd/vulkan/radv_cp_reg_shadowing.c \
$src_dir/src/amd/vulkan/radv_cs.c \
$src_dir/src/amd/vulkan/radv_debug.c \
$build_dir/src/amd/vulkan/radv_device.c \
$src_dir/src/amd/vulkan/radv_device_memory.c \
$src_dir/src/amd/vulkan/radv_dgc.c \
$src_dir/src/amd/vulkan/radv_descriptor_set.c \
$src_dir/src/amd/vulkan/radv_event.c \
$src_dir/src/amd/vulkan/radv_formats.c \
$src_dir/src/amd/vulkan/radv_image.c \
$src_dir/src/amd/vulkan/radv_image_view.c \
$src_dir/src/amd/vulkan/radv_instance.c \
$build_dir/src/amd/vulkan/meta/radv_meta.c \
$src_dir/src/amd/vulkan/meta/radv_meta_blit.c \
$src_dir/src/amd/vulkan/meta/radv_meta_blit2d.c \
$src_dir/src/amd/vulkan/meta/radv_meta_buffer.c \
$src_dir/src/amd/vulkan/meta/radv_meta_bufimage.c \
$src_dir/src/amd/vulkan/meta/radv_meta_clear.c \
$src_dir/src/amd/vulkan/meta/radv_meta_dcc_retile.c \
$build_dir/src/amd/vulkan/meta/radv_meta_copy.c \
$src_dir/src/amd/vulkan/meta/radv_meta_copy_vrs_htile.c \
$src_dir/src/amd/vulkan/meta/radv_meta_decompress.c \
$src_dir/src/amd/vulkan/meta/radv_meta_etc_decode.c \
$src_dir/src/amd/vulkan/meta/radv_meta_fast_clear.c \
$src_dir/src/amd/vulkan/meta/radv_meta_fmask_copy.c \
$src_dir/src/amd/vulkan/meta/radv_meta_fmask_expand.c \
$src_dir/src/amd/vulkan/meta/radv_meta_resolve.c \
$src_dir/src/amd/vulkan/meta/radv_meta_resolve_cs.c \
$src_dir/src/amd/vulkan/meta/radv_meta_resolve_fs.c \
$src_dir/src/amd/vulkan/nir/radv_meta_nir.c \
$src_dir/src/amd/vulkan/nir/radv_nir_apply_pipeline_layout.c \
$src_dir/src/amd/vulkan/nir/radv_nir_export_multiview.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_abi.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_cooperative_matrix.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_fs_barycentric.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_fs_intrinsics.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_hit_attrib_derefs.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_intrinsics_early.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_io.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_primitive_shading_rate.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_view_index.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_viewport_to_zero.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_vs_inputs.c \
$src_dir/src/amd/vulkan/nir/radv_nir_lower_ray_queries.c \
$src_dir/src/amd/vulkan/nir/radv_nir_opt_fs_builtins.c \
$src_dir/src/amd/vulkan/nir/radv_nir_opt_tid_function.c \
$src_dir/src/amd/vulkan/nir/radv_nir_remap_color_attachment.c \
$src_dir/src/amd/vulkan/nir/radv_nir_rt_common.c \
$src_dir/src/amd/vulkan/nir/radv_nir_rt_shader.c \
$src_dir/src/amd/vulkan/radv_perfcounter.c \
$build_dir/src/amd/vulkan/radv_physical_device.c \
$src_dir/src/amd/vulkan/radv_pipeline.c \
$src_dir/src/amd/vulkan/radv_pipeline_binary.c \
$src_dir/src/amd/vulkan/radv_pipeline_compute.c \
$src_dir/src/amd/vulkan/radv_pipeline_cache.c \
$src_dir/src/amd/vulkan/radv_pipeline_graphics.c \
$src_dir/src/amd/vulkan/radv_pipeline_rt.c \
$src_dir/src/amd/vulkan/radv_printf.c \
$src_dir/src/amd/vulkan/radv_queue.c \
$src_dir/src/amd/vulkan/radv_sampler.c \
$src_dir/src/amd/vulkan/radv_sdma.c \
$src_dir/src/amd/vulkan/radv_shader.c \
$src_dir/src/amd/vulkan/radv_shader_args.c \
$src_dir/src/amd/vulkan/radv_shader_info.c \
$src_dir/src/amd/vulkan/radv_shader_object.c \
$src_dir/src/amd/vulkan/radv_spm.c \
$src_dir/src/amd/vulkan/radv_query.c \
$src_dir/src/amd/vulkan/radv_video.c \
$src_dir/src/amd/vulkan/radv_video_enc.c \
$src_dir/src/amd/vulkan/radv_wsi.c \
\
$build_dir/src/amd/vulkan/radv_entrypoints.c \
\
$src_dir/src/amd/vulkan/winsys/amdgpu/radv_amdgpu_bo.c \
$src_dir/src/amd/vulkan/winsys/amdgpu/radv_amdgpu_cs.c \
$src_dir/src/amd/vulkan/winsys/amdgpu/radv_amdgpu_winsys.c \
\
$src_dir/src/amd/vulkan/winsys/null/radv_null_bo.c \
$src_dir/src/amd/vulkan/winsys/null/radv_null_cs.c \
$src_dir/src/amd/vulkan/winsys/null/radv_null_winsys.c \
\
$src_dir/src/amd/vulkan/layers/radv_metro_exodus.c \
$src_dir/src/amd/vulkan/layers/radv_quantic_dream.c \
$src_dir/src/amd/vulkan/layers/radv_rage2.c \
\
$build_dir/src/amd/vulkan/radv_no_tracers.c \
"

#$build_dir/src/amd/vulkan/layers/radv_sqtt_layer.c \

for f in $libvulkan_amd_common_files
do
	libvulkan_amd_common_obj=$build_dir/$(basename $f .c).o
	libvulkan_amd_common_a="$libvulkan_amd_common_a $libvulkan_amd_common_obj"

	$cc $cppflags $f -o $libvulkan_amd_common_obj &
done
libvulkan_amd_common_a="\
$libvulkan_amd_common_a \
$build_dir/xmlconfig.o"
#===============================================================================
# create the radeon vulkan driver
soname=libvulkan_radeon.so

# XXX:the attempt to generalize without kludge the link stage of real programs
# build is near a total and complete failure
if test "${driver_link_cmd-unset}" = unset ; then
driver_link_cmd="g++ -o $soname -Wl,-soname=$soname \
-shared -static-libgcc -static-libstdc++ \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib \
-Wl,--no-undefined,--gc-sections,-Bsymbolic,-s \
	-Wl,--whole-archive -Wl,--start-group \
		$libaco_a \
		$libvulkan_amd_common_a \
		$libvulkan_util_a \
		$libvulkan_runtime_a \
		$libvulkan_wsi_a \
		$libamd_common_a \
		$libamdgpu_addrlib_a \
		$libcompiler_a \
		$libnir_a \
		$libmesautil_a \
	-Wl,--end-group -Wl,--no-whole-archive \
	-Wl,--exclude-libs,$zlib_archives:$libelf_archives:libstdc++.a \
	$zlib_ldflags \
	$expat_ldflags \
	$libelf_ldflags \
	-Wl,--as-needed \
	$libdrm_ldflags \
	$libdrm_amdgpu_ldflags \
	$libx11_ldflags \
	$libxrandr_ldflags \
	$libxrender_ldflags \
	$libxcb_ldflags \
	$libxshmfence_ldflags \
	-lpthread \
	-ldl \
	-Wl,--no-as-needed"
fi
wait
eval $driver_link_cmd
#===============================================================================
