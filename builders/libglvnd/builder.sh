src_name=libglvnd
git_url0=https://gitlab.freedesktop.org/glvnd/$src_name

pkg_dir=/run/pkgs/$src_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p /run/pkgs
cp -r $src_dir $pkg_dir

#-------------------------------------------------------------------------------

cd $pkg_dir

# copy the canonical lean build scripts
cp -r $nyan_root/builders/$pkg_name/contrib .

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

#-------------------------------------------------------------------------------

build_dir=/run/builds/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# create the local configuration file
cat >$build_dir/local_conf.sh <<EOF
inc_dir_pathname=/nyan/$src_name/$slot/include
EOF

$pkg_dir/contrib/gcc_binutils_glibc_tsd_pure_c_opengl_egl_gl.sh

#-------------------------------------------------------------------------------

rm -Rf /nyan/$src_name/$slot/include
cp -r $build_dir/install_root/nyan/$src_name/$slot/include /nyan/$src_name/$slot/include

rm -Rf /nyan/$src_name/$slot/lib
cp -r $build_dir/install_root/usr/lib /nyan/$src_name/$slot/lib

rm -Rf $build_dir $pkg_dir
