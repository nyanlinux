src_name=flex
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
git_url0=git://github.com/westes/flex

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export "PATH=\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
/nyan/m4/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export 'CFLAGS=-O2 -pipe -fPIC'
export "CC=$target_gnu_triple-gcc\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export AR=$target_gnu_triple-ar
export NM=$target_gnu_triple-nm
export OBJDUMP=$target_gnu_triple-objdump
export "CC_FOR_BUILD=$CC"
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--disable-shared \
	--disable-libfl \
	--disable-bootstrap \
	--disable-nls
unset CFLAGS
unset CC
unset AR
unset NM
unset OBJDUMP
unset CC_FOR_BUILD

make -j $threads_n
make install

rm -Rf /nyan/$src_name/$slot/share

export PATH=$PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
