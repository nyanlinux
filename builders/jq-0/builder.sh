src_name=jq
git_commit=b436156f5bdcc9ce3f3f93dd30a7bcbdb87910f7
git_url0=https://github.com/stedolan/$src_name.git

src_dir=$src_dir_root/$src_name
pkg_dir=$pkgs_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

cd $pkg_dir

git checkout --force $git_commit
git reset --hard

#===============================================================================
OLD_PATH=$PATH
export PATH=$sdk_autoconf_path/bin:$sdk_automake_path/bin:$sdk_libtool_path/bin:$PATH
export "ACLOCAL_PATH=\
/nyan/libtool/current/share/aclocal"
export NOCONFIGURE=1

autoreconf -fiv

unset NOCONFIGURE
unset ACLOCAL_PATH
export PATH=$OLD_PATH
#===============================================================================

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$pkg_dir/configure				\
	--prefix=/nyan/jq/0			\
	--disable-valgrind			\
	--disable-asan				\
	--disable-ubsan				\
	--disable-gcov				\
	--disable-docs				\
	--disable-error-injection		\
	--disable-all-static			\
	--without-oniguruma			\
	--enable-pthread-tls
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup
rm -Rf /nyan/$src_name/0/share
rm -f /nyan/$src_name/0/lib/*.la
find /nyan/$src_name/0 -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then strip -s $f; fi; done

rm -Rf $build_dir $pkg_dir
