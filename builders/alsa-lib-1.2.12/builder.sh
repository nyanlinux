src_name=alsa-lib
mkdir /nyan/$src_name

version=${pkg_name##*-}
slot=$version
mkdir /nyan/$src_name/$version

archive_name=$src_name-$version.tar.bz2
url0=ftp://ftp.alsa-project.org/pub/lib/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir $pkgs_dir_root
cp $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/make/current/bin:\
$PATH\
"
export AR=/opt/toolchains/x64/elf/binutils-gcc/current/bin/ar
export OBJDUMP=/opt/toolchains/x64/elf/binutils-gcc/current/bin/objdump
export "CC=/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
	-isystem /nyan/linux-headers/current/include \
	-isystem /nyan/glibc/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-static-libgcc \
	-Wl,-s \
"
export 'CFLAGS=-O2 -pipe -fPIC -fpic'
$pkg_dir/configure \
	--prefix=/usr \
	--disable-static \
	--enable-shared \
	--disable-ucm \
	--disable-topology \
	--disable-old-symbols \
	--disable-python \
	--without-debug \
	--without-softfloat
unset AR
unset OBJDUMP
unset CFLAGS
unset CC

make -j $threads_n

# install in a fake root
install_dir=$build_dir/nyan_install_root
mkdir -p $install_dir
make install DESTDIR=$install_dir

rm -Rf /nyan/$src_name/$slot/include
cp -r $install_dir/usr/include /nyan/$src_name/$slot/include

rm -Rf /nyan/$src_name/$slot/lib
mkdir /nyan/$src_name/$slot/lib
cp $install_dir/usr/lib/libasound.so.2.0.0 /nyan/$src_name/$slot/lib/libasound.so.2.0.0
ln -s libasound.so.2.0.0 /nyan/$src_name/$slot/lib/libasound.so
ln -s libasound.so.2.0.0 /nyan/$src_name/$slot/lib/libasound.so.2

rm -Rf /nyan/$src_name/$slot/share
mkdir /nyan/$src_name/$slot/share
cp -r $install_dir/usr/share/alsa /nyan/$src_name/$slot/share/alsa

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
