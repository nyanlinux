src_name=ndisc6
version=1.0.3
archive_name=$src_name-$version.tar.bz2
url0=https://www.remlab.net/files/$src_name/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC -w'
$src_dir/configure			\
	--build=$build_gnu_triple	\
	--host=$target_gnu_triple	\
	--prefix=/nyan/ndisc6/0		\
	--disable-suid-install		\
	--disable-nls
unset CFLAGS
unset CC

make -j $threads_n AR=$cross_toolchain_dir_root/bin/$target_gnu_triple-ar
make install DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/$src_name/0/share
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/bin/* || true
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/sbin/* || true

rm -Rf $build_dir $src_dir
export PATH=$OLD_PATH
