src_name=libxkbcommon
mkdir /nyan/$src_name

git_commit=${pkg_name##*-}
slot=$git_commit
mkdir /nyan/$src_name/$slot

# version is at the top of the root meson.build file
version=1.7.0
git_url0=git://github.com/xkbcommon/libxkbcommon

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
cp -r $src_dir $pkg_dir

cd $pkg_dir

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

build_dir=$builds_dir_root/$pkg_name
echo rm -Rf $build_dir
echo mkdir $build_dir
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir
#---------------------------------------------------------------------------------------------------
# X11 locale is the "compose" mini input method
cat >config.h <<EOF
#define DFLT_XKB_CONFIG_ROOT		"/usr/share/X11/xkb"
#define DFLT_XKB_CONFIG_EXTRA_PATH	""
#define XLOCALEDIR			"/usr/share/X11/locale"
#define LIBXKBCOMMON_VERSION		"$version"
#define DEFAULT_XKB_RULES		"evdev"
#define DEFAULT_XKB_MODEL		"pc105"
#define DEFAULT_XKB_LAYOUT		"us"
#define DEFAULT_XKB_VARIANT		""
#define DEFAULT_XKB_OPTIONS		""
/* glibc */
#define _GNU_SOURCE		1
#define HAVE__BUILTIN_EXPECT 	1
#define HAVE_EUIDACCESS		1
#define HAVE_MMAP		1
#define HAVE_MKOSTEMP		1
#define HAVE_POSIX_FALLOCATE	1
#define HAVE_STRNDUP		1
#define HAVE_UNISTD_H		1
#define HAVE_ASPRINTF		1
#define HAVE_VASPRINTF		1
#define HAVE_OPEN_MEMSTREAM	1

#define HAVE_SECURE_GETENV	1
#undef HAVE___SECURE_GETENV
EOF

# DON'T USE BISON EXTENSIONS!
# DON'T USE A YACC PROGRAM, THOSE ARE _BAD_ PARSERS AND IT CREATES NASTY BUILD
# DEPS.
# ALWAYS MANUALLY WRITE YOUR PARSERS!
# JEZUS! FOR GOD SAKE! YOU ANIMALS! PYTHON IS NOT ENOUGH ALREADY!
# TOXIC HUMAN BEINGS!
/nyan/bison/current/bin/bison --defines=parser.h -o $build_dir/parser.c -p _xkbcommon_ \
	$pkg_dir/src/xkbcomp/parser.y
#===================================================================================================
# base lib
objs=
#---------------------------------------------------------------------------------------------------
mkdir $build_dir/xkbcommon
srcs="\
$build_dir/parser.c \
"
for f in $srcs
do
	obj=$build_dir/xkbcommon/$(basename $f .c).o
	/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
		-static-libgcc \
		-std=c11 \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		$f -o $obj &
	objs="$obj $objs"
done
#---------------------------------------------------------------------------------------------------
mkdir $build_dir/xkbcommon/src

srcs="\
$pkg_dir/src/atom.c \
$pkg_dir/src/context.c \
$pkg_dir/src/context-priv.c \
$pkg_dir/src/keysym.c \
$pkg_dir/src/keysym-case-mappings.c \
$pkg_dir/src/keysym-utf.c \
$pkg_dir/src/keymap.c \
$pkg_dir/src/keymap-priv.c \
$pkg_dir/src/state.c \
$pkg_dir/src/text.c \
$pkg_dir/src/utf8.c \
$pkg_dir/src/utils.c \
$pkg_dir/src/utils-paths.c \
"
for f in $srcs
do
	obj=$build_dir/xkbcommon/src/$(basename $f .c).o
	/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
		-static-libgcc \
		-std=c11 \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		$f -o $obj &
	objs="$obj $objs"
done
#---------------------------------------------------------------------------------------------------
mkdir $build_dir/xkbcommon/src/compose

srcs="\
$pkg_dir/src/compose/parser.c \
$pkg_dir/src/compose/paths.c \
$pkg_dir/src/compose/state.c \
$pkg_dir/src/compose/table.c \
"
for f in $srcs
do
	obj=$build_dir/xkbcommon/src/compose/$(basename $f .c).o
	/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
		-static-libgcc \
		-std=c11 \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		$f -o $obj &
	objs="$obj $objs"
done
#---------------------------------------------------------------------------------------------------
mkdir $build_dir/xkbcommon/src/xkbcomp

srcs="\
$pkg_dir/src/xkbcomp/action.c \
$pkg_dir/src/xkbcomp/ast-build.c \
$pkg_dir/src/xkbcomp/compat.c \
$pkg_dir/src/xkbcomp/expr.c \
$pkg_dir/src/xkbcomp/include.c \
$pkg_dir/src/xkbcomp/keycodes.c \
$pkg_dir/src/xkbcomp/keymap.c \
$pkg_dir/src/xkbcomp/keymap-dump.c \
$pkg_dir/src/xkbcomp/keywords.c \
$pkg_dir/src/xkbcomp/rules.c \
$pkg_dir/src/xkbcomp/scanner.c \
$pkg_dir/src/xkbcomp/symbols.c \
$pkg_dir/src/xkbcomp/types.c \
$pkg_dir/src/xkbcomp/vmod.c \
$pkg_dir/src/xkbcomp/xkbcomp.c \
"

for f in $srcs
do
	obj=$build_dir/xkbcommon/src/xkbcomp/$(basename $f .c).o
	/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
		-static-libgcc \
		-std=c11 \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		$f -o $obj &
	objs="$obj $objs"
done
#---------------------------------------------------------------------------------------------------
wait
/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
	-B/nyan/glibc/current/lib -shared \
	-Wl,--version-script=$pkg_dir/xkbcommon.map \
	-Wl,-soname=libxkbcommon.so.0 \
	-Wl,-s \
	-static-libgcc \
	$objs \
	-o $build_dir/libxkbcommon.so.0.0.0
#===================================================================================================
# x11 helper lib
objs=
#---------------------------------------------------------------------------------------------------
mkdir $build_dir/xkbcommon-x11
#---------------------------------------------------------------------------------------------------
mkdir $build_dir/xkbcommon-x11/src

srcs="\
$pkg_dir/src/context-priv.c \
$pkg_dir/src/keymap-priv.c \
$pkg_dir/src/atom.c \
"
for f in $srcs
do
	obj=$build_dir/xkbcommon-x11/src/$(basename $f .c).o
	/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
		-std=c11 \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-static-libgcc \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		-I/nyan/libxcb/current/include \
		$f -o $obj &
	objs="$obj $objs"
done
#---------------------------------------------------------------------------------------------------
mkdir $build_dir/xkbcommon-x11/src/x11

srcs="\
$pkg_dir/src/x11/keymap.c \
$pkg_dir/src/x11/state.c \
$pkg_dir/src/x11/util.c \
"
for f in $srcs
do
	obj=$build_dir/xkbcommon-x11/src/x11/$(basename $f .c).o
	/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
		-std=c11 \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-static-libgcc \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		-I/nyan/libxcb/current/include \
		$f -o $obj &
	objs="$obj $objs"
done
#---------------------------------------------------------------------------------------------------
wait
/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
	-static-libgcc \
	-B/nyan/glibc/current/lib -shared \
	-Wl,--version-script=$pkg_dir/xkbcommon-x11.map \
	-Wl,-soname=libxkbcommon-x11.so.0 \
	-Wl,-s \
	$objs \
	$build_dir/libxkbcommon.so.0.0.0 \
	-L/nyan/libxcb/current/lib -lxcb-xkb \
	-o $build_dir/libxkbcommon-x11.so.0.0.0
#===================================================================================================
# registry, pulling libxml2... jez... not event support for expat
# some other fanatics will push for json I guess.
# XXX: Should not be installed. Comment it out.
##objs=
###---------------------------------------------------------------------------------------------------
##mkdir $build_dir/xkbregistry
###---------------------------------------------------------------------------------------------------
##mkdir $build_dir/xkbregistry/src
##
##srcs="\
##$pkg_dir/src/registry.c \
##$pkg_dir/src/utils.c \
##$pkg_dir/src/util-list.c \
##"
##for f in $srcs
##do
##	obj=$build_dir/xkbregistry/src/$(basename $f .c).o
##	/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
##		-std=c11 \
##		-static-libgcc \
##		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
##		-isystem /nyan/glibc/current/include \
##		-isystem /nyan/linux-headers/current/include \
##		-I$build_dir \
##		-I$pkg_dir/src \
##		-I$pkg_dir/include \
##		-I$pkg_dir \
##		-I/nyan/libxml2/current/include/libxml2 \
##		$f -o $obj &
##	objs="$obj $objs"
##done
###---------------------------------------------------------------------------------------------------
##wait
##/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
##	-static-libgcc \
##	-B/nyan/glibc/current/lib -shared \
##	-Wl,-s \
##	-Wl,--version-script=$pkg_dir/xkbregistry.map \
##	-Wl,-soname=libxkbregistry.so.0 \
##	$objs \
##	-L/nyan/libxml2/current/lib -lxml2 \
##	-L/nyan/zlib/current/lib -lz \
##	-lm \
##	-o $build_dir/libxkbregistry.so.0.0.0
#===================================================================================================
rm -Rf /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/include/xkbcommon

cp 	$pkg_dir/include/xkbcommon/xkbcommon.h		/nyan/$src_name/$slot/include/xkbcommon/xkbcommon.h
cp 	$pkg_dir/include/xkbcommon/xkbcommon-compat.h	/nyan/$src_name/$slot/include/xkbcommon/xkbcommon-compat.h
cp 	$pkg_dir/include/xkbcommon/xkbcommon-compose.h	/nyan/$src_name/$slot/include/xkbcommon/xkbcommon-compose.h
cp 	$pkg_dir/include/xkbcommon/xkbcommon-keysyms.h	/nyan/$src_name/$slot/include/xkbcommon/xkbcommon-keysyms.h
cp 	$pkg_dir/include/xkbcommon/xkbcommon-names.h	/nyan/$src_name/$slot/include/xkbcommon/xkbcommon-names.h
cp 	$pkg_dir/include/xkbcommon/xkbcommon-x11.h	/nyan/$src_name/$slot/include/xkbcommon/xkbcommon-x11.h
cp 	$pkg_dir/include/xkbcommon/xkbregistry.h 	/nyan/$src_name/$slot/include/xkbcommon/xkbregistry.h

rm -Rf /nyan/$src_name/$slot/lib
mkdir /nyan/$src_name/$slot/lib

cp $build_dir/libxkbcommon.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbcommon.so.0.0.0
ln -s libxkbcommon.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbcommon.so
ln -s libxkbcommon.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbcommon.so.0

cp $build_dir/libxkbcommon-x11.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbcommon-x11.so.0.0.0
ln -s libxkbcommon-x11.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbcommon-x11.so
ln -s libxkbcommon-x11.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbcommon-x11.so.0

##cp $build_dir/libxkbregistry.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbregistry.so.0.0.0
##ln -s libxkbregistry.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbregistry.so
##ln -s libxkbregistry.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbregistry.so.0

rm -Rf $build_dir $pkg_dir
