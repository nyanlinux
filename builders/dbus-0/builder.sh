src_name=dbus
version=1.13.2
archive_name=$src_name-$version.tar.gz
url0=https://dbus.freedesktop.org/releases/dbus/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export "PKG_CONFIG_PATH=\
/nyan/expat/current/lib/pkgconfig"

export 'CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc'
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--prefix=/nyan/dbus/0		\
	--localstatedir=/var		\
	--enable-debug=no		\
	--disable-static		\
	--enable-shared			\
	--enable-inotify		\
	--enable-epoll			\
	--disable-checks		\
	--disable-stats
unset CFLAGS
unset CC

make -j $threads_n
make install 

# cleanup and tidying
rm -Rf /nyan/$src_name/0/share/doc
rm -f /nyan/$src_name/0/lib/*.la 
find /nyan/$src_name/0 -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then strip -s $f; fi; done

# it wants a "messagebus" entry in /etc/group, we choose 1001
if ! egrep -q '^messagebus:' /etc/group; then
	if egrep -q '1001' /etc/group; then
		echo 'error adding messagebus group, there is already a group with the 1001 id'
		exit 1
	else
		echo 'messagebus::1001:' >>/etc/group
	fi
fi

# it wants a "messagebus" entry in /etc/passwd, we choose 1001
if ! egrep -q '^messagebus:' /etc/passwd; then
	if egrep -q '1001' /etc/passwd; then
		echo 'error adding messagebus user, there is already a user with the 1001 id'
		exit 1
	else
		echo 'messagebus::1001:1001::/root:/bin/sh' >>/etc/passwd
	fi
fi

rm -Rf $build_dir $src_dir
