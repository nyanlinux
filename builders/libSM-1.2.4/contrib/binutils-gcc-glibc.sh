#!/bin/sh

# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the script at the top of mesa source tree, create somewhere else
# a build directory, cd into it, and call from there this script.

# XXX: the defaults are for our custom distro
#===================================================================================================
# build dir and src dir
build_dir=$(realpath .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(realpath $(dirname $0)/..)
echo "src_dir=$src_dir"
#===================================================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===================================================================================================
if test "${xorgproto_inc_dir-unset}" = unset; then
xorgproto_inc_dir=/nyan/xorgproto/current/include
fi
xorgproto_cppflags="-I$xorgproto_inc_dir"
#===================================================================================================
if test "${xtrans_cppflags-unset}" = unset; then
# the definitions here are internal to xtrans header/code for a glibc/linux system
xtrans_cppflags="
-I/nyan/xtrans/current/include \
-DHAS_FCHOWN -DHAS_STICKY_DIR_BIT \
"
fi
#===================================================================================================
if test "${libice_inc_dir-unset}" = unset; then
libice_inc_dir=/nyan/libICE/current/include
libice_lib_dir=/nyan/libICE/current/lib
fi
libice_cppflags="-I$libice_inc_dir"
if test "${libice_lib_dir-unset}" = unset; then
libice_lib_dir=/nyan/libICE/current/lib
fi
libice_ldflags="-L$libice_lib_dir -lICE"
#===================================================================================================
if test "${cpp-unset}" = unset; then
cpp="gcc -E \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
"
fi
#===================================================================================================
if test "${slib_cc-unset}" = unset; then
slib_cc="gcc -c \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-std=c11 \
	-pipe -fPIC -O2 -ftls-model=global-dynamic -fpic \
	-static-libgcc"
fi
#===================================================================================================
# we are still using the compiler driver, very bad idea
if test "${slib_ccld-unset}" = unset; then
slib_ccld="gcc \
	-shared \
	-static-libgcc \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-soname=libSM.so.6 \
	-Wl,--no-undefined \
	-Wl,-s \
"
fi
#===================================================================================================
# -DHAVE_ARC4RANDOM_BUF=1 only with very recent glibc
glibc_cppflags="\
-D_GNU_SOURCE \
-DHAVE_INTTYPES_H=1 \
-DHAVE_DLFCN_H=1 \
-DHAVE_STDINT_H=1 \
-DHAVE_STDIO_H=1 \
-DHAVE_STDLIB_H=1 \
-DHAVE_STRINGS_H=1 \
-DHAVE_STRING_H=1 \
-DHAVE_SYS_STAT_H=1 \
-DHAVE_SYS_TYPES_H=1 \
-DHAVE_UNISTD_H=1 \
-DHAVE_SOCKLEN_T=1 \
-DHAVE_STRCASECMP=1 \
"
# we want only unixconn
xtrans_cfg_cppflags="\
-DUNIXCONN=1 \
"
#===================================================================================================
src_files="\
$src_dir/src/sm_auth.c \
$src_dir/src/sm_client.c \
$src_dir/src/sm_error.c \
$src_dir/src/sm_genid.c \
$src_dir/src/sm_manager.c \
$src_dir/src/sm_misc.c \
$src_dir/src/sm_process.c \
"
#===================================================================================================
for f in $src_files
do
	cpp_file=$(basename $f .c).cpp.c
	printf "CPP $f -> $build_dir/$cpp_file\n"
	$cpp -o $build_dir/$cpp_file $f \
		-I$src_dir/include \
		$glibc_cppflags \
		$xorgproto_cppflags \
		$xtrans_cfg_cppflags \
		$xtrans_cppflags \
		$libice_cppflags
done
#===================================================================================================
wait
#===================================================================================================
os=
for f in $src_files
do
	cpp_file=$(basename $f .c).cpp.c
	o_file=$(basename $f .c).o
	os="$os $o_file"
	printf "SLIB_CC $build_dir/$cpp_file -> $build_dir/$o_file\n"
	$slib_cc -o $build_dir/$o_file $build_dir/$cpp_file &
done
#===================================================================================================
wait
#===================================================================================================
# see $archive/src/Makefile.am for the libtool version
printf "SLIB_CCLD $build_dir/libSM.so.6.0.1\n"
$slib_ccld -o $build_dir/libSM.so.6.0.1 $os \
	$libice_ldflags
#===================================================================================================
wait
