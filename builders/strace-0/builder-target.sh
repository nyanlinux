src_name=strace
version=4.22
archive_name=$src_name-$version.tar.xz
url0=https://strace.io/files/$version/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

export PKG_CONFIG_LIBDIR=
export PKG_CONFIG_SYSROOT_DIR=$target_sysroot

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
BUILD_CC=gcc
export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--build=$build_gnu_triple	\
	--host=$target_gnu_triple	\
	--prefix=/nyan/strace/0	
unset CFLAGS
unset CC
unset BUILD_CC

make -j $threads_n
make install DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/$src_name/0/share
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/bin/$src_name

rm -Rf $build_dir $src_dir
OLD_PATH=$PATH
