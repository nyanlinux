src_name=st
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=git://git.suckless.org/$src_name

src_dir=$src_dir_root/$src_name
pkg_dir=/run/pkgs/$src_name
rm -Rf $pkg_dir
mkdir -p /run/pkgs
cp -r $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
/nyan/git/current/bin:\
/nyan/pkgconf/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

if test x$git_commit = x; then
	git checkout --force $git_commit
	git reset --hard
fi

# copy our source configuration and build configuration files
cp -f $nyan_root/builders/$pkg_name/config.h ./config.h
cp -f $nyan_root/builders/$pkg_name/config.mk ./config.mk

export "PKG_CONFIG_LIBDIR=\
/nyan/libXau/current/lib/pkgconfig:\
/nyan/libpthread-stubs/current/lib/pkgconfig:\
/nyan/libxcb/current/lib/pkgconfig:\
/nyan/libX11/current/lib/pkgconfig:\
/nyan/libXrender/current/lib/pkgconfig:\
/nyan/libXft/current/lib/pkgconfig:\
/nyan/xorgproto/current/share/pkgconfig:\
/nyan/libpng/current/lib/pkgconfig:\
/nyan/zlib/current/lib/pkgconfig:\
/nyan/freetype/current/lib/pkgconfig:\
/nyan/nyanuuid/current/lib/pkgconfig:\
/nyan/expat/current/lib/pkgconfig:\
/nyan/fontconfig/current/lib/pkgconfig"

CFLAGS='-O2 -pipe -fPIC'
CC="$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-static-libgcc \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-s \
	-Wl,-rpath-link,\
/nyan/libXau/current/lib:\
/nyan/libxcb/current/lib:\
/nyan/libpng/current/lib:\
/nyan/libXrender/current/lib:\
/nyan/freetype/current/lib:\
/nyan/libXt/current/lib:\
/nyan/libX11/current/lib:\
/nyan/glibc/current/lib"

make \
	PREFIX=/nyan/$src_name/$slot \
	PKG_CONFIG=/nyan/pkgconf/current/bin/pkgconf \
	"CFLAGS=$CFLAGS" \
	"CC=$CC"

mkdir -p /nyan/$src_name/$slot/bin
cp -f ./$src_name /nyan/$src_name/$slot/bin

# the terminfo description
/nyan/ncurses/current/bin/tic $pkg_dir/st.info

#-------------------------------------------------------------------------------
# extra: default color inverted st
cp -f $nyan_root/builders/$pkg_name/config.h.invert ./config.h

make \
	PREFIX=/nyan/$src_name/$slot \
	PKG_CONFIG=/nyan/pkgconf/current/bin/pkgconf \
	"CFLAGS=$CFLAGS" \
	"CC=$CC"

cp -f ./$src_name /nyan/st/$slot/bin/$src_name-invert

unset PKG_CONFIG_LIBDIR
unset CFLAGS
unset CC

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $pkg_dir
