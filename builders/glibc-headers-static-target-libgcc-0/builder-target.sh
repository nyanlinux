src_name=glibc
git_commit=3dfd23eb4bdc7707048b115548e2238dacef064e
git_url0=git://sourceware.org/git/$src_name.git

. $nyan_root/builders/$src_name-common/fragments.sh

# here we use directly the src directory because it's too big just need to
# check the checkout-ed commit or source snapshot is the right one

src_dir=$src_dir_root/$src_name
build_dir=$builds_dir_root/$pkg_name-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

glibc_configure_headers_static_target_libgcc

make install-headers DESTDIR=$target_sysroot

glibc_add_empty_stubs_h

rm -Rf $build_dir
