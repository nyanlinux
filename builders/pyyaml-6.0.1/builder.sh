src_name=PyYAML
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=https://files.pythonhosted.org/packages/cd/e5/af35f7ea75cf72f2cd079c95ee16797de7cd71f29ea7c68ae5ce7be1eda0/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

# we use the PYTHONPATH to tell python where to locate PyYAML module
cd $pkg_dir
mkdir -p /nyan/$src_name/$slot
cp -rf ./lib/yaml /nyan/$src_name/$slot

rm -f $pkgs_dir_root/$archive_name
rm -Rf $pkg_dir
