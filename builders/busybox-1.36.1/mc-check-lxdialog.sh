#!/bin/sh
# Replace Omega trash with other Omega trash.

# What library to link
ldflags()
{
	printf -- '-L/nyan/ncurses/current/lib -lncurses -ltinfo -lpthread\n'
	exit
}

# Where is ncurses.h?
ccflags()
{
	printf -- '-I/nyan/ncurses/current/include/ncurses -I/nyan/ncurses/current/include -DCURSES_LOC="<ncurses.h>" -DNCURSES_WIDECHAR=1\n'
	# Do not exit is it falls back to outputting the LDFLAGS
}

check() {
	# yaya
	exit
}

usage() {
	printf "Usage: $0 [-check compiler options|-ccflags|-ldflags compiler options]\n"
}

if [ $# -eq 0 ]; then
	usage
	exit 1
fi

cc=""
case "$1" in
	"-check")
		shift
		check
		;;
	"-ccflags")
		ccflags
		;;
	"-ldflags")
		ldflags
		;;
	"*")
		usage
		exit 1
		;;
esac
