src_name=busybox
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.bz2
git_url0=https://www.busybox.net/downloads/$archive_name

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$pkg_dir/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkg_dir
cp $src_dir_root/$archive_name $pkg_dir
cd $pkg_dir
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir

cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/make/current/bin:
$PATH"

# our custom config
# XXX: 1.36.1: SIGWINCH is broken for shells
cp $nyan_root/builders/$pkg_name/config $src_dir/.config

make -j $threads_n -C $src_dir \
	"CC=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
		-O2 -fpic -pipe \
		-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include \
		-B/nyan/glibc/current/lib \
		-L/nyan/ncurses/current/lib" \
	"HOSTCC=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
		-O2 -fpic -pipe -static-libgcc \
		-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include \
		-B/nyan/glibc/current/lib \
		-L/nyan/ncurses/current/lib" \
	AR=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-ar \
	STRIP=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-strip \
	busybox
make -C $src_dir CONFIG_PREFIX=/nyan/busybox/$slot \
	"CC=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
		-O2 -fpic -pipe \
		-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include \
		-B/nyan/glibc/current/lib \
		-L/nyan/ncurses/current/lib" \
	"HOSTCC=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
		-O2 -fpic -pipe -static-libgcc \
		-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include \
		-B/nyan/glibc/current/lib \
		-L/nyan/ncurses/current/lib" \
	AR=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-ar \
	STRIP=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-strip \
	install
export PATH=$PATH_SAVED
rm -Rf $build_dir $pkg_dir
