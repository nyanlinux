src_name=nyanmp
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=git://repo.or.cz/$src_name

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -L -r $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/git/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

build_dir=/run/builds/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

#-------------------------------------------------------------------------------
# npa
$target_gnu_triple-gcc -c -static-libgcc -O2 -pipe -fPIC -D_GNU_SOURCE \
	-I$pkg_dir \
	-I/nyan/ffmpeg/current/include \
	-I/nyan/alsa-lib/current/include \
	\
	-idirafter /nyan/glibc/current/include \
	-idirafter /nyan/linux-headers/current/include \
	\
	-o $build_dir/npa.o $pkg_dir/npa/npa.c

# TODO: should build an audio only ffmpeg set of libraries
$target_gnu_triple-gcc -static-libgcc -Wl,-s -pipe -fPIC \
	\
	-L/nyan/alsa-lib/current/lib \
	\
	-L/nyan/ffmpeg/current/lib \
	\
	-L/nyan/zlib/current/lib \
	-L/nyan/dav1d/current/lib \
	-L/nyan/opus/current/lib \
	-L/nyan/libvpx/current/lib \
	-L/nyan/libass/current/lib \
	-L/nyan/freetype/current/lib \
	-L/nyan/fribidi/current/lib \
	-L/nyan/fontconfig/current/lib \
	-L/nyan/libressl/current/lib \
	\
	-B/nyan/glibc/current/lib \
	\
	-o $build_dir/npa $build_dir/npa.o \
	\
	-lasound \
	\
	-lavformat -lavfilter -lpostproc -lavcodec -lswresample -lswscale -lavutil \
	\
	-ldav1d -lvpx -lopus \
	\
	-lfreetype -lfontconfig -lfribidi -lass \
	\
	-ltls -lssl -lcrypto \
	\
	-lz -lgmp \
	\
	-lpthread -lm -ldl -lrt
#-------------------------------------------------------------------------------
# npv
$target_gnu_triple-gcc -c -static-libgcc -O2 -pipe -fPIC -D_GNU_SOURCE -DSTATIC=static\
	-I$pkg_dir \
	-I/nyan/ffmpeg/current/include \
	-I/nyan/alsa-lib/current/include \
	-I/nyan/libxcb/current/include \
	-I/nyan/freetype/current/include/freetype2 \
	\
	-idirafter /nyan/glibc/current/include \
	-idirafter /nyan/linux-headers/current/include \
	\
	-o $build_dir/npv.o $pkg_dir/npv/main.c

$target_gnu_triple-gcc -static-libgcc -Wl,-s -pipe -fPIC \
	\
	-B/nyan/glibc/current/lib \
	\
	-Wl,-rpath-link,/nyan/libXau/current/lib \
	\
	-L/nyan/toolchains/current/lib \
	-L/nyan/ffmpeg/current/lib \
	-L/nyan/alsa-lib/current/lib \
	-L/nyan/zlib/current/lib \
	-L/nyan/libressl/current/lib \
	-L/nyan/libvpx/current/lib \
	-L/nyan/dav1d/current/lib \
	-L/nyan/opus/current/lib \
	-L/nyan/fontconfig/current/lib \
	-L/nyan/freetype/current/lib \
	-L/nyan/libass/current/lib \
	-L/nyan/fribidi/current/lib \
	\
	-o $build_dir/npv $build_dir/npv.o \
	\
	-lasound \
	\
	-lavformat -lavfilter -lpostproc -lavcodec -lswresample -lswscale -lavutil \
	\
	-ldav1d -lvpx -lopus \
	\
	-lfreetype -lfontconfig -lfribidi -lass \
	\
	-ltls -lssl -lcrypto \
	\
	-lz -lgmp \
	\
	-lpthread -lm -ldl -lrt

mkdir -p /nyan/$src_name/$slot/bin
cp -f $build_dir/npa $build_dir/npv /nyan/$src_name/$slot/bin

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $build_dir $pkg_dir
