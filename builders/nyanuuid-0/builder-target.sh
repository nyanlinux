src_name=nyanuuid
git_commit=80beced99f930cc40550e6bb4a6d9e99af55e2e1
git_url0=git://github.com/sylware/$src_name

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

cd $pkg_dir

git checkout --force $git_commit
git reset --hard

build_dir=$builds_dir_root/$pkg_name-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

# we cheat, we cross-build a dynamic binary using the static lib
# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
$pkg_dir/make																								\
	--prefix=/nyan/nyanuuid/0																					\
	--disable-dynamic																						\
	"--bin-cc=$target_gnu_triple-gcc -O2 -pipe -fPIC -c"																		\
	"--bin-ccld=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"	\
	"--libuuid-cc=$target_gnu_triple-gcc -O2 -pipe -fPIC -c"																	\
	"--libuuid-ar=$target_gnu_triple-ar rcs"

cp -r $build_dir/fake_root/* $target_sysroot

# cleanup and tidying
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/bin/uuidgen

rm -Rf $build_dir $pkg_dir
