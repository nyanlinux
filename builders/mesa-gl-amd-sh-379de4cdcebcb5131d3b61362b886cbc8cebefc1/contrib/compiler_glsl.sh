printf "\tbuilding glsl compiler sub-components-->\n"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/compiler
mkdir $build_dir/src/compiler/glsl
mkdir $build_dir/src/compiler/glsl/glcpp
#-------------------------------------------------------------------------------
libglcpp_c_pathnames="\
$src_dir/src/compiler/glsl/glcpp/pp.c \
$src_dir/src/compiler/glsl/glcpp/pp_standalone_scaffolding.c \
$build_dir/src/compiler/glsl/glcpp/glcpp-lex.c \
$build_dir/src/compiler/glsl/glcpp/glcpp-parse.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $libglcpp_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/compiler/glsl/glcpp/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/compiler/glsl/glcpp/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/compiler/glsl/glcpp \
		-I$src_dir/src/compiler/glsl/glcpp \
		-I$build_dir/src/compiler/glsl \
		-I$src_dir/src/compiler/glsl \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mapi \
		-I$src_dir/src/mapi \
		-I$build_dir/src/main \
		-I$src_dir/src/main \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libglcpp_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/compiler/glsl/glcpp/$cpp_filename --> $build_dir/src/compiler/glsl/glcpp$asm_filename\n"
	$cc_s $build_dir/src/compiler/glsl/glcpp/$cpp_filename -o $build_dir/src/compiler/glsl/glcpp/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libglcpp_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/compiler/glsl/glcpp/$o_filename"
	printf "AS $build_dir/src/compiler/glsl/glcpp/$asm_filename --> $build_dir/src/compiler/glsl/glcpp/$o_filename\n"
	$as $build_dir/src/compiler/glsl/glcpp/$asm_filename -o $build_dir/src/compiler/glsl/glcpp/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libglcpp.a $os\n"
$ar_rcs $build_dir/libglcpp.a $os &
#===============================================================================
libglsl_c_pathnames="\
$src_dir/src/compiler/glsl/gl_nir_detect_function_recursion.c \
$src_dir/src/compiler/glsl/gl_nir_lower_atomics.c \
$src_dir/src/compiler/glsl/gl_nir_lower_blend_equation_advanced.c \
$src_dir/src/compiler/glsl/gl_nir_lower_discard_flow.c \
$src_dir/src/compiler/glsl/gl_nir_lower_images.c \
$src_dir/src/compiler/glsl/gl_nir_lower_buffers.c \
$src_dir/src/compiler/glsl/gl_nir_lower_named_interface_blocks.c \
$src_dir/src/compiler/glsl/gl_nir_lower_samplers.c \
$src_dir/src/compiler/glsl/gl_nir_lower_samplers_as_deref.c \
$src_dir/src/compiler/glsl/gl_nir_link_atomics.c \
$src_dir/src/compiler/glsl/gl_nir_link_functions.c \
$src_dir/src/compiler/glsl/gl_nir_link_interface_blocks.c \
$src_dir/src/compiler/glsl/gl_nir_link_uniform_initializers.c \
$src_dir/src/compiler/glsl/gl_nir_link_uniform_blocks.c \
$src_dir/src/compiler/glsl/gl_nir_link_uniforms.c \
$src_dir/src/compiler/glsl/gl_nir_link_varyings.c \
$src_dir/src/compiler/glsl/gl_nir_link_xfb.c \
$src_dir/src/compiler/glsl/gl_nir_linker.c \
$src_dir/src/compiler/glsl/gl_nir_lower_packed_varyings.c \
$src_dir/src/compiler/glsl/gl_nir_lower_xfb_varying.c \
$src_dir/src/compiler/glsl/gl_nir_opt_dead_builtin_varyings.c \
"
# There are still trash thinking coding c++ make them smart instead of filthy
# toxic.
libglsl_cxx_pathnames="\
$build_dir/src/compiler/glsl/glsl_parser.cpp \
$build_dir/src/compiler/glsl/glsl_lexer.cpp \
$src_dir/src/compiler/glsl/ast_array_index.cpp \
$src_dir/src/compiler/glsl/ast_expr.cpp \
$src_dir/src/compiler/glsl/ast_function.cpp \
$src_dir/src/compiler/glsl/ast_to_hir.cpp \
$src_dir/src/compiler/glsl/ast_type.cpp \
$src_dir/src/compiler/glsl/builtin_functions.cpp \
$src_dir/src/compiler/glsl/builtin_types.cpp \
$src_dir/src/compiler/glsl/builtin_variables.cpp \
$src_dir/src/compiler/glsl/glsl_parser_extras.cpp \
$src_dir/src/compiler/glsl/glsl_symbol_table.cpp \
$src_dir/src/compiler/glsl/glsl_to_nir.cpp \
$src_dir/src/compiler/glsl/hir_field_selection.cpp \
$src_dir/src/compiler/glsl/ir_array_refcount.cpp \
$src_dir/src/compiler/glsl/ir_basic_block.cpp \
$src_dir/src/compiler/glsl/ir_builder.cpp \
$src_dir/src/compiler/glsl/ir_clone.cpp \
$src_dir/src/compiler/glsl/ir_constant_expression.cpp \
$src_dir/src/compiler/glsl/ir.cpp \
$src_dir/src/compiler/glsl/ir_equals.cpp \
$src_dir/src/compiler/glsl/ir_expression_flattening.cpp \
$src_dir/src/compiler/glsl/ir_function_detect_recursion.cpp \
$src_dir/src/compiler/glsl/ir_function.cpp \
$src_dir/src/compiler/glsl/ir_hierarchical_visitor.cpp \
$src_dir/src/compiler/glsl/ir_hv_accept.cpp \
$src_dir/src/compiler/glsl/ir_print_visitor.cpp \
$src_dir/src/compiler/glsl/ir_rvalue_visitor.cpp \
$src_dir/src/compiler/glsl/ir_validate.cpp \
$src_dir/src/compiler/glsl/ir_variable_refcount.cpp \
$src_dir/src/compiler/glsl/linker_util.cpp \
$src_dir/src/compiler/glsl/lower_builtins.cpp \
$src_dir/src/compiler/glsl/lower_instructions.cpp \
$src_dir/src/compiler/glsl/lower_jumps.cpp \
$src_dir/src/compiler/glsl/lower_mat_op_to_vec.cpp \
$src_dir/src/compiler/glsl/lower_precision.cpp \
$src_dir/src/compiler/glsl/lower_packing_builtins.cpp \
$src_dir/src/compiler/glsl/lower_subroutine.cpp \
$src_dir/src/compiler/glsl/lower_vec_index_to_cond_assign.cpp \
$src_dir/src/compiler/glsl/lower_vector_derefs.cpp \
$src_dir/src/compiler/glsl/opt_algebraic.cpp \
$src_dir/src/compiler/glsl/opt_dead_builtin_variables.cpp \
$src_dir/src/compiler/glsl/opt_dead_code.cpp \
$src_dir/src/compiler/glsl/opt_dead_code_local.cpp \
$src_dir/src/compiler/glsl/opt_flatten_nested_if_blocks.cpp \
$src_dir/src/compiler/glsl/opt_flip_matrices.cpp \
$src_dir/src/compiler/glsl/opt_function_inlining.cpp \
$src_dir/src/compiler/glsl/opt_if_simplification.cpp \
$src_dir/src/compiler/glsl/opt_minmax.cpp \
$src_dir/src/compiler/glsl/opt_rebalance_tree.cpp \
$src_dir/src/compiler/glsl/opt_tree_grafting.cpp \
$src_dir/src/compiler/glsl/propagate_invariance.cpp \
$src_dir/src/compiler/glsl/string_to_uint_map.cpp \
$src_dir/src/compiler/glsl/serialize.cpp \
$src_dir/src/compiler/glsl/shader_cache.cpp \
"
#-------------------------------------------------------------------------------
for src_pathname in $libglsl_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/compiler/glsl/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/compiler/glsl/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/compiler/glsl \
		-I$src_dir/src/compiler/glsl \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mapi \
		-I$src_dir/src/mapi \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libglsl_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/compiler/glsl/$cpp_filename --> $build_dir/src/compiler/glsl/$asm_filename\n"
	$cc_s $build_dir/src/compiler/glsl/$cpp_filename -o $build_dir/src/compiler/glsl/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libglsl_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/compiler/glsl/$o_filename"
	printf "AS $build_dir/src/compiler/glsl/$asm_filename --> $build_dir/src/compiler/glsl/$o_filename\n"
	$as $build_dir/src/compiler/glsl/$asm_filename -o $build_dir/src/compiler/glsl/$o_filename &
done
#===============================================================================
for src_pathname in $libglsl_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	printf "CXXPP $src_pathname --> $build_dir/src/compiler/glsl/$cxxpp_filename\n"
	$cxxpp $src_pathname -o $build_dir/src/compiler/glsl/$cxxpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$cxx_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/compiler/glsl \
		-I$src_dir/src/compiler/glsl \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mapi \
		-I$src_dir/src/mapi \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libglsl_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	printf "CXX_S $build_dir/src/compiler/glsl/$cxxpp_filename --> $build_dir/src/compiler/glsl/$asm_filename\n"
	$cxx_s $build_dir/src/compiler/glsl/$cxxpp_filename -o $build_dir/src/compiler/glsl/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
# reuse os
for src_pathname in $libglsl_cxx_pathnames
do
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	o_filename=$(basename $src_pathname .cpp).cpp.cxx.s.o
	os="$os $build_dir/src/compiler/glsl/$o_filename"
	printf "AS $build_dir/src/compiler/glsl/$asm_filename --> $build_dir/src/compiler/glsl/$o_filename\n"
	$as $build_dir/src/compiler/glsl/$asm_filename -o $build_dir/src/compiler/glsl/$o_filename &
done
#===============================================================================
wait
#===============================================================================
printf "AR RCS $build_dir/libglsl.a $os\n"
$ar_rcs $build_dir/libglsl.a $os
#===============================================================================
printf "\t<--glsl compiler sub-components built\n"
