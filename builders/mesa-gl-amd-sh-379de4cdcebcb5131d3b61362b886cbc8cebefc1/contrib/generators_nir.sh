printf "Running nir code generators-->\n"
mkdir $build_dir/src
mkdir $build_dir/src/compiler
mkdir $build_dir/src/compiler/nir
#===============================================================================
# removing the braindamaged qsort class
cp $src_dir/src/compiler/nir/nir.c $build_dir/src/compiler/nir/nir.c
sed -i $build_dir/src/compiler/nir/nir.c \
	-e '/u_qsort.h/ d' \
	-e 's/util_qsort_r/qsort_r/' &
#===============================================================================
export PYTHONPATH=$mako
$python3 $src_dir/src/compiler/nir/nir_builder_opcodes_h.py \
>$build_dir/src/compiler/nir/nir_builder_opcodes.h &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/nir/nir_constant_expressions.py \
>$build_dir/src/compiler/nir/nir_constant_expressions.c &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/nir/nir_opcodes_h.py \
>$build_dir/src/compiler/nir/nir_opcodes.h &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/nir/nir_opcodes_c.py \
>$build_dir/src/compiler/nir/nir_opcodes.c &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/nir/nir_opt_algebraic.py \
--out $build_dir/src/compiler/nir/nir_opt_algebraic.c &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/nir/nir_intrinsics_h.py \
--outdir $build_dir/src/compiler/nir &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/nir/nir_intrinsics_c.py \
--outdir $build_dir/src/compiler/nir &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/nir/nir_intrinsics_indices_h.py \
--outdir $build_dir/src/compiler/nir &
unset PYTHONPATH

printf "<--nir code generation done\n"
