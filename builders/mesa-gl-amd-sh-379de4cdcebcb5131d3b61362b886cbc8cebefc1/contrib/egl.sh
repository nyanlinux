# libglvnd EGL provider for xserver glamor, xorg or xwayland
echo "building libglvnd egl provider for dri on drm(kms/gbm/dma-buf) components-->"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/egl
#-------------------------------------------------------------------------------
libegl_c_pathnames="\
$build_dir/src/egl/generate/g_egldispatchstubs.c \
\
$src_dir/src/egl/main/eglapi.c \
$src_dir/src/egl/main/eglarray.c \
$src_dir/src/egl/main/eglconfig.c \
$src_dir/src/egl/main/eglcontext.c \
$src_dir/src/egl/main/eglcurrent.c \
$src_dir/src/egl/main/eglconfigdebug.c \
$src_dir/src/egl/main/egldevice.c \
$src_dir/src/egl/main/egldisplay.c \
$src_dir/src/egl/main/eglglobals.c \
$src_dir/src/egl/main/eglimage.c \
$src_dir/src/egl/main/egllog.c \
$src_dir/src/egl/main/eglsurface.c \
$src_dir/src/egl/main/eglsync.c \
$src_dir/src/egl/main/eglglvnd.c \
$src_dir/src/egl/main/egldispatchstubs.c \
$src_dir/src/egl/drivers/dri2/egl_dri2.c \
$src_dir/src/egl/drivers/dri2/platform_surfaceless.c \
$src_dir/src/egl/drivers/dri2/platform_drm.c \
$src_dir/src/egl/drivers/dri2/platform_device.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $libegl_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/egl/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/egl/$cpp_filename \
		-DHAVE_DRM_PLATFORM=1 \
		-D_EGL_NATIVE_PLATFORM=_EGL_PLATFORM_DRM \
		\
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/frontends/dri \
		-I$src_dir/src/gallium/frontends/dri \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/gbm/backends/dri \
		-I$src_dir/src/gbm/backends/dri \
		-I$build_dir/src/gbm/main \
		-I$src_dir/src/gbm/main \
		-I$build_dir/src/loader_dri3 \
		-I$src_dir/src/loader_dri3 \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src/egl/drivers/dri2 \
		-I$src_dir/src/egl/drivers/dri2 \
		-I$build_dir/src/egl/main \
		-I$src_dir/src/egl/main \
		-I$build_dir/src/egl/generate \
		-I$src_dir/src/egl/generate \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#-------------------------------------------------------------------------------
for src_pathname in $libegl_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/egl/$cpp_filename --> $build_dir/src/egl/$asm_filename\n"
	$cc_s $build_dir/src/egl/$cpp_filename -o $build_dir/src/egl/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libegl_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/egl/$o_filename"
	printf "AS $build_dir/src/egl/$asm_filename --> $build_dir/src/egl/$o_filename\n"
	$as $build_dir/src/egl/$asm_filename -o $build_dir/src/egl/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libegl.a $os\n"
$ar_rcs $build_dir/libegl.a $os
#===============================================================================
wait
#===============================================================================
# if some c++ code was pulled, it is not using the c++ runtime
# TODO: should not use libloader_x11 (unless libloader pulls libloader_x11
# implicitely), but should depend on libpipe_loader_dynamic.
printf "BINUTILS LD $build_dir/to_install/libEGL_mesa.so.0.0.0\n"
$binutils_ld -o $build_dir/to_install/libEGL_mesa.so.0.0.0 \
	-shared \
	-soname=libEGL_mesa.so.0 \
	-Bsymbolic \
	--no-undefined \
	--gc-sections \
	-s \
	$syslib_ld_slib_start_files \
		$cc_ld_slib_start_files \
			--whole-archive \
				$build_dir/libegl.a \
			--no-whole-archive \
			\
			$build_dir/libpipe_loader_dynamic.a \
			$build_dir/libloader.a \
			$build_dir/libxmlconfig.a \
			$build_dir/libmesa_util.a \
			$cc_ld_support_lib \
			\
			--as-needed \
				$build_dir/to_install/libgallium_dri.so \
				$build_dir/to_install/libglapi.so.0.0.0 \
				$build_dir/to_install/libgbm.so.1.0.0 \
				$libdrm_ld_flags \
				$syslibs_libm_ld_flags \
				$syslibs_libdl_ld_flags \
				$syslibs_libpthread_ld_flags \
				$syslibs_libc_ld_flags \
			--no-as-needed \
		$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
#-------------------------------------------------------------------------------
ln -s libEGL_mesa.so.0.0.0 $build_dir/to_install/libEGL_mesa.so.0
#===============================================================================
mkdir $build_dir/to_install/glvnd
mkdir $build_dir/to_install/glvnd/egl_vendor.d
cat >$build_dir/to_install/glvnd/egl_vendor.d/50_mesa.json <<EOF
{
    "file_format_version" : "1.0.0",
    "ICD" : {
        "library_path" : "libEGL_mesa.so.0"
    }
}
EOF
#===============================================================================
mkdir $build_dir/to_install/include/EGL

cp $src_dir/include/EGL/eglext.h      $build_dir/to_install/include/EGL/eglext.h
cp $src_dir/include/EGL/egl.h         $build_dir/to_install/include/EGL/egl.h
cp $src_dir/include/EGL/eglmesaext.h  $build_dir/to_install/include/EGL/eglmesaext.h
cp $src_dir/include/EGL/eglplatform.h $build_dir/to_install/include/EGL/eglplatform.h
#===============================================================================
echo "<--libglvnd egl provider for dri on drm(kms/gbm/dma-buf) components"
