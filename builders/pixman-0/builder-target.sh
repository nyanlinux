src_name=pixman
version=0.34.0
archive_name=$src_name-$version.tar.gz
url0=http://cairographics.org/releases/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

export "PKG_CONFIG_LIBDIR=\
$target_sysroot/nyan/zlib/0/lib/pkgconfig:\
$target_sysroot/nyan/libpng/0/lib/pkgconfig"
export PKG_CONFIG_SYSROOT_DIR=$target_sysroot

export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--build=$build_gnu_triple	\
	--host=$target_gnu_triple	\
	--prefix=/nyan/pixman/0		\
	--enable-shared			\
	--disable-static		\
	--disable-openmp		\
	--enable-mmx			\
	--enable-sse2			\
	--enable-ssse3			\
	--enable-gcc-inline-asm		\
	--disable-gtk			\
	--disable-static-testprogs	\
	--enable-libpng
unset CC
unset CFLAGS

make -j $threads_n
make install DESTDIR=$target_sysroot

# cleanup and tidying
rm -f $target_sysroot/nyan/$src_name/0/lib/*.la 
find $target_sysroot/nyan/$src_name/0 -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then $target_gnu_triple-strip -s $f; fi; done

rm -Rf $build_dir $src_dir
export PATH=$OLD_PATH
