src_name=mudev
mkdir /nyan/$src_name

git_commit=${pkg_name##*-}
slot=$git_commit
mkdir /nyan/$src_name/$slot

git_url0=

pkg_dir=$pkgs_dir_root/$pkg_name
rm -Rf $pkg_dir
src_dir=$src_dir_root/$src_name
cp -r $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
$PATH\
"

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

# We still configure the package in /nyan/mudev/$slot and this "make" script is way too complex.
# It needs pkgconf removal too.
# usb and pci database are currently shared with libpciaccess still required by Xorg.
$pkg_dir/make \
	--prefix=/nyan/$src_name/$slot \
	--usb-database=/share/hwdata/usb.ids \
	--pci-database=/share/hwdata/pci.ids \
	--enable-logging \
	"--bin-cc=gcc -std=c99 -O2 -pipe -fPIC -c -isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include -static-libgcc" \
	"--bin-ccld=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,--as-needed -Wl,-s -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc" \
	"--libudev-cc=gcc -std=c99 -O2 -pipe -fPIC -c -isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include -static-libgcc" \
	"--libudev-ccld=gcc -shared -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-s -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc -Wl,--as-needed -Wl,-soname,libudev.so.0"

rm -Rf /nyan/$src_name/$slot
cp -r $build_dir/fake_root/* /

mkdir /nyan/$src_name/$slot/hwdata
cp -f $src_dir_root/pci.ids /nyan/$src_name/$slot/hwdata/pci.ids
cp -f $src_dir_root/usb.ids /nyan/$src_name/$slot/hwdata/usb.ids

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
