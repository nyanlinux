version=2.30
archive_name=binutils-$version.tar.xz
url0=http://ftpmirror.gnu.org/binutils/$archive_name

src_dir=$src_dir_root/binutils-$version
rm -Rf $src_dir
cd $src_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

ld_conf_opts=--enable-gold=no
gas_conf_opts=--disable-checks
bfd_conf_opts=--with-mmap 

# WARNING: do not configure "with-sysroot" option as gcc compilers driver will
# pass the "sysroot" command line option
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure							\
	--target=$target_gnu_triple					\
	--prefix=$cross_toolchain_static_target_libgcc_dir_root		\
	--with-gmp=$cross_toolchain_dir_root				\
	--with-mpfr=$cross_toolchain_dir_root				\
	--with-mpc=$cross_toolchain_dir_root				\
	--disable-host-shared						\
	--disable-multilib						\
	--enable-plugins						\
	--disable-nls							\
	$ld_conf_opts							\
	$gas_conf_opts							\
	$bfd_conf_opts
unset CFLAGS

make -j $threads_n
#The real installation is in $cross_toolchain_static_target_libgcc_dir_root/$target_gnu_triple, and it is where gcc will look for them at runtime.
#The $target_gnu_triple prefixed binaries in $target_gnu_triple/bin are just for consistency with gcc binaries...
make install

rm -Rf $src_dir $build_dir 
