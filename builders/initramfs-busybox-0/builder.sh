# XXX
# first param is the kernel release
# second param is the path of the build of the linux kernel (for the cpio bin)
# third param is the mounted EFI xfat partition
# fourth param is an optional suffix to the linux kernel release
shift # remove pkg path
if test -z "$1"; then
	printf 'ERROR:missing linux kernel release\n'
	exit 1
else
	printf "INFO:linux kernel release is \"$1\"\n"
fi
if test -z "$2"; then
	printf 'ERROR:missing path of the linux kernel build\n'
	exit 1
else
	printf "INFO:path of the linux kernel build is \"$2\"\n"
fi
if test -z "$3"; then
	printf 'ERROR:missing efi mounted partition path\n'
	exit 1
else
	printf "INFO:efi mounted partition path is \"$3\"\n"
fi
if test -z "$4"; then
	printf "INFO:using suffix \"$4\" to generate the initramfs\n"
fi
pkg_dir=$pkgs_dir_root/$pkg_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root/$pkg_name
#-------------------------------------------------------------------------------
# AuthenticAMD.bin firmware building
printf 'concatening AMD cpu firmware -- START\n'
firmware_dir=$src_dir_root/linux-firmware/amd-ucode
if test ! -d $firmware_dir; then
	printf "ERROR:missing linux-firmware \"$firmware_dir\"\n"
	exit 1
fi
git_url0=git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git

cat $(find $firmware_dir -name '*.bin' | sort) >$pkg_dir/AuthenticAMD.bin
printf 'concatening AMD cpu firmware -- END\n'
#-------------------------------------------------------------------------------
# cpio building
printf "cpio building for linux release $1 using $2/usr/gen_init_cpio -- START\n"
sed -r	-e "s:PKG_DIR:$pkg_dir:g" \
	-e "s:LINUX_RELEASE:$1:g" \
	-e "s:BUILDER_DIR:$nyan_root/builders/$pkg_name:g" \
	<$nyan_root/builders/$pkg_name/cpio.txt.in \
	>$pkg_dir/cpio.txt
$2/usr/gen_init_cpio $pkg_dir/cpio.txt >$pkg_dir/$1.cpio
gzip -9 -f -k $pkg_dir/$1.cpio
printf "cpio building for linux release $1 using $2/usr/gen_init_cpio -- END\n"
#-------------------------------------------------------------------------------
printf "installing initramfs cpio $1 in $3 -- START\n"
cp $pkg_dir/$1.cpio.gz $3/${1}${4}.cpio.gz
if test ! -e $3/$1.efi; then
	printf "WARNING:no linux kernel \"$1.efi\" found on the EFI partition\n"
fi
printf "installing initramfs cpio $1 in $3 -- END\n"

rm -Rf $pkg_dir
