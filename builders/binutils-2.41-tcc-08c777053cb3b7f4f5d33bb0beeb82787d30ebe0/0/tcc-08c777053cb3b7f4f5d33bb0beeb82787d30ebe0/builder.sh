src_name=tinycc
version=${pkg_name##*-}
git_commit=$version
binutils_pkg_version=$(printf "$pkg_path" | grep -E -o 'binutils-[[:digit:].]+')
slot=0
toolchain_name=$binutils_pkg_version-tcc-$version
url0=https://repo.or.cz/$src_name.git

pkg_dir=$pkgs_dir_root/$toolchain_name/tcc-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root/$toolchain_name
cp -Hr $src_dir_root/$src_name $pkgs_dir_root/$toolchain_name/tcc-$version

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/git/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

cd $pkg_dir

if test x$git_commit != x; then
	git reset --hard
	git checkout $git_commit
fi

# fix lib building on unix in separate build dir, been there like forever
cp -f $nyan_root/builders/$toolchain_name/$slot/$pkg_name/Makefile $pkg_dir/lib

build_dir=$builds_dir_root/$toolchain_name/tcc-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# XXX: in order to build the target libtcc, the host tcc must have the target system headers
# builtin with --sysincludepaths.
$pkg_dir/configure \
	--prefix=/nyan/toolchains/binutils-tcc/$toolchain_name/$slot \
	--cross-prefix=$target_gnu_triple- \
	"--cc=gcc \
		-static-libgcc \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-B/nyan/glibc/current/lib \
		-L/nyan/glibc/current/lib \
		-Wl,-s \
		-O2 -pipe -fPIC" \
	--enable-static \
	--sysincludepaths=/nyan/glibc/current/include:/nyan/linux-headers/current/include \
	--libpaths= \
	--crtprefix=
make -j $threads_n
make install

mv -f /nyan/toolchains/binutils-tcc/$toolchain_name/$slot/bin/tcc /nyan/toolchains/binutils-tcc/$toolchain_name/$slot/bin/$target_gnu_triple-tcc
ln -sTf $target_gnu_triple-tcc /nyan/toolchains/binutils-tcc/$toolchain_name/$slot/bin/$target_gnu_triple-cc
	
export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $build_dir $pkg_dir $builds_dir_root/$toolchain_name $pkgs_dir_root/$toolchain_name
