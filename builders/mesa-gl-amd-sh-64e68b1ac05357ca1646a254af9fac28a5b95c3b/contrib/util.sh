echo "building util -->"
#===============================================================================
mkdir $build_dir/src/
mkdir $build_dir/src/util
mkdir $build_dir/src/util/blake3
#-------------------------------------------------------------------------------
# filenames should be different since we build in one directory
libmesa_util_c_pathnames="\
$build_dir/src/util/format_srgb.c \
$build_dir/src/util/format/u_format_table.c \
$src_dir/src/util/anon_file.c \
$src_dir/src/util/bitscan.c \
$src_dir/src/util/blake3/blake3.c \
$src_dir/src/util/blake3/blake3_dispatch.c \
$src_dir/src/util/blake3/blake3_portable.c \
$src_dir/src/util/blob.c \
$src_dir/src/util/build_id.c \
$src_dir/src/util/compress.c \
$src_dir/src/util/cnd_monotonic.c \
$src_dir/src/util/crc32.c \
$src_dir/src/util/disk_cache.c \
$src_dir/src/util/disk_cache_os.c \
$src_dir/src/util/double.c \
$src_dir/src/util/fast_idiv_by_const.c \
$src_dir/src/util/fossilize_db.c \
$src_dir/src/util/futex.c \
$src_dir/src/util/half_float.c \
$src_dir/src/util/hash_table.c \
$src_dir/src/util/helpers.c \
$src_dir/src/util/log.c \
$src_dir/src/util/mesa-blake3.c \
$src_dir/src/util/mesa-sha1.c \
$build_dir/src/util/mesa_cache_db.c \
$src_dir/src/util/mesa_cache_db_multipart.c \
$src_dir/src/util/memstream.c \
$src_dir/src/util/os_file.c \
$src_dir/src/util/os_file_notify.c \
$src_dir/src/util/os_time.c \
$src_dir/src/util/os_memory_fd.c \
$src_dir/src/util/os_misc.c \
$src_dir/src/util/os_socket.c \
$src_dir/src/util/parson.c \
$src_dir/src/util/u_process.c \
$src_dir/src/util/sha1/sha1.c \
$src_dir/src/util/ralloc.c \
$src_dir/src/util/rand_xor.c \
$src_dir/src/util/rb_tree.c \
$src_dir/src/util/register_allocate.c \
$src_dir/src/util/rgtc.c \
$src_dir/src/util/rwlock.c \
$src_dir/src/util/set.c \
$src_dir/src/util/simple_mtx.c \
$src_dir/src/util/slab.c \
$src_dir/src/util/softfloat.c \
$src_dir/src/util/sparse_array.c \
$src_dir/src/util/streaming-load-memcpy.c \
$src_dir/src/util/string_buffer.c \
$src_dir/src/util/strtod.c \
$src_dir/src/util/thread_sched.c \
$src_dir/src/util/u_atomic.c \
$src_dir/src/util/u_dl.c \
$src_dir/src/util/u_debug_memory.c \
$src_dir/src/util/u_dynarray.c \
$src_dir/src/util/u_printf.c \
$src_dir/src/util/format/u_format.c \
$src_dir/src/util/format/u_format_bptc.c \
$src_dir/src/util/format/u_format_etc.c \
$src_dir/src/util/format/u_format_fxt1.c \
$src_dir/src/util/format/u_format_latc.c \
$src_dir/src/util/format/u_format_other.c \
$src_dir/src/util/format/u_format_rgtc.c \
$src_dir/src/util/format/u_format_s3tc.c \
$src_dir/src/util/format/u_format_tests.c \
$src_dir/src/util/format/u_format_yuv.c \
$src_dir/src/util/format/u_format_zs.c \
$src_dir/src/util/u_cpu_detect.c \
$src_dir/src/util/u_call_once.c \
$src_dir/src/util/u_debug.c \
$src_dir/src/util/u_debug_stack.c \
$src_dir/src/util/u_debug_symbol.c \
$src_dir/src/util/u_hash_table.c \
$src_dir/src/util/u_idalloc.c \
$src_dir/src/util/u_math.c \
$src_dir/src/util/u_mm.c \
$src_dir/src/util/u_queue.c \
$src_dir/src/util/u_thread.c \
$build_dir/src/util/perf/u_trace.c \
$src_dir/src/util/u_vector.c \
$src_dir/src/util/u_worklist.c \
$src_dir/src/util/vma.c \
\
$src_dir/src/c11/impl/threads_posix.c \
$src_dir/src/c11/impl/time.c \
"
# from the trashiest and toxiciest coders:
libmesa_util_cxx_pathnames="\
$src_dir/src/util/texcompress_astc_luts.cpp \
$src_dir/src/util/texcompress_astc_luts_wrap.cpp \
"
#-------------------------------------------------------------------------------
for src_pathname in $libmesa_util_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/util/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/util/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util/format \
		-I$src_dir/src/util/format \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libmesa_util_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/util/$cpp_filename --> $build_dir/src/util/$asm_filename\n"
	$cc_s $build_dir/src/util/$cpp_filename -o $build_dir/src/util/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libmesa_util_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/util/$o_filename"
	printf "AS $build_dir/src/util/$asm_filename --> $build_dir/src/util/$o_filename\n"
	$as $build_dir/src/util/$asm_filename -o $build_dir/src/util/$o_filename &
done
#===============================================================================
for src_pathname in $libmesa_util_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	printf "CXXPP $src_pathname --> $build_dir/src/util/$cxxpp_filename\n"
	$cxxpp $src_pathname -o $build_dir/src/util/$cxxpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$cxx_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util/format \
		-I$src_dir/src/util/format \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libmesa_util_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	printf "CXX_S $build_dir/src/util/$cxxpp_filename --> $build_dir/src/util/$asm_filename\n"
	$cxx_s $build_dir/src/util/$cxxpp_filename -o $build_dir/src/util/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
# reuse os
for src_pathname in $libmesa_util_cxx_pathnames
do
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	o_filename=$(basename $src_pathname .cpp).cpp.cxx.s.o
	os="$os $build_dir/src/util/$o_filename"
	printf "AS $build_dir/src/util/$asm_filename --> $build_dir/src/util/$o_filename\n"
	$as $build_dir/src/util/$asm_filename -o $build_dir/src/util/$o_filename &
done
#===============================================================================
# The following is for the ultra complex blake3 hash algorithm.
libmesautil_asm_pathnames="\
$src_dir/src/util/blake3/blake3_sse2_x86-64_unix.S \
$src_dir/src/util/blake3/blake3_sse41_x86-64_unix.S \
$src_dir/src/util/blake3/blake3_avx2_x86-64_unix.S \
$src_dir/src/util/blake3/blake3_avx512_x86-64_unix.S \
"
#------------------------------------------------------------------------------
for src_pathname in $libmesautil_asm_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.s

	printf "CPP $src_pathname --> $build_dir/src/util/blake3/$asm_filename\n"
	$cpp $src_pathname -o $build_dir/src/util/blake3/$asm_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util/format \
		-I$src_dir/src/util/format \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#-------------------------------------------------------------------------------
# reuse os
for src_pathname in $libmesautil_asm_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.s
	o_filename=$(basename $src_pathname .c).cpp.s.o
	os="$os $build_dir/src/util/blake3/$o_filename"

	printf "AS $build_dir/src/util/blake3/$asm_filename --> $build_dir/src/util/blake3/$o_filename\n"
	$as $build_dir/src/util/blake3/$asm_filename -o $build_dir/src/util/blake3/$o_filename &
done
#-------------------------------------------------------------------------------
wait
#-------------------------------------------------------------------------------
printf "AR RCS $build_dir/libmesa_util.a $os\n"
$ar_rcs $build_dir/libmesa_util.a $os
#===============================================================================
printf "CPP $src_dir/src/util/xmlconfig.c --> $build_dir/src/util/xmlconfig.cpp.c\n"
$cpp $src_dir/src/util/xmlconfig.c -o $build_dir/src/util/xmlconfig.cpp.c \
	-DSYSCONFDIR=\"$sysconfdir\" \
	-DDATADIR=\"$datadir_runtime\" \
	\
	-I$cc_internal_fixed_incdir \
	-I$cc_internal_incdir \
	-I$linux_incdir \
	-I$syslib_incdir \
	\
	-I$build_dir/src/util/blake3 \
	-I$src_dir/src/util/blake3 \
	-I$build_dir/src/util \
	-I$src_dir/src/util \
	-I$build_dir/src \
	-I$src_dir/src \
	-I$build_dir/include \
	-I$src_dir/include \
	\
	$syslib_cpp_flags_defs \
	$linux_cpp_flags_defs \
	$cc_builtins_cpp_flags_defs \
	$cc_attributes_cpp_flags_defs \
	$mesa_cpp_flags_defs \
	\
	$external_deps_cpp_flags
#-------------------------------------------------------------------------------
printf "CC_S $build_dir/src/util/xmlconfig.cpp.c --> $build_dir/src/util/xmlconfig.cpp.c.s\n"
$cc_s $build_dir/src/util/xmlconfig.cpp.c -o $build_dir/src/util/xmlconfig.cpp.c.s
#-------------------------------------------------------------------------------
printf "AS $build_dir/src/util/xmlconfig.cpp.c.s --> $build_dir/src/util/xmlconfig.cpp.c.s.o\n"
$as $build_dir/src/util/xmlconfig.cpp.c.s -o $build_dir/src/util/xmlconfig.cpp.c.s.o
#-------------------------------------------------------------------------------
printf "AR RCS $build_dir/libxmlconfig.a $build_dir/src/util/xmlconfig.cpp.c.s.o\n"
$ar_rcs $build_dir/libxmlconfig.a $build_dir/src/util/xmlconfig.cpp.c.s.o &
#===============================================================================
mkdir $build_dir/to_install/drirc.d
cp $src_dir/src/util/00-mesa-defaults.conf $build_dir/to_install/drirc.d/00-mesa-defaults.conf
#===============================================================================
echo "<-- util built"
