echo "building gpu compiler components-->"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/compiler
mkdir $build_dir/src/compiler/glsl
#------------------------------------------------------------------------------
libcompiler_c_pathnames="\
$src_dir/src/compiler/shader_enums.c \
$build_dir/src/compiler/builtin_types.c \
$src_dir/src/compiler/glsl_types.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $libcompiler_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/compiler/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/compiler/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/compiler/glsl \
		-I$src_dir/src/compiler/glsl \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libcompiler_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/compiler/$cpp_filename --> $build_dir/src/compiler/$asm_filename\n"
	$cc_s $build_dir/src/compiler/$cpp_filename -o $build_dir/src/compiler/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libcompiler_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/compiler/$o_filename"
	printf "AS $build_dir/src/compiler/$asm_filename --> $build_dir/src/compiler/$o_filename\n"
	$as $build_dir/src/compiler/$asm_filename -o $build_dir/src/compiler/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libcompiler.a $os\n"
$ar_rcs $build_dir/libcompiler.a $os &
#===============================================================================
. $script_dir/compiler_aco.sh
. $script_dir/compiler_nir.sh
. $script_dir/compiler_glsl.sh
#===============================================================================
echo "<--gpu compiler components built"
