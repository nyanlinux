printf "Running spirv code generators-->\n"
mkdir $build_dir/src
mkdir $build_dir/src/compiler
mkdir $build_dir/src/compiler/spirv

# nir related spirv stuff, even if, here, we build opengl, because nir deals
# with both glsl and spirv

export PYTHONPATH=$mako
$python3 $src_dir/src/compiler/spirv/vtn_gather_types_c.py \
$src_dir/src/compiler/spirv/spirv.core.grammar.json \
$build_dir/src/compiler/spirv/vtn_gather_types.c &

$python3 $src_dir/src/compiler/spirv/spirv_info_gen.py \
--json $src_dir/src/compiler/spirv/spirv.core.grammar.json \
--out-h $build_dir/src/compiler/spirv/spirv_info.h \
--out-c $build_dir/src/compiler/spirv/spirv_info.c &

$python3 $src_dir/src/compiler/spirv/vtn_generator_ids_h.py \
$src_dir/src/compiler/spirv/spir-v.xml \
$build_dir/src/compiler/spirv/vtn_generator_ids.h &
unset PYTHONPATH

printf "<--spirv code generation done\n"
