printf "Running util code generators-->\n"
mkdir $build_dir/src
mkdir $build_dir/src/util
#-------------------------------------------------------------------------------
# removing the braindamaged qsort class
cp $src_dir/src/util/mesa_cache_db.c $build_dir/src/util/mesa_cache_db.c
sed -i $build_dir/src/util/mesa_cache_db.c \
	-e '/u_qsort.h/ d' \
	-e 's/util_qsort_r/qsort_r/' &
#-------------------------------------------------------------------------------
# not enough static TLS anymore, remove the initial-exec forcing
cp $src_dir/src/util/u_thread.h $build_dir/src/util/u_thread.h
patch -i $script_dir/u_thread.h.patch $build_dir/src/util/u_thread.h
#-------------------------------------------------------------------------------
$python3 $src_dir/src/util/format_srgb.py >$build_dir/src/util/format_srgb.c &
#-------------------------------------------------------------------------------
mkdir $build_dir/src/util/format

export PYTHONPATH=$yaml
$python3 $src_dir/src/util/format/u_format_table.py \
--enums \
$src_dir/src/util/format/u_format.yaml \
>$build_dir/src/util/format/u_format_gen.h &

$python3 $src_dir/src/util/format/u_format_table.py \
--header \
$src_dir/src/util/format/u_format.yaml \
>$build_dir/src/util/format/u_format_pack.h &

$python3 $src_dir/src/util/format/u_format_table.py \
$src_dir/src/util/format/u_format.yaml \
>$build_dir/src/util/format/u_format_table.c &
unset PYTHONPATH

export PYTHONPATH=$mako
$python3 $src_dir/src/util/driconf_static.py \
$src_dir/src/util/00-mesa-defaults.conf \
$build_dir/src/util/driconf_static.h &
unset PYTHONPATH
#-------------------------------------------------------------------------------
# a stub since it is not generation&compile-able out cleanely
mkdir $build_dir/src/util/perf
cp $script_dir/u_trace.c $build_dir/src/util/perf
#-------------------------------------------------------------------------------
printf "<--util code generation done\n"
