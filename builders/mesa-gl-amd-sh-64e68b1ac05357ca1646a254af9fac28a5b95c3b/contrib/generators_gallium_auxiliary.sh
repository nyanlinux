printf "Running gallium auxiliary code generators-->\n"
#-------------------------------------------------------------------------------
# wow, some genius adding a new code generator to obsolete code
mkdir $build_dir/src/
mkdir $build_dir/src/gallium
mkdir $build_dir/src/gallium/auxiliary
mkdir $build_dir/src/gallium/auxiliary/driver_trace

$python3 $src_dir/src/gallium/auxiliary/driver_trace/enums2names.py \
	$src_dir/src/gallium/include/pipe/p_defines.h \
	$src_dir/src/gallium/include/pipe/p_video_enums.h \
	$src_dir/src/util/blend.h \
	-C $build_dir/src/gallium/auxiliary/driver_trace/tr_util.c \
	-H $build_dir/src/gallium/auxiliary/driver_trace/tr_util.h \
	-I tr_util.h
#-------------------------------------------------------------------------------
mkdir $build_dir/src/gallium/auxiliary/indices

$python3 $src_dir/src/gallium/auxiliary/indices/u_indices_gen.py \
$build_dir/src/gallium/auxiliary/indices/u_indices_gen.c

$python3 $src_dir/src/gallium/auxiliary/indices/u_unfilled_gen.py \
$build_dir/src/gallium/auxiliary/indices/u_unfilled_gen.c
#-------------------------------------------------------------------------------
printf "<--gallium auxiliary code generation done\n"
