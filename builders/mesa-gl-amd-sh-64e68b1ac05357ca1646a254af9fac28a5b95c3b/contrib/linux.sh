# linux related
if test "${linux_incdir-unset}" = unset; then
linux_incdir=/nyan/linux-headers/current/include
fi
#---------------------------------------------------------------------------------------------------
linux_cpp_flags_defs="\
-DHAVE_DRM=1 \
-DHAVE_LINUX_FUTEX_H=1 \
-DHAVE_MEMFD_CREATE=1 \
-DHAVE_MINCORE=1 \
-DHAVE_FMEMOPEN=1 \
"
