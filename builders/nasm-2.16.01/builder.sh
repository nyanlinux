src_name=nasm
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://www.nasm.us/pub/$src_name/releasebuilds/$archive_name/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

# Must be build into the src directory.
cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export "CC=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
export AR=$target_gnu_triple-ar
./configure \
	--prefix=/nyan/$src_name/$slot
unset CC
unset CFLAGS
unset AR

make -j $threads_n
make install

rm -Rf /nyan/$src_name/$slot/share

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $pkg_dir
