src_name=xkbcomp
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://www.x.org/releases/individual/app/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# XXX: for all X11 based header we do not define MALLOC_0_RETURNS_NULL for glibc allocator

mkdir -p $build_dir/bin
cat >$build_dir/bin/cc <<EOF
#!/bin/sh
exec \
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
\
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
\
-I/nyan/libxkbfile/current/include \
-I/nyan/libX11/current/include \
-I/nyan/xorgproto/current/include \
\
-O2 -pipe -fPIC -static-libgcc -ftls-model=global-dynamic -fpic \
\
-B/nyan/glibc/current/lib \
\
-L/nyan/libxkbfile/current/lib \
-L/nyan/libX11/current/lib \
-L/nyan/glibc/current/lib \
\
"\$@"
EOF
chmod +x $build_dir/bin/cc
export PATH_SAVED=$PATH
export PATH="\
$build_dir/bin:\
$PATH\
"

#/nyan/bison/current/bin/bison --defines=xkbparse.h -o $build_dir/xkbparse.c \
#	$pkg_dir/xkbparse.y

xkbcomp_src_files="\
$pkg_dir/xkbparse.c \
$pkg_dir/action.c \
$pkg_dir/alias.c \
$pkg_dir/compat.c \
$pkg_dir/expr.c \
$pkg_dir/geometry.c \
$pkg_dir/indicators.c \
$pkg_dir/keycodes.c \
$pkg_dir/keymap.c \
$pkg_dir/keytypes.c \
$pkg_dir/listing.c \
$pkg_dir/misc.c \
$pkg_dir/parseutils.c \
$pkg_dir/symbols.c \
$pkg_dir/utils.c \
$pkg_dir/vmod.c \
$pkg_dir/xkbcomp.c \
$pkg_dir/xkbpath.c \
$pkg_dir/xkbscan.c \
"
#---------------------------------------------------------------------------------------------------
# TODO: we are still using the compiler driver, bad
for f in $xkbcomp_src_files
do
	o=$(basename $f .c).o
	os="$os $o"

	printf "CC $f->$o\n"
	cc -c $f -o $build_dir/$o \
		-DDFLT_XKB_CONFIG_ROOT='"/usr/share/X11/xkb"' \
		-DHAVE_STRDUP \
		-DHAVE_STRCASECMP \
		-DPACKAGE_VERSION=\"$version\" &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
# TODO: we are still using the compiler driver, bad
printf "CCLD xkbcomp\n"
cc -o $build_dir/xkbcomp \
	-Wl,-s \
	$os \
	-lxkbfile \
	-lX11
#---------------------------------------------------------------------------------------------------
# XXX: The xserver should have been configured to find xkbcomp properly.
mkdir -p /nyan/$src_name/$slot/bin
cp -f $build_dir/xkbcomp /nyan/$src_name/$slot/bin

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
