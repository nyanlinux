src_name=xorgproto
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=git://anongit.freedesktop.org/xorg/proto/$src_name


pkg_dir=$pkgs_dir_root/$src_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

cd $pkg_dir

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

PATH_SAVED=$PATH
export PATH="\
/nyan/autoconf/current/bin\
:/nyan/automake/current/bin\
:$PATH"
export ACLOCAL_PATH="\
/nyan/pkgconf/current/share/aclocal:\
/nyan/util-macro/current/share/aclocal"
export NOCONFIGURE=1
./autogen.sh
unset NOCONFIGURE
unset ACLOCAL_PATH
export PATH=$PATH_SAVED

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/make/current/bin\
:/nyan/toolchains/current/bin\
:$PATH"

export PKG_CONFIG_LIBDIR=
# require a working compiler because using the common util-macro
export "CC=gcc -isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -static-libgcc \
-Wl,-rpath-link,\
/nyan/glibc/current/lib"
export 'CFLAGS=-O2 -pipe -fPIC'
$pkg_dir/configure \
	--prefix=/nyan/xorgproto/$slot \
	--disable-specs
unset PKG_CONFIG_LIBDIR
unset CC
unset CFLAGS

make
make install

export PATH=$PATH_SAVED
rm -Rf $build_dir $pkg_dir
