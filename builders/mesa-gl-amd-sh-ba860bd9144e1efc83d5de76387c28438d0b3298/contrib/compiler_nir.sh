printf "\tbuilding nir compiler sub-components-->\n"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/compiler
mkdir $build_dir/src/compiler/nir
#------------------------------------------------------------------------------
libnir_c_pathnames="\
$build_dir/src/compiler/spirv/vtn_gather_types.c \
$build_dir/src/compiler/spirv/spirv_info.c \
$build_dir/src/compiler/nir/nir_constant_expressions.c \
$build_dir/src/compiler/nir/nir_opcodes.c \
$build_dir/src/compiler/nir/nir_opt_algebraic.c \
$build_dir/src/compiler/nir/nir_intrinsics.c \
$build_dir/src/compiler/nir/nir.c \
$src_dir/src/compiler/nir/nir_builder.c \
$src_dir/src/compiler/nir/nir_builtin_builder.c \
$src_dir/src/compiler/nir/nir_clone.c \
$src_dir/src/compiler/nir/nir_control_flow.c \
$src_dir/src/compiler/nir/nir_deref.c \
$src_dir/src/compiler/nir/nir_divergence_analysis.c \
$src_dir/src/compiler/nir/nir_dominance.c \
$src_dir/src/compiler/nir/nir_format_convert.c \
$src_dir/src/compiler/nir/nir_from_ssa.c \
$src_dir/src/compiler/nir/nir_fixup_is_exported.c \
$src_dir/src/compiler/nir/nir_gather_info.c \
$src_dir/src/compiler/nir/nir_gather_tcs_info.c \
$src_dir/src/compiler/nir/nir_gather_types.c \
$src_dir/src/compiler/nir/nir_gather_xfb_info.c \
$src_dir/src/compiler/nir/nir_group_loads.c \
$src_dir/src/compiler/nir/nir_gs_count_vertices.c \
$src_dir/src/compiler/nir/nir_functions.c \
$src_dir/src/compiler/nir/nir_inline_uniforms.c \
$src_dir/src/compiler/nir/nir_instr_set.c \
$src_dir/src/compiler/nir/nir_legacy.c \
$src_dir/src/compiler/nir/nir_linking_helpers.c \
$src_dir/src/compiler/nir/nir_liveness.c \
$src_dir/src/compiler/nir/nir_loop_analyze.c \
$src_dir/src/compiler/nir/nir_lower_alu.c \
$src_dir/src/compiler/nir/nir_lower_alu_width.c \
$src_dir/src/compiler/nir/nir_lower_alpha_test.c \
$src_dir/src/compiler/nir/nir_lower_amul.c \
$src_dir/src/compiler/nir/nir_lower_array_deref_of_vec.c \
$src_dir/src/compiler/nir/nir_lower_atomics.c \
$src_dir/src/compiler/nir/nir_lower_atomics_to_ssbo.c \
$src_dir/src/compiler/nir/nir_lower_bitmap.c \
$src_dir/src/compiler/nir/nir_lower_blend.c \
$src_dir/src/compiler/nir/nir_lower_bool_to_bitsize.c \
$src_dir/src/compiler/nir/nir_lower_bool_to_float.c \
$src_dir/src/compiler/nir/nir_lower_bool_to_int32.c \
$src_dir/src/compiler/nir/nir_lower_calls_to_builtins.c \
$src_dir/src/compiler/nir/nir_lower_cl_images.c \
$src_dir/src/compiler/nir/nir_lower_clamp_color_outputs.c \
$src_dir/src/compiler/nir/nir_lower_clip.c \
$src_dir/src/compiler/nir/nir_lower_clip_cull_distance_arrays.c \
$src_dir/src/compiler/nir/nir_lower_clip_disable.c \
$src_dir/src/compiler/nir/nir_lower_clip_halfz.c \
$src_dir/src/compiler/nir/nir_lower_const_arrays_to_uniforms.c \
$src_dir/src/compiler/nir/nir_lower_continue_constructs.c \
$src_dir/src/compiler/nir/nir_lower_convert_alu_types.c \
$src_dir/src/compiler/nir/nir_lower_discard_if.c \
$src_dir/src/compiler/nir/nir_lower_double_ops.c \
$src_dir/src/compiler/nir/nir_lower_drawpixels.c \
$src_dir/src/compiler/nir/nir_lower_fb_read.c \
$src_dir/src/compiler/nir/nir_lower_flatshade.c \
$src_dir/src/compiler/nir/nir_lower_flrp.c \
$src_dir/src/compiler/nir/nir_lower_fp16_conv.c \
$src_dir/src/compiler/nir/nir_lower_frag_coord_to_pixel_coord.c \
$src_dir/src/compiler/nir/nir_lower_fragcolor.c \
$src_dir/src/compiler/nir/nir_lower_fragcoord_wtrans.c \
$src_dir/src/compiler/nir/nir_lower_frexp.c \
$src_dir/src/compiler/nir/nir_lower_global_vars_to_local.c \
$src_dir/src/compiler/nir/nir_lower_goto_ifs.c \
$src_dir/src/compiler/nir/nir_lower_gs_intrinsics.c \
$src_dir/src/compiler/nir/nir_lower_helper_writes.c \
$src_dir/src/compiler/nir/nir_lower_image.c \
$src_dir/src/compiler/nir/nir_lower_image_atomics_to_global.c \
$src_dir/src/compiler/nir/nir_lower_input_attachments.c \
$src_dir/src/compiler/nir/nir_lower_int_to_float.c \
$src_dir/src/compiler/nir/nir_lower_interpolation.c \
$src_dir/src/compiler/nir/nir_lower_is_helper_invocation.c \
$src_dir/src/compiler/nir/nir_lower_load_const_to_scalar.c \
$src_dir/src/compiler/nir/nir_lower_locals_to_regs.c \
$src_dir/src/compiler/nir/nir_lower_idiv.c \
$src_dir/src/compiler/nir/nir_lower_indirect_derefs.c \
$src_dir/src/compiler/nir/nir_lower_int64.c \
$src_dir/src/compiler/nir/nir_lower_io.c \
$src_dir/src/compiler/nir/nir_lower_io_arrays_to_elements.c \
$src_dir/src/compiler/nir/nir_lower_io_to_temporaries.c \
$src_dir/src/compiler/nir/nir_lower_io_to_scalar.c \
$src_dir/src/compiler/nir/nir_lower_io_to_vector.c \
$src_dir/src/compiler/nir/nir_lower_terminate_to_demote.c \
$src_dir/src/compiler/nir/nir_lower_mediump.c \
$src_dir/src/compiler/nir/nir_lower_mem_access_bit_sizes.c \
$src_dir/src/compiler/nir/nir_lower_memcpy.c \
$src_dir/src/compiler/nir/nir_lower_memory_model.c \
$src_dir/src/compiler/nir/nir_lower_multiview.c \
$src_dir/src/compiler/nir/nir_lower_non_uniform_access.c \
$src_dir/src/compiler/nir/nir_lower_packing.c \
$src_dir/src/compiler/nir/nir_lower_passthrough_edgeflags.c \
$src_dir/src/compiler/nir/nir_lower_patch_vertices.c \
$src_dir/src/compiler/nir/nir_lower_phis_to_scalar.c \
$src_dir/src/compiler/nir/nir_lower_pntc_ytransform.c \
$src_dir/src/compiler/nir/nir_lower_point_size.c \
$src_dir/src/compiler/nir/nir_lower_point_size_mov.c \
$src_dir/src/compiler/nir/nir_lower_point_smooth.c \
$src_dir/src/compiler/nir/nir_lower_poly_line_smooth.c \
$src_dir/src/compiler/nir/nir_lower_readonly_images_to_tex.c \
$src_dir/src/compiler/nir/nir_lower_reg_intrinsics_to_ssa.c \
$src_dir/src/compiler/nir/nir_lower_returns.c \
$src_dir/src/compiler/nir/nir_lower_robust_access.c \
$src_dir/src/compiler/nir/nir_lower_samplers.c \
$src_dir/src/compiler/nir/nir_lower_scratch.c \
$src_dir/src/compiler/nir/nir_lower_scratch_to_var.c \
$src_dir/src/compiler/nir/nir_lower_single_sampled.c \
$src_dir/src/compiler/nir/nir_lower_subgroups.c \
$src_dir/src/compiler/nir/nir_lower_sysvals_to_varyings.c \
$src_dir/src/compiler/nir/nir_lower_system_values.c \
$src_dir/src/compiler/nir/nir_lower_task_shader.c \
$src_dir/src/compiler/nir/nir_lower_tess_coord_z.c \
$src_dir/src/compiler/nir/nir_lower_tex.c \
$src_dir/src/compiler/nir/nir_lower_tex_shadow.c \
$src_dir/src/compiler/nir/nir_lower_texcoord_replace.c \
$src_dir/src/compiler/nir/nir_lower_texcoord_replace_late.c \
$src_dir/src/compiler/nir/nir_lower_two_sided_color.c \
$src_dir/src/compiler/nir/nir_lower_ubo_vec4.c \
$src_dir/src/compiler/nir/nir_lower_vars_to_ssa.c \
$src_dir/src/compiler/nir/nir_lower_var_copies.c \
$src_dir/src/compiler/nir/nir_lower_variable_initializers.c \
$src_dir/src/compiler/nir/nir_lower_vec_to_regs.c \
$src_dir/src/compiler/nir/nir_lower_vec3_to_vec4.c \
$src_dir/src/compiler/nir/nir_lower_view_index_to_device_index.c \
$src_dir/src/compiler/nir/nir_lower_viewport_transform.c \
$src_dir/src/compiler/nir/nir_lower_wpos_center.c \
$src_dir/src/compiler/nir/nir_lower_wpos_ytransform.c \
$src_dir/src/compiler/nir/nir_lower_wrmasks.c \
$src_dir/src/compiler/nir/nir_lower_bit_size.c \
$src_dir/src/compiler/nir/nir_lower_uniforms_to_ubo.c \
$src_dir/src/compiler/nir/nir_metadata.c \
$src_dir/src/compiler/nir/nir_mod_analysis.c \
$src_dir/src/compiler/nir/nir_move_output_stores_to_end.c \
$src_dir/src/compiler/nir/nir_move_vec_src_uses_to_dest.c \
$src_dir/src/compiler/nir/nir_normalize_cubemap_coords.c \
$src_dir/src/compiler/nir/nir_opt_access.c \
$src_dir/src/compiler/nir/nir_opt_barriers.c \
$src_dir/src/compiler/nir/nir_opt_clip_cull_const.c \
$src_dir/src/compiler/nir/nir_opt_combine_stores.c \
$src_dir/src/compiler/nir/nir_opt_comparison_pre.c \
$src_dir/src/compiler/nir/nir_opt_conditional_discard.c \
$src_dir/src/compiler/nir/nir_opt_constant_folding.c \
$src_dir/src/compiler/nir/nir_opt_copy_prop_vars.c \
$src_dir/src/compiler/nir/nir_opt_copy_propagate.c \
$src_dir/src/compiler/nir/nir_opt_cse.c \
$src_dir/src/compiler/nir/nir_opt_dce.c \
$src_dir/src/compiler/nir/nir_opt_dead_cf.c \
$src_dir/src/compiler/nir/nir_opt_dead_write_vars.c \
$src_dir/src/compiler/nir/nir_opt_find_array_copies.c \
$src_dir/src/compiler/nir/nir_opt_frag_coord_to_pixel_coord.c \
$src_dir/src/compiler/nir/nir_opt_fragdepth.c \
$src_dir/src/compiler/nir/nir_opt_gcm.c \
$src_dir/src/compiler/nir/nir_opt_generate_bfi.c \
$src_dir/src/compiler/nir/nir_opt_idiv_const.c \
$src_dir/src/compiler/nir/nir_opt_if.c \
$src_dir/src/compiler/nir/nir_opt_intrinsics.c \
$src_dir/src/compiler/nir/nir_opt_large_constants.c \
$src_dir/src/compiler/nir/nir_opt_licm.c \
$src_dir/src/compiler/nir/nir_opt_load_store_vectorize.c \
$src_dir/src/compiler/nir/nir_opt_loop.c \
$src_dir/src/compiler/nir/nir_opt_loop_unroll.c \
$src_dir/src/compiler/nir/nir_opt_memcpy.c \
$src_dir/src/compiler/nir/nir_opt_move.c \
$src_dir/src/compiler/nir/nir_opt_move_discards_to_top.c \
$src_dir/src/compiler/nir/nir_opt_mqsad.c \
$src_dir/src/compiler/nir/nir_opt_non_uniform_access.c \
$src_dir/src/compiler/nir/nir_opt_offsets.c \
$src_dir/src/compiler/nir/nir_opt_peephole_select.c \
$src_dir/src/compiler/nir/nir_opt_phi_precision.c \
$src_dir/src/compiler/nir/nir_opt_preamble.c \
$src_dir/src/compiler/nir/nir_opt_ray_queries.c \
$src_dir/src/compiler/nir/nir_opt_reassociate_bfi.c \
$src_dir/src/compiler/nir/nir_opt_rematerialize_compares.c \
$src_dir/src/compiler/nir/nir_opt_remove_phis.c \
$src_dir/src/compiler/nir/nir_opt_shrink_stores.c \
$src_dir/src/compiler/nir/nir_opt_shrink_vectors.c \
$src_dir/src/compiler/nir/nir_opt_sink.c \
$src_dir/src/compiler/nir/nir_opt_undef.c \
$src_dir/src/compiler/nir/nir_opt_uniform_atomics.c \
$src_dir/src/compiler/nir/nir_opt_varyings.c \
$src_dir/src/compiler/nir/nir_opt_vectorize.c \
$src_dir/src/compiler/nir/nir_opt_vectorize_io.c \
$src_dir/src/compiler/nir/nir_passthrough_gs.c \
$src_dir/src/compiler/nir/nir_passthrough_tcs.c \
$src_dir/src/compiler/nir/nir_phi_builder.c \
$src_dir/src/compiler/nir/nir_print.c \
$src_dir/src/compiler/nir/nir_propagate_invariant.c \
$src_dir/src/compiler/nir/nir_range_analysis.c \
$src_dir/src/compiler/nir/nir_remove_dead_variables.c \
$src_dir/src/compiler/nir/nir_remove_tex_shadow.c \
$src_dir/src/compiler/nir/nir_repair_ssa.c \
$src_dir/src/compiler/nir/nir_search.c \
$src_dir/src/compiler/nir/nir_scale_fdiv.c \
$src_dir/src/compiler/nir/nir_schedule.c \
$src_dir/src/compiler/nir/nir_serialize.c \
$src_dir/src/compiler/nir/nir_split_64bit_vec3_and_vec4.c \
$src_dir/src/compiler/nir/nir_split_per_member_structs.c \
$src_dir/src/compiler/nir/nir_split_var_copies.c \
$src_dir/src/compiler/nir/nir_split_vars.c \
$src_dir/src/compiler/nir/nir_sweep.c \
$src_dir/src/compiler/nir/nir_to_lcssa.c \
$src_dir/src/compiler/nir/nir_trivialize_registers.c \
$src_dir/src/compiler/nir/nir_use_dominance.c \
$src_dir/src/compiler/nir/nir_validate.c \
$src_dir/src/compiler/nir/nir_worklist.c \
$src_dir/src/compiler/nir/nir_xfb_info.h \
$src_dir/src/compiler/spirv/gl_spirv.c \
$src_dir/src/compiler/spirv/spirv_to_nir.c \
$src_dir/src/compiler/spirv/vtn_alu.c \
$src_dir/src/compiler/spirv/vtn_amd.c \
$src_dir/src/compiler/spirv/vtn_bindgen.c \
$src_dir/src/compiler/spirv/vtn_cfg.c \
$src_dir/src/compiler/spirv/vtn_debug.c \
$src_dir/src/compiler/spirv/vtn_glsl450.c \
$src_dir/src/compiler/spirv/vtn_structured_cfg.c \
$src_dir/src/compiler/spirv/vtn_subgroup.c \
$src_dir/src/compiler/spirv/vtn_variables.c \
$src_dir/src/compiler/spirv/vtn_cmat.c \
$src_dir/contrib/vtn_opencl.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $libnir_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/compiler/nir/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/compiler/nir/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/compiler/spirv \
		-I$src_dir/src/compiler/spirv \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libnir_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/compiler/nir/$cpp_filename --> $build_dir/src/compiler/nir/$asm_filename\n"
	$cc_s $build_dir/src/compiler/nir/$cpp_filename -o $build_dir/src/compiler/nir/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libnir_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/compiler/nir/$o_filename"
	printf "AS $build_dir/src/compiler/nir/$asm_filename --> $build_dir/src/compiler/nir/$o_filename\n"
	$as $build_dir/src/compiler/nir/$asm_filename -o $build_dir/src/compiler/nir/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libnir.a $os\n"
$ar_rcs $build_dir/libnir.a $os &
#===============================================================================
printf "\t<--nir compiler sub-components built\n"
