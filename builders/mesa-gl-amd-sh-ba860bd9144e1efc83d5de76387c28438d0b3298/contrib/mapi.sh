echo "building mapi (APIs de/multiplexer) components-->"
#===============================================================================
# "static glapi" static library, only mapi/entry.c in mapi bridge mode
mkdir $build_dir/src
mkdir $build_dir/src/mapi
#------------------------------------------------------------------------------
printf "CPP $src_dir/src/mapi/entry.c --> $build_dir/src/mapi/entry_mode_bridge.cpp.c\n"
$cpp $src_dir/src/mapi/entry.c -o $build_dir/src/mapi/entry_mode_bridge.cpp.c \
	-DMAPI_MODE_BRIDGE=1 \
	-DMAPI_ABI_HEADER=\"$build_dir/src/mapi/glapi/static_glapi_mapi_tmp.h\" \
	\
	-I$cc_internal_fixed_incdir \
	-I$cc_internal_incdir \
	-I$linux_incdir \
	-I$syslib_incdir \
	\
	-I$build_dir/src/util \
	-I$src_dir/src/util \
	-I$build_dir/src \
	-I$src_dir/src \
	-I$build_dir/include \
	-I$src_dir/include \
	\
	$syslib_cpp_flags_defs \
	$linux_cpp_flags_defs \
	$cc_builtins_cpp_flags_defs \
	$cc_attributes_cpp_flags_defs \
	$mesa_cpp_flags_defs \
	\
	$external_deps_cpp_flags
#------------------------------------------------------------------------------
printf "CC_S $build_dir/src/mapi/entry_mode_bridge.cpp.c --> $build_dir/src/mapi/entry_mode_bridge.cpp.c.s\n"
$cc_s $build_dir/src/mapi/entry_mode_bridge.cpp.c -o $build_dir/src/mapi/entry_mode_bridge.cpp.c.s
#------------------------------------------------------------------------------
printf "AS $build_dir/src/mapi/entry_mode_bridge.cpp.c.s --> $build_dir/src/mapi/entry_mode_bridge.cpp.c.s.o\n"
$as $build_dir/src/mapi/entry_mode_bridge.cpp.c.s -o $build_dir/src/mapi/entry_mode_bridge.cpp.c.s.o
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libglapi_static.a $build_dir/src/entry_mode_bridge.cpp.c.s.o\n"
$ar_rcs $build_dir/libglapi_static.a $build_dir/src/mapi/entry_mode_bridge.cpp.c.s.o &
#===============================================================================
# "shared glapi" static library
mapi_glapi_c_pathnames="\
$src_dir/src/mapi/entry.c \
$src_dir/src/mapi/shared-glapi/glapi.c \
$src_dir/src/mapi/shared-glapi/stub.c \
$src_dir/src/mapi/shared-glapi/table.c \
"
mapi_util_c_pathnames="\
$src_dir/src/mapi/u_current.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $mapi_glapi_c_pathnames $mapi_util_c_pathnames
do
	if test "$(basename $src_pathname)" = "entry.c"; then
		cpp_filename=entry_mode_glapi.cpp.c
	else
		cpp_filename=$(basename $src_pathname .c).cpp.c
	fi
	printf "CPP $src_pathname --> $build_dir/src/mapi/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/mapi/$cpp_filename \
		-DMAPI_MODE_GLAPI=1 \
		-DMAPI_ABI_HEADER=\"$build_dir/src/mapi/glapi/shared_glapi_mapi_tmp.h\" \
		\
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/mapi/shared-glapi \
		-I$src_dir/src/mapi/shared-glapi \
		-I$build_dir/src/mapi \
		-I$src_dir/src/mapi \
		-I$build_dir/src/ \
		-I$src_dir/src/ \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $mapi_glapi_c_pathnames $mapi_util_c_pathnames
do
	if test "$(basename $src_pathname)" = "entry.c"; then
		cpp_filename=entry_mode_glapi.cpp.c
		asm_filename=entry_mode_glapi.cpp.c.s
	else
		cpp_filename=$(basename $src_pathname .c).cpp.c
		asm_filename=$(basename $src_pathname .c).cpp.c.s
	fi
	printf "CC_S $build_dir/src/mapi/$cpp_filename --> $build_dir/src/mapi/$asm_filename\n"
	$cc_s $build_dir/src/mapi/$cpp_filename -o $build_dir/src/mapi/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
libglapi_shared=
for src_pathname in $mapi_glapi_c_pathnames $mapi_util_c_pathnames
do
	if test "$(basename $src_pathname)" = "entry.c"; then
		asm_filename=entry_mode_glapi.cpp.c.s
		o_filename=entry_mode_glapi.cpp.c.s.o
	else
		asm_filename=$(basename $src_pathname .c).cpp.c.s
		o_filename=$(basename $src_pathname .c).cpp.c.s.o
	fi
	libglapi_shared="$libglapi_shared $build_dir/src/mapi/$o_filename"
	printf "AS $build_dir/src/mapi/$asm_filename --> $build_dir/src/mapi/$o_filename\n"
	$as $build_dir/src/mapi/$asm_filename -o $build_dir/src/mapi/$o_filename &
done
#------------------------------------------------------------------------------
#===============================================================================
wait
#===============================================================================
printf "AR RCS $build_dir/libglapi_shared.a $libglapi_shared\n"
$ar_rcs $build_dir/libglapi_shared.a $libglapi_shared &
#===============================================================================
echo "<--mapi components built"
