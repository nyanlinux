src_name=elfutils
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.bz2
url0=

# XXX: we are interested only in libelf
# In elfutils there is serious trash/sewer/
# corpo(microsoft?apple?google?ibm?etc?) grade code (wtf is that??)

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

#-------------------------------------------------------------------------------

cd $pkg_dir

# copy the canonical lean build scripts
cp -r $nyan_root/builders/$pkg_name/contrib .

#-------------------------------------------------------------------------------

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
$PATH\
"

target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

cat <<EOF  >$build_dir/local_conf.sh
prefix=/nyan/$src_name/$slot
zlib_cppflags='-I/nyan/zlib/current/include'
slibcc='$target_gnu_triple-gcc \
	-c \
	-std=c99 -O2 -fPIC -pipe -static-libgcc -fvisibility=hidden \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include'
ar='$target_gnu_triple-ar rcs'
EOF

#-------------------------------------------------------------------------------

$pkg_dir/contrib/libelf.sh
cp -r $build_dir/install_root/* /

#-------------------------------------------------------------------------------

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
