src_name=autoconf
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://ftpmirror.gnu.org/$src_name/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/m4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"

export PERL=/nyan/perl/current/bin/perl
$pkg_dir/configure --prefix=/nyan/$src_name/$slot
unset PERL

make
make install

rm -Rf /nyan/$src_name/$slot/share/info
rm -Rf /nyan/$src_name/$slot/share/man

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
