src_name=nyanefivars
git_commit=${pkg_name##*-}
slot=$git_commit
url0=git://repo.or.cz/$src_name

pkg_dir=$pkgs_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -Hr $src_dir_root/$src_name $pkgs_dir_root

PATH_SAVED=$PATH
export PATH="\
/nyan/git/current/bin:\
/nyan/toolchains/binutils-2.36.1-tinycc-0378168c1318352bf13f24f210a23aa2fbeb1895/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-tinycc-0378168c1318352bf13f24f210a23aa2fbeb1895/current/bin/*-tcc)")
target_gnu_triple=${target_gnu_triple%-tcc}

cd $pkg_dir

if test x$git_commit != x; then
	git reset --hard
	git checkout $git_commit
fi

mkdir -p /nyan/$src_name/$slot/bin

$target_gnu_triple-tcc \
	$pkg_dir/main.c \
	-o /nyan/$src_name/current/bin/$src_name
	
export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $build_dir $pkg_dir
