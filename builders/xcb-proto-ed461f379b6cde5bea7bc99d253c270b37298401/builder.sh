src_name=xcb-proto
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=git://anongit.freedesktop.org/xcb/proto

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/xcb/proto
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

cd $pkg_dir

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

PATH_SAVED=$PATH
export PATH="\
/nyan/autoconf/current/bin:\
/nyan/automake/current/bin:\
/nyan/make/current/bin:\
$PATH\
"

export NOCONFIGURE=1
./autogen.sh
unset NOCONFIGURE

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export PYTHON=/nyan/python/current/bin/python3
$pkg_dir/configure --prefix=/nyan/$src_name/$slot
unset PYTHON

make
make install

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
