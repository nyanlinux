printf "\tbuilding aco compiler sub-components-->\n"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/amd
mkdir $build_dir/src/amd/compiler
#------------------------------------------------------------------------------
# Gaben did not get the memo about c++ toxicity. That said, this is orders
# of magnitude less worse than llvm.
libaco_cxx_pathnames="\
$src_dir/src/amd/compiler/aco_form_hard_clauses.cpp \
$src_dir/src/amd/compiler/aco_dead_code_analysis.cpp \
$src_dir/src/amd/compiler/aco_dominance.cpp \
$src_dir/src/amd/compiler/aco_insert_delay_alu.cpp \
$src_dir/src/amd/compiler/aco_instruction_selection.cpp \
$src_dir/src/amd/compiler/aco_instruction_selection_setup.cpp \
$src_dir/src/amd/compiler/aco_interface.cpp \
$src_dir/src/amd/compiler/aco_assembler.cpp \
$src_dir/src/amd/compiler/aco_insert_exec_mask.cpp \
$src_dir/src/amd/compiler/aco_insert_NOPs.cpp \
$src_dir/src/amd/compiler/aco_insert_waitcnt.cpp \
$src_dir/src/amd/compiler/aco_ir.cpp \
$src_dir/src/amd/compiler/aco_jump_threading.cpp \
$src_dir/src/amd/compiler/aco_reduce_assign.cpp \
$src_dir/src/amd/compiler/aco_reindex_ssa.cpp \
$src_dir/src/amd/compiler/aco_register_allocation.cpp \
$src_dir/src/amd/compiler/aco_live_var_analysis.cpp \
$src_dir/src/amd/compiler/aco_lower_branches.cpp \
$src_dir/src/amd/compiler/aco_lower_phis.cpp \
$src_dir/src/amd/compiler/aco_lower_subdword.cpp \
$src_dir/src/amd/compiler/aco_lower_to_cssa.cpp \
$src_dir/src/amd/compiler/aco_lower_to_hw_instr.cpp \
$build_dir/src/amd/compiler/aco_opcodes.cpp \
$src_dir/src/amd/compiler/aco_optimizer.cpp \
$src_dir/src/amd/compiler/aco_optimizer_postRA.cpp \
$src_dir/src/amd/compiler/aco_opt_value_numbering.cpp \
$src_dir/src/amd/compiler/aco_print_asm.cpp \
$src_dir/src/amd/compiler/aco_print_ir.cpp \
$src_dir/src/amd/compiler/aco_repair_ssa.cpp \
$src_dir/src/amd/compiler/aco_scheduler.cpp \
$src_dir/src/amd/compiler/aco_scheduler_ilp.cpp \
$src_dir/src/amd/compiler/aco_ssa_elimination.cpp \
$src_dir/src/amd/compiler/aco_spill.cpp \
$src_dir/src/amd/compiler/aco_statistics.cpp \
$src_dir/src/amd/compiler/aco_validate.cpp \
"
#------------------------------------------------------------------------------
for src_pathname in $libaco_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	printf "CXXPP $src_pathname --> $build_dir/src/amd/compiler/$cxxpp_filename\n"
	$cxxpp $src_pathname -o $build_dir/src/amd/compiler/$cxxpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$cxx_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/amd/compiler \
		-I$src_dir/src/amd/compiler \
		-I$src_dir/src/amd/vulkan \
		-I$build_dir/src/amd/common \
		-I$src_dir/src/amd/common \
		-I$build_dir/src/amd \
		-I$src_dir/src/amd \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libaco_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	printf "CXX_S $build_dir/src/amd/compiler/$cxxpp_filename --> $build_dir/src/amd/compiler/$asm_filename\n"
	$cxx_s $build_dir/src/amd/compiler/$cxxpp_filename -o $build_dir/src/amd/compiler/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
libaco_a=
for src_pathname in $libaco_cxx_pathnames
do
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	o_filename=$(basename $src_pathname .cpp).cpp.cxx.s.o
	libaco_a="$libaco_a $build_dir/src/amd/compiler/$o_filename"
	printf "AS $build_dir/src/amd/compiler/$asm_filename --> $build_dir/src/amd/compiler/$o_filename\n"
	$as $build_dir/src/amd/compiler/$asm_filename -o $build_dir/src/amd/compiler/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libaco.a $libaco_a\n"
$ar_rcs $build_dir/libaco.a $libaco_a &
#===============================================================================
printf "\t<--aco compiler sub-components built\n"
