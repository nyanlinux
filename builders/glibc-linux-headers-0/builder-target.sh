build_dir=$builds_dir_root/$pkg_name-$target_gnu_triple

rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export KBUILD_OUTPUT=$build_dir
make    ARCH=$target_linux_arch					\
        -C $target_sysroot$target_linux_src			\
	INSTALL_HDR_PATH=$target_sysroot/nyan/glibc/0/linux	\
	O=$build_dir						\
	headers_install

rm -Rf $build_dir
