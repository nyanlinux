#include <stdio.h>
#include <vulkan/vulkan_core.h>

const char * vk_Result_to_str(VkResult input)
{
	static char buf[64];
	sprintf(buf, "%ld", input);
	return buf;
}
const char *vk_ObjectType_to_ObjectName(VkObjectType type)
{
	static char buf[64];
	sprintf(buf, "0x%lx", type);
	return buf;
}
const char *vk_ObjectType_to_str(VkObjectType type)
{
	static char buf[64];
	sprintf(buf, "0x%lx", type);
	return buf;
}
const char *vk_ExternalMemoryHandleTypeFlagBits_to_str(enum VkExternalMemoryHandleTypeFlagBits type)
{
	static char buf[64];
	sprintf(buf, "0x%lx", type);
	return buf;
}
