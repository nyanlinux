#ifndef MESA_VK_ENUM_TO_STR_H
#define MESA_VK_ENUM_TO_STR_H
#include <vulkan/vulkan_core.h>
const char * vk_Result_to_str(VkResult input);
const char *vk_ObjectType_to_ObjectName(VkObjectType type);
const char *vk_ObjectType_to_str(VkObjectType type);
const char *vk_ExternalMemoryHandleTypeFlagBits_to_str(enum VkExternalMemoryHandleTypeFlagBits type);
#endif
