src_name=Mako
version=1.0.7
archive_name=$src_name-$version.tar.gz
url0=https://pypi.python.org/packages/eb/f3/67579bb486517c0d49547f9697e36582cd19dafb5df9e687ed8e22de57fa/$archive_name

src_dir=$src_dir_root/$src_name-$version
rm -Rf $src_dir
cd $src_dir_root
tar xf $archive_name

cd $src_dir
cp -rf ./mako /nyan/python2/current/lib/python2.7

rm -Rf $src_dir
