src_name=chrpath
version=0.16
archive_name=${src_name}_$version.orig.tar.gz
url0=http://deb.debian.org/debian/pool/main/c/chrpath/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_dir
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export "CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -static-libgcc \
-Wl,-rpath-link,\
/nyan/glibc/current/lib"
export "CFLAGS=-O2 -pipe -fPIC"
$src_dir/configure --prefix=/nyan/$src_name/0
unset CC
unset CFLAGS

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/0/share
rm -Rf /nyan/$src_name/0/doc
strip -s /nyan/$src_name/0/bin/$src_name

rm -Rf $src_dir $build_dir
