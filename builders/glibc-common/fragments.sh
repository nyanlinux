glibc_fix_pre_conf_ssp()
{
# no stack protector used to build the crossed glibc, please
printf "libc_cv_ssp=no\nlibc_cv_ssp_all=no\nlibc_cv_ssp_strong=no\n" >>$build_dir/config.cache
}

opts_common="\
	--build=$build_gnu_triple							\
	--host=$target_gnu_triple							\
	--prefix=/nyan/glibc/0								\
	--exec-prefix=/nyan/glibc/0							\
	--bindir=/nyan/glibc/0/bin							\
	--sbindir=/nyan/glibc/0/bin							\
	--libexecdir=/nyan/glibc/0/libexec						\
	--sysconfdir=/nyan/glibc/0/etc							\
	--localstatedir=/nyan/glibc/0/var						\
	--libdir=/nyan/glibc/0/lib							\
	--datarootdir=/nyan/glibc/0/share						\
	--datadir=/nyan/glibc/0/share							\
	--localedir=/nyan/glibc/0/share/locale						\
	--with-headers=$target_sysroot/nyan/glibc/0/linux/include			\
	--disable-multi-arch								\
	--disable-build-nscd								\
	--disable-nscd									\
	--disable-timezone-tools							\
	--enable-stack-protector=no							\
	--disable-stackguard-randomization						\
	--enable-kernel=$glibc_oldest_linux_version.$glibc_oldest_linux_major.$glibc_oldest_linux_minor"

# From configure you get a config.make (generated from config.make.in) which
# content will be overridden by the content of configparm file, all that is 
# managed by Makeconfig.
# Those headers are for gcc (compiler specific headers) and libgcc.
glibc_configure_headers_static_target_libgcc()
{
$src_dir/configure							\
	$opts_common							\
	--includedir=/nyan/glibc/0/include-static-target-libgcc-linux
}

glibc_add_empty_stubs_h()
{
# This file is generated based on the glibc build. It contains compile time
# mecanics to tell the devs if they are using libc functions which is actually
# empty(=stubs).
# AKA: "wtf, you are blowing away ez bootstraping for *THAT*, you broken brains!"
touch $target_sysroot/nyan/glibc/0/include-static-target-libgcc-linux/gnu/stubs.h
}

glibc_fix_iconvconfig()
{
echo "sbindir=/nyan/glibc/0/bin" >>$build_dir/configparms
}

glibc_configure()
{
# bash/perl/python are evil, force bash to /bin/sh (could be dash)
# the glibc system scripts (i.e. ldd) will be configured to use /bin/sh
export BASH_SHELL=/bin/sh
export 'CFLAGS=-O2 -pipe -fPIC'
# you must have perl for the git version: idotic code generation
export "PERL=$sdk_perl_path/bin/perl"
# we have to force rootsbindir, or it will be configure to sbin ignoring
# autoconf settings
$src_dir/configure					\
	$opts_common					\
	--includedir=/nyan/glibc/0/include-linux	\
	libc_cv_rootsbindir=/nyan/glibc/0/bin
}
unset PERL
unset CFLAGS
unset BASH_SHELL
