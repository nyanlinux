src_name=gcc
version=7.3.0
archive_name=$src_name-$version.tar.xz
url0=ftp://ftp.lip6.fr/pub/$src_name/releases/$src_name-$version/$archive_name

. $nyan_root/builders/gcc-common/fragments.sh

src_dir=$src_dir_root/$src_name-$version
rm -Rf $src_dir
cd $src_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

# work around gcc fixinclude system headers location bug (only for gcc<8)
# see gcc-common, we don't define sysroot here because it's a native compiler.
# trick the fixinclude garbage
restore_build_system_hdrs=no
if $(readlink -f /nyan/glibc/current/include-linux >/dev/null 2>&1); then
	mv -f /nyan/glibc/current/include-linux /nyan/glibc/current/include-linux.BK || true
	restore_build_system_hdrs=yes
fi
mkdir -p /nyan/glibc/current
ln -fs $target_sysroot/nyan/glibc/current/include-linux /nyan/glibc/current/include-linux

gcc_compilers_cross_configure

make -j $threads_n all-gcc
make install-gcc DESTDIR=$target_sysroot

# restore any existing build system includes
if test "x$restore_build_system_hdrs" = "xyes"; then
	mv -f /nyan/glibc/current/include-linux.BK /nyan/glibc/current/include-linux || true
else
	rm -f /nyan/glibc/current/include-linux
fi
rmdir -p /nyan/glibc/current || true

make all-target-libgcc
make install-target-libgcc DESTDIR=$target_sysroot

make all-target-libstdc++-v3
make install-target-libstdc++-v3 DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/toolchains/0/share/info
rm -Rf $target_sysroot/nyan/toolchains/0/share/man
rm -Rf $target_sysroot/nyan/toolchains/0/share/gcc*
rmdir -p $target_sysroot/nyan/toolchains/0/share || true
find $target_sysroot/nyan/toolchains/0 -type f -name '*.la' | xargs rm -f
find $target_sysroot/nyan/toolchains/0 -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then $target_gnu_triple-strip -s $f; fi; done

rm -Rf $build_dir $src_dir

export PATH=$OLD_PATH
