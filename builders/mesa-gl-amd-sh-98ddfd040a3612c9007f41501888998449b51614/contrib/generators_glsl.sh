printf "Running glsl code generators-->\n"
mkdir $build_dir/src
mkdir $build_dir/src/compiler
mkdir $build_dir/src/compiler/glsl

export PYTHONPATH=$mako
$python3 $src_dir/src/compiler/glsl/ir_expression_operation.py enum \
>$build_dir/src/compiler/glsl/ir_expression_operation.h &
unset PYTHONPATH
#===============================================================================
mkdir $build_dir/src/compiler/glsl/glcpp

$bison \
-o $build_dir/src/compiler/glsl/glcpp/glcpp-parse.c \
-p glcpp_parser_ \
--defines=$build_dir/src/compiler/glsl/glcpp/glcpp-parse.h \
$src_dir/src/compiler/glsl/glcpp/glcpp-parse.y  &
#------------------------------------------------------------------------------
$flex \
-o $build_dir/src/compiler/glsl/glcpp/glcpp-lex.c \
$src_dir/src/compiler/glsl/glcpp/glcpp-lex.l &
#===============================================================================
$bison \
-o $build_dir/src/compiler/glsl/glsl_parser.cpp \
-p _mesa_glsl_ \
--defines=$build_dir/src/compiler/glsl/glsl_parser.h \
$src_dir/src/compiler/glsl/glsl_parser.yy &
#------------------------------------------------------------------------------
$flex \
-o $build_dir/src/compiler/glsl/glsl_lexer.cpp \
$src_dir/src/compiler/glsl/glsl_lexer.ll &
#------------------------------------------------------------------------------
export PYTHONPATH=$mako
$python3 $src_dir/src/compiler/glsl/ir_expression_operation.py constant \
>$build_dir/src/compiler/glsl/ir_expression_operation_constant.h &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/glsl/ir_expression_operation.py strings \
>$build_dir/src/compiler/glsl/ir_expression_operation_strings.h &
unset PYTHONPATH
#------------------------------------------------------------------------------
$python3 $src_dir/src/util/xxd.py \
$src_dir/src/compiler/glsl/float64.glsl \
$build_dir/src/compiler/glsl/float64_glsl.h \
-n float64_source &
#------------------------------------------------------------------------------
$python3 $src_dir/src/util/xxd.py \
$src_dir/src/compiler/glsl/CrossPlatformSettings_piece_all.glsl \
$build_dir/src/compiler/glsl/cross_platform_settings_piece_all.h \
-n cross_platform_settings_piece_all_header &
#------------------------------------------------------------------------------
$python3 $src_dir/src/util/xxd.py \
$src_dir/src/compiler/glsl/bc1.glsl \
$build_dir/src/compiler/glsl/bc1_glsl.h \
-n bc1_source &
#------------------------------------------------------------------------------
$python3 $src_dir/src/util/xxd.py \
$src_dir/src/compiler/glsl/bc4.glsl \
$build_dir/src/compiler/glsl/bc4_glsl.h \
-n bc4_source &
#------------------------------------------------------------------------------
$python3 $src_dir/src/util/xxd.py \
$src_dir/src/compiler/glsl/etc2_rgba_stitch.glsl \
$build_dir/src/compiler/glsl/etc2_rgba_stitch_glsl.h \
-n etc2_rgba_stitch_source &
#------------------------------------------------------------------------------
$python3 $src_dir/src/util/xxd.py \
$src_dir/src/compiler/glsl/astc_decoder.glsl \
$build_dir/src/compiler/glsl/astc_glsl.h \
-n astc_source &
#------------------------------------------------------------------------------
printf "<--glsl code generation done\n"
