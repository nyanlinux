printf "\tbuilding targets sub-components-->\n"
#===============================================================================
mkdir $build_dir/src/gallium/
mkdir $build_dir/src/gallium/targets
mkdir $build_dir/src/gallium/targets/dri
#-------------------------------------------------------------------------------
printf "CPP $src_dir/src/gallium/targets/dri/dri_target.c --> $build_dir/src/gallium/targets/dri/dri_target.cpp.c\n"
$cpp $src_dir/src/gallium/targets/dri/dri_target.c -o $build_dir/src/gallium/targets/dri/dri_target.cpp.c \
	-DGALLIUM_RADEONSI \
	\
	-I$cc_internal_fixed_incdir \
	-I$cc_internal_incdir \
	-I$linux_incdir \
	-I$syslib_incdir \
	\
	-I$build_dir/src/gallium/frontends/dri \
	-I$src_dir/src/gallium/frontends/dri \
	-I$build_dir/src/gallium/winsys \
	-I$src_dir/src/gallium/winsys \
	-I$build_dir/src/gallium/drivers \
	-I$src_dir/src/gallium/drivers \
	-I$build_dir/src/gallium/auxiliary \
	-I$src_dir/src/gallium/auxiliary \
	-I$build_dir/src/gallium/include \
	-I$src_dir/src/gallium/include \
	-I$build_dir/src/mesa/drivers/dri/common \
	-I$src_dir/src/mesa/drivers/dri/common \
	-I$build_dir/src/mesa \
	-I$src_dir/src/mesa \
	-I$build_dir/src/util \
	-I$src_dir/src/util \
	-I$build_dir/src \
	-I$src_dir/src \
	-I$build_dir/include \
	-I$src_dir/include \
	\
	$syslib_cpp_flags_defs \
	$linux_cpp_flags_defs \
	$cc_builtins_cpp_flags_defs \
	$cc_attributes_cpp_flags_defs \
	$mesa_cpp_flags_defs \
	\
	$external_deps_cpp_flags
#------------------------------------------------------------------------------
printf "CC_S $build_dir/src/gallium/targets/dri/dri_target.cpp.c --> $build_dir/src/gallium/targets/dri/dri_target.cpp.c.s\n"
$cc_s $build_dir/src/gallium/targets/dri/dri_target.cpp.c -o $build_dir/src/gallium/targets/dri/dri_target.cpp.c.s
#------------------------------------------------------------------------------
printf "AS $build_dir/src/gallium/targets/dri/dri_target.cpp.c.s --> $build_dir/src/gallium/targets/dri/dri_target.cpp.c.s.o\n"
$as $build_dir/src/gallium/targets/dri/dri_target.cpp.c.s -o $build_dir/src/gallium/targets/dri/dri_target.cpp.c.s.o
#===============================================================================
# This is the opengl common ("mesa") dri gallium driver. One dri shared object
# can contain more than one dri driver. Inclusion selection is done in the
# dri dri_target.c file (we have only the radeonsi one, selected with
# GALLIUM_RADEONSI. Nowadays, only one driver or a very small subset of drivers
# is in one dri shared object.
# This is only for _xorg_ drivers supporting glamor, the other _xorg_ drivers
# must use dril.
#-------------------------------------------------------------------------------
printf "BINUTILS LD $build_dir/to_install/libgallium_dri.so\n"
$binutils_ld -o $build_dir/to_install/libgallium_dri.so  \
	-shared \
	-soname libgallium_dri.so \
	--version-script $build_dir/src/gallium/targets/dri/dri.sym \
	--no-undefined \
	--gc-sections \
	-s \
	$syslib_ld_slib_start_files \
		$cc_ld_slib_start_files \
			$build_dir/src/gallium/targets/dri/dri_target.cpp.c.s.o \
			\
			--whole-archive \
				$build_dir/libdricommon.a \
				$build_dir/libdri.a \
			--no-whole-archive \
			\
			--start-group \
				$build_dir/libmesa_gallium.a \
				$build_dir/libglsl.a \
				$build_dir/libglcpp.a \
				$build_dir/libnir.a \
				$build_dir/libaco.a \
				$build_dir/libcompiler.a \
				\
				$build_dir/libgallium.a \
				$build_dir/libgalliumvl.a \
				\
				$build_dir/libpipe_loader_static.a \
				$build_dir/libloader.a \
				$build_dir/libxmlconfig.a \
				\
				$build_dir/libradeonsi.a \
				$libradeonsi_gfx_libs \
				$build_dir/libmesa_util.a \
				$build_dir/libgallium_winsys_amdgpu_drm.a \
				$build_dir/libgallium_winsys_sw_dri.a \
				$build_dir/libgallium_winsys_sw_kms_dri.a \
				$build_dir/libgallium_winsys_sw_null.a \
				$build_dir/libgallium_winsys_sw_wrapper.a \
				$build_dir/libaddrlib.a \
				$build_dir/libamd_common.a \
				\
				$build_dir/libglapi_shared.a \
				\
				$libelf_static_ld_flags \
				$zlib_static_ld_flags \
				\
				$cxx_runtime_lib \
				$cc_ld_support_lib \
				$cc_ld_eh_support_lib \
			--end-group \
			\
			--as-needed \
				$libxcb_ld_flags \
				$libxshmfence_ld_flags \
				$libdrm_amdgpu_ld_flags \
				$libdrm_ld_flags \
				$syslibs_libm_ld_flags \
				$syslibs_libdl_ld_flags \
				$syslibs_libpthread_ld_flags \
				$syslibs_libc_ld_flags \
			--no-as-needed \
		$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
#===============================================================================
printf "\t<--targets sub-components built\n"
