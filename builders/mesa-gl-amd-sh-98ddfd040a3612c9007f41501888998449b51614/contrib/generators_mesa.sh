printf "Running mesa code generators-->\n"
#===============================================================================
# mesa/program
mkdir $build_dir/src
mkdir $build_dir/src/mesa
mkdir $build_dir/src/mesa/program
#-------------------------------------------------------------------------------
$flex \
-o $build_dir/src/mesa/program/lex.yy.c \
$src_dir/src/mesa/program/program_lexer.l &
#-------------------------------------------------------------------------------
$bison \
-o $build_dir/src/mesa/program/program_parse.tab.c \
--defines=$build_dir/src/mesa/program/program_parse.tab.h \
$src_dir/src/mesa/program/program_parse.y &
#===============================================================================
# mesa/main
mkdir $build_dir/src/mesa/main
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/gl_table.py \
-f $src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
-m dispatch \
>$build_dir/src/mesa/main/dispatch.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/gl_marshal_h.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml 8 \
>$build_dir/src/mesa/main/marshal_generated.h &
#===============================================================================
# mesa
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mesa/main/get_hash_generator.py \
-f $src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
>$build_dir/src/mesa/get_hash.h &
#-------------------------------------------------------------------------------
export PYTHONPATH="$mako"
$python3 $src_dir/src/mesa/main/format_fallback.py \
$src_dir/src/mesa/main/formats.csv \
$build_dir/src/mesa/format_fallback.c &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mesa/main/format_info.py \
$src_dir/src/mesa/main/formats.csv \
>$build_dir/src/mesa/format_info.h &
unset PYTHONPATH
#===============================================================================
printf "<--mesa code generation done\n"
