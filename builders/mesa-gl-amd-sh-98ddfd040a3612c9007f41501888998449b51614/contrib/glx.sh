# client libglvnd glx provider, direct rendering mode with dri
echo "building client libglvnd glx provider components-->"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/glx
#-------------------------------------------------------------------------------
# filenames should be different since we build in one directory
libglx_c_pathnames="\
$build_dir/src/mapi/glapi/indirect.c \
$build_dir/src/mapi/glapi/indirect_init.c \
$build_dir/src/mapi/glapi/indirect_size.c \
\
$src_dir/src/glx/glxglvnd.c \
$src_dir/src/glx/drisw_glx.c \
$src_dir/src/glx/g_glxglvnddispatchfuncs.c \
\
$src_dir/src/glx/clientattrib.c \
$src_dir/src/glx/clientinfo.c \
$src_dir/src/glx/compsize.c \
$src_dir/src/glx/create_context.c \
$src_dir/src/glx/eval.c \
$src_dir/src/glx/glx_error.c \
$src_dir/src/glx/glx_pbuffer.c \
$src_dir/src/glx/glx_query.c \
$src_dir/src/glx/glxcmds.c \
$src_dir/src/glx/glxconfig.c \
$src_dir/src/glx/glxcurrent.c \
$src_dir/src/glx/glxext.c \
$src_dir/src/glx/glxextensions.c \
$src_dir/src/glx/glxhash.c \
$src_dir/src/glx/indirect_glx.c \
$src_dir/src/glx/indirect_texture_compression.c \
$src_dir/src/glx/indirect_transpose_matrix.c \
$src_dir/src/glx/indirect_vertex_array.c \
$src_dir/src/glx/indirect_vertex_program.c \
$src_dir/src/glx/indirect_window_pos.c \
$src_dir/src/glx/pixel.c \
$src_dir/src/glx/pixelstore.c \
$src_dir/src/glx/query_renderer.c \
$src_dir/src/glx/render2.c \
$src_dir/src/glx/renderpix.c \
$src_dir/src/glx/single2.c \
$src_dir/src/glx/singlepix.c \
$src_dir/src/glx/vertarr.c \
$src_dir/src/glx/dri_common.c \
$src_dir/src/glx/dri_common_query_renderer.c \
$src_dir/src/glx/xfont.c \
$src_dir/src/glx/dri3_glx.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $libglx_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/glx/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/glx/$cpp_filename \
		-D_REENTRANT=1 \
		-DGL_LIB_NAME=\"libGLX_mesa.so.0.0.0\" \
		\
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/frontends/dri \
		-I$src_dir/src/gallium/frontends/dri \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mapi/glapi \
		-I$src_dir/src/mapi/glapi \
		-I$build_dir/src/glx \
		-I$src_dir/src/glx \
		-I$build_dir/src/loader_dri3 \
		-I$src_dir/src/loader_dri3 \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src/x11 \
		-I$src_dir/src/x11 \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include/GL/internal \
		-I$src_dir/include/GL/internal \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libglx_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/glx/$cpp_filename --> $build_dir/src/glx/$asm_filename\n"
	$cc_s $build_dir/src/glx/$cpp_filename -o $build_dir/src/glx/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libglx_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/glx/$o_filename"
	printf "AS $build_dir/src/glx/$asm_filename --> $build_dir/src/glx/$o_filename\n"
	$as $build_dir/src/glx/$asm_filename -o $build_dir/src/glx/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libglx.a $os\n"
$ar_rcs $build_dir/libglx.a $os
#===============================================================================
# This is the client shared lib, which does routing among the enabled GL 
# related APIs and hardware drivers (in our case, hardware supported by
# gallium drivers). For the glx API, it deals with indirect/direct rendering
# too.
#-------------------------------------------------------------------------------
# XXX: if some c++ were pulled in, it is not using the c++ runtime.
# TODO:should depends on libpipe_loader_dynamic
printf "BINUTILS LD $build_dir/to_install/libGLX_mesa.so.0.0.0\n"
$binutils_ld -o $build_dir/to_install/libGLX_mesa.so.0.0.0  \
	-shared \
	-soname=libGLX_mesa.so.0 \
	-Bsymbolic \
	--no-undefined \
	--gc-sections \
	-s \
	$syslib_ld_slib_start_files \
		$cc_ld_slib_start_files \
			--whole-archive \
				$build_dir/libglx.a \
			--no-whole-archive \
			\
			$build_dir/libglapi_static.a \
			$build_dir/libpipe_loader_dynamic.a \
			$build_dir/libloader_x11.a \
			$build_dir/libloader.a \
			$build_dir/libxmlconfig.a \
			$build_dir/libmesa_util.a \
			\
			--as-needed \
				$build_dir/to_install/libgallium_dri.so \
				$libx11_ld_flags \
				$libx11_xcb_ld_flags \
				$libxcb_ld_flags \
				$libxext_ld_flags \
				$libxxf86vm_ld_flags \
				$libxshmfence_ld_flags \
				$libxfixes_ld_flags \
				$libdrm_ld_flags \
				$syslibs_libm_ld_flags \
				$syslibs_libdl_ld_flags \
				$syslibs_libpthread_ld_flags \
				$syslibs_libc_ld_flags \
			--no-as-needed \
		$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
#-------------------------------------------------------------------------------
ln -s libGLX_mesa.so.0.0.0 $build_dir/to_install/libGLX_mesa.so.0
#===============================================================================
mkdir $build_dir/to_install/include/GL
cp $src_dir/include/GL/glcorearb.h $build_dir/to_install/include/GL/glcorearb.h
cp $src_dir/include/GL/gl.h        $build_dir/to_install/include/GL/gl.h
cp $src_dir/include/GL/glext.h     $build_dir/to_install/include/GL/glext.h
cp $src_dir/include/GL/glx.h       $build_dir/to_install/include/GL/glx.h
cp $src_dir/include/GL/glxext.h    $build_dir/to_install/include/GL/glxext.h
#------------------------------------------------------------------------------
mkdir $build_dir/to_install/include/GL/internal
cp $src_dir/include/GL/internal/dri_interface.h $build_dir/to_install/include/GL/internal/dri_interface.h
#------------------------------------------------------------------------------
mkdir $build_dir/to_install/include/KHR
cp $src_dir/include/KHR/khrplatform.h $build_dir/to_install/include/KHR/khrplatform.h
#===============================================================================
echo "<--client libglvnd glx provider components built"
