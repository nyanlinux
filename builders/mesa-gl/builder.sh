src_name=mesa
mkdir /nyan/mesa-gl
mkdir /nyan/mesa-gl/$slot
git_url0=git://anongit.freedesktop.org/mesa/$src_name

pkg_dir=/run/pkgs/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p /run/pkgs
cp -Hr $src_dir $pkg_dir

#-------------------------------------------------------------------------------

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/git/current/bin:\
$PATH\
"

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

# copy the canonical lean build scripts
cp -r $nyan_root/builders/$pkg_name/contrib .

#-------------------------------------------------------------------------------

build_dir=/run/builds/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

#-------------------------------------------------------------------------------

$pkg_dir/contrib/x86_64_linux_glibc_amdgpu.sh

#-------------------------------------------------------------------------------

rm -Rf /nyan/mesa-gl/$slot/include
cp -r $build_dir/to_install/include /nyan/mesa-gl/$slot/include

mkdir /nyan/mesa-gl/$slot/gbm
cp $build_dir/to_install/gbm/dri_gbm.so /nyan/mesa-gl/$slot/gbm/dri_gbm.so

mkdir /nyan/mesa-gl/$slot/drirc.d
cp $build_dir/to_install/drirc.d/00-mesa-defaults.conf /nyan/mesa-gl/$slot/drirc.d/00-mesa-defaults.conf

mkdir /nyan/mesa-gl/$slot/glvnd
mkdir /nyan/mesa-gl/$slot/glvnd/egl_vendor.d
cp $build_dir/to_install/glvnd/egl_vendor.d/50_mesa.json /nyan/mesa-gl/$slot/glvnd/egl_vendor.d/50_mesa.json

cp $build_dir/to_install/libEGL_mesa.so.0.0.0 /nyan/mesa-gl/$slot/libEGL_mesa.so.0.0.0
cp $build_dir/to_install/libGLX_mesa.so.0.0.0 /nyan/mesa-gl/$slot/libGLX_mesa.so.0.0.0
cp $build_dir/to_install/libgbm.so.1.0.0      /nyan/mesa-gl/$slot/libgbm.so.1.0.0
cp $build_dir/to_install/libgallium_dri.so    /nyan/mesa-gl/$slot/libgallium_dri.so

# the xserver will use directly libgbm 
ln -s libgbm.so.1.0.0 /nyan/mesa-gl/$slot/libgbm.so

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
