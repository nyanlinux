src_name=expat
major=2
minor=5
micro=0
version=$major.$minor.$micro
slot=$version
archive_name=$src_name-$version.tar.xz
url0=https://github.com/libexpat/libexpat/releases/download/R_$major_$minor_$micro/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export 'CFLAGS=-O2 -pipe -fPIC'
export "CC=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export AR=$target_gnu_triple-ar
export NM=$target_gnu_triple-nm
$pkg_dir/configure \
	--prefix=/nyan/expat/$slot \
	--disable-shared
unset CFLAGS
unset CC
unset AR
unset NM

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/$slot/share
rm -f /nyan/$src_name/$slot/lib/*.la

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
