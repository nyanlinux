#!/bin/sh

# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the script at the top of mesa source tree, create somewhere else
# a build directory, cd into it, and call from there this script.

# XXX: the defaults are for our custom distro
#===================================================================================================
# build dir and src dir
build_dir=$(readlink -f .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(readlink -f $(dirname $0)/..)
echo "src_dir=$src_dir"
#===================================================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===================================================================================================
if test "${version-unset}" = unset; then
	version=unknown
fi
#===================================================================================================
if test "${prefix-unset}" = unset; then
prefix="/nyan/libX11/$version"
fi
#===================================================================================================
if test "${inc_dir-unset}" = unset; then
inc_dir="$prefix/include"
fi
#===================================================================================================
if test "${data_dir-unset}" = unset; then
data_dir="$prefix/share"
fi
#===================================================================================================
if test "${lib_dir-unset}" = unset; then
lib_dir="$prefix/lib"
fi
#===================================================================================================
if test "${pkgconf_inc_dir-unset}" = unset; then
pkgconf_inc_dir="$inc_dir"
fi
#===================================================================================================
if test "${pkgconf_lib_dir-unset}" = unset; then
pkgconf_lib_dir="$lib_dir"
fi
#===================================================================================================
if test "${xorgproto_inc_dir-unset}" = unset; then
xorgproto_inc_dir=/nyan/xorgproto/current/include
fi
xorgproto_cppflags="-I$xorgproto_inc_dir"
#===================================================================================================
if test "${xtrans_glibc_cppflags-unset}" = unset; then
xtrans_glibc_cppflags="
-I/nyan/xtrans/current/include \
-DHAS_FCHOWN \
-DHAS_STICKY_DIR_BIT \
"
fi
#===================================================================================================
if test "${xcb_cppflags-unset}" = unset; then
xcb_cppflags='-I/nyan/libxcb/current/include'
fi
if test "${xcb_ldflags-unset}" = unset; then
xcb_ldflags='-L/nyan/libxcb/current/lib -lxcb'
fi
#===================================================================================================
if test "${cc-unset}" = unset; then
#-fvisibility=hidden"
cc="/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc -c \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-pipe -fPIC -O2 -ftls-model=global-dynamic -fpic \
	-static-libgcc"
fi
#===================================================================================================
if test "${build_ccld-unset}" = unset; then
build_ccld="/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-pipe -fPIC -O2 \
	-static-libgcc \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-s"
fi
#===================================================================================================
if test "${rawcpp-unset}" = unset; then
rawcpp="/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
	-E -undef -traditional-cpp -x c"
fi
#===================================================================================================
if test "${slib_ccld_tmpl-unset}" = unset; then
slib_ccld_tmpl="/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
	-shared \
	-static-libgcc \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-soname=\$soname \
	-Wl,--no-undefined \
	-Wl,-s \
"
fi
#===================================================================================================
mkdir -p $build_dir/fakeroot/$inc_dir/X11/extensions
# use our pre configured XlibConf.h
cp -f $src_dir/contrib/XlibConf.h $build_dir/fakeroot/$inc_dir/X11
cp -f \
	$src_dir/include/X11/XKBlib.h \
	$src_dir/include/X11/Xcms.h \
	$src_dir/include/X11/Xlib.h \
	$src_dir/include/X11/Xlibint.h \
	$src_dir/include/X11/Xlocale.h \
	$src_dir/include/X11/Xresource.h \
	$src_dir/include/X11/Xutil.h \
	$src_dir/include/X11/cursorfont.h \
	$src_dir/include/X11/Xregion.h \
	$src_dir/include/X11/ImUtil.h \
	$src_dir/include/X11/Xlib-xcb.h \
\
	$build_dir/fakeroot/$inc_dir/X11
cp -f $src_dir/include/X11/extensions/XKBgeom.h $build_dir/fakeroot/$inc_dir/X11/extensions
#===================================================================================================
# we group all glibc related cppflags (xtrans ones are not here)
glibc_cppflags="\
-D_GNU_SOURCE \
-DHAVE_SYS_IOCTL_H=1 \
-DHAVE_SYS_SELECT_H=1 \
-DHAVE_SYS_SOCKET_H=1 \
-DHAVE_INTTYPES_H=1 \
-DHAVE_UNISTD_H=1 \
\
-DHAVE_REALLOCARRAY=1 \
-DHAVE_SETEUID=1 \
-DHASGETRESUID=1 \
-DHAS_SHM=1 \
-DUSE_POLL=1 \
"
gcc_cppflags="\
-DHAVE___BUILTIN_POPCOUNTL=1 \
"
#===================================================================================================
# modules/im/ximcp

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/modules/im/ximcp/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/xlibi18n/Xlcint.h \
	$src_dir/src/xlibi18n/Ximint.h \
	$src_dir/src/xlibi18n/XimintP.h \
	$src_dir/src/xlibi18n/XimProto.h \
	$src_dir/src/xlibi18n/XlcPublic.h \
	$src_dir/src/xlibi18n/XimintL.h \
	$src_dir/src/xlibi18n/XlcPubI.h \
	$src_dir/src/xlibi18n/XimTrInt.h \
	$src_dir/src/xlibi18n/XimImSw.h \
	$src_dir/src/xlibi18n/XimThai.h \
	$src_dir/src/xlibi18n/XimTrans.h \
	$src_dir/src/xlibi18n/XimTrX.h \
\
	$build_dir/modules/im/ximcp/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/pathmax.h \
\
	$build_dir/modules/im/ximcp/include_cherry_picked/src
#---------------------------------------------------------------------------------------------------
modules_im_ximcp_xtrans_cppflags="\
-DUNIXCONN=1 \
-DXIM_t \
-DTRANS_CLIENT \
"
modules_im_ximcp_cppflags="\
-DCOMPOSECACHE=1 \
$xorgproto_cppflags \
$xtrans_glibc_cppflags \
$modules_im_ximcp_xtrans_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/modules/im/ximcp/include_cherry_picked/src/xlibi18n \
-I$build_dir/modules/im/ximcp/include_cherry_picked/src \
"
modules_im_ximcp_src_files="\
$src_dir/modules/im/ximcp/imCallbk.c \
$src_dir/modules/im/ximcp/imDefFlt.c \
$src_dir/modules/im/ximcp/imDefIc.c \
$src_dir/modules/im/ximcp/imDefIm.c \
$src_dir/modules/im/ximcp/imDefLkup.c \
$src_dir/modules/im/ximcp/imDispch.c \
$src_dir/modules/im/ximcp/imEvToWire.c \
$src_dir/modules/im/ximcp/imExten.c \
$src_dir/modules/im/ximcp/imImSw.c \
$src_dir/modules/im/ximcp/imInsClbk.c \
$src_dir/modules/im/ximcp/imInt.c \
$src_dir/modules/im/ximcp/imLcFlt.c \
$src_dir/modules/im/ximcp/imLcGIc.c \
$src_dir/modules/im/ximcp/imLcIc.c \
$src_dir/modules/im/ximcp/imLcIm.c \
$src_dir/modules/im/ximcp/imLcLkup.c \
$src_dir/modules/im/ximcp/imLcPrs.c \
$src_dir/modules/im/ximcp/imLcSIc.c \
$src_dir/modules/im/ximcp/imRmAttr.c \
$src_dir/modules/im/ximcp/imRm.c \
$src_dir/modules/im/ximcp/imThaiFlt.c \
$src_dir/modules/im/ximcp/imThaiIc.c \
$src_dir/modules/im/ximcp/imThaiIm.c \
$src_dir/modules/im/ximcp/imTrans.c \
$src_dir/modules/im/ximcp/imTransR.c \
$src_dir/modules/im/ximcp/imTrX.c \
"
mkdir -p $build_dir/modules/im/ximcp
for f in $modules_im_ximcp_src_files
do
	modules_im_ximcp_o=$build_dir/modules/im/ximcp/$(basename $f .c).o
	printf "CC $f\n"
	$cc $f \
		$modules_im_ximcp_cppflags \
		-o $modules_im_ximcp_o &
	modules_im_ximcp_os="$modules_im_ximcp_os $modules_im_ximcp_o"
done
#===================================================================================================
# modules/lc/Utf8

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/modules/lc/Utf8/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/xlibi18n/Xlcint.h \
	$src_dir/src/xlibi18n/XlcPubI.h \
	$src_dir/src/xlibi18n/XlcPublic.h \
	$src_dir/src/xlibi18n/XlcGeneric.h \
\
	$build_dir/modules/lc/Utf8/include_cherry_picked/src/xlibi18n
#---------------------------------------------------------------------------------------------------
modules_lc_Utf8_cppflags="\
$xorgproto_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/modules/lc/Utf8/include_cherry_picked/src/xlibi18n \
"
printf "CC $src_dir/modules/lc/Utf8/lcUTF8Load.c\n"
$cc $src_dir/modules/lc/Utf8/lcUTF8Load.c \
	$modules_lc_Utf8_cppflags \
	-o $build_dir/modules/lc/Utf8/lcUTF8Load.o &
#===================================================================================================
# modules/lc/def

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/modules/lc/def/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/xlibi18n/XlcGeneric.h \
	$src_dir/src/xlibi18n/XlcPublic.h \
	$src_dir/src/xlibi18n/XlcPubI.h \
	$src_dir/src/xlibi18n/Xlcint.h \
\
	$build_dir/modules/lc/def/include_cherry_picked/src/xlibi18n
#---------------------------------------------------------------------------------------------------
modules_lc_def_cppflags="\
$xorgproto_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/modules/lc/def/include_cherry_picked/src/xlibi18n \
"
printf "CC $src_dir/modules/lc/def/lcDefConv.c\n"
$cc $src_dir/modules/lc/def/lcDefConv.c \
	$modules_lc_def_cppflags \
	-o $build_dir/modules/lc/def/lcDefConv.o &
#===================================================================================================
# modules/lc/gen

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/modules/lc/gen/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/xlibi18n/XlcGeneric.h \
	$src_dir/src/xlibi18n/XlcPublic.h \
	$src_dir/src/xlibi18n/XlcPubI.h \
	$src_dir/src/xlibi18n/Xlcint.h \
\
	$build_dir/modules/lc/gen/include_cherry_picked/src/xlibi18n
#---------------------------------------------------------------------------------------------------
modules_lc_gen_cppflags="\
$xorgproto_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/modules/lc/gen/include_cherry_picked/src/xlibi18n \
"
printf "CC $src_dir/modules/lc/gen/lcGenConv.c\n"
$cc $src_dir/modules/lc/gen/lcGenConv.c \
	$modules_lc_gen_cppflags \
	-o $build_dir/modules/lc/gen/lcGenConv.o &
#===================================================================================================
# modules/om/generic

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/modules/om/generic/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/xlibi18n/XlcPublic.h \
	$src_dir/src/xlibi18n/Xlcint.h \
	$src_dir/src/xlibi18n/XlcGeneric.h \
	$src_dir/src/xlibi18n/XlcPubI.h \
\
	$build_dir/modules/om/generic/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/XomGeneric.h \
\
	$build_dir/modules/om/generic/include_cherry_picked/src
#---------------------------------------------------------------------------------------------------
modules_om_generic_cppflags="\
$xorgproto_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/modules/om/generic/include_cherry_picked/src/xlibi18n \
-I$build_dir/modules/om/generic/include_cherry_picked/src \
"
modules_om_generic_src_files="\
$src_dir/modules/om/generic/omDefault.c \
$src_dir/modules/om/generic/omGeneric.c \
$src_dir/modules/om/generic/omImText.c \
$src_dir/modules/om/generic/omText.c \
$src_dir/modules/om/generic/omTextEsc.c \
$src_dir/modules/om/generic/omTextExt.c \
$src_dir/modules/om/generic/omTextPer.c \
$src_dir/modules/om/generic/omXChar.c \
"
for f in $modules_om_generic_src_files
do
	modules_om_generic_o=$build_dir/modules/om/generic/$(basename $f .c).o
	printf "CC $f\n"
	$cc $f \
		$modules_om_generic_cppflags \
		-o $modules_om_generic_o &
	modules_om_generic_os="$modules_om_generic_os $modules_om_generic_o"
done
#===================================================================================================
# xcms

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/xcms/include_cherry_picked/src
cp -f \
	$src_dir/src/reallocarray.h \
	$src_dir/src/Cmap.h \
\
	$build_dir/xcms/include_cherry_picked/src
#---------------------------------------------------------------------------------------------------
xcms_cppflags="\
$glibc_cppflags \
$xorgproto_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/xcms/include_cherry_picked/src \
-DXCMSDIR=\"$data_dir/X11\" \
"
#---------------------------------------------------------------------------------------------------
xcms_src_files="\
$src_dir/src/xcms/AddDIC.c \
$src_dir/src/xcms/AddSF.c \
$src_dir/src/xcms/CCC.c \
$src_dir/src/xcms/CvColW.c \
$src_dir/src/xcms/CvCols.c \
$src_dir/src/xcms/HVC.c \
$src_dir/src/xcms/HVCGcC.c \
$src_dir/src/xcms/HVCGcV.c \
$src_dir/src/xcms/HVCGcVC.c \
$src_dir/src/xcms/HVCMnV.c \
$src_dir/src/xcms/HVCMxC.c \
$src_dir/src/xcms/HVCMxV.c \
$src_dir/src/xcms/HVCMxVC.c \
$src_dir/src/xcms/HVCMxVs.c \
$src_dir/src/xcms/HVCWpAj.c \
$src_dir/src/xcms/IdOfPr.c \
$src_dir/src/xcms/LRGB.c \
$src_dir/src/xcms/Lab.c \
$src_dir/src/xcms/LabGcC.c \
$src_dir/src/xcms/LabGcL.c \
$src_dir/src/xcms/LabGcLC.c \
$src_dir/src/xcms/LabMnL.c \
$src_dir/src/xcms/LabMxC.c \
$src_dir/src/xcms/LabMxL.c \
$src_dir/src/xcms/LabMxLC.c \
$src_dir/src/xcms/LabWpAj.c \
$src_dir/src/xcms/Luv.c \
$src_dir/src/xcms/LuvGcC.c \
$src_dir/src/xcms/LuvGcL.c \
$src_dir/src/xcms/LuvGcLC.c \
$src_dir/src/xcms/LuvMnL.c \
$src_dir/src/xcms/LuvMxC.c \
$src_dir/src/xcms/LuvMxL.c \
$src_dir/src/xcms/LuvMxLC.c \
$src_dir/src/xcms/LuvWpAj.c \
$src_dir/src/xcms/OfCCC.c \
$src_dir/src/xcms/PrOfId.c \
$src_dir/src/xcms/QBlack.c \
$src_dir/src/xcms/QBlue.c \
$src_dir/src/xcms/QGreen.c \
$src_dir/src/xcms/QRed.c \
$src_dir/src/xcms/QWhite.c \
$src_dir/src/xcms/QuCol.c \
$src_dir/src/xcms/QuCols.c \
$src_dir/src/xcms/SetCCC.c \
$src_dir/src/xcms/SetGetCols.c \
$src_dir/src/xcms/StCol.c \
$src_dir/src/xcms/StCols.c \
$src_dir/src/xcms/UNDEFINED.c \
$src_dir/src/xcms/XRGB.c \
$src_dir/src/xcms/XYZ.c \
$src_dir/src/xcms/cmsAllCol.c \
$src_dir/src/xcms/cmsAllNCol.c \
$src_dir/src/xcms/cmsCmap.c \
$src_dir/src/xcms/cmsColNm.c \
$src_dir/src/xcms/cmsGlobls.c \
$src_dir/src/xcms/cmsInt.c \
$src_dir/src/xcms/cmsLkCol.c \
$src_dir/src/xcms/cmsMath.c \
$src_dir/src/xcms/cmsProp.c \
$src_dir/src/xcms/cmsTrig.c \
$src_dir/src/xcms/uvY.c \
$src_dir/src/xcms/xyY.c \
"
for f in $xcms_src_files
do
	xcms_o=$build_dir/xcms/$(basename $f .c).o
	printf "CC $f\n"
	$cc $f \
		$xcms_cppflags \
		-o $xcms_o &
	xcms_os="$xcms_os $xcms_o"
done

mkdir -p $build_dir/fakeroot/$data_dir/X11
printf 'SHIP Xcms.txt\n'
cp -f $src_dir/src/xcms/Xcms.txt $build_dir/fakeroot/$data_dir/X11 &
#===================================================================================================
# xlibi18n

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/xlibi18n/include_cherry_picked/src
cp -f \
	$src_dir/src/reallocarray.h \
	$src_dir/src/locking.h \
	$src_dir/src/pathmax.h \
\
	$build_dir/xlibi18n/include_cherry_picked/src

# additional xtrans flags are set into the code directly: XIM_t and TRANS_CLIENT in
# xlibi18n/xim_trans.c
xlibi18s_xtrans_cppflags="\
-DUNIXCONN=1 \
"

xlibi18n_cppflags="\
$glibc_cppflags \
$xorgproto_cppflags \
$xtrans_glibc_cppflags \
$xlibi18s_xtrans_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/xlibi18n/include_cherry_picked/src \
-DXLOCALEDIR=\"$data_dir/X11/locale\" \
-DXLOCALELIBDIR=\"$lib_dir/X11/locale\" \
"
xlibi18n_src_files="\
$src_dir/src/xlibi18n/XDefaultIMIF.c \
$src_dir/src/xlibi18n/XDefaultOMIF.c \
$src_dir/src/xlibi18n/xim_trans.c \
$src_dir/src/xlibi18n/ICWrap.c \
$src_dir/src/xlibi18n/IMWrap.c \
$src_dir/src/xlibi18n/imKStoUCS.c \
$src_dir/src/xlibi18n/lcCT.c \
$src_dir/src/xlibi18n/lcCharSet.c \
$src_dir/src/xlibi18n/lcConv.c \
$src_dir/src/xlibi18n/lcDB.c \
$src_dir/src/xlibi18n/lcDynamic.c \
$src_dir/src/xlibi18n/lcFile.c \
$src_dir/src/xlibi18n/lcGeneric.c \
$src_dir/src/xlibi18n/lcInit.c \
$src_dir/src/xlibi18n/lcPrTxt.c \
$src_dir/src/xlibi18n/lcPubWrap.c \
$src_dir/src/xlibi18n/lcPublic.c \
$src_dir/src/xlibi18n/lcRM.c \
$src_dir/src/xlibi18n/lcStd.c \
$src_dir/src/xlibi18n/lcTxtPr.c \
$src_dir/src/xlibi18n/lcUTF8.c \
$src_dir/src/xlibi18n/lcUtil.c \
$src_dir/src/xlibi18n/lcWrap.c \
$src_dir/src/xlibi18n/mbWMProps.c \
$src_dir/src/xlibi18n/mbWrap.c \
$src_dir/src/xlibi18n/utf8WMProps.c \
$src_dir/src/xlibi18n/utf8Wrap.c \
$src_dir/src/xlibi18n/wcWrap.c \
"
for f in $xlibi18n_src_files
do
	xlibi18n_o=$build_dir/xlibi18n/$(basename $f .c).o
	printf "CC $f\n"
	$cc $f \
		$xlibi18n_cppflags \
		-o $xlibi18n_o &
	xlibi18n_os="$xlibi18n_os $xlibi18n_o"
done
#===================================================================================================
# xkb

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/xkb/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/xlibi18n/Xlcint.h \
	$src_dir/src/xlibi18n/XlcPubI.h \
	$src_dir/src/xlibi18n/XlcPublic.h \
	$src_dir/src/xlibi18n/Ximint.h \
	$src_dir/src/xlibi18n/XimintP.h \
	$src_dir/src/xlibi18n/XimProto.h \
	$src_dir/src/xlibi18n/XimintL.h \
\
	$build_dir/xkb/include_cherry_picked/src/xlibi18n
cp -f \
	$src_dir/src/reallocarray.h \
\
	$build_dir/xkb/include_cherry_picked/src

xkb_cppflags="\
$glibc_cppflags \
$xorgproto_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/xkb/include_cherry_picked/src/xlibi18n \
-I$build_dir/xkb/include_cherry_picked/src \
"
xkb_src_files="\
$src_dir/src/xkb/XKB.c \
$src_dir/src/xkb/XKBBind.c \
$src_dir/src/xkb/XKBCompat.c \
$src_dir/src/xkb/XKBCtrls.c \
$src_dir/src/xkb/XKBCvt.c \
$src_dir/src/xkb/XKBGetMap.c \
$src_dir/src/xkb/XKBGetByName.c \
$src_dir/src/xkb/XKBNames.c \
$src_dir/src/xkb/XKBRdBuf.c \
$src_dir/src/xkb/XKBSetMap.c \
$src_dir/src/xkb/XKBUse.c \
$src_dir/src/xkb/XKBleds.c \
$src_dir/src/xkb/XKBBell.c \
$src_dir/src/xkb/XKBGeom.c \
$src_dir/src/xkb/XKBSetGeom.c \
$src_dir/src/xkb/XKBExtDev.c \
$src_dir/src/xkb/XKBList.c \
$src_dir/src/xkb/XKBMisc.c \
$src_dir/src/xkb/XKBMAlloc.c \
$src_dir/src/xkb/XKBGAlloc.c \
$src_dir/src/xkb/XKBAlloc.c \
"
for f in $xkb_src_files
do
	xkb_o=$build_dir/xkb/$(basename $f .c).o
	printf "CC $f\n"
	$cc $f \
		$xkb_cppflags \
		-o $xkb_o &
	xkb_os="$xkb_os $xkb_o"
done
#===================================================================================================
# ks_tables.h
mkdir -p $build_dir/src/util
$build_ccld \
	$src_dir/src/util/makekeys.c \
	-o $build_dir/src/util/makekeys
$build_dir/src/util/makekeys \
	$xorgproto_inc_dir/X11/keysymdef.h \
	$xorgproto_inc_dir/X11/XF86keysym.h \
	$xorgproto_inc_dir/X11/Sunkeysym.h \
	$xorgproto_inc_dir/X11/DECkeysym.h \
	$xorgproto_inc_dir/X11/HPkeysym.h >$build_dir/ks_tables.h
#===================================================================================================
# libX11

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/include_cherry_picked_libX11/src/xcms
cp -f \
	$src_dir/src/xcms/Xcmsint.h \
\
	$build_dir/include_cherry_picked_libX11/src/xcms
mkdir -p $build_dir/include_cherry_picked_libX11/src/xlibi18n
cp -f \
	$src_dir/src/xlibi18n/Xlcint.h \
	$src_dir/src/xlibi18n/XlcPubI.h \
	$src_dir/src/xlibi18n/XlcPublic.h \
	$src_dir/src/xlibi18n/Ximint.h \
	$src_dir/src/xlibi18n/XimintP.h \
	$src_dir/src/xlibi18n/XimProto.h \
	$src_dir/src/xlibi18n/XimintL.h \
\
	$build_dir/include_cherry_picked_libX11/src/xlibi18n
mkdir -p $build_dir/include_cherry_picked_libX11/src/xkb
cp -f \
	$src_dir/src/xkb/XKBlibint.h \
\
	$build_dir/include_cherry_picked_libX11/src/xkb
mkdir -p $build_dir/include_cherry_picked_libX11/src
cp -f \
	$src_dir/src/reallocarray.h \
\
	$build_dir/include_cherry_picked_libX11/src
#---------------------------------------------------------------------------------------------------
# XXX: we have to add XTHREADS=1 here, because some libX11 header do not include the configuration
# header file.
# xtrans flags are not supposed to be used by xtrans code directly since there is not usage of
# xtrans includes. They don't seem to reconfigure include files from with xlibi18n. 
libX11_xtrans_cppflags="\
-DX11_t \
-DTRANS_CLIENT \
"
libX11_cppflags="\
$gcc_cppflags \
$glibc_cppflags \
$xcb_cppflags \
$xorgproto_cppflags \
$libX11_xtrans_cppflags \
-DXCMS=1 \
-DUSE_DYNAMIC_XCURSOR=1 \
-DXERRORDB=\"$data_dir/X11/XErrorDB\" \
-DXLOCALE=1 \
-DXF86BIGFONT=1 \
-DXKB=1 \
-DXKEYSYMDB=\"$data_dir/X11/XKeysymDB\" \
-DUSE_THREAD_SAFETY_CONSTRUCTOR=1 \
-DXTHREADS=1 \
-DXUSE_MTSAFE_API=1 \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/include_cherry_picked_libX11/src/xcms \
-I$build_dir/include_cherry_picked_libX11/src/xlibi18n \
-I$build_dir/include_cherry_picked_libX11/src/xkb \
-I$build_dir/include_cherry_picked_libX11/src \
-I$build_dir \
"
libX11_src_files="\
$src_dir/src/AllCells.c \
$src_dir/src/AllowEv.c \
$src_dir/src/AllPlanes.c \
$src_dir/src/AutoRep.c \
$src_dir/src/Backgnd.c \
$src_dir/src/BdrWidth.c \
$src_dir/src/Bell.c \
$src_dir/src/Border.c \
$src_dir/src/ChAccCon.c \
$src_dir/src/ChActPGb.c \
$src_dir/src/ChClMode.c \
$src_dir/src/ChCmap.c \
$src_dir/src/ChGC.c \
$src_dir/src/ChKeyCon.c \
$src_dir/src/ChkIfEv.c \
$src_dir/src/ChkMaskEv.c \
$src_dir/src/ChkTypEv.c \
$src_dir/src/ChkTypWEv.c \
$src_dir/src/ChkWinEv.c \
$src_dir/src/ChPntCon.c \
$src_dir/src/ChProp.c \
$src_dir/src/ChSaveSet.c \
$src_dir/src/ChWAttrs.c \
$src_dir/src/ChWindow.c \
$src_dir/src/CirWin.c \
$src_dir/src/CirWinDn.c \
$src_dir/src/CirWinUp.c \
$src_dir/src/ClDisplay.c \
$src_dir/src/ClearArea.c \
$src_dir/src/Clear.c \
$src_dir/src/ConfWind.c \
$src_dir/src/Context.c \
$src_dir/src/ConvSel.c \
$src_dir/src/CopyArea.c \
$src_dir/src/CopyCmap.c \
$src_dir/src/CopyGC.c \
$src_dir/src/CopyPlane.c \
$src_dir/src/CrBFData.c \
$src_dir/src/CrCmap.c \
$src_dir/src/CrCursor.c \
$src_dir/src/CrGC.c \
$src_dir/src/CrGlCur.c \
$src_dir/src/CrPFBData.c \
$src_dir/src/CrPixmap.c \
$src_dir/src/CrWindow.c \
$src_dir/src/Cursor.c \
$src_dir/src/DefCursor.c \
$src_dir/src/DelProp.c \
$src_dir/src/Depths.c \
$src_dir/src/DestSubs.c \
$src_dir/src/DestWind.c \
$src_dir/src/DisName.c \
$src_dir/src/DrArc.c \
$src_dir/src/DrArcs.c \
$src_dir/src/DrLine.c \
$src_dir/src/DrLines.c \
$src_dir/src/DrPoint.c \
$src_dir/src/DrPoints.c \
$src_dir/src/DrRect.c \
$src_dir/src/DrRects.c \
$src_dir/src/DrSegs.c \
$src_dir/src/ErrDes.c \
$src_dir/src/ErrHndlr.c \
$src_dir/src/evtomask.c \
$src_dir/src/EvToWire.c \
$src_dir/src/FetchName.c \
$src_dir/src/FillArc.c \
$src_dir/src/FillArcs.c \
$src_dir/src/FillPoly.c \
$src_dir/src/FillRct.c \
$src_dir/src/FillRcts.c \
$src_dir/src/FilterEv.c \
$src_dir/src/Flush.c \
$src_dir/src/Font.c \
$src_dir/src/FontInfo.c \
$src_dir/src/FontNames.c \
$src_dir/src/FreeCmap.c \
$src_dir/src/FreeCols.c \
$src_dir/src/FreeCurs.c \
$src_dir/src/FreeEData.c \
$src_dir/src/FreeEventData.c \
$src_dir/src/FreeGC.c \
$src_dir/src/FreePix.c \
$src_dir/src/FSSaver.c \
$src_dir/src/FSWrap.c \
$src_dir/src/GCMisc.c \
$src_dir/src/Geom.c \
$src_dir/src/GetAtomNm.c \
$src_dir/src/GetColor.c \
$src_dir/src/GetDflt.c \
$src_dir/src/GetEventData.c \
$src_dir/src/GetFPath.c \
$src_dir/src/GetFProp.c \
$src_dir/src/GetGCVals.c \
$src_dir/src/GetGeom.c \
$src_dir/src/GetHColor.c \
$src_dir/src/GetHints.c \
$src_dir/src/GetIFocus.c \
$src_dir/src/GetImage.c \
$src_dir/src/GetKCnt.c \
$src_dir/src/GetMoEv.c \
$src_dir/src/GetNrmHint.c \
$src_dir/src/GetPCnt.c \
$src_dir/src/GetPntMap.c \
$src_dir/src/GetProp.c \
$src_dir/src/GetRGBCMap.c \
$src_dir/src/GetSOwner.c \
$src_dir/src/GetSSaver.c \
$src_dir/src/GetStCmap.c \
$src_dir/src/GetTxtProp.c \
$src_dir/src/GetWAttrs.c \
$src_dir/src/GetWMCMapW.c \
$src_dir/src/GetWMProto.c \
$src_dir/src/globals.c \
$src_dir/src/GrButton.c \
$src_dir/src/GrKeybd.c \
$src_dir/src/GrKey.c \
$src_dir/src/GrPointer.c \
$src_dir/src/GrServer.c \
$src_dir/src/Host.c \
$src_dir/src/Iconify.c \
$src_dir/src/IfEvent.c \
$src_dir/src/imConv.c \
$src_dir/src/ImText16.c \
$src_dir/src/ImText.c \
$src_dir/src/ImUtil.c \
$src_dir/src/InitExt.c \
$src_dir/src/InsCmap.c \
$src_dir/src/IntAtom.c \
$src_dir/src/KeyBind.c \
$src_dir/src/KeysymStr.c \
$src_dir/src/KillCl.c \
$src_dir/src/LiHosts.c \
$src_dir/src/LiICmaps.c \
$src_dir/src/LiProps.c \
$src_dir/src/ListExt.c \
$src_dir/src/LoadFont.c \
$src_dir/src/LockDis.c \
$src_dir/src/locking.c \
$src_dir/src/LookupCol.c \
$src_dir/src/LowerWin.c \
$src_dir/src/Macros.c \
$src_dir/src/MapRaised.c \
$src_dir/src/MapSubs.c \
$src_dir/src/MapWindow.c \
$src_dir/src/MaskEvent.c \
$src_dir/src/Misc.c \
$src_dir/src/ModMap.c \
$src_dir/src/MoveWin.c \
$src_dir/src/NextEvent.c \
$src_dir/src/OCWrap.c \
$src_dir/src/OMWrap.c \
$src_dir/src/OpenDis.c \
$src_dir/src/ParseCmd.c \
$src_dir/src/ParseCol.c \
$src_dir/src/ParseGeom.c \
$src_dir/src/PeekEvent.c \
$src_dir/src/PeekIfEv.c \
$src_dir/src/Pending.c \
$src_dir/src/PixFormats.c \
$src_dir/src/PmapBgnd.c \
$src_dir/src/PmapBord.c \
$src_dir/src/PolyReg.c \
$src_dir/src/PolyTxt16.c \
$src_dir/src/PolyTxt.c \
$src_dir/src/PropAlloc.c \
$src_dir/src/PutBEvent.c \
$src_dir/src/PutImage.c \
$src_dir/src/Quarks.c \
$src_dir/src/QuBest.c \
$src_dir/src/QuColor.c \
$src_dir/src/QuColors.c \
$src_dir/src/QuCurShp.c \
$src_dir/src/QuExt.c \
$src_dir/src/QuKeybd.c \
$src_dir/src/QuPntr.c \
$src_dir/src/QuStipShp.c \
$src_dir/src/QuTextE16.c \
$src_dir/src/QuTextExt.c \
$src_dir/src/QuTileShp.c \
$src_dir/src/QuTree.c \
$src_dir/src/RaiseWin.c \
$src_dir/src/RdBitF.c \
$src_dir/src/RecolorC.c \
$src_dir/src/ReconfWin.c \
$src_dir/src/ReconfWM.c \
$src_dir/src/Region.c \
$src_dir/src/RegstFlt.c \
$src_dir/src/RepWindow.c \
$src_dir/src/RestackWs.c \
$src_dir/src/RotProp.c \
$src_dir/src/ScrResStr.c \
$src_dir/src/SelInput.c \
$src_dir/src/SendEvent.c \
$src_dir/src/SetBack.c \
$src_dir/src/SetClMask.c \
$src_dir/src/SetClOrig.c \
$src_dir/src/SetCRects.c \
$src_dir/src/SetDashes.c \
$src_dir/src/SetFont.c \
$src_dir/src/SetFore.c \
$src_dir/src/SetFPath.c \
$src_dir/src/SetFunc.c \
$src_dir/src/SetHints.c \
$src_dir/src/SetIFocus.c \
$src_dir/src/SetLocale.c \
$src_dir/src/SetLStyle.c \
$src_dir/src/SetNrmHint.c \
$src_dir/src/SetPMask.c \
$src_dir/src/SetPntMap.c \
$src_dir/src/SetRGBCMap.c \
$src_dir/src/SetSOwner.c \
$src_dir/src/SetSSaver.c \
$src_dir/src/SetState.c \
$src_dir/src/SetStCmap.c \
$src_dir/src/SetStip.c \
$src_dir/src/SetTile.c \
$src_dir/src/SetTSOrig.c \
$src_dir/src/SetTxtProp.c \
$src_dir/src/SetWMCMapW.c \
$src_dir/src/SetWMProto.c \
$src_dir/src/StBytes.c \
$src_dir/src/StColor.c \
$src_dir/src/StColors.c \
$src_dir/src/StName.c \
$src_dir/src/StNColor.c \
$src_dir/src/StrKeysym.c \
$src_dir/src/StrToText.c \
$src_dir/src/Sync.c \
$src_dir/src/Synchro.c \
$src_dir/src/Text16.c \
$src_dir/src/Text.c \
$src_dir/src/TextExt16.c \
$src_dir/src/TextExt.c \
$src_dir/src/TextToStr.c \
$src_dir/src/TrCoords.c \
$src_dir/src/UndefCurs.c \
$src_dir/src/UngrabBut.c \
$src_dir/src/UngrabKbd.c \
$src_dir/src/UngrabKey.c \
$src_dir/src/UngrabPtr.c \
$src_dir/src/UngrabSvr.c \
$src_dir/src/UninsCmap.c \
$src_dir/src/UnldFont.c \
$src_dir/src/UnmapSubs.c \
$src_dir/src/UnmapWin.c \
$src_dir/src/VisUtil.c \
$src_dir/src/WarpPtr.c \
$src_dir/src/Window.c \
$src_dir/src/WinEvent.c \
$src_dir/src/Withdraw.c \
$src_dir/src/WMGeom.c \
$src_dir/src/WMProps.c \
$src_dir/src/WrBitF.c \
$src_dir/src/xcb_disp.c \
$src_dir/src/xcb_io.c \
$src_dir/src/XlibAsync.c \
$src_dir/src/XlibInt.c \
$src_dir/src/Xrm.c \
"
for f in $libX11_src_files
do
	libX11_o=$build_dir/$(basename $f .c).o
	printf "CC $f\n"
	$cc $f \
		$libX11_cppflags \
		-o $libX11_o &
	libX11_os="$libX11_os $libX11_o"
done
#===================================================================================================
# libX11-xcb

# we do cherry pick the include files from somewhere else in the src tree
mkdir -p $build_dir/include_cherry_picked_libX11_xcb/src
cp -f \
	$src_dir/src/Xxcbint.h \
	$src_dir/src/locking.h \
\
	$build_dir/include_cherry_picked_libX11_xcb/src
#---------------------------------------------------------------------------------------------------
libX11_xcb_cppflags="\
$xorgproto_cppflags \
$xcb_cppflags \
-I$build_dir/fakeroot/$inc_dir \
-I$build_dir/fakeroot/$inc_dir/X11 \
-I$build_dir/include_cherry_picked_libX11_xcb/src \
"
printf "CC $src_dir/src/x11_xcb.c\n"
$cc $src_dir/src/x11_xcb.c \
	$libX11_xcb_cppflags \
	-o $build_dir/x11_xcb.o &
#===================================================================================================
mkdir -p $build_dir/fakeroot/$data_dir/X11
printf 'SHIP XErrorDB\n'
cp -f $src_dir/src/XErrorDB $build_dir/fakeroot/$data_dir/X11 &
#===================================================================================================
wait # need to wait for everything which is being working on
#===================================================================================================
# link the shared libs
mkdir -p $build_dir/fakeroot/$lib_dir

lib_name=libX11-xcb.so
soname=$lib_name.1
slib_ccld=$(eval echo $slib_ccld_tmpl)
printf 'SLIB_CCLD libX11-xcb.so\n'
$slib_ccld \
	$build_dir/x11_xcb.o \
	-ldl \
	-lc \
	-o $build_dir/fakeroot/$lib_dir/$soname.0.0 &
ln -sTf $soname.0.0 $build_dir/fakeroot/$lib_dir/$soname &
ln -sTf $soname $build_dir/fakeroot/$lib_dir/$lib_name &
#---------------------------------------------------------------------------------------------------
lib_name=libX11.so
soname=$lib_name.6
slib_ccld=$(eval echo $slib_ccld_tmpl)
printf 'SLIB_CCLD libX11.so\n'
$slib_ccld \
	-Wl,--start-group \
	$libX11_os \
	\
	$modules_im_ximcp_os \
	$build_dir/modules/lc/Utf8/lcUTF8Load.o \
	$build_dir/modules/lc/def/lcDefConv.o \
	$build_dir/modules/lc/gen/lcGenConv.o \
	$modules_om_generic_os \
	$xlibi18n_os \
	$xcms_os \
	$xkb_os \
	-Wl,--end-group \
	\
	$xcb_ldflags \
	-ldl \
	-lc \
	-o $build_dir/fakeroot/$lib_dir/$soname.4.0 &
ln -sTf $soname.4.0 $build_dir/fakeroot/$lib_dir/$soname &
ln -sTf $soname $build_dir/fakeroot/$lib_dir/$lib_name &
#===================================================================================================
# nls
mkdir -p $build_dir/fakeroot/$data_dir/X11/locale
mkdir -p $build_dir/nls
# carefull, there are tabs
CPP_SED_MAGIC_CMD="\
sed	-e '/^#  *[0-9][0-9]*  *.*\$/d' \
	-e '/^#line  *[0-9][0-9]*  *.*\$/d' \
	-e '/^[	 ]*XCOMM\$/s/XCOMM/#/' \
	-e '/^[	 ]*XCOMM[^a-zA-Z0-9_]/s/XCOMM/#/' \
	-e '/^[	 ]*XHASH/s/XHASH/#/' \
	-e 's,X11_LOCALEDATADIR,$data_dir/X11/locale,g' \
	-e '/@@\$/s/@@\$/\\\\/' \
	-e '/^\$/d'"

gen_a() {
	printf "GEN_A $1\n"
	$rawcpp "-DWCHAR32=1" $src_dir/nls/$1.pre \
			>$build_dir/nls/$1.l0

	# quoted eval to avoid tabs becoming simple spaces	
	LC_CTYPES=C eval "$CPP_SED_MAGIC_CMD" \
		<$build_dir/nls/$1.l0 \
		>$build_dir/nls/$1.l1
	
	LC_CTYPES=C sed \
		-e '/^[^#][^	 ]*:/s/://' \
		-e '/^[^#].*[	 ].*:/d' \
		<$build_dir/nls/$1.l1 \
		>$build_dir/nls/$1.l2
	
	# why? I don't know.
	cat $build_dir/nls/$1.l2 $build_dir/nls/$1.l1 \
		>$build_dir/fakeroot/$data_dir/X11/locale/$1 &
}

gen_a locale.alias &
gen_a compose.dir &
gen_a locale.dir &

gen_b() {
	d=$1
	src=$2
	dst=$(basename $2 .pre)
	printf "GEN_B $d $src->$dst\n"

	mkdir -p $build_dir/nls/$d

	$rawcpp "-DWCHAR32=1" $src \
			>$build_dir/nls/$d/$dst.l0

	mkdir -p $build_dir/fakeroot/$data_dir/X11/locale/$d

	# quoted eval to avoid tabs becoming simple spaces	
	LC_CTYPES=C eval "$CPP_SED_MAGIC_CMD" \
		<$build_dir/nls/$d/$dst.l0 \
		>$build_dir/fakeroot/$data_dir/X11/locale/$d/$dst &
}

for dl in $(find $src_dir/nls -mindepth 1 -maxdepth 1 -type d)
do
	d=$(basename $dl)
	mkdir -p $build_dir/fakeroot/$data_dir/X11/locale/$d
	for pre in $(find $dl -mindepth 1 -maxdepth 1 -name '*.pre' -type f)
	do
		gen_b $d $pre &
	done &
	# verbatim copy of the other ones
	for verbatim in $(find $dl -mindepth 1 -maxdepth 1 ! -name '*.pre' -type f)
	do
		cp -f $verbatim $build_dir/fakeroot/$data_dir/X11/locale/$d &
	done &
done &
#===================================================================================================
# pkgconfig files
SED_CMD="\
sed -E \
	-e \"s:@prefix@:$prefix:\" \
	-e \"s:@exec_prefix@:$prefix:\" \
	-e \"s:@libdir@:$pkgconf_lib_dir:\" \
	-e \"s:@includedir@:$pkgconf_inc_dir:\" \
	-e \"s/@XTHREADLIB@/-lpthread/\" \
	-e \"s/@PACKAGE_VERSION@/$version/\" \
	-e \"s/@XKBPROTO_REQUIRES@/kbproto/\" \
	-e \"s/@X11_EXTRA_DEPS@/xcb >= 1.11.1/\" \
	-e \"s/@XTHREAD_CFLAGS@//\""
mkdir -p $build_dir/fakeroot/$lib_dir/pkgconfig
printf 'GEN x11.pc\n'
eval "$SED_CMD" <$src_dir/x11.pc.in \
	>$build_dir/fakeroot/$lib_dir/pkgconfig/x11.pc &
printf 'GEN x11-xcb.pc\n'
eval "$SED_CMD" <$src_dir/x11-xcb.pc.in \
	>$build_dir/fakeroot/$lib_dir/pkgconfig/x11-xcb.pc &
#===================================================================================================
wait
