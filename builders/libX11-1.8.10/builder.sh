src_name=libX11
mkdir /nyan/$src_name

version=${pkg_name##*-}
slot=$version
mkdir /nyan/$src_name/$slot

archive_name=$src_name-$version.tar.xz
url0=http://xorg.freedesktop.org/releases/individual/lib/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

# install our build system
cp -r $nyan_root/builders/$pkg_name/contrib $pkg_dir

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

cat >$build_dir/local_conf.sh <<EOF
version=$version
prefix=/usr
pkgconf_inc_dir=/nyan/$src_name/$slot/include
pkgconf_lib_dir=/nyan/$src_name/$slot/lib
EOF

$pkg_dir/contrib/binutils-gcc-glibc.sh

mkdir /nyan/$src_name/$slot/lib
mkdir /nyan/$src_name/$slot/lib/pkgconfig

ln -sTf libX11-xcb.so.1.0.0 /nyan/$src_name/$slot/lib/libX11-xcb.so
ln -sTf libX11-xcb.so.1.0.0 /nyan/$src_name/$slot/lib/libX11-xcb.so.1
cp -f $build_dir/fakeroot/usr/lib/libX11-xcb.so.1.0.0 /nyan/$src_name/$slot/lib/libX11-xcb.so.1.0.0
# XXX:should go away
cp -f $build_dir/fakeroot/usr/lib/pkgconfig/x11-xcb.pc /nyan/$src_name/$slot/lib/pkgconfig/x11-xcb.pc

cp -f $build_dir/fakeroot/usr/lib/libX11.so.6.4.0 /nyan/$src_name/$slot/lib/libX11.so.6.4.0
ln -sTf libX11.so.6.4.0 /nyan/$src_name/$slot/lib/libX11.so
ln -sTf libX11.so.6.4.0 /nyan/$src_name/$slot/lib/libX11.so.6
# XXX:should go away
cp -f $build_dir/fakeroot/usr/lib/pkgconfig/x11.pc /nyan/$src_name/$slot/lib/pkgconfig/x11.pc

rm -Rf /nyan/$src_name/$slot/include
mkdir -p /nyan/$src_name/$slot
cp -r $build_dir/fakeroot/usr/include /nyan/$src_name/$slot
# xkb compose runtime data files, you can override this location with XLOCALEDIR environment variable
# but invasive and trash software is unable to handle another location than /usr/share
rm -Rf /nyan/$src_name/$slot/share
mkdir /nyan/$src_name/$slot/share
mkdir /nyan/$src_name/$slot/share/X11
cp $build_dir/fakeroot/usr/share/X11/XErrorDB /nyan/$src_name/$slot/share/X11/XErrorDB
cp $build_dir/fakeroot/usr/share/X11/Xcms.txt /nyan/$src_name/$slot/share/X11/Xcms.txt

rm -Rf /nyan/$src_name/$slot/share/X11/locale
cp -r $build_dir/fakeroot/usr/share/X11/locale /nyan/$src_name/$slot/share/X11/locale

rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
