#XXX not yet in a toochain
src_name=qbe
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=git://c9x.me/qbe.git

pkg_dir=/run/pkgs/$src_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p /run/pkgs
cp -r $src_dir $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-tinycc/current/bin:\
/nyan/make/current/bin:\
/nyan/git/current/bin:\
$PATH\
"
toolchain_dir=/nyan/toolchains/binutils-tinycc/current
# XXX: we need libgccS with what wants the target glibc.
libgcc_dir=/opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0
target_gnu_triple=$(basename "$(ls -d $toolchain_dir/bin/*-tcc)")
target_gnu_triple=${target_gnu_triple%-tcc}

cd $pkg_dir

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# XXX: we use our own build system.

printf "#define Deftgt T_amd64_sysv\n" >$build_dir/config.h

CPP="$target_gnu_triple-tcc -nostdinc -E -x c"
CC="$target_gnu_triple-tcc -nostdinc -nostdlib -x c -c"
LDBIN="$target_gnu_triple-ld"

src_files=" \
main.c \
util.c \
parse.c \
abi.c \
cfg.c \
mem.c \
ssa.c \
alias.c \
load.c \
copy.c \
fold.c \
simpl.c \
live.c \
spill.c \
rega.c \
emit.c \
\
amd64/targ.c \
amd64/sysv.c \
amd64/isel.c \
amd64/emit.c \
\
arm64/targ.c \
arm64/abi.c \
arm64/isel.c \
arm64/emit.c \
\
rv64/targ.c \
rv64/abi.c \
rv64/isel.c \
rv64/emit.c \
"

mkdir -p $build_dir/amd64
mkdir -p $build_dir/arm64
mkdir -p $build_dir/rv64

for src_file in $src_files
do
	cpp=$build_dir/$(dirname $src_file)/$(basename $src_file .c).cpp.c

	printf "CPP $src_dir/$src_file-->$cpp\n"
	$CPP -o $cpp \
		-I$build_dir \
		-I/nyan/linux-headers/current/include \
		-I/nyan/glibc/current/include \
		-I$toolchain_dir/lib/tcc/include \
		$src_dir/$src_file &
done

wait

for src_file in $src_files
do
	cpp=$build_dir/$(dirname $src_file)/$(basename $src_file .c).cpp.c
	o=$build_dir/$(dirname $src_file)/$(basename $src_file .c).o
	os="$os $o"

	printf "CC $cpp-->$o\n"
	$CC -o $o $cpp &
done

wait

# XXX: trying to build a static pie or even a static executable is full of conflicts
# don't try unless with brand new build of the glibc.
printf "LDDBIN qbe\n"
$LDBIN -o $build_dir/qbe \
	-s \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	/nyan/glibc/current/lib/crt1.o \
	/nyan/glibc/current/lib/crti.o \
	$os \
	-L/nyan/glibc/current/lib \
	-lc \
	-L$toolchain_dir/lib/tcc \
	-ltcc1 \
	/nyan/glibc/current/lib/crtn.o

mkdir -p /nyan/qbe/$slot/bin
cp -f ./qbe //nyan/qbe/$slot/bin

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $pkg_dir $build_dir
