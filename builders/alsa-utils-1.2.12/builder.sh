src_name=alsa-utils
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.bz2
url0=ftp://ftp.alsa-project.org/pub/utils/$src_name-$version.tar.bz2

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir $pkgs_dir_root
cp $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
export "CPPFLAGS=\
-I/nyan/ncurses/current/include/ncurses \
-I/nyan/ncurses/current/include \
-I/nyan/alsa-lib/current/include"
export "CC=gcc \
	-isystem /nyan/linux-headers/current/include \
	-isystem /nyan/glibc/current/include \
	-static-libgcc \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-s \
"
export 'CFLAGS=-O2 -pipe -fPIC -fpic'
export "LDFLAGS=\
-L/nyan/ncurses/current/lib \
-L/nyan/alsa-lib/current/lib \
"
export LIBS=-ltinfo
$pkg_dir/configure \
	--prefix=/nyan/alsa-utils/$slot	\
	--disable-shared \
	--disable-nls \
	--disable-rpath \
	--disable-alsatest \
	--disable-xmlto \
	--disable-rst2man
unset CPPFLAGS
unset CFLAGS
unset CC
unset LDFLAGS
unset LIBS

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/$slot/share/man

# fix detected bashism
sed -i -e 's/bash/sh/' \
/nyan/alsa-utils/$slot/sbin/alsabat-test.sh \
/nyan/alsa-utils/$slot/sbin/alsaconf \
/nyan/alsa-utils/$slot/sbin/alsa-info.sh

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
