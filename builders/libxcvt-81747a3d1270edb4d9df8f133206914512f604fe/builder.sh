src_name=libxcvt
git_commit=${pkg_name##*-}
slot=$git_commit
url0=https://gitlab.freedesktop.org/ofourdan/$src_name/.git

pkg_dir=$pkgs_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -Hr $src_dir_root/$src_name $pkgs_dir_root

PATH_SAVED=$PATH
TOOLCHAIN_PATH=/nyan/toolchains/binutils-2.36.1-tinycc-0378168c1318352bf13f24f210a23aa2fbeb1895/current
export PATH="\
/nyan/git/current/bin:\
$TOOLCHAIN_PATH/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-tinycc-0378168c1318352bf13f24f210a23aa2fbeb1895/current/bin/*-tcc)")
target_gnu_triple=${target_gnu_triple%-tcc}

cd $pkg_dir

if test x$git_commit != x; then
	git reset --hard
	git checkout $git_commit
fi

build_dir=$builds_dir_root/$src_name
rm -Rf $build_dir
mkdir -p $build_dir

$target_gnu_triple-tcc \
	-c \
	-I$pkg_dir/include \
	$pkg_dir/lib/$src_name.c \
	-o $build_dir/$src_name.o

install_dir=/nyan/$src_name/$slot
rm -Rf $install_dir
mkdir -p $install_dir/lib
$target_gnu_triple-ar rcs \
	$install_dir/lib/$src_name.a \
	$build_dir/$src_name.o
mkdir -p $install_dir/lib/pkgconfig
# XXX: hopefully libtcc1.a won't conflict with libgcc.a
cat >$install_dir/lib/pkgconfig/libxcvt.pc <<EOF
prefix=/nyan/libxcvt/current
includedir=\${prefix}/include
libdir=\${prefix}/lib

Name: libxcvt
Description: external CVT code
Version: 0.0.0
Cflags: -I\${includedir}
Libs: -L\${libdir} -lxcvt -L$TOOLCHAIN_PATH/lib/tcc -ltcc1
EOF
mkdir -p $install_dir/include
cp -r $pkg_dir/include/$src_name $install_dir/include
rm -f $install_dir/include/$src_name/meson.build
	
export PATH=$PATH_SAVED
unset PATH_SAVED
unset TOOLCHAIN_PATH
unset target_gnu_triple
unset install_dir
rm -Rf $build_dir $pkg_dir
