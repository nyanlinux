src_name=efibootmgr
git_commit=325d7d94a873fac2e64aeb0312ae09a23bf9874a
git_url0=git://github.com/rhboot/efibootmgr

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

#-------------------------------------------------------------------------------
cd $pkg_dir

git checkout --force $git_commit
git reset --hard

# we use our build script, because the official one is straight from a seriously
# ill brain
cp -f $nyan_root/builders/$pkg_name/efibootmgr.sh ./
#-------------------------------------------------------------------------------

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$sdk_texinfo_path/bin:$sdk_help2man_path/bin:$PATH

export PKG_CONFIG_LIBDIR=$target_sysroot/nyan/popt/0/lib/pkgconfig
export PKG_CONFIG_SYSROOT_DIR=$target_sysroot

host_cc="$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
host_cflags='-O2 -pipe -fPIC'
host_ccld="$host_cc"
efivar_path=$target_sysroot/nyan/efivar/0

destdir=$target_sysroot/nyan/$src_name/0

. ./efibootmgr.sh

unset host_cc
unset host_cflags
unset host_ccld
unset efivar_path
unset destdir

unset PKG_CONFIG_LIBDIR
unset PKG_CONFIG_SYSROOT

# cleanup and tidying
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/bin/* || true

rm -Rf $pkg_dir
