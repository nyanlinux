cd ./src

# the following should be defined
#host_cc=gcc
#host_cflags='-O2 -pipe -fPIC'
#host_ccld=gcc
#efivar_path=$destdir

efidir=nyan
efi_loader=linux.efi
version=15

mkdir -p $destdir/bin
localedir=$destdir/share/locale

efibootmgr_cc="$host_cc $host_cflags					\
	-I./include							\
	-D_GNU_SOURCE -DLOCALEDIR=\"$localedir\"			\
	-DEFIBOOTMGR_VERSION=\"$version\"				\
	-DDEFAULT_LOADER=\"\\\\EFI\\\\$efidir\\\\$efi_loader\"		\
	-I$efivar_path/include/efivar -I$efivar_path/include -c -o"

$efibootmgr_cc efibootmgr.o efibootmgr.c
$efibootmgr_cc efi.o efi.c
$efibootmgr_cc unparse_path.o unparse_path.c

$host_ccld -o $destdir/bin/efibootmgr efibootmgr.o efi.o unparse_path.o -L$efivar_path/lib -lefivar -lefiboot -ldl

efibootdump_cc="$host_cc $host_cflags					\
	-I./include							\
	-D_GNU_SOURCE -DLOCALEDIR=\"$localedir\"			\
	-DEFIBOOTMGR_VERSION=\"$version\"				\
	-DDEFAULT_LOADER=\"\\\\EFI\\\\$efidir\\\\$efi_loader\"		\
	-I$efivar_path/include/efivar -I$efivar_path/include $(pkg-config --cflags popt) -c -o"

$efibootdump_cc efibootdump.o efibootdump.c
$efibootdump_cc unparse_path.o unparse_path.c

$host_ccld -o $destdir/bin/efibootdump efibootdump.o unparse_path.o -L$efivar_path/lib -lefivar -lefiboot $(pkg-config --libs popt) -ldl

unset efidir
unset efi_loader
unset version
unset localedir
unset efibootmgr_cc
unset efibootdump_cc

cd ..
