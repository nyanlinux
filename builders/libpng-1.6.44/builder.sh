src_name=libpng
mkdir /nyan/$src_name
version=${pkg_name##*-}
slot=$version
mkdir /nyan/$src_name/$slot
archive_name=$src_name-$version.tar.xz
url0=https://download.sourceforge.net/$src_name/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
export "CPPFLAGS=\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-I/nyan/zlib/current/include"
export 'CFLAGS=-O2 -pipe -fPIC -static-libgcc'
export "LDFLAGS=\
	-L/nyan/zlib/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
# must include -B here, since libtool crap is filtering it out
export "CC=gcc -B/nyan/glibc/current/lib"
$pkg_dir/configure \
	--prefix=/usr \
	--enable-shared \
	--enable-hardware-optimizations \
	--disable-static
unset CPPFLAGS
unset CFLAGS
unset LDFLAGS
unset CC

make -j $threads_n
#---------------------------------------------------------------------------------------------------
# attempt to get rid of pkgconf
install_dir=$build_dir/nyan_install_root
make DESTDIR=$install_dir install

rm -Rf /nyan/$src_name/$slot/include
cp -r $install_dir/usr/include /nyan/$src_name/$slot/include

mkdir /nyan/$src_name/$slot/lib
cp -f $install_dir/usr/lib/libpng16.so.16.44.0 /nyan/$src_name/$slot/lib/libpng16.so.16.44.0
ln -sTf libpng16.so.16.44.0 /nyan/$src_name/$slot/lib/libpng16.so
ln -sTf libpng16.so.16.44.0 /nyan/$src_name/$slot/lib/libpng.so
ln -sTf libpng16.so.16.44.0 /nyan/$src_name/$slot/lib/libpng16.so.16

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
