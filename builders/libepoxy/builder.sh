src_name=libepoxy
mkdir /nyan/$src_name
mkdir /nyan/$src_name/$slot
git_url0=git://github.com/anholt/$src_name

src_dir=$src_dir_root/$src_name
pkg_dir=$pkgs_dir_root/$pkg_name
rm -Rf $pkg_dir
cp -r $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/git/current/bin:\
$PATH\
"

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

# install our custom mini build system
cp -r $nyan_root/builders/$pkg_name/contrib ./

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

cat >$build_dir/local_conf.sh <<EOF
inc_store_dir=/nyan/$src_name/$slot/include
inc_store_virtual_dir=/nyan/$src_name/$slot/include
lib_store_dir=/nyan/$src_name/$slot/lib
lib_store_virtual_dir=/nyan/$src_name/$slot/lib
runtime_lib_dir=/nyan/$src_name/$slot/lib

libx11_inc_dir=/nyan/libX11/current/include
xorgproto_inc_dir=/nyan/xorgproto/current/include
mesa_gl_inc_dir=/nyan/mesa-gl/current/include

python3=/nyan/python/current/bin/python3
dslibepoxy_cc='gcc -ftls-model=global-dynamic -fpic -static-libgcc -O2 -pipe -fPIC -fvisibility=hidden -c -isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include'
dslibepoxy_ccld='gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-s -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc -shared -Wl,-soname,\$soname -Bsymbolic -Wl,--no-undefined'
EOF

$pkg_dir/contrib/make.gnulinux

cp -rf $build_dir/install_root/nyan/$src_name/$slot/* /nyan/$src_name/$slot

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
