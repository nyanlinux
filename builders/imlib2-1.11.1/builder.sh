src_name=imlib2
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://sourceforge.net/projects/enlightenment/files/$src_name-src/$version/$archive_name/download

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/pkgconf/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export "PKG_CONFIG_LIBDIR=\
/nyan/libjpeg-turbo/current/lib/pkgconfig:\
/nyan/libXext/current/lib/pkgconfig:\
/nyan/zlib/current/lib/pkgconfig:\
/nyan/freetype/current/lib/pkgconfig:\
/nyan/libpng/current/lib/pkgconfig:\
/nyan/zlib/current/lib/pkgconfig:\
/nyan/freetype/current/lib/pkgconfig:\
/nyan/libXau/current/lib/pkgconfig:\
/nyan/libpthread-stubs/current/lib/pkgconfig:\
/nyan/libxcb/current/lib/pkgconfig:\
/nyan/libX11/current/lib/pkgconfig:\
/nyan/xorgproto/current/share/pkgconfig"

# configure runs some programs
SAVED_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
export "LD_LIBRARY_PATH=\
/nyan/libXext/current/lib:\
/nyan/libXau/current/lib:\
/nyan/libxcb/current/lib:\
/nyan/libX11/current/lib:\
$LD_LIBRARY_PATH"

export PKG_CONFIG=pkgconf
export "CC=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-static-libgcc \
-Wl,-rpath-link,\
/nyan/libpng/current/lib:\
/nyan/libXau/current/lib:\
/nyan/libxcb/current/lib:\
/nyan/libx/current/lib:\
/nyan/glibc/current/lib"
export CPPFLAGS="\
	$(pkgconf --cflags-only-I x11 xext) \
	-I/nyan/bzip2/current/include \
	-I/nyan/zlib/current/include \
	-I/nyan/giflib/current/include"
export CFLAGS='-O2 -pipe -fPIC'
export LDFLAGS="\
	$(pkgconf --libs x11 xext) \
	-L/nyan/bzip2/current/lib \
	-L/nyan/zlib/current/lib \
	-L/nyan/giflib/current/lib \
	-Wl,-s"
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--disable-static
unset PKG_CONFIG_LIBDIR
unset PKG_CONFIG
export LD_LIBRARY_PATH=$SAVED_LD_LIBRARY_PATH
unset CC
unset CPPFLAGS
unset CFLAGS
unset LDFLAGS

make -j $threads_n
make install

find /nyan/$src_name/$slot -name '*.la' | xargs rm -f

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
