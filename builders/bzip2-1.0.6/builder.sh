src_name=bzip2
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=http://www.bzip.org/$version/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

# RANLIB=$target_gnu_triple-ranlib																			\
make	"CC=$target_gnu_triple-gcc \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-B/nyan/glibc/current/lib \
		-L/nyan/glibc/current/lib \
		-Wl,-rpath-link,/nyan/glibc/current/lib \
		-Wl,-s \
		-static-libgcc"	\
	AR=$target_gnu_triple-ar \
	'CFLAGS=-fPIC -O2 -pipe -D_FILE_OFFSET_BITS=64' \
	all

make install PREFIX=/nyan/$src_name/$slot

rm -Rf /nyan/$src_name/$slot/man

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
