src_name=texinfo
version=6.5
archive_name=$src_name-$version.tar.xz
url0=http://ftpmirror.gnu.org/$src_name/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

# need to patch directly the configure script, since we are bootstraping a SDK
cd $src_dir
sed -i -e 's/tinfo/tinfow/g' ./configure

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export "CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
export LDFLAGS=-L/nyan/ncurses/current/lib
export PERL=/nyan/perl/current/bin/perl
$src_dir/configure			\
	--prefix=/nyan/texinfo/0	\
	--disable-nls
unset PERL
unset LDFLAGS
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/0/share/man
# we leave the shared info dir for texinfo...
find /nyan/$src_name/0 -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then strip -s $f; fi; done

rm -Rf $build_dir $src_dir
