src_name=binutils
version=${pkg_name##*-}
tcc_pkg_git_commit=$(printf "$pkg_path" | grep -E -o 'tcc-[[:digit:]abcdef]+')
slot=0
archive_name=binutils-$version.tar.xz
toolchain_name=$src_name-$version-$tcc_pkg_git_commit
url0=http://ftpmirror.gnu.org/binutils/$archive_name

pkg_dir=$pkgs_dir_root/$toolchain_name/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root/$toolchain_name
cp -f $src_dir_root/$archive_name $pkgs_dir_root/$toolchain_name
cd $pkgs_dir_root/$toolchain_name
tar xf $archive_name

build_dir=$builds_dir_root/$toolchain_name/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

bfd_sub_conf_opts="\
	--with-mmap  \
"
binutils_sub_conf_opts="\
	--disable-libctf \
	--disable-rpath \
"
ld_gas_bfd_sub_conf_opts="\
	--disable-plugins \
	--disable-nls \
"

# Usual autotools/meson/cmake crap.
mkdir $build_dir/bin
cat >$build_dir/bin/makeinfo <<EOF
#!/bin/sh
printf "Makeinfo crap detected!\n"
EOF
chmod +x $build_dir/bin/makeinfo

PATH_SAVED=$PATH
export PATH="\
$build_dir/bin:\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

# nm and ar were configured first with build system ones

# XXX: the "--static" option is special as it will setup the autotools and libtool to generate
# static executables.
export "CC_FOR_BUILD=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-s \
	--static"
export "CFLAGS_FOR_BUILD=-O2 -pipe -fPIC"
# don't pass CPP or the brain damage autotools/meson/cmake/etc will get it all wrong
export "CPPFLAGS=\
	-I/nyan/zlib/current/include \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include"
export "CFLAGS=-O2 -pipe -fPIC"

# XXX: the "--static" option is special as it will setup the autotools and libtool to generate
# static executables.
export "LDFLAGS=-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-L/nyan/zlib/current/lib \
	-Wl,-s \
	-static-libgcc \
	--static"
# you must add the CPPFLAGS and LDFLAGS here to please that horrible autotools/meson/cmake/etc
export "CC=$target_gnu_triple-gcc $CPPFLAGS $LDFLAGS"
export "AR=$target_gnu_triple-ar"
$pkg_dir/configure \
	--prefix=/nyan/toolchains/binutils-tcc/$toolchain_name/$slot \
	--program-prefix=$target_gnu_triple- \
	--enable-gold=no \
	--enable-gprofng=no \
	--with-system-zlib \
  	--with-static-standard-libraries \
	--disable-multilib \
	--disable-plugins \
	--enable-serial-host-configure \
	--enable-serial-build-configure \
	--enable-serial-target-configure \
	--enable-year2038 \
	$bfd_sub_conf_opts \
	$binutils_sub_conf_opts \
	$ld_gas_bfd_sub_conf_opts
unset CC_FOR_BUILD
unset CFLAGS_FOR_BUILD
unset CPPFLAGS
unset CFLAGS
unset LDFLAGS
unset CC
unset CXX
unset AR

make -j $threads_n
make install

rm -Rf /nyan/toolchains/binutils-tcc/$toolchain_name/$slot/share
rm -Rf /nyan/toolchains/binutils-tcc/$toolchain_name/$slot/lib/*.la

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$toolchain_name/$archive_name
rm -Rf $build_dir $pkg_dir $builds_dir_root/$toolchain_name $pkgs_dir_root/$toolchain_name
