src_name=mesa
git_url0=git://anongit.freedesktop.org/mesa/$src_name

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -Hr $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
# c++ crap is at least c++17, jez... gaben did not get the memo about c++
# toxicity, which is even worse at the system level.
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/git/current/bin:\
$PATH\
"

if test "x$git_commit" != "x"; then
	git checkout --force $git_commit
	git reset --hard
fi

# install our canonical build system from the contrib dir
cp -rf $nyan_root/builders/$pkg_name/contrib $pkg_dir

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

$pkg_dir/contrib/x86_64_amdgpu_linux_gnu_vulkan_x11_drm_gcc.sh

mkdir -p /nyan/mesa-vulkan/$slot/lib
mkdir -p /nyan/mesa-vulkan/$slot/share/vulkan/icd.d

cp -f $build_dir/libvulkan_radeon.so /nyan/mesa-vulkan/$slot/libvulkan_radeon.so
cp -f $nyan_root/builders/$pkg_name/radeon_icd.x86_64.json /nyan/mesa-vulkan/$slot/radeon_icd.x86_64.json

#-------------------------------------------------------------------------------
sed -i -e "\
s:MAX_API_VERSION:1.3:;\
s:LIB_INSTALL_DIR:/usr/lib:" \
/nyan/mesa-vulkan/$slot/radeon_icd.x86_64.json
#-------------------------------------------------------------------------------

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
