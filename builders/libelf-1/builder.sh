src_name=libelf
version=0.8.13
archive_name=$src_name-$version.tar.gz
url0=http://www.mr511.de/software/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--prefix=/nyan/libelf/1		\
	--disable-compat		\
	--enable-elf64			\
	--disable-versioning		\
	--disable-nls			\
	--disable-shared		\
	--enable-static			\
	--disable-gnu-names		\
	--disable-extended-format	\
	--disable-sanity-checks		\
	--disable-debug
unset CFLAGS
unset CC

make -j $threads_n AR=ar
make install

rm -Rf $build_dir $src_dir
