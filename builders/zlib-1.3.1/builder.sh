src_name=zlib
mkdir /nyan/$src_name
version=${pkg_name##*-}
slot=$version
mkdir /nyan/$src_name/$slot
archive_name=$src_name-$version.tar.xz
url0=http://zlib.net/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
export "CFLAGS=\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-O2 -pipe -fPIC -static-libgcc -Wno-error"
./configure --prefix=/usr

make -j $threads_n

install_dir=$pkg_dir/nyan_install_root
mkdir $install_dir
make DESTDIR=$install_dir install
unset CFLAGS
#---------------------------------------------------------------------------------------------------
rm -Rf /nyan/$src_name/$slot/include
cp -r $install_dir/usr/include /nyan/$src_name/$slot/include

mkdir /nyan/$src_name/$slot/lib
cp -f $install_dir/usr/lib/libz.a /nyan/$src_name/$slot/lib/libz.a
cp -f $install_dir/usr/lib/libz.so.1.3.1 /nyan/$src_name/$slot/lib/libz.so.1.3.1
ln -sTf libz.so.1.3.1 /nyan/$src_name/$slot/lib/libz.so
ln -sTf libz.so.1.3.1 /nyan/$src_name/$slot/lib/libz.so.1
#---------------------------------------------------------------------------------------------------
export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $pkg_dir
