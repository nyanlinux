printf "Running compiler code generators-->\n"
mkdir $build_dir/src
mkdir $build_dir/src/compiler
#===============================================================================
export PYTHONPATH=$mako
$python3 $src_dir/src/compiler/builtin_types_h.py \
$build_dir/src/compiler/builtin_types.h &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/builtin_types_c.py \
$build_dir/src/compiler/builtin_types.c &
#------------------------------------------------------------------------------
unset PYTHONPATH
printf "<--compiler code generation started\n"
