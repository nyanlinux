src_name=perl
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=http://www.cpan.org/src/5.0/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name


cd $pkg_dir

PATH_SAVED=$PATH
# expect sh to be in sh path
export PATH="\
/bin:\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}
OLD_PATH=$PATH
export PATH=/bin:$PATH

# Without adding manually our system include directory, Configure script will badly configure the build
# Configure thinks dlopen is in libc, add -ldl
# miniperl linking thinks some math functions are in libc, add -lm

# ** WARNING ** miniperl and full perl want hardcoded /bin/pwd or /usr/bin/pwd
# for the Cwd.pm "cwd" perl function to work. Must use getcwd perl function to
# call libc one.
ln -Tfs /nyan/busybox/current/bin/pwd /bin/pwd

./Configure \
	"-Dcc=$target_gnu_triple-gcc \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
-B/nyan/glibc/current/lib \
-L/nyan/glibc/current/lib \
-Wl,-rpath-link,/nyan/glibc/current/lib \
-Wl,-s \
-static-libgcc"	\
	-des \
	-Dprefix=/nyan/$src_name/$slot \
	-Doptimize=-O2 \
	'-Accflags=-fPIC -pipe -w' \
	-Dar=$target_gnu_triple-ar \
	"-Alocincpth=\
/nyan/glibc/current/include \
/nyan/linux-headers/current/include" \
	'-Alibs=-lm -ldl'

sed -i -e 's/-fstack-protector-strong//' ./cflags
sed -i -e 's/-fstack-protector//' ./cflags

make -j $threads_n
make install

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $pkg_dir
