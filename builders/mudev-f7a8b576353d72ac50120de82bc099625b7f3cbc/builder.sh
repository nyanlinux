src_name=mudev
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=git://git.launchpad.net/$src_name

src_dir=$src_dir_root/$src_name
pkg_dir=$pkgs_dir_root/$pkg_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -Hr $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

$pkg_dir/make \
	--prefix=/nyan/$src_name/$slot \
	--usb-database=/share/hwdata/usb.ids \
	--pci-database=/share/hwdata/pci.ids \
	--enable-logging \
	"--bin-cc=$target_gnu_triple-gcc -std=c99 -O2 -pipe -fPIC -c -isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include -static-libgcc" \
	"--bin-ccld=$target_gnu_triple-gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,--as-needed -Wl,-s -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc" \
	"--libudev-cc=$target_gnu_triple-gcc -std=c99 -O2 -pipe -fPIC -c -isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include -static-libgcc" \
	"--libudev-ccld=$target_gnu_triple-gcc -shared -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-s -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc -Wl,--as-needed -Wl,-soname,libudev.so.0"

rm -Rf /nyan/$src_name/$slot
cp -r $build_dir/fake_root/* /

# add/update the hardware data from the src dir
mkdir -p /share/hwdata
cp $src_dir_root/pci.ids /share/hwdata
cp $src_dir_root/usb.ids /share/hwdata

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $build_dir $pkg_dir
