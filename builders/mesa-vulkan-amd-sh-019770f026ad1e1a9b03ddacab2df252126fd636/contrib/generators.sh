#===============================================================================
# removing the braindamaged qsort class
cp -f $src_dir/src/compiler/nir/nir.c $build_dir/src/compiler/nir/nir.c
sed -i $build_dir/src/compiler/nir/nir.c \
	-e '/u_qsort.h/ d' \
	-e 's/util_qsort_r/qsort_r/' &
cp -f $src_dir/src/util/mesa_cache_db.c $build_dir/src/util/mesa_cache_db.c
sed -i $build_dir/src/util/mesa_cache_db.c \
	-e '/u_qsort.h/ d' \
	-e 's/util_qsort_r/qsort_r/' &
#===============================================================================
# that compute radix sort implementation ... OMFG! Text book what NOT to do.
# Where is the plain and simple C coded spir-v assembler for those shaders?
# blame: Konstantin Seurer
cp -f $src_dir/src/amd/vulkan/meta/radv_meta.c $build_dir/src/amd/vulkan/meta
patch -p 1 -i $src_dir/contrib/radv_meta.c.patch
cp -f $src_dir/src/amd/vulkan/meta/radv_meta_copy.c $build_dir/src/amd/vulkan/meta
patch -p 1 -i $src_dir/contrib/radv_meta_copy.c.patch
cp -f $src_dir/src/amd/vulkan/radv_device.c $build_dir/src/amd/vulkan
patch -p 1 -i $src_dir/contrib/radv_device.c.patch
# dudes... tracers should be cleanly compilable out... smells forced corpo crap
cp -f $src_dir/contrib/radv_no_tracers.c $build_dir/src/amd/vulkan
#===============================================================================
# vulkan util
export PYTHONPATH=$mako
$python3 $src_dir/src/vulkan/util/gen_enum_to_str.py \
	--beta false \
	--xml $vulkan_api_xml \
	--outdir $build_dir/src/vulkan/util 

# our ndebug/release version
cp -f $src_dir/contrib/vk_enum_to_str.c $src_dir/contrib/vk_enum_to_str.h \
	$build_dir/src/vulkan/util 

$python3 $src_dir/src/vulkan/util/vk_dispatch_table_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--out-c $build_dir/src/vulkan/util/vk_dispatch_table.c \
	--out-h $build_dir/src/vulkan/util/vk_dispatch_table.h &

$python3 $src_dir/src/vulkan/util/vk_extensions_gen.py \
	--xml $vulkan_api_xml \
	--out-c $build_dir/src/vulkan/util/vk_extensions.c \
	--out-h $build_dir/src/vulkan/util/vk_extensions.h &

$python3 $src_dir/src/vulkan/util/vk_struct_type_cast_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--outdir $build_dir/src/vulkan/util &

$python3 $src_dir/src/compiler/nir/nir_builder_opcodes_h.py \
>$build_dir/src/compiler/nir/nir_builder_opcodes.h &

$python3 $src_dir/src/compiler/nir/nir_opcodes_h.py \
>$build_dir/src/compiler/nir/nir_opcodes.h &

$python3 $src_dir/src/compiler/nir/nir_intrinsics_h.py \
--outdir $build_dir/src/compiler/nir &

$python3 $src_dir/src/compiler/nir/nir_intrinsics_indices_h.py \
--outdir $build_dir/src/compiler/nir &
unset PYTHONPATH
#===============================================================================
# vulkan layer crap
#export PYTHONPATH=$mako
#$python3 $src_dir/src/amd/vulkan/layers/radv_annotate_layer_gen.py \
#	--xml $vulkan_api_xml \
#	--out-c $build_dir/src/amd/vulkan/layers/radv_annotate_layer.c \
#	--beta false
#unset PYTHONPATH
#===============================================================================
# vulkan runtime
export PYTHONPATH=$mako
$python3 $src_dir/src/vulkan/util/vk_entrypoints_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--proto --weak \
	--out-h $build_dir/src/vulkan/runtime/vk_common_entrypoints.h \
	--out-c $build_dir/src/vulkan/runtime/vk_common_entrypoints.c \
	--prefix vk_common &
$python3 $src_dir/src/vulkan/util/vk_entrypoints_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--proto --weak \
	--out-h $build_dir/src/vulkan/runtime/vk_cmd_enqueue_entrypoints.h \
	--out-c $build_dir/src/vulkan/runtime/vk_cmd_enqueue_entrypoints.c \
	--prefix vk_cmd_enqueue &
$python3 $src_dir/src/vulkan/util/vk_cmd_queue_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--out-h $build_dir/src/vulkan/runtime/vk_cmd_queue.h \
	--out-c $build_dir/src/vulkan/runtime/vk_cmd_queue.c &
$python3 $src_dir/src/vulkan/util/vk_dispatch_trampolines_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--out-h $build_dir/src/vulkan/runtime/vk_dispatch_trampolines.h \
	--out-c $build_dir/src/vulkan/runtime/vk_dispatch_trampolines.c &
$python3 $src_dir/src/vulkan/util/vk_physical_device_features_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--out-h $build_dir/src/vulkan/runtime/vk_physical_device_features.h \
	--out-c $build_dir/src/vulkan/runtime/vk_physical_device_features.c &
$python3 $src_dir/src/vulkan/util/vk_physical_device_properties_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--out-h $build_dir/src/vulkan/runtime/vk_physical_device_properties.h \
	--out-c $build_dir/src/vulkan/runtime/vk_physical_device_properties.c &
$python3 $src_dir/src/vulkan/util/vk_physical_device_spirv_caps_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--out-c $build_dir/src/vulkan/runtime/vk_physical_device_spirv_caps.c &
$python3 $src_dir/src/vulkan/runtime/vk_format_info_gen.py \
	--xml $vulkan_api_xml \
	--out-h $build_dir/src/vulkan/runtime/vk_format_info.h \
	--out-c $build_dir/src/vulkan/runtime/vk_format_info.c &
$python3 $src_dir/src/vulkan/util/vk_synchronization_helpers_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--out-c $build_dir/src/vulkan/runtime/vk_synchronization_helpers.c &
unset PYTHONPATH
#===============================================================================
# wsi x11 (Window System Interface)
export PYTHONPATH=$mako
$python3 $src_dir/src/vulkan/util/vk_entrypoints_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--proto --weak \
	--out-h $build_dir/src/vulkan/wsi/wsi_common_entrypoints.h \
	--out-c $build_dir/src/vulkan/wsi/wsi_common_entrypoints.c \
	--prefix wsi &
unset PYTHONPATH
#===============================================================================
# amd common archive
$python3 $src_dir/src/amd/common/sid_tables.py \
	$src_dir/src/amd/common/sid.h \
	$src_dir/src/amd/registers/gfx6.json \
	$src_dir/src/amd/registers/gfx7.json \
	$src_dir/src/amd/registers/gfx8.json \
	$src_dir/src/amd/registers/gfx81.json \
	$src_dir/src/amd/registers/gfx9.json \
	$src_dir/src/amd/registers/gfx940.json \
	$src_dir/src/amd/registers/gfx10.json \
	$src_dir/src/amd/registers/gfx10-rsrc.json \
	$src_dir/src/amd/registers/gfx103.json \
	$src_dir/src/amd/registers/gfx11.json \
	$src_dir/src/amd/registers/gfx115.json \
	$src_dir/src/amd/registers/gfx11-rsrc.json \
	$src_dir/src/amd/registers/gfx12.json \
	$src_dir/src/amd/registers/gfx12-rsrc.json \
	$src_dir/src/amd/registers/pkt3.json \
	$src_dir/src/amd/registers/registers-manually-defined.json \
	>$build_dir/src/amd/common/sid_tables.h &
$python3 $src_dir/src/amd/registers/makeregheader.py \
	$src_dir/src/amd/registers/gfx6.json \
	$src_dir/src/amd/registers/gfx7.json \
	$src_dir/src/amd/registers/gfx8.json \
	$src_dir/src/amd/registers/gfx81.json \
	$src_dir/src/amd/registers/gfx9.json \
	$src_dir/src/amd/registers/gfx940.json \
	$src_dir/src/amd/registers/gfx10.json \
	$src_dir/src/amd/registers/gfx10-rsrc.json \
	$src_dir/src/amd/registers/gfx103.json \
	$src_dir/src/amd/registers/gfx11.json \
	$src_dir/src/amd/registers/gfx115.json \
	$src_dir/src/amd/registers/gfx11-rsrc.json \
	$src_dir/src/amd/registers/gfx12.json \
	$src_dir/src/amd/registers/gfx12-rsrc.json \
	$src_dir/src/amd/registers/pkt3.json \
	$src_dir/src/amd/registers/registers-manually-defined.json \
	--sort address \
	--guard AMDGFXREGS_H \
	>$build_dir/src/amd/common/amdgfxregs.h &
#===============================================================================
# libcompiler required by libnir
export PYTHONPATH=$mako
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/glsl/ir_expression_operation.py enum \
	>$build_dir/src/compiler/glsl/ir_expression_operation.h &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/builtin_types_h.py \
$build_dir/src/compiler/builtin_types.h &
#------------------------------------------------------------------------------
$python3 $src_dir/src/compiler/builtin_types_c.py \
$build_dir/src/compiler/builtin_types.c &
#------------------------------------------------------------------------------
unset PYTHONPATH
#===============================================================================
# libnir
export PYTHONPATH=$mako
$python3 $src_dir/src/compiler/spirv/spirv_info_gen.py \
--json	$src_dir/src/compiler/spirv/spirv.core.grammar.json \
--out-h	$build_dir/src/compiler/spirv/spirv_info.h \
--out-c	$build_dir/src/compiler/spirv/spirv_info.c &

$python3 $src_dir/src/compiler/spirv/vtn_gather_types_c.py \
	$src_dir/src/compiler/spirv/spirv.core.grammar.json \
	$build_dir/src/compiler/spirv/vtn_gather_types.c &

$python3 $src_dir/src/compiler/spirv/vtn_generator_ids_h.py \
	$src_dir/src/compiler/spirv/spir-v.xml \
	$build_dir/src/compiler/spirv/vtn_generator_ids.h &
# nir generated files
$python3 $src_dir/src/compiler/nir/nir_constant_expressions.py \
	>$build_dir/src/compiler/nir/nir_constant_expressions.c &

$python3 $src_dir/src/compiler/nir/nir_opt_algebraic.py \
	--out $build_dir/src/compiler/nir/nir_opt_algebraic.c &

$python3 $src_dir/src/compiler/nir/nir_opcodes_c.py \
>$build_dir/src/compiler/nir/nir_opcodes.c &

$python3 $src_dir/src/compiler/nir/nir_intrinsics_c.py \
--outdir $build_dir/src/compiler/nir &
unset PYTHONPATH
#===============================================================================
# libaco
export PYTHONPATH=$mako
$python3 $src_dir/src/amd/compiler/aco_opcodes_h.py \
	>$build_dir/src/amd/compiler/aco_opcodes.h &
$python3 $src_dir/src/amd/compiler/aco_opcodes_cpp.py \
	>$build_dir/src/amd/compiler/aco_opcodes.cpp &
$python3 $src_dir/src/amd/compiler/aco_builder_h.py \
	>$build_dir/src/amd/compiler/aco_builder.h &
unset PYTHONPATH
#===============================================================================
# libmesautils
export PYTHONPATH=$mako:$yaml
$python3 $src_dir/src/util/format_srgb.py \
	>$build_dir/src/util/format_srgb.c

$python3 $src_dir/src/util/format/u_format_table.py \
--enums \
$src_dir/src/util/format/u_format.yaml \
	>$build_dir/src/util/format/u_format_gen.h &

$python3 $src_dir/src/util/format/u_format_table.py \
--header \
$src_dir/src/util/format/u_format.yaml \
	>$build_dir/src/util/format/u_format_pack.h

$python3 $src_dir/src/util/format/u_format_table.py \
$src_dir/src/util/format/u_format.yaml \
	>$build_dir/src/util/u_format_table.c

# oooof!
$python3 $src_dir/src/util/driconf_static.py \
$src_dir/src/util/00-mesa-defaults.conf \
$build_dir/src/util/driconf_static.h
unset PYTHONPATH
#$src_dir/src/util/bitset_test.cpp wtf? rotten brain?
#===============================================================================
# amd vulkan
export PYTHONPATH=$mako:$yaml
#------------------------------------------------------------------------------
# specific API to work around _still maintained at the time of writing _ game
# executable... this is the beginning of the end... not even all native
# elf/linux... god...
$python3 $src_dir/src/vulkan/util/vk_entrypoints_gen.py \
	--beta false \
	--xml $vulkan_api_xml \
	--proto --weak \
	--out-h $build_dir/src/amd/vulkan/radv_entrypoints.h \
	--out-c $build_dir/src/amd/vulkan/radv_entrypoints.c \
	--prefix radv \
	--device-prefix metro_exodus \
	--device-prefix quantic_dream \
	--device-prefix rage2 &
#------------------------------------------------------------------------------
#$python3 $src_dir/src/amd/vulkan/radv_extensions.py \
#	--xml $vulkan_api_xml \
#	--out-c radv_extensions.c \
#	--out-h radv_extensions.h &
#------------------------------------------------------------------------------
$python3 $src_dir/src/amd/common/gfx10_format_table.py \
	$src_dir/src/util/format/u_format.yaml \
	$src_dir/src/amd/registers/gfx10-rsrc.json \
	$src_dir/src/amd/registers/gfx11-rsrc.json \
	>$build_dir/src/amd/common/gfx10_format_table.c &
#------------------------------------------------------------------------------
git_sha1=no_git_sha1_available
if test -d $src_dir/.git; then
	git_sha1=$(git --git-dir=$src_dir/.git rev-parse HEAD)
fi
echo git_sha1=$git_sha1
echo "#define MESA_GIT_SHA1 \"$git_sha1\"" >$build_dir/src/git_sha1.h &
unset PYTHONPATH
#===============================================================================
