src_name=openssh
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
#	"--with-libs=$(pkgconf --static --libs libcrypto)"
export 'CFLAGS=-O2 -pipe -fPIC'
export "CC=gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--without-stackprotect \
	--without-hardening \
	--disable-strip \
	--with-zlib=/nyan/zlib/current \
	--with-ssl-dir=/nyan/libressl/current
unset CFLAGS
unset CC

make -j $threads_n
make install

rm -Rf /nyan/$src_name/$slot/share

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
