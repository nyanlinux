src_name=vulkan-headers
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=https://github.com/khronosgroup/$pkg_name


pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

#-------------------------------------------------------------------------------

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/git/current/bin:\
$PATH\
"

if test "x$git_commit" != "x"; then
	git checkout --force $git_commit
	git reset --hard
fi

#-------------------------------------------------------------------------------

install_dir=/nyan/vulkan-headers/$slot/include
mkdir -p $install_dir
cp -r $pkg_dir/include/vulkan $install_dir
cp -r $pkg_dir/include/vk_video $install_dir

#-------------------------------------------------------------------------------

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $pkg_dir
