src_name=lynx
version=${pkg_name##*-}
slot=$version
# meh
archive_name=$src_name-cur.tar.bz2
url0=http://invisible-island.net/datafiles/release/$archive_name

slot=$version

pkg_dir=$pkgs_dir_root/$src_name$version
mkdir -p $pkgs_dir_root
rm -Rf $pkg_dir
cp $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name
cd $pkg_dir

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export "PATH=\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
/nyan/ncurses/current/bin:\
$PATH\
"
export CPPFLAGS="\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-I/nyan/libressl/current/include \
	-I/nyan/zlib/current/include \
	-I/nyan/bzip2/current/include"
export CFLAGS='-O2 -pipe -fPIC -static-libgcc'
export LDFLAGS="\
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-L/nyan/libressl/current/lib \
	-L/nyan/zlib/current/lib \
	-L/nyan/bzip2/current/lib \
	-Wl,-s \
	-static-libgcc"
export CC=gcc
export AR=ar
export LIBS=-lpthread
# --disable-trace is broken
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--disable-debug \
	--enable-trace \
	--disable-nls \
	--disable-full-paths \
	--with-ssl \
	--without-pkg-config \
	--enable-ipv6 \
	--with-screen=ncurses \
	--enable-widec \
	--enable-htmlized-cfg \
	--disable-local-docs \
	--disable-bibp-urls \
  	--disable-menu-options \
	--disable-sessions \
	--disable-session-cache \
	--enable-japanese-utf8 \
	--enable-wcwidth-support \
	--enable-default-colors \
	--enable-nested-tables \
	--disable-idna \
	--disable-partial \
	--disable-scrollbar \
	--enable-charset-choice \
	--enable-externs \
	--disable-nsl-fork \
	--with-bzlib \
	--with-zlib \
	--disable-finger \
	--disable-gopher \
	--disable-news \
	--disable-dired \
	--without-x \
	--disable-rpath-hack
unset CPPFLAGS
unset CFLAGS
unset LDFLAGS
unset CC
unset AR
unset LIBS

make -j $threads_n
make install 

rm -Rf /nyan/$src_name/$slot/share

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
