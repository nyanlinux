src_name=util-macros
version=1.19.2
archive_name=$src_name-$version.tar.bz2
url0=http://xorg.freedesktop.org/releases/individual/util/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export PKG_CONFIG_LIBDIR=
export PKG_CONFIG_SYSROOT_DIR=$target_sysroot

# m4 macros which are specific to xorg 
$src_dir/configure				\
	--build=$build_gnu_triple		\
	--host=$target_gnu_triple		\
	--prefix=/nyan/util-macro/0

make
make install DESTDIR=$target_sysroot

rm -Rf $build_dir $src_dir
