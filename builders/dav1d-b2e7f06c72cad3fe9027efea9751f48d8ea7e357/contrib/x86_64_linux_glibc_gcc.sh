#!/bin/sh

# Canonical specialized, cheap build scripts for x86_64 gnu/linux distros.
# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the script in the "contrib" dir at the top of the source tree,
# create somewhere else a build directory, cd into it, and call from there this
# script.
#===============================================================================


#===============================================================================
# build dir and src dir
build_dir=$(readlink -f .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(readlink -f $(dirname $0)/..)
echo "src_dir=$src_dir"
#===============================================================================


#===============================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===============================================================================


#===============================================================================
# all symbols are tagged hidden by default, then only the public symbol
# will be tagged public explicitely in the code (with extensions to C)
if test "${cc-unset}" = unset; then
cc="gcc -pipe -fPIC -O2 -c -static-libgcc \
-fvisibility=hidden \
-fomit-frame-pointer -ffast-math \
-idirafter /nyan/glibc/current/include \
-idirafter /nyan/linux-headers/current/include"
fi
#===============================================================================


#===============================================================================
if test "${nasm-unset}" = unset; then
nasm="nasm"
fi
#===============================================================================


#===============================================================================
if test "${ar-unset}" = unset; then
ar='ar rcs'
fi
#===============================================================================

#===============================================================================
if test "${git_version-unset}" = unset; then
git_version="tamereenshort"
fi
#===============================================================================


#===============================================================================
# should be in sync with the code
version_major=6
version_minor=6
version_patch=0
mkdir -p $build_dir/dav1d
cp -f $src_dir/include/dav1d/version.h.in $build_dir/dav1d/version.h
sed -e "s:@DAV1D_API_VERSION_MAJOR@:$version_major:;\
s:@DAV1D_API_VERSION_MINOR@:$version_minor:;\
s:@DAV1D_API_VERSION_PATCH@:$version_patch:" -i $build_dir/dav1d/version.h
#------------------------------------------------------------------------------
cp -f $src_dir/include/vcs_version.h.in $build_dir/vcs_version.h
sed -e "s:@VCS_TAG@:$git_version:" -i $build_dir/vcs_version.h
#===============================================================================


#===============================================================================
touch $build_dir/config.h
touch $build_dir/config.asm
#===============================================================================


#===============================================================================
# make it work with cpp _and_ nasm: '= 1' for the macros and an ending '/' for the 
# paths

dav1d_cppflags="\
-DNDEBUG=1 \
-DHAVE_ASM=1 \
-DHAVE_AVX512ICL=1 \
-DCONFIG_8BPC=1 \
-DCONFIG_16BPC=1 \
-DARCH_X86=1 \
-DARCH_X86_64=1 \
-DPIC=1 \
-DSTACK_ALIGNMENT=32 \
-DFORCE_VEX_ENCODING=1 \
\
-I$build_dir/ \
-I$src_dir/src/ \
-I$src_dir/ \
-I$src_dir/include/compat/gcc/ \
-I$src_dir/include/dav1d/ \
-I$src_dir/include/ \
"

glibc_cppflags="\
-DHAVE_POSIX_MEMALIGN=1 \
-DHAVE_DLSYM=1 \
-D_GNU_SOURCE=1 \
"

libdav1d_nobitdepth_generic_c_files="\
src/cdf.c \
src/cpu.c \
src/ctx.c \
src/data.c \
src/decode.c \
src/dequant_tables.c \
src/getbits.c \
src/intra_edge.c \
src/itx_1d.c \
src/lf_mask.c \
src/log.c \
src/mem.c \
src/msac.c \
src/obu.c \
src/pal.c \
src/picture.c \
src/qm.c \
src/ref.c \
src/refmvs.c \
src/scan.c \
src/tables.c \
src/warpmv.c \
src/wedge.c \
"

libdav1d_nobitdepth_x86_c_files="\
src/x86/cpu.c \
"

libdav1d_bitdepth_generic_c_files="\
src/cdef_apply_tmpl.c \
src/cdef_tmpl.c \
src/fg_apply_tmpl.c \
src/filmgrain_tmpl.c \
src/ipred_prepare_tmpl.c \
src/ipred_tmpl.c \
src/itx_tmpl.c \
src/lf_apply_tmpl.c \
src/loopfilter_tmpl.c \
src/looprestoration_tmpl.c \
src/lr_apply_tmpl.c \
src/mc_tmpl.c \
src/recon_tmpl.c \
"

libdav1d_nobitdepth_x86_nasm_files="\
src/x86/cpuid.asm \
src/x86/msac.asm \
src/x86/pal.asm \
src/x86/refmvs.asm \
src/x86/cdef_avx2.asm \
src/x86/itx_avx2.asm \
src/x86/looprestoration_avx2.asm \
src/x86/cdef_sse.asm \
src/x86/itx_sse.asm \
"

libdav1d_8bitdepth_x86_nasm_files="\
src/x86/cdef_avx512.asm \
src/x86/filmgrain_avx512.asm \
src/x86/ipred_avx512.asm \
src/x86/itx_avx512.asm \
src/x86/loopfilter_avx512.asm \
src/x86/looprestoration_avx512.asm \
src/x86/mc_avx512.asm \
\
src/x86/filmgrain_avx2.asm \
src/x86/ipred_avx2.asm \
src/x86/loopfilter_avx2.asm \
src/x86/mc_avx2.asm \
\
src/x86/filmgrain_sse.asm \
src/x86/ipred_sse.asm \
src/x86/loopfilter_sse.asm \
src/x86/looprestoration_sse.asm \
src/x86/mc_sse.asm \
"

libdav1d_16bitdepth_x86_nasm_files="\
src/x86/cdef16_avx512.asm \
src/x86/filmgrain16_avx512.asm \
src/x86/ipred16_avx512.asm \
src/x86/itx16_avx512.asm \
src/x86/loopfilter16_avx512.asm \
src/x86/looprestoration16_avx512.asm \
src/x86/mc16_avx512.asm \
\
src/x86/cdef16_avx2.asm \
src/x86/filmgrain16_avx2.asm \
src/x86/ipred16_avx2.asm \
src/x86/itx16_avx2.asm \
src/x86/loopfilter16_avx2.asm \
src/x86/looprestoration16_avx2.asm \
src/x86/mc16_avx2.asm \
\
src/x86/cdef16_sse.asm \
src/x86/filmgrain16_sse.asm \
src/x86/ipred16_sse.asm \
src/x86/itx16_sse.asm \
src/x86/loopfilter16_sse.asm \
src/x86/looprestoration16_sse.asm \
src/x86/mc16_sse.asm \
"

libdav1d_entrypoints_c_files="\
src/lib.c \
src/thread_task.c \
"
#===============================================================================


#===============================================================================
for f in $libdav1d_nobitdepth_generic_c_files $libdav1d_nobitdepth_x86_c_files $libdav1d_entrypoints_c_files
do
	mkdir -p $build_dir/$(dirname $f)
	libdav1d_obj=$build_dir/$f.o
	libdav1d_objs="$libdav1d_objs $libdav1d_obj"

	printf "CC NOBITDEPTH $src_dir/$f\n"
	$cc $dav1d_cppflags $glibc_cppflags $src_dir/$f -o $libdav1d_obj &
done

for f in $libdav1d_bitdepth_generic_c_files
do
	mkdir -p $build_dir/$(dirname $f)
	libdav1d_8_obj=$build_dir/$f.8.o
	libdav1d_16_obj=$build_dir/$f.16.o
	libdav1d_objs="$libdav1d_objs $libdav1d_8_obj $libdav1d_16_obj"

	printf "CC 8BITS $src_dir/$f\n"
	$cc $dav1d_cppflags $glibc_cppflags $src_dir/$f -DBITDEPTH=8 -o $libdav1d_8_obj &
	printf "CC 16BITS $src_dir/$f\n"
	$cc $dav1d_cppflags $glibc_cppflags $src_dir/$f -DBITDEPTH=16 -o $libdav1d_16_obj &
done

for f in $libdav1d_nobitdepth_x86_nasm_files $libdav1d_8bitdepth_x86_nasm_files $libdav1d_16bitdepth_x86_nasm_files
do
	mkdir -p $build_dir/$(dirname $f)
	libdav1d_obj=$build_dir/$f.o
	libdav1d_objs="$libdav1d_objs $libdav1d_obj"

	printf "ASM NOBITDEPTH $src_dir/$f\n"
	$nasm -f elf64 \
		-o $libdav1d_obj \
		$dav1d_cppflags $glibc_cppflags \
		-Dprivate_prefix=dav1d \
		-DARCH_X86_32=0 $src_dir/$f &
done
#===============================================================================


#===============================================================================
wait

if test "${slib_link_cmd-unset}" = unset ; then
slib_link_cmd="gcc -o $build_dir/libdav1d.so.$version_major.$version_minor.$version_patch \
-Wl,-soname=libdav1d.so.$version_major \
-shared -static-libgcc \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib \
-Wl,--no-undefined,--gc-sections,-Bsymbolic \
	$libdav1d_objs \
	-Wl,--as-needed \
	-lpthread \
	-lm \
	-ldl \
	-Wl,--no-as-needed"
fi
printf 'CCLD\n'
eval $slib_link_cmd
