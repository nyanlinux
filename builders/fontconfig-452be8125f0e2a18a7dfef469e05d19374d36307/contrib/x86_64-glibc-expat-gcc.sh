#/bin/sh

# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.


#===============================================================================
version=2.13.91
# current = 13
# revision = 0
# age = 12
soname=libfontconfig.so.1
lib_name=$soname.13.0
#===============================================================================


#===============================================================================
# build dir and src dir
build_dir=$(readlink -f .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(readlink -f $(dirname $0)/..)
echo "src_dir=$src_dir"
#===============================================================================


#===============================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===============================================================================


#===============================================================================
# expat (static)
if test "${expat_cppflags-unset}" = unset; then
expat_cppflags='-I/nyan/expat/current/include'
fi
if test "${expat_archives-unset}" = unset; then
expat_archives=libexpat.a
fi
if test "${expat_ldflags-unset}" = unset; then
expat_ldflags="/nyan/expat/current/lib/$expat_archives"
fi
#===============================================================================


#===============================================================================
# freetype (shared)
if test "${freetype_cppflags-unset}" = unset; then
freetype_cppflags="\
-I/nyan/freetype/current/include/freetype2 \
\
-DHAVE_FT_GET_X11_FONT_FORMAT \
-DHAVE_FT_GET_BDF_PROPERTY \
-DHAVE_FT_GET_PS_FONT_INFO \
-DHAVE_FT_DONE_MM_VAR \
"
fi
if test "${freetype_ldflags-unset}" = unset; then
freetype_ldflags="/nyan/freetype/current/lib/libfreetype.so"
fi
#===============================================================================


#===============================================================================
if test "${prefix-unset}" = unset; then
	prefix=/usr
fi
exec_prefix=$prefix
echo prefix=$prefix
echo exec_prefix=$exec_prefix
#-------------------------------------------------------------------------------
if test "${bin_dir-unset}" = unset; then
	bin_dir=$prefix/bin
fi
echo bin_dir=$bin_dir
#-------------------------------------------------------------------------------
if test "${lib_dir-unset}" = unset; then
	lib_dir=$prefix/lib
fi
echo lib_dir=$lib_dir
#-------------------------------------------------------------------------------
if test "${include_dir-unset}" = unset; then
	include_dir=$prefix/include
fi
echo include_dir=$include_dir
#-------------------------------------------------------------------------------
if test "${sysconf_dir-unset}" = unset; then
	if test $prefix = /usr; then
		sysconf_dir=/etc
	else
		sysconf_dir=$prefix/etc
	fi
fi
echo sysconf_dir=$sysconf_dir
#-------------------------------------------------------------------------------
if test "${localstate_dir-unset}" = unset; then
	if test $prefix = /usr; then
		localstate_dir=/var
	else
		localstate_dir=$prefix/var
	fi
fi
echo localstate_dir=$localstate_dir
#-------------------------------------------------------------------------------
if test "${default_fonts_dir-unset}" = unset; then
	default_fonts_dir=$prefix/share/fonts
fi
echo default_fonts_dir=$default_fonts_dir
#-------------------------------------------------------------------------------
if test "${default_cache_dir-unset}" = unset; then
	default_cache_dir=$localstate_dir/cache/fontconfig
fi
echo default_cache_dir=$default_cache_dir
#-------------------------------------------------------------------------------
if test "${template_dir-unset}" = unset; then
	template_dir=$prefix/share/fontconfig/conf.avail
fi
echo template_dir=$template_dir
#-------------------------------------------------------------------------------
if test "${base_config_dir-unset}" = unset; then
	base_config_dir=$sysconf_dir/fonts
fi
echo base_config_dir=$base_config_dir 
#===============================================================================


#===============================================================================
# meh
mkdir -p $build_dir
echo '#include <stdint.h>' >$build_dir/fcstdint.h
#===============================================================================


#===============================================================================
/bin/sh $src_dir/src/makealias $src_dir/src $build_dir/fcalias.h $build_dir/fcaliastail.h \
	$src_dir/fontconfig/fontconfig.h \
	$src_dir/src/fcdeprecate.h \
	$src_dir/fontconfig/fcprivate.h
#-------------------------------------------------------------------------------
/bin/sh $src_dir/src/makealias $src_dir/src $build_dir/fcftalias.h $build_dir/fcftaliastail.h \
	$src_dir/fontconfig/fcfreetype.h
#===============================================================================


#===============================================================================
if test "${host_cc-unset}" = unset; then
host_cc="gcc -pipe -fPIC -O2 -c -static-libgcc"
fi

if test "${host_ccld-unset}" = unset; then
host_ccld="gcc -pipe -fPIC -static-libgcc"
fi

if test "${host_rpath_link-unset}" = unset; then
host_rpath_link="/nyan/glibc/current/lib:/nyan/freetype/current/lib"
fi
#===============================================================================


#===============================================================================
if test "${build_ccld-unset}" = unset; then
build_ccld="gcc -pipe -O2 -static-libgcc \
-DFLEXIBLE_ARRAY_MEMBER"
fi
#===============================================================================


#===============================================================================
if test "${cpp-unset}" = unset; then
cpp="cpp"
fi
#===============================================================================


#===============================================================================
if test "${gperf-unset}" = unset; then
gperf="/nyan/gperf/current/bin/gperf"
gperf_cppflags="\
-DFC_GPERF_SIZE_T=size_t \
"
fi
#===============================================================================


#===============================================================================
if test "${awk-unset}" = unset; then
awk="/nyan/busybox/current/bin/awk"
fi
#===============================================================================


#===============================================================================
# fccase.h generation
mkdir -p $build_dir/fc-case
#-------------------------------------------------------------------------------
echo "BUILD_CC fc-case.c"
$build_ccld -o $build_dir/fc-case/fc-case $src_dir/fc-case/fc-case.c \
	-I$build_dir \
	-I$src_dir/src \
	-I$src_dir
#-------------------------------------------------------------------------------
echo "GEN fccase.h"
$build_dir/fc-case/fc-case $src_dir/fc-case/CaseFolding.txt \
	<$src_dir/fc-case/fccase.tmpl.h \
	>$build_dir/fc-case/fccase.h
#===============================================================================


#===============================================================================
# fclang.h generation
mkdir -p $build_dir/fc-lang
#-------------------------------------------------------------------------------
# meh...
mkdir -p $build_dir/src
OLD_PWD=$PWD
cd $build_dir/src

echo "BUILD_CC fc-lang.c"
$build_ccld -o $build_dir/fc-lang/fc-lang $src_dir/fc-lang/fc-lang.c \
	-I$build_dir \
	-I$src_dir/src \
	-I$src_dir \
	-I.

cd $OLD_PWD
#-------------------------------------------------------------------------------
. $src_dir/contrib/orth.sh

echo "GEN fclang.h"
$build_dir/fc-lang/fc-lang -d $src_dir/fc-lang $ORTH \
	<$src_dir/fc-lang/fclang.tmpl.h \
	>$build_dir/fc-lang/fclang.h
#===============================================================================


#===============================================================================
echo "GEN fcobjshash.h"
$cpp -I$src_dir -I$src_dir/src - <$src_dir/src/fcobjshash.gperf.h \
	| sed 's/^ *//;s/ *, */,/' \
	| $awk ' \
                /CUT_OUT_BEGIN/ { no_write=1; next; }; \
                /CUT_OUT_END/ { no_write=0; next; }; \
                /^$/||/^#/ { next; }; \
                { if (!no_write) print; next; }; \
        ' - >$build_dir/fcobjshash.gperf
$gperf -d --pic -m 100 $build_dir/fcobjshash.gperf >$build_dir/fcobjshash.h 2>/run/e
#===============================================================================


#===============================================================================
# fontconfig.map generation
echo "GEN fontconfig.map"
echo '{ global:' >$build_dir/fontconfig.map
cat	$src_dir/fontconfig/fontconfig.h \
	$src_dir/src/fcdeprecate.h \
	$src_dir/fontconfig/fcprivate.h \
	$src_dir/fontconfig/fcfreetype.h \
| grep '^Fc[^ ]* *(' | sed -e 's/ *(.*$/;/' -e 's/^/	/' | sort \
>>$build_dir/fontconfig.map
echo ' local: *; };' >>$build_dir/fontconfig.map
#===============================================================================


#===============================================================================
glibc_cppflags="\
-D_GNU_SOURCE \
-DHAVE_SYS_VFS_H \
-DHAVE_SYS_STATVFS_H \
-DHAVE_SYS_STATFS_H \
-DHAVE_SYS_PARAM_H \
-DHAVE_SYS_MOUNT_H \
-DHAVE_SYS_TYPES_H \
-DHAVE_SCHED_H \
-DHAVE_FCNTL_H \
-DHAVE_UNISTD_H \
\
-DHAVE_STRERROR_R \
-DHAVE_LINK \
-DHAVE_MMAP \
-DHAVE_POSIX_FADVISE -DPOSIX_FADV_WILLNEED \
-DHAVE_MKOSTEMP \
-DHAVE_MKSTEMP \
-DHAVE_RANDOM_R \
-DHAVE_READLINK \
-DHAVE_LSTAT \
-DHAVE_SCHED_YIELD \
\
-DHAVE_STRUCT_STATFS_F_FLAGS \
-DHAVE_STRUCT_DIRENT_D_TYPE \
-DHAVE_STRUCT_STAT_ST_MTIM \
\
-DUSE_ICONV \
-DHAVE_PTHREAD \
"

gcc_cppflags="\
-DPIC \
-DHAVE_INTEL_ATOMIC_PRIMITIVES \
-DFLEXIBLE_ARRAY_MEMBER \
-DSIZEOF_VOID_P=8 \
-DALIGNOF_VOID_P=8 \
"

fontconfig_cppflags="\
-DFC_DEFAULT_FONTS=\"$default_fonts_dir\" \
-DFC_CACHEDIR=\"$default_cache_dir\" \
-DFC_TEMPLATEDIR=\"$template_dir\" \
-DFONTCONFIG_PATH=\"$base_config_dir\" \
"

cppflags="\
$glibc_cppflags \
$freetype_cppflags \
$gcc_cppflags \
$fontconfig_cppflags \
\
$gperf_cppflags \
\
$expat_cppflags \
$freetype_cppflags \
"
#===============================================================================


#===============================================================================
libfontconfig_c_files="\
$src_dir/src/fcatomic.c \
$src_dir/src/fccache.c \
$src_dir/src/fccfg.c \
$src_dir/src/fccharset.c \
$src_dir/src/fccompat.c \
$src_dir/src/fcdbg.c \
$src_dir/src/fcdefault.c \
$src_dir/src/fcdir.c \
$src_dir/src/fcformat.c \
$src_dir/src/fcfreetype.c \
$src_dir/src/fcfs.c \
$src_dir/src/fcptrlist.c \
$src_dir/src/fchash.c \
$src_dir/src/fcinit.c \
$src_dir/src/fclang.c \
$src_dir/src/fclist.c \
$src_dir/src/fcmatch.c \
$src_dir/src/fcmatrix.c \
$src_dir/src/fcname.c \
$src_dir/src/fcobjs.c \
$src_dir/src/fcpat.c \
$src_dir/src/fcrange.c \
$src_dir/src/fcserialize.c \
$src_dir/src/fcstat.c \
$src_dir/src/fcstr.c \
$src_dir/src/fcweight.c \
$src_dir/src/fcxml.c \
$src_dir/src/ftglue.c \
"

# meh...
mkdir -p $build_dir/src
OLD_PWD=$PWD
cd $build_dir/src

for f in $libfontconfig_c_files
do
	libfontconfig_c_obj=$build_dir/$(basename $f .c).o
	libfontconfig_c_objs="$libfontconfig_c_obj $libfontconfig_c_objs"

	echo "CC $f"
	$host_cc $cppflags $f -o $libfontconfig_c_obj \
		-I$build_dir \
		-I$src_dir/src \
		-I$src_dir \
		-I. &
done

cd $OLD_PWD
#===============================================================================


#===============================================================================
wait

echo "CCLD $lib_name"
mkdir -p $build_dir/fakeroot$lib_dir

$host_ccld -o $build_dir/fakeroot$lib_dir/$lib_name -Wl,-soname=$soname \
-shared \
-Wl,-rpath-link,$host_rpath_link \
-Wl,--no-undefined,--gc-sections,--version-script=$build_dir/fontconfig.map \
	$libfontconfig_c_objs \
	$expat_ldflags \
	-Wl,--as-needed \
	$freetype_ldflags \
	-lpthread \
	-Wl,--no-as-needed

ln -sTf $lib_name $build_dir/fakeroot$lib_dir/$soname
ln -sTf $soname $build_dir/fakeroot$lib_dir/libfontconfig.so
#===============================================================================


#===============================================================================
mkdir -p $build_dir/fakeroot$bin_dir

tools="\
cache \
cat \
conflist \
list \
match \
pattern \
query \
scan \
validate"

for t in $tools
do
	(echo "CC $t";
	$host_cc $src_dir/fc-$t/fc-$t.c -o $build_dir/fc-$t.o \
		-DHAVE_GETOPT=1 -DHAVE_GETOPT_LONG=1 \
		$cppflags \
		-I$build_dir \
		-I$src_dir;

	echo "CCLD $t";
	$host_ccld -o $build_dir/fakeroot$bin_dir/fc-$t \
		$build_dir/fakeroot$lib_dir/$soname \
		fc-$t.o \
		-Wl,-rpath-link,$host_rpath_link \
		-Wl,--as-needed \
		$freetype_ldflags \
		-lpthread \
		-Wl,--no-as-needed;) &
done
#===============================================================================


#===============================================================================
echo "generate 35-lang-normalize.conf"

echo "<fontconfig>" >$build_dir/35-lang-normalize.conf
for i in $(echo $ORTH | sed -e 's/ /\n/g' | grep -v _ | sed -e 's/\.orth$//g' | sort)
do
	echo "  <!-- $i* -> $i -->" >>$build_dir/35-lang-normalize.conf 
	echo "  <match>" >>$build_dir/35-lang-normalize.conf
	echo "    <test name=\"lang\" compare=\"contains\"><string>$i</string></test>" >>$build_dir/35-lang-normalize.conf
	echo "    <edit name=\"lang\" mode=\"assign\" binding=\"same\"><string>$i</string></edit>" >>$build_dir/35-lang-normalize.conf
	echo "  </match>" >>$build_dir/35-lang-normalize.conf
done
echo "</fontconfig>" >>$build_dir/35-lang-normalize.conf
#===============================================================================


#===============================================================================
echo "installing xml configuration files in fakeroot"
mkdir -p $build_dir/fakeroot$template_dir

cp -f \
$src_dir/conf.d/05-reset-dirs-sample.conf	\
$src_dir/conf.d/09-autohint-if-no-hinting.conf	\
$src_dir/conf.d/10-autohint.conf		\
$src_dir/conf.d/10-hinting-full.conf		\
$src_dir/conf.d/10-hinting-medium.conf		\
$src_dir/conf.d/10-hinting-none.conf		\
$src_dir/conf.d/10-hinting-slight.conf		\
$src_dir/conf.d/10-no-sub-pixel.conf		\
$src_dir/conf.d/10-scale-bitmap-fonts.conf	\
$src_dir/conf.d/10-sub-pixel-bgr.conf		\
$src_dir/conf.d/10-sub-pixel-rgb.conf		\
$src_dir/conf.d/10-sub-pixel-vbgr.conf		\
$src_dir/conf.d/10-sub-pixel-vrgb.conf		\
$src_dir/conf.d/10-unhinted.conf		\
$src_dir/conf.d/11-lcdfilter-default.conf	\
$src_dir/conf.d/11-lcdfilter-legacy.conf	\
$src_dir/conf.d/11-lcdfilter-light.conf		\
$src_dir/conf.d/20-unhint-small-vera.conf	\
$src_dir/conf.d/25-unhint-nonlatin.conf		\
$src_dir/conf.d/30-metric-aliases.conf		\
$build_dir/35-lang-normalize.conf		\
$src_dir/conf.d/40-nonlatin.conf		\
$src_dir/conf.d/45-generic.conf			\
$src_dir/conf.d/45-latin.conf			\
$src_dir/conf.d/49-sansserif.conf		\
$src_dir/conf.d/50-user.conf			\
$src_dir/conf.d/51-local.conf			\
$src_dir/conf.d/60-generic.conf			\
$src_dir/conf.d/60-latin.conf			\
$src_dir/conf.d/65-fonts-persian.conf		\
$src_dir/conf.d/65-khmer.conf			\
$src_dir/conf.d/65-nonlatin.conf		\
$src_dir/conf.d/69-unifont.conf			\
$src_dir/conf.d/70-no-bitmaps.conf		\
$src_dir/conf.d/70-yes-bitmaps.conf		\
$src_dir/conf.d/80-delicious.conf		\
$src_dir/conf.d/90-synthetic.conf		\
$build_dir/fakeroot$template_dir

default_links="\
10-hinting-full.conf \
10-scale-bitmap-fonts.conf \
20-unhint-small-vera.conf \
30-metric-aliases.conf \
40-nonlatin.conf \
45-generic.conf \
45-latin.conf \
49-sansserif.conf \
50-user.conf \
51-local.conf \
60-generic.conf \
60-latin.conf \
65-fonts-persian.conf \
65-nonlatin.conf \
69-unifont.conf \
80-delicious.conf \
90-synthetic.conf"

mkdir -p  $build_dir/fakeroot$base_config_dir/conf.d
for l in $default_links
do
	ln -sTf $template_dir/$l $build_dir/fakeroot$base_config_dir/conf.d/$l &
done
#===============================================================================


#===============================================================================
echo "generate fonts.conf"
mkdir -p $build_dir/fakeroot$base_config_dir

sed \
-e "s:@FC_CACHEDIR@:$default_cache_dir:g" \
-e "s:@FC_DEFAULT_FONTS@:$default_fonts_dir:g" \
-e "s:@FC_FONTPATH@::g" \
-e "s:@CONFIGDIR@:conf.d:g" \
-e "s:@PACKAGE@:fontconfig:g" \
-e "s:@VERSION@:$version:g" \
$src_dir/fonts.conf.in >$build_dir/fakeroot$base_config_dir/fonts.conf &
#===============================================================================


#===============================================================================
echo "include files"
mkdir -p $build_dir/fakeroot$include_dir/fontconfig

cp -f \
$src_dir/fontconfig/fcfreetype.h \
$src_dir/fontconfig/fcprivate.h \
$src_dir/fontconfig/fontconfig.h \
$build_dir/fakeroot$include_dir/fontconfig &
#===============================================================================


#===============================================================================
echo "generate fontconfig.pc (the pkgconfig file)"
mkdir -p $build_dir/fakeroot$lib_dir/pkgconfig

pc=$build_dir/fakeroot$lib_dir/pkgconfig/fontconfig.pc
cp -f $src_dir/fontconfig.pc.in $pc
# we presume expat and freetype are using pkgconfig
sed \
-e "s:@prefix@:$prefix:g" \
-e "s:@exec_prefix@:$exec_prefix:g" \
-e "s:@libdir@:$lib_dir:g" \
-e "s:@includedir@:$include_dir:g" \
-e "s:@sysconfdir@:$sysconf_dir:g" \
-e "s:@localstatedir@:$localstate_dir:g" \
-e "s:@PACKAGE@:fontconfig:g" \
-e "s:@BASECONFIGDIR@:$base_config_dir:g" \
-e "s:@fc_cachedir@:$default_cache_dir:g" \
-e "s:@VERSION@:$version:g" \
-e "s:@PKGCONFIG_REQUIRES@:freetype2:g" \
-e "s:@PKGCONFIG_REQUIRES_PRIVATELY@:expat:g" \
-e '/^Libs\.private/ d' \
-e "s:@ICONV_CFLAGS@::g" \
-e "s:@PKG_EXPAT_CFLAGS@::g" \
-i $pc &
#===============================================================================
wait
