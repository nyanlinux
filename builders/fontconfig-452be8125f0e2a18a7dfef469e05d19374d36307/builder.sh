src_name=fontconfig
mkdir /nyan/$src_name
version=${pkg_name##*-}
slot=$version
mkdir /nyan/$src_name/$slot

archive_name=$src_name-$version.tar.bz2

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
$PATH\
"
cd $pkg_dir

# install our canonical build system from the contrib dir
cp -rf $nyan_root/builders/$pkg_name/contrib $pkg_dir

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

# create the build local configuration file
# the configuration can be augmented by fonts
# then must be shared
cat >$build_dir/local_conf.sh <<EOF
expat_cppflags=-I/nyan/expat/current/include
expat_archives=libexpat.a
expat_ldflags=/nyan/expat/current/lib/libexpat.a

# CAREFUL, THE PREPROCESSOR FLAGS ARE MANDATORY
freetype_cppflags="\
-I/nyan/freetype/current/include/freetype2 \
\
-DHAVE_FT_GET_X11_FONT_FORMAT \
-DHAVE_FT_GET_BDF_PROPERTY \
-DHAVE_FT_GET_PS_FONT_INFO \
-DHAVE_FT_DONE_MM_VAR \
"
freetype_ldflags=/nyan/freetype/current/lib/libfreetype.so

host_cc="gcc -pipe -fPIC -O2 -c -static-libgcc \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include"

host_ccld="gcc -pipe -fPIC -static-libgcc \
-B/nyan/glibc/current/lib -Wl,-s"
host_rpath_link=/nyan/glibc/current/lib:/nyan/freetype/current/lib

# CAREFUL, THE PREPROCESSOR FLAGS ARE MANDATORY
build_ccld="gcc -pipe -O2 -static-libgcc \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
-B/nyan/glibc/current/lib \
-DFLEXIBLE_ARRAY_MEMBER"

cpp="gcc -E \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include"

# CAREFUL, THE PREPROCESSOR FLAGS ARE MANDATORY
gperf="/nyan/gperf/current/bin/gperf"
gperf_cppflags="\
-DFC_GPERF_SIZE_T=size_t \
"

awk=/nyan/busybox/current/bin/awk
EOF

$pkg_dir/contrib/x86_64-glibc-expat-gcc.sh

rm -Rf /nyan/$src_name/$slot/include
cp -r $build_dir/fakeroot/usr/include /nyan/$src_name/$slot/include

rm -Rf /nyan/$src_name/$slot/bin
cp -r $build_dir/fakeroot/usr/bin /nyan/$src_name/$slot/bin

rm -Rf /nyan/$src_name/$slot/fontconfig
cp -r $build_dir/fakeroot/usr/share/fontconfig /nyan/$src_name/$slot/fontconfig

rm -Rf /nyan/$src_name/$slot/fonts
cp -r $build_dir/fakeroot/etc/fonts /nyan/$src_name/$slot/fonts

mkdir /nyan/$src_name/$slot/lib
cp -f $build_dir/fakeroot/usr/lib/libfontconfig.so.1.13.0 /nyan/$src_name/$slot/lib/libfontconfig.so.1.13.0
ln -sTf libfontconfig.so.1.13.0 /nyan/$src_name/$slot/lib/libfontconfig.so
ln -sTf libfontconfig.so.1.13.0 /nyan/$src_name/$slot/lib/libfontconfig.so.1

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
