git_url0=git://git.kernel.org/pub/scm/linux/kernel/git/firmware/$pkg_name.git

mkdir -p $pkgs_dir_root
pkg_dir=$pkgs_dir_root/$pkg_name-$target_gnu_triple
src_dir=$src_dir_root/$pkg_name
rm -Rf $pkg_dir
cp -r $src_dir $pkg_dir
rm -Rf $pkg_dir/.git
rm -Rf $target_sysroot/lib/firmware
mkdir -p $target_sysroot/lib
cp -rf $pkg_dir $target_sysroot/lib/firmware
rm -Rf $pkg_dir
