src_name=ncurses
version_all=${pkg_name#*-}
slot=$version_all
version_base=${version_all%-*}
version_patch=${version_all##*-}
archive_name=$src_name-$version_base.tar.gz
patch_name=patch-$version_all.sh.gz
url0=http://ftpmirror.gnu.org/$src_name/$archive_name
url1=ftp://ftp.invisible-island.net/$src_name/$version_base/patch_name

pkg_dir=$pkgs_dir_root/$src_name-$version_base
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

cd $pkg_dir
cp -f $src_dir_root/$patch_name ./
gunzip ./$patch_name
chmod +x ./$(basename $patch_name .gz)
./$(basename $patch_name .gz)

build_dir=$builds_dir_root/$pkg_name-$version_all
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export PKG_CONFIG=/nyan/pkgconf/current/bin/pkgconf
export "CPPFLAGS=\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include"
export "CPP=$target_gnu_triple-gcc -E"
export 'CFLAGS=-O2 -pipe -fPIC -static-libgcc'
export LDFLAGS="\
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export CC=$target_gnu_triple-gcc
export AR=$target_gnu_triple-ar
export "BUILD_CPPFLAGS=$CPPFLAGS"
export "BUILD_CPP=$CPP"
export "BUILD_CFLAGS=$CFLAGS"
export "BUILD_LDFLAGS=$LDFLAGS"
export "BUILD_CC=$CC"
export "BUILD_AR=$AR"
$pkg_dir/configure \
	--prefix=/nyan/ncurses/$slot \
	--without-ada \
	--without-cxx \
	--without-cxx-binding \
	--enable-db-install \
	--without-manpages \
	--with-progs \
	--without-tests \
	--with-pkg-config-libdir=/nyan/ncurses/$slot/lib/pkgconfig \
	--enable-pc-files \
	--disable-mixed-case \
	--without-libtool \
	--without-shared \
	--with-normal \
	--without-debug \
	--without-profile \
	--with-termlib \
	--with-ticlib \
	--without-gpm \
	--disable-lib-suffixes \
	--enable-database \
	--disable-termcap \
	--disable-root-environ \
	--enable-symlinks \
	--enable-widec \
	--enable-ext-funcs \
	--enable-sp-funcs \
	--enable-ext-colors \
	--disable-ext-mouse \
	--enable-sigwinch \
	--with-pthread \
	--enable-opaque-curses \
	--enable-opaque-form \
	--enable-opaque-menu  \
	--enable-opaque-panel \
	--without-develop \
	--enable-fvisibility \
	--disable-stripping
unset PKG_CONFIG
unset CPPFLAGS
unset CPP
unset CFLAGS
unset LDFLAGS
unset CC
unset AR
unset BUILD_CPPFLAGS
unset BUILD_CPP
unset BUILD_CFLAGS
unset BUILD_LDFLAGS
unset BUILD_CC
unset BUILD_AR

make -j $threads_n
make install

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
