src_name=bash
version=4.4.18
archive_name=$src_name-$version.tar.gz
url0=http://ftpmirror.gnu.org/$src_name/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

cd $src_dir
### we use wide version of ncurses, be brutal telling bash configure
##sed -i 's/tinfo/tinfow/' ./configure

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

# help bash configuration find the right tinfo
ln -fs libtinfow.a $target_sysroot/nyan/ncurses/0/lib/libtinfo.a

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
export "LDFLAGS=-L$target_sysroot/nyan/readline/0/lib -L$target_sysroot/nyan/ncurses/0/lib"
$src_dir/configure			\
	--build=$build_gnu_triple	\
	--host=$target_gnu_triple	\
	--prefix=/nyan/bash/0		\
	--enable-multibyte		\
	--enable-net-redirections	\
	--disable-nls			\
	--with-installed-readline	\
	--with-curses
unset LDFLAGS
unset CFLAGS
unset CC

make -j $threads_n
make install DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/$src_name/0/share
find $target_sysroot/nyan/$src_name/0 -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then $target_gnu_triple-strip -s $f; fi; done

rm -f $target_sysroot/nyan/ncurses/0/lib/libtinfo.a

rm -Rf $build_dir $src_dir
export PATH=$OLD_PATH
