src_name=mtdev
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.bz2
url0=http://bitmath.org/code/$pkg_name/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export PKG_CONFIG_LIBDIR=

export "CC=$target_gnu_triple-gcc -isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$pkg_dir/configure \
	--prefix=/nyan/mtdev/$slot \
	--disable-shared \
	--enable-static
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup and tidying
rm -f /nyan/$src_name/$slot/lib/*.la
strip -s /nyan/$src_name/$slot/bin/* || true

rm -Rf $build_dir $pkg_dir
