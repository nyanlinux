src_name=freetype
mkdir /nyan/$src_name

version=${pkg_name##*-}
slot=$version
mkdir /nyan/$src_name/$slot

archive_name=$src_name-$version.tar.xz
url0=http://download.savannah.gnu.org/releases/$src_name/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
export LIBPNG_CFLAGS="-I/nyan/libpng/current/include"
export LIBPNG_LIBS="-L/nyan/libpng/current/lib -lpng"
export "CPPFLAGS=\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include"
export 'CFLAGS=-O2 -pipe -fPIC -static-libgcc'
export "LDFLAGS=\
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
# must add the full blow CC for libtool...
export "CC=gcc \
	$CPPFLAGS \
	$LDFLAGS"
$pkg_dir/configure \
	--prefix=/usr \
	--disable-static \
	--enable-shared \
	--enable-mmap \
	--enable-year2038 \
	--with-png=yes \
	--with-harfbuzz=no \
	--with-brotli=no \
	--with-librsvg=no	
unset LIBPNG_CFLAGS
unset LIBPNG_LIBS
unset CPPFLAGS
unset CFLAGS
unset LDFLAGS
unset CC

make -j $threads_n
#---------------------------------------------------------------------------------------------------
install_dir=$build_dir/nyan_install_root
make DESTDIR=$install_dir install

rm -Rf /nyan/$src_name/$slot/include
cp -r $install_dir/usr/include /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/lib
cp -f $install_dir/usr/lib/libfreetype.so.6.20.2 /nyan/$src_name/$slot/lib/libfreetype.so.6.20.2
ln -sTf libfreetype.so.6.20.2 /nyan/$src_name/$slot/lib/libfreetype.so
ln -sTf libfreetype.so.6.20.2 /nyan/$src_name/$slot/lib/libfreetype.so.6

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
