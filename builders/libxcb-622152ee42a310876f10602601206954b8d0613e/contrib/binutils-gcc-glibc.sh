#!/bin/sh

# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the script at the top of mesa source tree, create somewhere else
# a build directory, cd into it, and call from there this script.

# XXX: the defaults are for our custom distro
#===================================================================================================
# build dir and src dir
build_dir=$(realpath .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(realpath $(dirname $0)/..)
echo "src_dir=$src_dir"
#===================================================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===================================================================================================
if test "${python3-unset}" = unset; then
python3=/nyan/python/current/bin/python3
fi
#===================================================================================================
if test "${proto_dir-unset}" = unset; then
proto_dir=/nyan/xcb-proto/current
fi
#===================================================================================================
if test "${xorgproto_inc_dir-unset}" = unset; then
xorgproto_inc_dir=/nyan/xorgproto/current/include
fi
xorgproto_cppflags="-I$xorgproto_inc_dir"
#===================================================================================================
if test "${libx11_inc_dir-unset}" = unset; then
libx11_inc_dir=/nyan/libX11/current/include
fi
libx11_cppflags="-I$libx11_inc_dir"
#---------------------------------------------------------------------------------------------------
if test "${libx11_lib_dir-unset}" = unset; then
libx11_lib_dir=/nyan/libX11/current/lib
fi
libx11_ldflags="-L$libx11_lib_dir -lX11"
#===================================================================================================
if test "${libxdmcp_inc_dir-unset}" = unset; then
libxdmcp_inc_dir=/nyan/libXdmcp/current/include
fi
libxdmcp_cppflags="-I$libxdmcp_inc_dir"
#---------------------------------------------------------------------------------------------------
if test "${libxdmcp_lib_dir-unset}" = unset; then
libxdmcp_lib_dir=/nyan/libXdmcp/current/lib
fi
libxdmcp_ldflags="-L$libxdmcp_lib_dir -lXdmcp"
#===================================================================================================
if test "${libxau_inc_dir-unset}" = unset; then
libxau_inc_dir=/nyan/libXau/current/include
fi
libxau_cppflags="-I$libxau_inc_dir"
#---------------------------------------------------------------------------------------------------
if test "${libxau_lib_dir-unset}" = unset; then
libxau_lib_dir=/nyan/libXau/current/lib
fi
libxau_ldflags="-L$libxau_lib_dir -lXau"
#===================================================================================================
if test "${cpp-unset}" = unset; then
cpp="gcc -E \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
"
fi
#===================================================================================================
if test "${slib_cc-unset}" = unset; then
slib_cc="gcc -c \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-std=c11 \
	-pipe -fPIC -O2 -ftls-model=global-dynamic -fpic \
	-static-libgcc"
fi
#===================================================================================================
# we are still using the compiler driver, very bad idea
if test "${slib_ccld-unset}" = unset; then
slib_ccld="gcc \
	-shared \
	-static-libgcc \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,--no-undefined \
	-Wl,-s \
"
fi
####################################################################################################
# code generation using the proto python module
#===================================================================================================
xml_files="\
xproto.xml \
bigreq.xml \
xc_misc.xml \
composite.xml \
damage.xml \
dbe.xml \
dpms.xml \
dri2.xml \
dri3.xml \
present.xml \
glx.xml \
randr.xml \
record.xml \
render.xml \
res.xml \
screensaver.xml \
shape.xml \
shm.xml \
sync.xml \
xf86dri.xml \
xfixes.xml \
xinerama.xml \
xinput.xml \
xkb.xml \
xtest.xml \
xv.xml \
xvmc.xml \
"
xml_files_only_headers="\
ge.xml \
xevie.xml \
xprint.xml \
xselinux.xml \
"
#===================================================================================================
export PYTHONPYCACHEPREFIX=$build_dir/python_cache
export PYTHONPATH=$proto_dir
for f in $xml_files $xml_files_only_headers
do
$python3 $src_dir/src/c_client.py -c man_center_footer -l man_left_footer -s man_section \
	$proto_dir/src/$f &
done
unset PYTHONPYCACHEPREFIX
unset PYTHONPATH
wait
####################################################################################################
glibc_cppflags="\
-D_GNU_SOURCE \
-DUSE_POLL=1 \
-DHAVE_SENDMSG=1 \
-DHAVE_GETADDRINFO=1 \
"
gcc_cppflags="\
-DGCC_HAS_VISIBILITY=1 \
"
linux_cppflags="\
-DHAVE_ABSTRACT_SOCKETS=1 \
"
xcb_cfg_cppflags="\
-DHASXDMAUTH=1 \
-DXCB_QUEUE_BUFFER_SIZE=16384 \
"
#===================================================================================================
# libxcb and its minions
src_files="\
$build_dir/xproto.c \
$build_dir/bigreq.c \
$build_dir/xc_misc.c \
\
$src_dir/src/xcb_conn.c \
$src_dir/src/xcb_out.c \
$src_dir/src/xcb_in.c \
$src_dir/src/xcb_ext.c \
$src_dir/src/xcb_xid.c \
$src_dir/src/xcb_list.c \
$src_dir/src/xcb_util.c \
$src_dir/src/xcb_auth.c \
\
\
$build_dir/composite.c \
$build_dir/damage.c \
$build_dir/dbe.c \
$build_dir/dpms.c \
$build_dir/dri2.c \
$build_dir/dri3.c \
$build_dir/present.c \
$build_dir/glx.c \
$build_dir/randr.c \
$build_dir/record.c \
$build_dir/render.c \
$build_dir/res.c \
$build_dir/screensaver.c \
$build_dir/shape.c \
$build_dir/shm.c \
$build_dir/sync.c \
$build_dir/xf86dri.c \
$build_dir/xfixes.c \
$build_dir/xinerama.c \
$build_dir/xinput.c \
$build_dir/xkb.c \
$build_dir/xtest.c \
$build_dir/xv.c \
$build_dir/xvmc.c \
"
#===================================================================================================
for f in $src_files
do
	cpp_file=$(basename $f .c).cpp.c
	printf "CPP $f -> $build_dir/$cpp_file\n"
	$cpp -o $build_dir/$cpp_file $f \
		-I$build_dir \
		-I$src_dir/src \
		$glibc_cppflags \
		$gcc_cppflags \
		$linux_cppflags \
		$xcb_cfg_cppflags \
		$xorgproto_cppflags \
		$libxau_cppflags \
		$libxdmcp_cppflags &
done
#===================================================================================================
wait
#===================================================================================================
for f in $src_files
do
	cpp_file=$(basename $f .c).cpp.c
	o_file=$(basename $f .c).o
	printf "SLIB_CC $build_dir/$cpp_file -> $build_dir/$o_file\n"
	$slib_cc -o $build_dir/$o_file $build_dir/$cpp_file &
done
#===================================================================================================
wait
#===================================================================================================
# check upstream build system for the brain damage libtool versions
#===================================================================================================
printf "SLIB_CCLD $build_dir/libxcb.so.1.1.0\n"
$slib_ccld -o $build_dir/libxcb.so.1.1.0 \
	$build_dir/xproto.o \
	$build_dir/bigreq.o \
	$build_dir/xc_misc.o\
	\
	$build_dir/xcb_conn.o \
	$build_dir/xcb_out.o \
	$build_dir/xcb_in.o \
	$build_dir/xcb_ext.o \
	$build_dir/xcb_xid.o \
	$build_dir/xcb_list.o \
	$build_dir/xcb_util.o \
	$build_dir/xcb_auth.o \
	-Wl,-soname=libxcb.so.1 \
	$libxau_ldflags \
	$libxdmcp_ldflags
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-composite.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-composite.so.0.0.0 \
	$build_dir/composite.o \
	-Wl,-soname=libxcb-composite.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-damage.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-damage.so.0.0.0 \
	$build_dir/damage.o \
	-Wl,-soname=libxcb-damage.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-dbe.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-dbe.so.0.0.0 \
	$build_dir/dbe.o \
	-Wl,-soname=libxcb-dbe.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-dpms.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-dpms.so.0.0.0 \
	$build_dir/dpms.o \
	-Wl,-soname=libxcb-dpms.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-dri2.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-dri2.so.0.0.0 \
	$build_dir/dri2.o \
	-Wl,-soname=libxcb-dri2.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-dri3.so.0.1.0\n"
$slib_ccld -o $build_dir/libxcb-dri3.so.0.1.0 \
	$build_dir/dri3.o \
	-Wl,-soname=libxcb-dri3.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-present.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-present.so.0.0.0 \
	$build_dir/present.o \
	-Wl,-soname=libxcb-present.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-glx.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-glx.so.0.0.0 \
	$build_dir/glx.o \
	-Wl,-soname=libxcb-glx.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-randr.so.0.1.0\n"
$slib_ccld -o $build_dir/libxcb-randr.so.0.1.0 \
	$build_dir/randr.o \
	-Wl,-soname=libxcb-randr.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-record.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-record.so.0.0.0 \
	$build_dir/record.o \
	-Wl,-soname=libxcb-record.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-render.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-render.so.0.0.0 \
	$build_dir/render.o \
	-Wl,-soname=libxcb-render.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-res.so.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-res.so.0.0.0 \
	$build_dir/res.o \
	-Wl,-soname=libxcb-res.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-screensaver.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-screensaver.so.0.0.0 \
	$build_dir/screensaver.o \
	-Wl,-soname=libxcb-screensaver.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-shape.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-shape.so.0.0.0 \
	$build_dir/shape.o \
	-Wl,-soname=libxcb-shape.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-shm.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-shm.so.0.0.0 \
	$build_dir/shm.o \
	-Wl,-soname=libxcb-shm.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-sync.1.0.0\n"
$slib_ccld -o $build_dir/libxcb-sync.so.1.0.0 \
	$build_dir/sync.o \
	-Wl,-soname=libxcb-sync.so.1 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-xf86dri.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-xf86dri.so.0.0.0 \
	$build_dir/xf86dri.o \
	-Wl,-soname=libxcb-xf86dri.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-xfixes.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-xfixes.so.0.0.0 \
	$build_dir/xfixes.o \
	-Wl,-soname=libxcb-xfixes.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-xinerama.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-xinerama.so.0.0.0 \
	$build_dir/xinerama.o \
	-Wl,-soname=libxcb-xinerama.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-xinput.0.1.0\n"
$slib_ccld -o $build_dir/libxcb-xinput.so.0.1.0 \
	$build_dir/xinput.o \
	-Wl,-soname=libxcb-xinput.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-xkb.1.0.0\n"
$slib_ccld -o $build_dir/libxcb-xkb.so.1.0.0 \
	$build_dir/xkb.o \
	-Wl,-soname=libxcb-xkb.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-xtest.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-xtest.so.0.0.0 \
	$build_dir/xtest.o \
	-Wl,-soname=libxcb-xtest.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-xv.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-xv.so.0.0.0 \
	$build_dir/xv.o \
	-Wl,-soname=libxcb-xv.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#---------------------------------------------------------------------------------------------------
printf "SLIB_CCLD $build_dir/libxcb-xvmc.0.0.0\n"
$slib_ccld -o $build_dir/libxcb-xvmc.so.0.0.0 \
	$build_dir/xvmc.o \
	-Wl,-soname=libxcb-xvmc.so.0 \
	$build_dir/libxcb.so.1.1.0 \
	$libxau_ldflags &
#===================================================================================================
wait
