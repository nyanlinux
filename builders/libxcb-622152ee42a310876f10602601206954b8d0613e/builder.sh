src_name=libxcb
mkdir /nyan/$src_name

git_commit=${pkg_name##*-}
slot=$git_commit
mkdir /nyan/$src_name/$slot

git_url0=https://gitlab.freedesktop.org/xorg/lib/$src_name.git

pkg_dir=$pkgs_dir_root/$src_name
rm -Rf $pkg_dir
mkdir $pkgs_dir_root
src_dir=$src_dir_root/xcb/lib
cp -r $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export "PATH=\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
$PATH"

if test x$git_commit != x; then
	/nyan/git/current/bin/git checkout --force $git_commit
	/nyan/git/current/bin/git reset --hard
fi

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir

cd $build_dir

# install our build system
cp -r $nyan_root/builders/$pkg_name/contrib $pkg_dir

$pkg_dir/contrib/binutils-gcc-glibc.sh

#===============================================================================
mkdir /nyan/$src_name/$slot/lib
cp -f \
$build_dir/libxcb.so.1.1.0 \
$build_dir/libxcb-composite.so.0.0.0 \
$build_dir/libxcb-damage.so.0.0.0 \
$build_dir/libxcb-dbe.so.0.0.0 \
$build_dir/libxcb-dpms.so.0.0.0 \
$build_dir/libxcb-dri2.so.0.0.0 \
$build_dir/libxcb-dri3.so.0.1.0 \
$build_dir/libxcb-present.so.0.0.0 \
$build_dir/libxcb-glx.so.0.0.0 \
$build_dir/libxcb-randr.so.0.1.0 \
$build_dir/libxcb-record.so.0.0.0 \
$build_dir/libxcb-render.so.0.0.0 \
$build_dir/libxcb-res.so.0.0.0 \
$build_dir/libxcb-screensaver.so.0.0.0 \
$build_dir/libxcb-shape.so.0.0.0 \
$build_dir/libxcb-shm.so.0.0.0 \
$build_dir/libxcb-sync.so.1.0.0 \
$build_dir/libxcb-xf86dri.so.0.0.0 \
$build_dir/libxcb-xfixes.so.0.0.0 \
$build_dir/libxcb-xinerama.so.0.0.0 \
$build_dir/libxcb-xinput.so.0.1.0 \
$build_dir/libxcb-xkb.so.1.0.0 \
$build_dir/libxcb-xtest.so.0.0.0 \
$build_dir/libxcb-xv.so.0.0.0 \
$build_dir/libxcb-xvmc.so.0.0.0 \
	/nyan/$src_name/$slot/lib

ln -sTf libxcb.so.1.1.0		          /nyan/$src_name/$slot/lib/libxcb.so
ln -sTf libxcb-composite.so.0.0.0         /nyan/$src_name/$slot/lib/libxcb-composite.so
ln -sTf libxcb-damage.so.0.0.0            /nyan/$src_name/$slot/lib/libxcb-damage.so
ln -sTf libxcb-dbe.so.0.0.0               /nyan/$src_name/$slot/lib/libxcb-dbe.so
ln -sTf libxcb-dpms.so.0.0.0              /nyan/$src_name/$slot/lib/libxcb-dpms.so
ln -sTf libxcb-dri2.so.0.0.0              /nyan/$src_name/$slot/lib/libxcb-dri2.so
ln -sTf libxcb-dri3.so.0.1.0              /nyan/$src_name/$slot/lib/libxcb-dri3.so
ln -sTf libxcb-present.so.0.0.0           /nyan/$src_name/$slot/lib/libxcb-present.so
ln -sTf libxcb-glx.so.0.0.0               /nyan/$src_name/$slot/lib/libxcb-glx.so
ln -sTf libxcb-randr.so.0.1.0             /nyan/$src_name/$slot/lib/libxcb-randr.so
ln -sTf libxcb-record.so.0.0.0            /nyan/$src_name/$slot/lib/libxcb-record.so
ln -sTf libxcb-render.so.0.0.0            /nyan/$src_name/$slot/lib/libxcb-render.so
ln -sTf libxcb-res.so.0.0.0               /nyan/$src_name/$slot/lib/libxcb-res.so
ln -sTf libxcb-screensaver.so.0.0.0       /nyan/$src_name/$slot/lib/libxcb-screensaver.so
ln -sTf libxcb-shape.so.0.0.0             /nyan/$src_name/$slot/lib/libxcb-shape.so
ln -sTf libxcb-shm.so.0.0.0               /nyan/$src_name/$slot/lib/libxcb-shm.so
ln -sTf libxcb-sync.so.1.0.0              /nyan/$src_name/$slot/lib/libxcb-sync.so
ln -sTf libxcb-xf86dri.so.0.0.0           /nyan/$src_name/$slot/lib/libxcb-xf86dri.so
ln -sTf libxcb-xfixes.so.0.0.0            /nyan/$src_name/$slot/lib/libxcb-xfixes.so
ln -sTf libxcb-xinerama.so.0.0.0          /nyan/$src_name/$slot/lib/libxcb-xinerama.so
ln -sTf libxcb-xinput.so.0.1.0            /nyan/$src_name/$slot/lib/libxcb-xinput.so
ln -sTf libxcb-xkb.so.1.0.0               /nyan/$src_name/$slot/lib/libxcb-xkb.so
ln -sTf libxcb-xtest.so.0.0.0             /nyan/$src_name/$slot/lib/libxcb-xtest.so
ln -sTf libxcb-xv.so.0.0.0                /nyan/$src_name/$slot/lib/libxcb-xv.so
ln -sTf libxcb-xvmc.so.0.0.0              /nyan/$src_name/$slot/lib/libxcb-xvmc.so

ln -sTf libxcb.so.1.1.0               /nyan/$src_name/$slot/lib/libxcb.so.1
ln -sTf libxcb-composite.so.0.0.0     /nyan/$src_name/$slot/lib/libxcb-composite.so.0
ln -sTf libxcb-damage.so.0.0.0        /nyan/$src_name/$slot/lib/libxcb-damage.so.0
ln -sTf libxcb-dbe.so.0.0.0           /nyan/$src_name/$slot/lib/libxcb-dbe.so.0
ln -sTf libxcb-dpms.so.0.0.0          /nyan/$src_name/$slot/lib/libxcb-dpms.so.0
ln -sTf libxcb-dri2.so.0.0.0          /nyan/$src_name/$slot/lib/libxcb-dri2.so.0
ln -sTf libxcb-dri3.so.0.1.0          /nyan/$src_name/$slot/lib/libxcb-dri3.so.0
ln -sTf libxcb-present.so.0.0.0       /nyan/$src_name/$slot/lib/libxcb-present.so.0
ln -sTf libxcb-glx.so.0.0.0           /nyan/$src_name/$slot/lib/libxcb-glx.so.0
ln -sTf libxcb-randr.so.0.1.0         /nyan/$src_name/$slot/lib/libxcb-randr.so.0
ln -sTf libxcb-record.so.0.0.0        /nyan/$src_name/$slot/lib/libxcb-record.so.0
ln -sTf libxcb-render.so.0.0.0        /nyan/$src_name/$slot/lib/libxcb-render.so.0
ln -sTf libxcb-res.so.0.0.0           /nyan/$src_name/$slot/lib/libxcb-res.so.0
ln -sTf libxcb-screensaver.so.0.0.0   /nyan/$src_name/$slot/lib/libxcb-screensaver.so.0
ln -sTf libxcb-shape.so.0.0.0         /nyan/$src_name/$slot/lib/libxcb-shape.so.0
ln -sTf libxcb-shm.so.0.0.0           /nyan/$src_name/$slot/lib/libxcb-shm.so.0
ln -sTf libxcb-sync.so.1.0.0          /nyan/$src_name/$slot/lib/libxcb-sync.so.1
ln -sTf libxcb-xf86dri.so.0.0.0       /nyan/$src_name/$slot/lib/libxcb-xf86dri.so.0
ln -sTf libxcb-xfixes.so.0.0.0        /nyan/$src_name/$slot/lib/libxcb-xfixes.so.0
ln -sTf libxcb-xinerama.so.0.0.0      /nyan/$src_name/$slot/lib/libxcb-xinerama.so.0
ln -sTf libxcb-xinput.so.0.1.0        /nyan/$src_name/$slot/lib/libxcb-xinput.so.0
ln -sTf libxcb-xkb.so.1.0.0           /nyan/$src_name/$slot/lib/libxcb-xkb.so.1
ln -sTf libxcb-xtest.so.0.0.0         /nyan/$src_name/$slot/lib/libxcb-xtest.so.0
ln -sTf libxcb-xv.so.0.0.0            /nyan/$src_name/$slot/lib/libxcb-xv.so.0
ln -sTf libxcb-xvmc.so.0.0.0          /nyan/$src_name/$slot/lib/libxcb-xvmc.so.0

rm -Rf /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/include/xcb
cp $build_dir/*.h 		/nyan/$src_name/$slot/include/xcb
cp $src_dir/src/xcb.h		/nyan/$src_name/$slot/include/xcb/xcb.h
cp $src_dir/src/xcbext.h	/nyan/$src_name/$slot/include/xcb/xcbext.h

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
