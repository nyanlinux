src_name=curl
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://curl.haxx.se/download/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export "CFLAGS=-O2 -pipe -fPIC"
export "CC=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export AR=$target_gnu_triple-ar
export PATH_SEPARATOR=:
$pkg_dir/configure \
	--prefix=/nyan/curl/$slot \
	--disable-debug \
	--enable-optimize \
	--disable-warnings \
	--disable-werror \
	--disable-curldebug \
	--enable-symbol-hiding \
	--disable-ares \
	--disable-shared \
	--enable-http \
	--disable-hsts \
	--enable-ftp \
	--disable-file \
	--disable-ldap \
	--disable-ldaps \
	--disable-rtsp \
	--disable-proxy \
	--disable-dict \
	--disable-telnet \
	--disable-tftp \
	--disable-pop3 \
	--disable-imap \
	--disable-smb \
	--disable-smtp \
	--disable-gopher \
	--disable-mqtt \
	--disable-docs \
	--disable-manual \
	--enable-ipv6 \
	--disable-versioned-symbols \
	--enable-threaded-resolver \
	--enable-pthreads \
	--enable-verbose \
	--disable-sspi \
	--disable-ntlm-wb \
	--disable-tls-srp \
	--enable-cookies \
	--with-zlib=/nyan/zlib/current \
	--without-brotli \
	--with-openssl=/nyan/libressl/current \
	--without-gnutls \
	--without-mbedtls \
	--without-wolfssl \
	--without-bearssl \
	--with-ca-fallback \
	--without-libssh2 \
	--without-libssh \
	--without-wolfssh \
	--without-librtmp \
	--without-libidn2 \
	--without-nghttp2 \
	--without-ngtcp2 \
	--without-nghttp3 \
	--without-quiche \
	--without-msh3 \
	--without-hyper
unset CFLAGS
unset CC
unset AR

make -j $threads_n
make install
unset PATH_SEPARATOR

# cleanup and tidying
rm -Rf /nyan/$src_name/$slot/share/man
rm -f /nyan/$src_name/$slot/lib/*.la 

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
