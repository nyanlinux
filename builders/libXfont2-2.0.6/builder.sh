src_name=libXfont2
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://xorg.freedesktop.org/releases/individual/lib/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
/nyan/pkgconf/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export "PKG_CONFIG_LIBDIR=\
/nyan/zlib/current/lib/pkgconfig:\
/nyan/libpng/current/lib/pkgconfig:\
/nyan/freetype/current/lib/pkgconfig:\
/nyan/xtrans/current/share/pkgconfig:\
/nyan/libfontenc/current/lib/pkgconfig:\
/nyan/util-macro/current/share/pkgconfig:\
/nyan/xorgproto/current/share/pkgconfig"
export PKG_CONFIG=pkgconf

export 'CFLAGS=-O2 -pipe -fPIC'
export "CC=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export "LDFLAGS=$($PKG_CONFIG --libs-only-L zlib)"
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--enable-shared \
	--disable-static \
	--disable-devel-docs \
	--disable-fc \
	--without-xmlto \
	--without-fop \
	--enable-unix-transport \
	--enable-tcp-transport	
unset CC
unset CFLAGS
unset LDFLAGS

make -j $threads_n
make install

rm -f /nyan/$src_name/$slot/lib/*.la 

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
