src_name=feh
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.bz2
url0=http://feh.finalrewind.org/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

cd $pkg_dir

export CC="$target_gnu_triple-gcc \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
-B/nyan/glibc/current/lib \
-L/nyan/glibc/current/lib \
-static-libgcc \
-Wl,-rpath-link,\
/nyan/libXau/current/lib:\
/nyan/libX11/current/lib:\
/nyan/freetype/current/lib:\
/nyan/libxcb/current/lib:\
/nyan/libXext/current/lib:\
/nyan/glibc/current/lib"
export "CFLAGS=-O2 -pipe -fPIC \
-I/nyan/libX11/current/include \
-I/nyan/xorgproto/current/include \
-I/nyan/libXinerama/current/include \
-I/nyan/imlib2/current/include \
-I/nyan/libexif/current/include \
-I/nyan/libpng/current/include \
-I/nyan/curl/current/include \
-I/nyan/libXt/current/include"
export "LDFLAGS=\
-L/nyan/zlib/current/lib \
-L/nyan/libpng/current/lib \
-L/nyan/libX11/current/lib \
-L/nyan/imlib2/current/lib \
-L/nyan/libressl/current/lib \
-L/nyan/curl/current/lib \
-L/nyan/libexif/current/lib \
-L/nyan/libXinerama/current/lib \
-Wl,-s"
export "LDLIBS=-lcurl -lssl -lcrypto -lz -lpthread" 
export PREFIX=/nyan/$src_name/$slot
make exif=1 curl=1
unset PREFIX
unset LDFLAGS
unset CFLAGS
unset CC

export PREFIX=/nyan/$src_name/$slot
make install

rm -Rf /nyan/$src_name/$slot/share/man
rm -Rf /nyan/$src_name/$slot/share/applications
rm -Rf /nyan/$src_name/$slot/share/doc

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $pkg_dir
