src_name=links
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.bz2
url0=http://links.twibright.com/download/$src_name-$version.tar.bz2

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cd $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

mkdir -p $build_dir/bin
cat >$build_dir/bin/cc <<EOF
#!/bin/sh
exec /opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc \
\
-isystem /nyan/linux-headers/current/include \
-isystem /nyan/glibc/current/include \
\
-I/nyan/libjpeg-turbo/current/include \
-I/nyan/libpng/current/include \
-I/nyan/zlib/current/include \
-I/nyan/freetype/current/include/freetype2 \
-I/nyan/fontconfig/current/include \
-I/nyan/libressl/current/include \
-I/nyan/xorgproto/current/include \
-I/nyan/libX11/current/include \
-I/nyan/libXt/current/include \
\
\
-O2 -pipe -fpic -fPIC -ftls-model=global-dynamic -static-libgcc \
\
\
-Wl,--as-needed \
-Wl,-s \
-B/nyan/glibc/current/lib \
\
-L/nyan/libressl/current/lib \
-L/nyan/zlib/current/lib \
-L/nyan/libX11/current/lib \
-L/nyan/fontconfig/current/lib \
-L/nyan/freetype/current/lib \
-L/nyan/libpng/current/lib \
-L/nyan/libjpeg-turbo/current/lib \
-L/nyan/libXt/current/lib \
\
-Wl,-rpath-link,\
/nyan/freetype/current/lib:\
/nyan/fontconfig/current/lib:\
/nyan/libICE/current/lib:\
/nyan/libSM/current/lib:\
/nyan/libXau/current/lib:\
/nyan/libxcb/current/lib:\
/nyan/libX11/current/lib:\
/nyan/zlib/current/lib:\
/nyan/glibc/current/lib \
\
"\$@"
EOF
chmod +x $build_dir/bin/cc

cat >$build_dir/bin/egrep <<EOF
#!/bin/sh
# links configure script is expecting egrep which is being removed
exec grep -E "\$@"
EOF
chmod +x $build_dir/bin/egrep

PATH_SAVED=$PATH
export PATH="\
$build_dir/bin:\
/nyan/make/current/bin:\
$PATH\
"

export CC=cc
export LIBS="\
-lpthread \
"
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--enable-graphics
unset CC
unset LIBS

make -j $threads_n
make install

rm -Rf /nyan/$src_name/$slot/man

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
