src_name=mutt
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=ftp://ftp.mutt.org/pub/$src_name/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export "CPPFLAGS=\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-I/nyan/ncurses/current/include/ncurses" 
export 'CFLAGS=-O2 -pipe -fPIC -static-libgcc'
export LDFLAGS="\
	-L/nyan/ncurses/current/lib \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export CC=$target_gnu_triple-gcc
export 'LIBS=-ltinfo -lpthread'
# need to force "working iconv" with nyanglibc 2.33 
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--disable-gpgme \
	--disable-pgp \
	--disable-smime \
	--disable-external-dotlock \
	--disable-pop \
	--disable-imap \
	--disable-smtp \
	--disable-debug \
	--disable-nfs-fix \
	--disable-mailtool \
	--disable-locales-fix \
	--enable-exact-address \
	--disable-hcache \
	--enable-iconv \
	--disable-nls \
	--disable-doc \
	--disable-full-doc \
	--with-curses=/nyan/ncurses/current \
	--with-bundled-regex \
	--with-mailpath=/var/mail \
	--without-qdbm \
	--without-gdbm \
	--without-bdb \
	--with-wc-funcs	\
	am_cv_func_iconv_works=yes
unset CPPFLAGS
unset CFLAGS
unset LDFLAGS
unset CC
unset LIBS

# doc is forced, again...
sed -i ./Makefile -e '536 c\
SUBDIRS = m4 po contrib'

make -j $threads_n
make install 

mkdir -p /var/mail

rm -Rf /nyan/$src_name/$slot/share

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
