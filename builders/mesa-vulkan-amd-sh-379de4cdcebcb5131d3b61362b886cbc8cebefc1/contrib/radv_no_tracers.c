#include <stdbool.h>

/* memory tracing */
void radv_rmv_collect_trace_events(void){}
void radv_rmv_log_bo_allocate(void){}
void radv_rmv_log_bo_destroy(void){}
void radv_rmv_log_border_color_palette_create(void){}
void radv_rmv_log_border_color_palette_destroy(void){}
void radv_rmv_log_buffer_bind(void){}
void radv_rmv_log_command_buffer_bo_create(void){}
void radv_rmv_log_command_buffer_bo_destroy(void){}
void radv_rmv_log_compute_pipeline_create(void){}
void radv_rmv_log_descriptor_pool_create(void){}
void radv_rmv_log_event_create(void){}
void radv_rmv_log_graphics_pipeline_create(void){}
void radv_rmv_log_heap_create(void){}
void radv_rmv_log_image_bind(void){}
void radv_rmv_log_image_create(void){}
void radv_rmv_log_query_pool_create(void){}
void radv_rmv_log_resource_destroy(void){}
void radv_rmv_log_rt_pipeline_create(void){}
void radv_rmv_log_sparse_add_residency(void){}
void radv_rmv_log_sparse_remove_residency(void){}
void radv_rmv_log_submit(void){}
void radv_rmv_fill_device_info(void){}
void radv_memory_trace_init(void){}
void radv_memory_trace_finish(void){}
void vk_memory_trace_init(void){}
void vk_memory_trace_finish(void){}
void vk_rmv_log_buffer_create(void){}
void vk_rmv_log_cpu_map(void){}
void vk_dump_rmv_capture(void){}

/* raytracing tracing abomination */
bool radv_rra_trace_enabled(void) {return false;}
void radv_rra_trace_init(void){}
void radv_rra_trace_finish(void){}
void radv_rra_dump_trace(void){}
void radv_rra_accel_struct_buffers_unref(void){}

/* sqtt thread tracing */
void radv_thread_trace_init(void){}
void radv_thread_trace_finish(void){}
void radv_emit_inhibit_clockgating(void){}
void radv_emit_spi_config_cntl(void){}
void radv_sqtt_emit_relocated_shaders(void){}
bool radv_sqtt_init(void){return false;}
void radv_sqtt_finish(void){}
bool radv_sqtt_queue_events_enabled(void){return false;}

/* ctx roll tracing */
void ac_gather_context_rolls(void){}

/* don't know, but seems related to sqtt thread tracing */
bool radv_is_instruction_timing_enabled(void) {return false;}

void radv_describe_dispatch(void){}
void radv_describe_layout_transition(void){}
void radv_describe_draw(void){}

void radv_describe_barrier_start(void){}
void radv_describe_barrier_end(void){}
void radv_describe_barrier_end_delayed(void){}

void radv_describe_begin_cmd_buffer(void){}
void radv_describe_end_cmd_buffer(void){}

void radv_describe_begin_render_pass_clear(void){}
void radv_describe_end_render_pass_clear(void){}
void radv_describe_begin_render_pass_resolve(void){}
void radv_describe_end_render_pass_resolve(void){}


/* this actually is related to the currently extremely toxic acceleration structure code */
void *radv_acceleration_structure_get_va(void *a)
{
	return 0;
}
