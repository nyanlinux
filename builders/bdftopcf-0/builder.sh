src_name=bdftopcf
git_commit=7a9d318d64c85a9000f8baeb23c267ac8629c6c0
git_url0=git://anongit.freedesktop.org/xorg/app/$src_name

slot=0

src_dir=$src_dir_root/$src_name
pkg_dir=$pkgs_dir_root/$pkg_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

cd $pkg_dir

git checkout --force $git_commit
git reset --hard

#===============================================================================
OLD_PATH=$PATH
export PATH=$sdk_autoconf_path/bin:$sdk_automake_path/bin:$PATH
export "ACLOCAL_PATH=\
/nyan/pkg-config/current/share/aclocal:\
/nyan/util-macro/current/share/aclocal"
export NOCONFIGURE=1

./autogen.sh

unset NOCONFIGURE
unset ACLOCAL_PATH
export PATH=$OLD_PATH
#===============================================================================

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export "PKG_CONFIG_LIBDIR=\
/nyan/xorgproto/current/share/pkgconfig:\
/nyan/util-macro/current/share/pkgconfig"

export "CC=gcc -static-libgcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
-Wl,-rpath-link,\
/nyan/glibc/current/lib\
"
export 'CFLAGS=-O2 -pipe -fPIC'
$pkg_dir/configure --prefix=/nyan/bdftopcf/$slot
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup
rm -Rf /nyan/$src_name/$slot/share
strip -s /nyan/$src_name/$slot/bin/$src_name

rm -Rf $build_dir $pkg_dir $install_dir
