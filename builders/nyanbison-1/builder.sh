src_name=nyanbison
git_commit=3ee791f2da4d841c91f9cec47a01de89a884481d
git_url0=git://git.launchpad.net/$src_name

gnulib_git_commit=f5f067f896b71a96c882b96c35759aeea2fa3072
gnulib_git_url0=git://git.savannah.gnu.org/gnulib.git

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

# augment the basic SDK
OLD_PATH=$PATH
export PATH=$sdk_autoconf_path/bin:$sdk_automake_path/bin:$PATH

#-------------------------------------------------------------------------------
cd $pkg_dir
# we do trust the version of the gnulib
git checkout --force $git_commit
git reset --hard

# from configmake module
$src_dir_root/gnulib/gnulib-tool --dir=$pkg_dir --source-base=gnulib --m4-base=m4/gnulib --import \
	spawn-pipe				\
	xconcat-filename			\
	xstrndup				\
	fopen-safer				\
	stdbool					\
	xmemdup0				\
	mbswidth				\
	argmatch				\
	hash					\
	closeout				\
	quotearg				\
	progname				\
	configmake				\
	xalloc					\
	verify					\
	unlocked-io

touch NEWS
touch README
touch AUTHORS
touch ChangeLog
autoreconf -v -f -i
#-------------------------------------------------------------------------------

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'

$pkg_dir/configure --prefix=/nyan/nyanbison/1

# parallel build breaks code generation
make 
make install

# cleanup and tidying
strip -s /nyan/$src_name/1/bin/bison

rm -Rf $build_dir $pkg_dir
export PATH=$OLD_PATH
