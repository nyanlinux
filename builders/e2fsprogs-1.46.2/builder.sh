src_name=e2fsprogs
version=1.46.2
slot=$version
archive_name=$src_name-$version.tar.xz
url0=https://www.kernel.org/pub/linux/kernel/people/tytso/$src_name/v$version/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

cd $pkg_dir
# jez...
sed -r -e 's/\$\(MANPAGES\)//g;s/\$\(FMANPAGES\)//g' -i $pkg_dir/e2fsck/Makefile.in
sed -r -e 's/\$\(SMANPAGES\)//g;s/\$\(FMANPAGES\)//g;s/\$\(UMANPAGES\)//g' -i $pkg_dir/misc/Makefile.in

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
# it wants echo in /bin... jez...
ln -sTf /nyan/busybox/current/bin/echo /bin/echo
export "PATH=\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export 'CFLAGS=-O2 -pipe -fPIC'
export "CC=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export AR=$target_gnu_triple-ar
# must create those paths or the installation sdk script will explode, lol
mkdir -p /nyan/$src_name/$slot/bin
mkdir -p /nyan/$src_name/$slot/lib
mkdir -p /nyan/$src_name/$slot/lib/pkgconfig
mkdir -p /nyan/$src_name/$slot/share/man/man8
mkdir -p /nyan/$src_name/$slot/share/man/man5
mkdir -p /nyan/$src_name/$slot/share/man/man3
mkdir -p /nyan/$src_name/$slot/share/man/man1
mkdir -p /nyan/$src_name/$slot/etc
mkdir -p /nyan/$src_name/$slot/include/et
mkdir -p /nyan/$src_name/$slot/include/ss
mkdir -p /nyan/$src_name/$slot/include/e2p
mkdir -p /nyan/$src_name/$slot/include/uuid
mkdir -p /nyan/$src_name/$slot/include/blkid
mkdir -p /nyan/$src_name/$slot/include/ext2fs
mkdir -p /nyan/$src_name/$slot/share/et
mkdir -p /nyan/$src_name/$slot/share/ss
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--enable-symlink-install \
	--enable-relative-symlinks \
	--enable-libuuid \
	--enable-libblkid \
	--disable-backtrace \
	--disable-debugfs \
	--disable-uuidd \
	--disable-nls
unset CFLAGS
unset CC
unset AR

make -j $threads_n
make install

ln -sTf $slot /nyan/$src_name/current

export PATH=$PATH_SAVED
rm -f /bin/echo
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
