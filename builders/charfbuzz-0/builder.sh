src_name=charfbuzz
git_commit=d42521e6a7d4d65561704947994baa8ba2ba373d
git_url0=git://github.com/sylware/$src_name

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

cd $pkg_dir

git checkout --force $git_commit
git reset --hard

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export "PKG_CONFIG_PATH=\
/nyan/libpng/current/lib/pkgconfig:\
/nyan/zlib/current/lib/pkgconfig:\
/nyan/freetype/current/lib/pkgconfig"

$pkg_dir/make																					\
	--disable-glib																				\
	--prefix=/nyan/charfbuzz/0																		\
	'--slib-cc=gcc -static-libgcc -O2 -pipe -fPIC -c'															\
	'--slib-ccld=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc -shared -Wl,-soname=libharfbuzz.so.0'

unset PKG_CONFIG_PATH

mkdir -p /nyan/charfbuzz/0
cp -r $build_dir/fake_root/* /

# cleanup and tidying
# remove the shared lib
rm -f /nyan/charfbuzz/0/lib/*.so*

rm -Rf $build_dir $pkg_dir
