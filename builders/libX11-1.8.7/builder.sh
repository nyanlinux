src_name=libX11
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://xorg.freedesktop.org/releases/individual/lib/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

# install our build system
cp -r $nyan_root/builders/$pkg_name/contrib $pkg_dir

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

cat >$build_dir/local_conf.sh <<EOF
version=$version
prefix=/usr
pkgconf_inc_dir=/nyan/$src_name/$slot/include
pkgconf_lib_dir=/nyan/$src_name/$slot/lib
EOF

$pkg_dir/contrib/binutils-gcc-glibc.sh

# we did override the pkg-config file to put dev stuff here
mkdir -p /nyan/$src_name/$slot/lib/pkgconfig
cp -f $build_dir/fakeroot/usr/lib/libX11-xcb.so.1.0.0 $build_dir/fakeroot/usr/lib/libX11.so.6.4.0 /nyan/$src_name/$slot/lib
ln -sTf libX11-xcb.so.1.0.0 /nyan/$src_name/$slot/lib/libX11-xcb.so
ln -sTf libX11.so.6.4.0 /nyan/$src_name/$slot/lib/libX11.so
cp -f $build_dir/fakeroot/usr/lib/pkgconfig/x11-xcb.pc $build_dir/fakeroot/usr/lib/pkgconfig/x11.pc /nyan/$src_name/$slot/lib/pkgconfig

# include files
rm -Rf /nyan/$src_name/$slot/include
mkdir -p /nyan/$src_name/$slot
cp -r $build_dir/fakeroot/usr/include /nyan/$src_name/$slot

# we go for a mono block runtime, XXX:current not slot
mkdir -p /usr/lib
ln -sTf /nyan/$src_name/current/lib/libX11-xcb.so.1.0.0 /usr/lib/libX11-xcb.so.1
ln -sTf /nyan/$src_name/current/lib/libX11.so.6.4.0 /usr/lib/libX11.so.6

# xkb compose runtime data files, you can override this location with XLOCALEDIR environment variable
# but invasive and trash software is unable to handle another location than /usr/share
rm -Rf /nyan/$src_name/$slot/share/X11/locale
mkdir -p /nyan/$src_name/$slot/share/X11
cp -r $build_dir/fakeroot/usr/share/X11/locale /nyan/$src_name/$slot/share/X11
mkdir -p /usr/share/X11
# XXX:current not slot
ln -sTf /nyan/$src_name/current/share/X11/locale /usr/share/X11/locale

rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
