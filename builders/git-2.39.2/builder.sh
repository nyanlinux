src_name=git
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=https://www.kernel.org/pub/software/scm/$src_name/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

# libcurl dependencies must be provided manually
# In order to trick the makefile, we override the CURL_LIBCURL initial value

cd $pkg_dir

# must add manually libressl library directory 
# we strip the binaries directly using the compiler driver passing -s option
# to the linker
make -j $nthreads_n install \
	"CC=$target_gnu_triple-gcc \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-B/nyan/glibc/current/lib \
		-L/nyan/libressl/current/lib \
		-L/nyan/glibc/current/lib \
		-std=c99 \
		-Wl,-s \
		-Wl,-rpath-link,/nyan/glibc/current/lib \
		-static-libgcc"	\
	'NO_R_TO_GCC_LINKER=1' \
	AR=$target_gnu_triple-ar \
	'CFLAGS=-O2 -pipe -fPIC' \
	prefix=/nyan/$src_name/$slot \
	NO_PERL=YesPlease NO_PYTHON=YesPlease NO_OPENSSL=YesPlease NO_TCLTK=YesPlease NO_GETTEXT=YesPlease HAVE_DEV_TTY=YesPlease DEFAULT_EDITOR=vim \
	ZLIB_PATH=/nyan/zlib/current \
	CURL_CONFIG=/nyan/curl/current/bin/curl-config \
	EXPATDIR=/nyan/expat/current \
	RUNTIME_PREFIX=1 PROCFS_EXECUTABLE_PATH=1

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
