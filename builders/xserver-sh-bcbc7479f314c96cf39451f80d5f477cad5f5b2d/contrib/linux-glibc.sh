#!/bin/sh
version_0=21
version_1=1
version_2=99
version_3=1
#===================================================================================================
# build dir and src dir
build_dir=$(readlink -f .)
printf "build_dir=$build_dir\n"
# we are in contrib subdir
src_dir=$(readlink -f $(dirname $0)/..)
printf "src_dir=$src_dir\n"
#===================================================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset" in
# those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===================================================================================================
if test "${sysconfdir-unset}" = unset; then
sysconfdir='/etc'
fi
#===================================================================================================
if test "${defaultfontdir-unset}" = unset; then
defaultfontdir='/usr/share/fonts'
fi
#===================================================================================================
if test "${miscconfigdir-unset}" = unset; then
miscconfigdir='/usr/lib/xorg'
fi
#===================================================================================================
if test "${dridriverpath-unset}" = unset; then
dridriverpath='/usr/lib/dri'
fi
#===================================================================================================
if test "${projectroot-unset}" = unset; then
# careful, will be used to configure some source files, no idea if it is benign
projectroot=/usr
fi
if test "${bindir_store-unset}" = unset; then
bindir_store=/usr/store/xserver/git/bin
fi
if test "${bindir_store_virtual-unset}" = unset; then
bindir_store_virtual=/usr/store/xserver/current/bin
fi
if test "${bindir-unset}" = unset; then
bindir=/usr/bin
fi
#===================================================================================================
if test "${xkbbasedir-unset}" = unset; then
xkbbasedir='/usr/share/X11/xkb'
fi
#===================================================================================================
if test "${xkbbindir-unset}" = unset; then
xkbbindir='/usr/bin'
fi
#===================================================================================================
if test "${xkmoutputdir-unset}" = unset; then
xkmoutputdir='/usr/share/X11/xkb/compiled'
fi
#===================================================================================================
if test "${module_store-unset}" = unset; then
module_store='/usr/store/xserver/git/modules'
fi
if test "${module_store_virtual-unset}" = unset; then
module_store_virtual='/usr/store/xserver/current/modules'
fi
if test "${defaultmodulepath-unset}" = unset; then
defaultmodulepath='/usr/lib/xorg/modules'
fi
#===================================================================================================
if test "${defaultlibrarypath-unset}" = unset; then
defaultlibrarypath='/usr/lib'
fi
#===================================================================================================
if test "${defaultlogdir-unset}" = unset; then
defaultlogdir='/var/log'
fi
#===================================================================================================
if test "${datadir-unset}" = unset; then
datadir='/usr/share'
fi
#===================================================================================================
if test "${incdir_store-unset}" = unset; then
incdir_store=/usr/store/xserver/git/include
fi
if test "${incdir_store_virtual-unset}" = unset; then
incdir_store_virtual=/usr/store/xserver/current/include
fi
#===================================================================================================
if test "${cpp-unset}" = unset; then
cpp=false
fi
#===================================================================================================
if test "${cc-unset}" = unset; then
cc=false
fi
#===================================================================================================
# you must use binutils ld export-dynamic option because dynamically loaded modules will refer back
# to symbols in the xorg binary
if test "${xorg_ccld_prolog-unset}" = unset; then
xorg_ccld_prolog=false
fi
if test "${xorg_ccld_epilog-unset}" = unset; then
xorg_ccld_epilog=
fi
#===================================================================================================
# this template expect the variable "module_name" to be defined
if test "${module_ccld_prolog_tmpl-unset}" = unset; then
module_ccld_prolog_tmpl=false
fi
if test "${module_ccld_epilog_tmpl-unset}" = unset; then
module_ccld_epilog_tmpl=
fi
#===================================================================================================
if test "${ar_rcs-unset}" = unset; then
ar_rcs=false
fi
#===================================================================================================
if test "${awk-unset}" = unset; then
awk=false
fi
#===================================================================================================
if test "${xorgproto_cppflags-unset}" = unset; then
xorgproto_cppflags=
fi
#===================================================================================================
if test "${xtrans_cppflags-unset}" = unset; then
xtrans_cppflags=
fi
#===================================================================================================
if test "${libxcvt_cppflags-unset}" = unset; then
libxcvt_cppflags=
fi
if test "${libxcvt_ldflags-unset}" = unset; then
libxcvt_ldflags=
fi
#===================================================================================================
if test "${mesa_gl_cppflags-unset}" = unset; then
mesa_gl_cppflags=
fi
if test "${mesa_gl_ldflags-unset}" = unset; then
mesa_gl_ldflags=
fi
if test "${mesa_gbm_ldflags-unset}" = unset; then
mesa_gbm_ldflags=
fi
#===================================================================================================
if test "${libxkbfile_cppflags-unset}" = unset; then
libxkbfile_cppflags=
fi
#===================================================================================================
if test "${pixman_cppflags-unset}" = unset; then
pixman_cppflags=
fi
if test "${pixman_ldflags-unset}" = unset; then
pixman_ldflags=
fi
#===================================================================================================
# why? we are server side
if test "${libX11_cppflags-unset}" = unset; then
libX11_cppflags=
fi
#===================================================================================================
# why? we are server side
if test "${libxcb_cppflags-unset}" = unset; then
libxcb_cppflags=
fi
#===================================================================================================
# used by the server
if test "${libxau_cppflags-unset}" = unset; then
libxau_cppflags=
fi
if test "${libxau_ldflags-unset}" = unset; then
libxau_ldflags=
fi
#===================================================================================================
# used by the server
if test "${libxfont2_cppflags-unset}" = unset; then
libxfont2_cppflags=
fi
if test "${libxfont2_ldflags-unset}" = unset; then
libxfont2_ldflags=
fi
#===================================================================================================
if test "${libfontenc_cppflags-unset}" = unset; then
libfontenc_cppflags=
fi
#===================================================================================================
if test "${freetype_cppflags-unset}" = unset; then
freetype_cppflags=
fi
#===================================================================================================
if test "${zlib_cppflags-unset}" = unset; then
zlib_cppflags=
fi
#===================================================================================================
if test "${libpng_cppflags-unset}" = unset; then
libpng_cppflags=
fi
#===================================================================================================
if test "${libxshmfence_cppflags-unset}" = unset; then
libxshmfence_cppflags=
fi
if test "${libxshmfence_ldflags-unset}" = unset; then
libxshmfence_ldflags=
fi
#===================================================================================================
if test "${ssl_cppflags-unset}" = unset; then
ssl_cppflags=
fi
if test "${ssl_ldflags-unset}" = unset; then
ssl_ldflags=
fi
#===================================================================================================
if test "${libdrm_cppflags-unset}" = unset; then
libdrm_cppflags=
fi
if test "${libdrm_ldflags-unset}" = unset; then
libdrm_ldflags=
fi
#===================================================================================================
if test "${libepoxy_cppflags-unset}" = unset; then
libepoxy_cppflags=
fi
if test "${libepoxy_ldflags-unset}" = unset; then
libepoxy_ldflags=
fi
#===================================================================================================
if test "${libudev_cppflags-unset}" = unset; then
libudev_cppflags=
fi
if test "${libudev_ldflags-unset}" = unset; then
libudev_ldflags=
fi
#===================================================================================================
if test "${libpciaccess_cppflags-unset}" = unset; then
libpciaccess_cppflags=
fi
if test "${libpciaccess_ldflags-unset}" = unset; then
libpciaccess_ldflags=
fi
#===================================================================================================
if test "${libglvnd_cppflags-unset}" = unset; then
libglvnd_cppflags=
fi
if test "${libglvnd_gl_ldflags-unset}" = unset; then
libglvnd_gl_ldflags=
fi
#===================================================================================================
mkdir -p $build_dir/dix
sed	-e "s:@sysconfdir@:$sysconfdir:g" \
	-e "s:@miscconfigpath@:$miscconfigpath:g" \
	-e "s:@defaultfontdir@:$defaultfontdir:g" \
	-e "s:@dridriverpath@:$dridriverpath:g" \
	-e "s:@projectroot@:$projectroot:g" \
	-e "s:@version_0@:$version_0:g" \
	-e "s:@version_1@:$version_1:g" \
	-e "s:@version_2@:$version_2:g" \
	<$src_dir/contrib/dix-config.h.in \
	>$build_dir/dix/dix-config.h
#---------------------------------------------------------------------------------------------------
#  bash enshitification detected, blame: Enrico Weigelt, 44e6558934e39d067550b8e5b25b2e75ba1bae73
echo '#!/bin/sh' >$build_dir/dix/generate-atoms
cat $src_dir/dix/generate-atoms >>$build_dir/dix/generate-atoms
chmod 500 $build_dir/dix/generate-atoms
$build_dir/dix/generate-atoms $src_dir/dix/BuiltInAtoms $build_dir/dix/BuiltInAtoms.c
#---------------------------------------------------------------------------------------------------
mkdir -p $build_dir/include
sed	-e "s:@version_0@:$version_0:g" \
	-e "s:@version_1@:$version_1:g" \
	-e "s:@version_2@:$version_2:g" \
	-e "s:@version_2@:$version_3:g" \
	<$src_dir/contrib/version-config.h.in \
	>$build_dir/include/version-config.h
#---------------------------------------------------------------------------------------------------
mkdir -p $build_dir/include
sed	-e "s:@xkbbasedir@:$xkbbasedir:g" \
	-e "s:@xkbbindir@:$xkbbindir:g" \
	-e "s:@xkmoutputdir@:$xkmoutputdir:g" \
	<$src_dir/contrib/xkb-config.h.in \
	>$build_dir/include/xkb-config.h
#===================================================================================================
xtrans_cfg_cppflags="\
-D_DEFAULT_SOURCE \
-D_BSD_SOURCE \
-DHAS_FCHOWN \
-DHAS_STICKY_DIR_BIT \
-DHAVE_SOCKLEN_T=1 \
"
xserver_cppflags="\
-DHAVE_CONFIG_H \
-DHAVE_DIX_CONFIG_H \
-DHAVE_MODESETTING_DRIVER \
-DXORG_NO_SDKSYMS \
"
cppflags="\
$xtrans_cfg_cppflags \
$xserver_cppflags \
$xorgproto_cppflags \
$xtrans_cppflags \
$libxcvt_cppflags \
$pixman_cppflags \
$libx11_cppflags \
$libxcb_cppflags \
$libxau_cppflags \
$libxfont2_cppflags \
$libfontenc_cppflags \
$freetype_cppflags \
$zlib_cppflags \
$libpng_cppflags \
$libxshmfence_cppflags \
$ssl_cppflags \
$libxkbfile_cppflags \
$libglvnd_cppflags \
$mesa_gl_cppflags \
$libdrm_cppflags \
$libepoxy_cppflags \
$libudev_cppflags \
$libpciaccess_cppflags \
-I$build_dir/dix \
-I$src_dir/dix \
-I$build_dir/include \
-I$src_dir/include \
-I$src_dir/glamor \
-I$src_dir/glx \
-I$src_dir/Xext \
-I$src_dir/composite \
-I$src_dir/damageext \
-I$src_dir/xfixes \
-I$src_dir/Xi \
-I$src_dir/mi \
-I$src_dir/miext/sync \
-I$src_dir/miext/shadow \
-I$src_dir/miext/damage \
-I$src_dir/render \
-I$src_dir/randr \
-I$src_dir/fb \
-I$src_dir/dbe \
-I$src_dir/present \
-I$src_dir/dri3 \
-I$build_dir \
-I$src_dir \
"
#===================================================================================================
dix_c_files="\
$build_dir/dix/BuiltInAtoms.c \
$src_dir/dix/atom.c \
$src_dir/dix/color.c \
$src_dir/dix/colormap.c \
$src_dir/dix/cursor.c \
$src_dir/dix/devices.c \
$src_dir/dix/dispatch.c \
$src_dir/dix/dixfonts.c \
$src_dir/dix/events.c \
$src_dir/dix/main.c \
$src_dir/dix/dixutils.c \
$src_dir/dix/enterleave.c \
$src_dir/dix/eventconvert.c \
$src_dir/dix/extension.c \
$src_dir/dix/gc.c \
$src_dir/dix/getevents.c \
$src_dir/dix/gestures.c \
$src_dir/dix/globals.c \
$src_dir/dix/glyphcurs.c \
$src_dir/dix/grabs.c \
$src_dir/dix/inpututils.c \
$src_dir/dix/pixmap.c \
$src_dir/dix/privates.c \
$src_dir/dix/property.c \
$src_dir/dix/ptrveloc.c \
$src_dir/dix/region.c \
$src_dir/dix/registry.c \
$src_dir/dix/resource.c \
$src_dir/dix/selection.c \
$src_dir/dix/swaprep.c \
$src_dir/dix/swapreq.c \
$src_dir/dix/tables.c \
$src_dir/dix/touch.c \
$src_dir/dix/window.c \
\
$src_dir/dix/stubmain.c \
"
mkdir -p $build_dir/dix
for f in $dix_c_files
do
	dix_obj=$build_dir/dix/$(basename $f .c).o
	dix_objs="$dix_objs $dix_obj"

	$cc $cppflags $f -o $dix_obj &
done
#===================================================================================================
fb_c_files_with_wrapper="\
$src_dir/fb/fballpriv.c \
$src_dir/fb/fbarc.c \
$src_dir/fb/fbbits.c \
$src_dir/fb/fbblt.c \
$src_dir/fb/fbbltone.c \
$src_dir/fb/fbcmap_mi.c \
$src_dir/fb/fbcopy.c \
$src_dir/fb/fbfill.c \
$src_dir/fb/fbfillrect.c \
$src_dir/fb/fbfillsp.c \
$src_dir/fb/fbgc.c \
$src_dir/fb/fbgetsp.c \
$src_dir/fb/fbglyph.c \
$src_dir/fb/fbimage.c \
$src_dir/fb/fbline.c \
$src_dir/fb/fboverlay.c \
$src_dir/fb/fbpict.c \
$src_dir/fb/fbpixmap.c \
$src_dir/fb/fbpoint.c \
$src_dir/fb/fbpush.c \
$src_dir/fb/fbscreen.c \
$src_dir/fb/fbseg.c \
$src_dir/fb/fbsetsp.c \
$src_dir/fb/fbsolid.c \
$src_dir/fb/fbtile.c \
$src_dir/fb/fbtrap.c \
$src_dir/fb/fbutil.c \
$src_dir/fb/fbwindow.c \
"
mkdir -p $build_dir/fb
for f in $fb_c_files_with_wrapper
do
	fb_obj=$build_dir/fb/fb_$(basename $f .c).o
	fbw_obj=$build_dir/fb/fbw_$(basename $f .c).o
	fb_objs="$fb_objs $fb_obj"
	fbw_objs="$fbw_objs $fbw_obj"

	$cc $cppflags $f -o $fb_obj &
	# w as in 'W'rapper
	$cc $cppflags -DFB_ACCESS_WRAPPER $f -o $fbw_obj &
done
#===================================================================================================
mi_c_files="\
$src_dir/mi/miarc.c \
$src_dir/mi/micmap.c \
$src_dir/mi/micopy.c \
$src_dir/mi/midash.c \
$src_dir/mi/midispcur.c \
$src_dir/mi/mieq.c \
$src_dir/mi/miexpose.c \
$src_dir/mi/mifillarc.c \
$src_dir/mi/migc.c \
$src_dir/mi/miglblt.c \
$src_dir/mi/mipointer.c \
$src_dir/mi/mipoly.c \
$src_dir/mi/mipolypnt.c \
$src_dir/mi/mipolyrect.c \
$src_dir/mi/mipolyseg.c \
$src_dir/mi/mipolytext.c \
$src_dir/mi/mipushpxl.c \
$src_dir/mi/miscrinit.c \
$src_dir/mi/misprite.c \
$src_dir/mi/mivaltree.c \
$src_dir/mi/miwideline.c \
$src_dir/mi/miwindow.c \
$src_dir/mi/mizerarc.c \
$src_dir/mi/mizerclip.c \
$src_dir/mi/mizerline.c \
"
mkdir -p $build_dir/mi
for f in $mi_c_files
do
	mi_obj=$build_dir/mi/$(basename $f .c).o
	mi_objs="$mi_objs $mi_obj"

	$cc $cppflags $f -o $mi_obj &
done
#===================================================================================================
xext_c_files="\
$src_dir/Xext/bigreq.c \
$src_dir/Xext/geext.c \
$src_dir/Xext/shape.c \
$src_dir/Xext/sync.c \
$src_dir/Xext/xcmisc.c \
$src_dir/Xext/xtest.c \
$src_dir/Xext/shm.c \
$src_dir/Xext/xvmain.c \
$src_dir/Xext/xvdisp.c \
$src_dir/Xext/xvmc.c \
$src_dir/Xext/xres.c \
$src_dir/Xext/saver.c \
$src_dir/Xext/panoramiX.c \
$src_dir/Xext/panoramiXprocs.c \
$src_dir/Xext/panoramiXSwap.c \
$src_dir/Xext/xace.c \
$src_dir/Xext/dpms.c \
$src_dir/Xext/hashtable.c \
$src_dir/Xext/sleepuntil.c \
"
mkdir -p $build_dir/xext
for f in $xext_c_files
do
	xext_obj=$build_dir/xext/$(basename $f .c).o
	xext_objs="$xext_objs $xext_obj"

	$cc $cppflags $f -o $xext_obj &
done
#---------------------------------------------------------------------------------------------------
xext_vidmod_c_files="\
$src_dir/Xext/vidmode.c \
"
for f in $xext_vidmod_c_files
do
	xext_vidmod_obj=$build_dir/xext/$(basename $f .c).o
	xext_vidmod_objs="$xext_vidmod_objs $xext_vidmod_obj"

	$cc $cppflags $f -o $xext_vidmod_obj &
done
#===================================================================================================
miext_sync_c_files="\
$src_dir/miext/sync/misync.c \
$src_dir/miext/sync/misyncfd.c \
$src_dir/miext/sync/misyncshm.c \
"
mkdir -p $build_dir/miext/sync
for f in $miext_sync_c_files
do
	miext_sync_obj=$build_dir/miext/sync/$(basename $f .c).o
	miext_sync_objs="$miext_sync_objs $miext_sync_obj"

	$cc $cppflags $f -o $miext_sync_obj &
done
#===================================================================================================
miext_damage_c_files="\
$src_dir/miext/damage/damage.c \
"
mkdir -p $build_dir/miext/damage
for f in $miext_damage_c_files
do
	miext_damage_obj=$build_dir/miext/damage/$(basename $f .c).o
	miext_damage_objs="$miext_damage_objs $miext_damage_obj"

	$cc $cppflags $f -o $miext_damage_obj &
done
#===================================================================================================
miext_shadow_c_files="\
$src_dir/miext/shadow/shadow.c \
$src_dir/miext/shadow/sh3224.c \
$src_dir/miext/shadow/shafb4.c \
$src_dir/miext/shadow/shafb8.c \
$src_dir/miext/shadow/shiplan2p4.c \
$src_dir/miext/shadow/shiplan2p8.c \
$src_dir/miext/shadow/shpacked.c \
$src_dir/miext/shadow/shplanar8.c \
$src_dir/miext/shadow/shplanar.c \
$src_dir/miext/shadow/shrot16pack_180.c \
$src_dir/miext/shadow/shrot16pack_270.c \
$src_dir/miext/shadow/shrot16pack_270YX.c \
$src_dir/miext/shadow/shrot16pack_90.c \
$src_dir/miext/shadow/shrot16pack_90YX.c \
$src_dir/miext/shadow/shrot16pack.c \
$src_dir/miext/shadow/shrot32pack_180.c \
$src_dir/miext/shadow/shrot32pack_270.c \
$src_dir/miext/shadow/shrot32pack_90.c \
$src_dir/miext/shadow/shrot32pack.c \
$src_dir/miext/shadow/shrot8pack_180.c \
$src_dir/miext/shadow/shrot8pack_270.c \
$src_dir/miext/shadow/shrot8pack_90.c \
$src_dir/miext/shadow/shrot8pack.c \
$src_dir/miext/shadow/shrotate.c \
"
mkdir -p $build_dir/miext/shadow
for f in $miext_shadow_c_files
do
	miext_shadow_obj=$build_dir/miext/shadow/$(basename $f .c).o
	miext_shadow_objs="$miext_shadow_objs $miext_shadow_obj"

	$cc $cppflags $f -o $miext_shadow_obj &
done
#===================================================================================================
miext_rootless_c_files="\
$src_dir/miext/rootless/rootlessCommon.c \
$src_dir/miext/rootless/rootlessGC.c \
$src_dir/miext/rootless/rootlessScreen.c \
$src_dir/miext/rootless/rootlessValTree.c \
$src_dir/miext/rootless/rootlessWindow.c \
"
mkdir -p $build_dir/miext/rootless
for f in $miext_rootless_c_files
do
	miext_rootless_obj=$build_dir/miext/rootless/$(basename $f .c).o
	miext_rootless_objs="$miext_rootless_objs $miext_rootless_obj"

	$cc $cppflags $f -o $miext_rootless_obj &
done
#===================================================================================================
os_c_files="\
$src_dir/os/access.c \
$src_dir/os/alloc.c \
$src_dir/os/auth.c \
$src_dir/os/WaitFor.c \
$src_dir/os/backtrace.c \
$src_dir/os/client.c \
$src_dir/os/connection.c \
$src_dir/os/fmt.c \
$src_dir/os/inputthread.c \
$src_dir/os/io.c \
$src_dir/os/mitauth.c \
$src_dir/os/osinit.c \
$src_dir/os/ospoll.c \
$src_dir/os/utils.c \
$src_dir/os/xdmauth.c \
$src_dir/os/xsha1.c \
$src_dir/os/xstrans.c \
$src_dir/os/xprintf.c \
$src_dir/os/log.c \
$src_dir/os/busfault.c \
$src_dir/os/serverlock.c \
$src_dir/os/string.c \
$src_dir/os/strlcat.c \
$src_dir/os/strlcpy.c \
$src_dir/os/timingsafe_memcmp.c \
"
mkdir $build_dir/os
for f in $os_c_files
do
	os_obj=$build_dir/os/$(basename $f .c).o
	os_objs="$os_objs $os_obj"

	$cc $cppflags $f -o $os_obj &
done
#===================================================================================================
randr_c_files="\
$src_dir/randr/randr.c \
$src_dir/randr/rrcrtc.c \
$src_dir/randr/rrdispatch.c \
$src_dir/randr/rrinfo.c \
$src_dir/randr/rrlease.c \
$src_dir/randr/rrmode.c \
$src_dir/randr/rrmonitor.c \
$src_dir/randr/rroutput.c \
$src_dir/randr/rrpointer.c \
$src_dir/randr/rrproperty.c \
$src_dir/randr/rrprovider.c \
$src_dir/randr/rrproviderproperty.c \
$src_dir/randr/rrscreen.c \
$src_dir/randr/rrsdispatch.c \
$src_dir/randr/rrtransform.c \
$src_dir/randr/rrxinerama.c \
"
mkdir -p $build_dir/randr
for f in $randr_c_files
do
	randr_obj=$build_dir/randr/$(basename $f .c).o
	randr_objs="$randr_objs $randr_obj"

	$cc $cppflags $f -o $randr_obj &
done
#===================================================================================================
render_c_files="\
$src_dir/render/animcur.c \
$src_dir/render/filter.c \
$src_dir/render/glyph.c \
$src_dir/render/matrix.c \
$src_dir/render/miindex.c \
$src_dir/render/mipict.c \
$src_dir/render/mirect.c \
$src_dir/render/mitrap.c \
$src_dir/render/mitri.c \
$src_dir/render/picture.c \
$src_dir/render/render.c \
"
mkdir -p $build_dir/render
for f in $render_c_files
do
	render_obj=$build_dir/render/$(basename $f .c).o
	render_objs="$render_objs $render_obj"

	$cc $cppflags $f -o $render_obj &
done
#===================================================================================================
xi_c_files="\
$src_dir/Xi/allowev.c \
$src_dir/Xi/chgdctl.c \
$src_dir/Xi/chgfctl.c \
$src_dir/Xi/chgkbd.c \
$src_dir/Xi/chgkmap.c \
$src_dir/Xi/chgprop.c \
$src_dir/Xi/chgptr.c \
$src_dir/Xi/closedev.c \
$src_dir/Xi/devbell.c \
$src_dir/Xi/exevents.c \
$src_dir/Xi/extinit.c \
$src_dir/Xi/getbmap.c \
$src_dir/Xi/getdctl.c \
$src_dir/Xi/getfctl.c \
$src_dir/Xi/getfocus.c \
$src_dir/Xi/getkmap.c \
$src_dir/Xi/getmmap.c \
$src_dir/Xi/getprop.c \
$src_dir/Xi/getselev.c \
$src_dir/Xi/getvers.c \
$src_dir/Xi/grabdev.c \
$src_dir/Xi/grabdevb.c \
$src_dir/Xi/grabdevk.c \
$src_dir/Xi/gtmotion.c \
$src_dir/Xi/listdev.c \
$src_dir/Xi/opendev.c \
$src_dir/Xi/queryst.c \
$src_dir/Xi/selectev.c \
$src_dir/Xi/sendexev.c \
$src_dir/Xi/setbmap.c \
$src_dir/Xi/setdval.c \
$src_dir/Xi/setfocus.c \
$src_dir/Xi/setmmap.c \
$src_dir/Xi/setmode.c \
$src_dir/Xi/ungrdev.c \
$src_dir/Xi/ungrdevb.c \
$src_dir/Xi/ungrdevk.c \
$src_dir/Xi/xiallowev.c \
$src_dir/Xi/xibarriers.c \
$src_dir/Xi/xichangecursor.c \
$src_dir/Xi/xichangehierarchy.c \
$src_dir/Xi/xigetclientpointer.c \
$src_dir/Xi/xigrabdev.c \
$src_dir/Xi/xipassivegrab.c \
$src_dir/Xi/xiproperty.c \
$src_dir/Xi/xiquerydevice.c \
$src_dir/Xi/xiquerypointer.c \
$src_dir/Xi/xiqueryversion.c \
$src_dir/Xi/xiselectev.c \
$src_dir/Xi/xisetclientpointer.c \
$src_dir/Xi/xisetdevfocus.c \
$src_dir/Xi/xiwarppointer.c \
"
mkdir -p $build_dir/xi
for f in $xi_c_files
do
	xi_obj=$build_dir/xi/$(basename $f .c).o
	xi_objs="$xi_objs $xi_obj"

	$cc $cppflags $f -o $xi_obj &
done
#---------------------------------------------------------------------------------------------------
xi_stubs_c_files="\
$src_dir/Xi/stubs.c \
"
for f in $xi_stubs_c_files
do
	xi_stubs_obj=$build_dir/xi/$(basename $f .c).o
	xi_stubs_objs="$xi_stubs_objs $xi_stubs_obj"

	$cc $cppflags $f -o $xi_stubs_obj &
done
#===================================================================================================
xkb_c_files="\
$src_dir/xkb/ddxBeep.c \
$src_dir/xkb/ddxCtrls.c \
$src_dir/xkb/ddxLEDs.c \
$src_dir/xkb/ddxLoad.c \
$src_dir/xkb/xkb.c \
$src_dir/xkb/xkbUtils.c \
$src_dir/xkb/xkbEvents.c \
$src_dir/xkb/xkbAccessX.c \
$src_dir/xkb/xkbSwap.c \
$src_dir/xkb/xkbLEDs.c \
$src_dir/xkb/xkbInit.c \
$src_dir/xkb/xkbActions.c \
$src_dir/xkb/xkbPrKeyEv.c \
$src_dir/xkb/maprules.c \
$src_dir/xkb/xkmread.c \
$src_dir/xkb/xkbtext.c \
$src_dir/xkb/xkbfmisc.c \
$src_dir/xkb/xkbout.c \
$src_dir/xkb/XKBMisc.c \
$src_dir/xkb/XKBAlloc.c \
$src_dir/xkb/XKBGAlloc.c \
$src_dir/xkb/XKBMAlloc.c \
"
mkdir -p $build_dir/xkb
for f in $xkb_c_files
do
	xkb_obj=$build_dir/xkb/$(basename $f .c).o
	xkb_objs="$xkb_objs $xkb_obj"

	$cc $cppflags $f -o $xkb_obj &
done
#---------------------------------------------------------------------------------------------------
xkb_stubs_c_files="\
$src_dir/xkb/ddxVT.c \
$src_dir/xkb/ddxPrivate.c \
$src_dir/xkb/ddxKillSrv.c \
"
for f in $xkb_stubs_c_files
do
	xkb_stubs_obj=$build_dir/xkb/$(basename $f .c).o
	xkb_stubs_objs="$xkb_stubs_objs $xkb_stubs_obj"

	$cc $cppflags $f -o $xkb_stubs_obj &
done
#===================================================================================================
dbe_c_files="\
$src_dir/dbe/dbe.c \
$src_dir/dbe/midbe.c \
"
mkdir -p $build_dir/dbe
for f in $dbe_c_files
do
	dbe_obj=$build_dir/dbe/$(basename $f .c).o
	dbe_objs="$dbe_objs $dbe_obj"

	$cc $cppflags $f -o $dbe_obj &
done
#===================================================================================================
record_c_files="\
$src_dir/record/record.c \
$src_dir/record/set.c \
"
mkdir -p $build_dir/record
for f in $record_c_files
do
	record_obj=$build_dir/record/$(basename $f .c).o
	record_objs="$record_objs $record_obj"

	$cc $cppflags $f -o $record_obj &
done
#===================================================================================================
xfixes_c_files="\
$src_dir/xfixes/xfixes.c \
$src_dir/xfixes/cursor.c \
$src_dir/xfixes/disconnect.c \
$src_dir/xfixes/region.c \
$src_dir/xfixes/saveset.c \
$src_dir/xfixes/select.c \
"
mkdir -p $build_dir/xfixes
for f in $xfixes_c_files
do
	xfixes_obj=$build_dir/xfixes/$(basename $f .c).o
	xfixes_objs="$xfixes_objs $xfixes_obj"

	$cc $cppflags $f -o $xfixes_obj &
done
#===================================================================================================
damageext_c_files="\
$src_dir/damageext/damageext.c \
"
mkdir -p $build_dir/damageext
for f in $damageext_c_files
do
	damageext_obj=$build_dir/damageext/$(basename $f .c).o
	damageext_objs="$damageext_objs $damageext_obj"

	$cc $cppflags $f -o $damageext_obj &
done
#===================================================================================================
composite_c_files="\
$src_dir/composite/compalloc.c \
$src_dir/composite/compext.c \
$src_dir/composite/compinit.c \
$src_dir/composite/compoverlay.c \
$src_dir/composite/compwindow.c \
"
mkdir -p $build_dir/composite
for f in $composite_c_files
do
	composite_obj=$build_dir/composite/$(basename $f .c).o
	composite_objs="$composite_objs $composite_obj"

	$cc $cppflags $f -o $composite_obj &
done
#===================================================================================================
glx_c_files="\
$src_dir/glx/indirect_dispatch.c \
$src_dir/glx/indirect_dispatch_swap.c \
$src_dir/glx/indirect_reqsize.c \
$src_dir/glx/indirect_size_get.c \
$src_dir/glx/indirect_table.c \
$src_dir/glx/clientinfo.c \
$src_dir/glx/createcontext.c \
$src_dir/glx/extension_string.c \
$src_dir/glx/indirect_util.c \
$src_dir/glx/indirect_program.c \
$src_dir/glx/indirect_texture_compression.c \
$src_dir/glx/glxcmds.c \
$src_dir/glx/glxcmdsswap.c \
$src_dir/glx/glxext.c \
$src_dir/glx/glxdriswrast.c \
$src_dir/glx/glxdricommon.c \
$src_dir/glx/glxscreens.c \
$src_dir/glx/render2.c \
$src_dir/glx/render2swap.c \
$src_dir/glx/renderpix.c \
$src_dir/glx/renderpixswap.c \
$src_dir/glx/rensize.c \
$src_dir/glx/single2.c \
$src_dir/glx/single2swap.c \
$src_dir/glx/singlepix.c \
$src_dir/glx/singlepixswap.c \
$src_dir/glx/singlesize.c \
$src_dir/glx/swap_interval.c \
$src_dir/glx/xfont.c \
"
mkdir -p $build_dir/glx
for f in $glx_c_files
do
	glx_obj=$build_dir/glx/$(basename $f .c).o
	glx_objs="$glx_objs $glx_obj"

	$cc	-I$src_dir/glx \
		-I$src_dir/hw/xfree86/common \
		-I$src_dir/hw/xfree86/dri2 \
		$cppflags \
		-D__GLX_ALIGN64 $f -o $glx_obj &
done
#---------------------------------------------------------------------------------------------------
glxdri_c_files="\
$src_dir/glx/glxdri2.c \
"
for f in $glxdri_c_files
do
	glxdri_obj=$build_dir/glx/$(basename $f .c).o
	glxdri_objs="$glxdri_objs $glxdri_obj"

	$cc	-I$src_dir/glx \
		-I$src_dir/hw/xfree86/common \
		-I$src_dir/hw/xfree86/dri2 \
		$cppflags \
		-D__GLX_ALIGN64 $f -o $glxdri_obj &
done
#---------------------------------------------------------------------------------------------------
glxvnd_c_files="\
$src_dir/glx/vndcmds.c \
$src_dir/glx/vndext.c \
$src_dir/glx/vndservermapping.c \
$src_dir/glx/vndservervendor.c \
"
for f in $glxvnd_c_files
do
	glxvnd_obj=$build_dir/glx/$(basename $f .c).o
	glxvnd_objs="$glxvnd_objs $glxvnd_obj"

	$cc	-I$src_dir/glx \
		-I$src_dir/hw/xfree86/common \
		-I$src_dir/hw/xfree86/dri2 \
		$cppflags \
		-D__GLX_ALIGN64 $f -o $glxvnd_obj &
done
#===================================================================================================
present_c_files="\
$src_dir/present/present.c \
$src_dir/present/present_event.c \
$src_dir/present/present_execute.c \
$src_dir/present/present_fake.c \
$src_dir/present/present_fence.c \
$src_dir/present/present_notify.c \
$src_dir/present/present_request.c \
$src_dir/present/present_scmd.c \
$src_dir/present/present_screen.c \
$src_dir/present/present_vblank.c \
"
mkdir -p $build_dir/present
for f in $present_c_files
do
	present_obj=$build_dir/present/$(basename $f .c).o
	present_objs="$present_objs $present_obj"

	$cc $cppflags $f -o $present_obj &
done
#===================================================================================================
dri3_c_files="\
$src_dir/dri3/dri3.c \
$src_dir/dri3/dri3_request.c \
$src_dir/dri3/dri3_screen.c \
"
mkdir -p $build_dir/dri3
for f in $dri3_c_files
do
	dri3_obj=$build_dir/dri3/$(basename $f .c).o
	dri3_objs="$dri3_objs $dri3_obj"

	$cc $cppflags $f -o $dri3_obj &
done
#===================================================================================================
exa_c_files="\
$src_dir/exa/exa.c \
$src_dir/exa/exa_classic.c \
$src_dir/exa/exa_migration_classic.c \
$src_dir/exa/exa_driver.c \
$src_dir/exa/exa_mixed.c \
$src_dir/exa/exa_migration_mixed.c \
$src_dir/exa/exa_accel.c \
$src_dir/exa/exa_glyphs.c \
$src_dir/exa/exa_offscreen.c \
$src_dir/exa/exa_render.c \
$src_dir/exa/exa_unaccel.c \
"
mkdir -p $build_dir/exa
for f in $exa_c_files
do
	exa_obj=$build_dir/exa/$(basename $f .c).o
	exa_objs="$exa_objs $exa_obj"

	$cc $cppflags $f -o $exa_obj &
done
#===================================================================================================
glamor_c_files="\
$src_dir/glamor/glamor.c \
$src_dir/glamor/glamor_copy.c \
$src_dir/glamor/glamor_core.c \
$src_dir/glamor/glamor_dash.c \
$src_dir/glamor/glamor_font.c \
$src_dir/glamor/glamor_composite_glyphs.c \
$src_dir/glamor/glamor_image.c \
$src_dir/glamor/glamor_lines.c \
$src_dir/glamor/glamor_segs.c \
$src_dir/glamor/glamor_render.c \
$src_dir/glamor/glamor_gradient.c \
$src_dir/glamor/glamor_prepare.c \
$src_dir/glamor/glamor_program.c \
$src_dir/glamor/glamor_rects.c \
$src_dir/glamor/glamor_spans.c \
$src_dir/glamor/glamor_text.c \
$src_dir/glamor/glamor_transfer.c \
$src_dir/glamor/glamor_transform.c \
$src_dir/glamor/glamor_trapezoid.c \
$src_dir/glamor/glamor_triangles.c \
$src_dir/glamor/glamor_addtraps.c \
$src_dir/glamor/glamor_glyphblt.c \
$src_dir/glamor/glamor_points.c \
$src_dir/glamor/glamor_pixmap.c \
$src_dir/glamor/glamor_largepixmap.c \
$src_dir/glamor/glamor_picture.c \
$src_dir/glamor/glamor_vbo.c \
$src_dir/glamor/glamor_window.c \
$src_dir/glamor/glamor_fbo.c \
$src_dir/glamor/glamor_compositerects.c \
$src_dir/glamor/glamor_utils.c \
$src_dir/glamor/glamor_sync.c \
$src_dir/glamor/glamor_xv.c \
$src_dir/glamor/glamor_glx_provider.c \
"
mkdir -p $build_dir/glamor
for f in $glamor_c_files
do
	glamor_obj=$build_dir/glamor/$(basename $f .c).o
	glamor_objs="$glamor_objs $glamor_obj"

	$cc $cppflags $f -o $glamor_obj &
done
#===================================================================================================
config_c_files="\
$src_dir/config/config.c \
$src_dir/config/udev.c \
"
mkdir -p $build_dir/config
for f in $config_c_files
do
	config_obj=$build_dir/config/$(basename $f .c).o
	config_objs="$config_objs $config_obj"

	$cc $cppflags $f -o $config_obj &
done
#===================================================================================================
mkdir -p $build_dir/include
sed	-e "s:@version_0@:$version_0:g" \
	-e "s:@version_1@:$version_1:g" \
	-e "s:@version_2@:$version_2:g" \
	-e "s:@version_3@:$version_3:g" \
	-e "s:@defaultmodulepath@:$defaultmodulepath:g" \
	-e "s:@defaultlibrarypath@:$defaultlibrarypath:g" \
	-e "s:@defaultlogdir@:$defaultlogdir:g" \
	<$src_dir/contrib/xorg-config.h.in \
	>$build_dir/include/xorg-config.h
#---------------------------------------------------------------------------------------------------
mkdir -p $build_dir/hw/xfree86/common

cat $src_dir/hw/xfree86/common/vesamodes $src_dir/hw/xfree86/common/extramodes | \
	LC_ALL=C $awk -f $src_dir/hw/xfree86/common/modeline2c.awk \
	> $build_dir/hw/xfree86/common/xf86DefModeSet.c

hw_xfree86_common_c_files="\
$build_dir/hw/xfree86/common/xf86DefModeSet.c \
$src_dir/hw/xfree86/common/xf86Configure.c \
$src_dir/hw/xfree86/common/xf86Bus.c \
$src_dir/hw/xfree86/common/xf86Config.c \
$src_dir/hw/xfree86/common/xf86Cursor.c \
$src_dir/hw/xfree86/common/xf86DPMS.c \
$src_dir/hw/xfree86/common/xf86Events.c \
$src_dir/hw/xfree86/common/xf86Globals.c \
$src_dir/hw/xfree86/common/xf86AutoConfig.c \
$src_dir/hw/xfree86/common/xf86Option.c \
$src_dir/hw/xfree86/common/xf86Init.c \
$src_dir/hw/xfree86/common/xf86VidMode.c \
$src_dir/hw/xfree86/common/xf86fbman.c \
$src_dir/hw/xfree86/common/xf86cmap.c \
$src_dir/hw/xfree86/common/xf86Helper.c \
$src_dir/hw/xfree86/common/xf86PM.c \
$src_dir/hw/xfree86/common/xf86Xinput.c \
$src_dir/hw/xfree86/common/xisb.c \
$src_dir/hw/xfree86/common/xf86Mode.c \
$src_dir/hw/xfree86/common/xorgHelper.c \
$src_dir/hw/xfree86/common/xf86Extensions.c \
$src_dir/hw/xfree86/common/xf86xv.c \
$src_dir/hw/xfree86/common/xf86xvmc.c \
$src_dir/hw/xfree86/common/xf86fbBus.c \
$src_dir/hw/xfree86/common/xf86noBus.c \
$src_dir/hw/xfree86/common/xf86pciBus.c \
$src_dir/hw/xfree86/common/xf86VGAarbiter.c \
$src_dir/hw/xfree86/common/xf86platformBus.c \
$src_dir/hw/xfree86/common/xf86RandR.c \
"
for f in $hw_xfree86_common_c_files
do
	hw_xfree86_common_obj=$build_dir/hw/xfree86/common/$(basename $f .c).o
	hw_xfree86_common_objs="$hw_xfree86_common_objs $hw_xfree86_common_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H -DHAVE_ACPI \
		-I$src_dir/hw/xfree86/os-support/bus \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/parser \
		-I$src_dir/hw/xfree86/loader \
		-I$src_dir/hw/xfree86/ramdac \
		-I$src_dir/hw/xfree86/ddc \
		-I$src_dir/hw/xfree86/i2c \
		-I$src_dir/hw/xfree86/modes \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_common_obj &
done
#===================================================================================================
hw_xfree86_ddc_c_files="\
$src_dir/hw/xfree86/ddc/ddc.c \
$src_dir/hw/xfree86/ddc/interpret_edid.c \
$src_dir/hw/xfree86/ddc/print_edid.c \
$src_dir/hw/xfree86/ddc/ddcProperty.c \
"
mkdir -p $build_dir/hw/xfree86/ddc
for f in $hw_xfree86_ddc_c_files
do
	hw_xfree86_ddc_obj=$build_dir/hw/xfree86/ddc/$(basename $f .c).o
	hw_xfree86_ddc_objs="$hw_xfree86_ddc_objs $hw_xfree86_ddc_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/i2c \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_ddc_obj &
done
#===================================================================================================
hw_xfree86_x86emu_c_files="\
$src_dir/hw/xfree86/x86emu/debug.c \
$src_dir/hw/xfree86/x86emu/decode.c \
$src_dir/hw/xfree86/x86emu/fpu.c \
$src_dir/hw/xfree86/x86emu/ops2.c \
$src_dir/hw/xfree86/x86emu/ops.c \
$src_dir/hw/xfree86/x86emu/prim_ops.c \
$src_dir/hw/xfree86/x86emu/sys.c \
"
mkdir -p $build_dir/hw/xfree86/x86emu
for f in $hw_xfree86_x86emu_c_files
do
	hw_xfree86_x86emu_obj=$build_dir/hw/xfree86/x86emu/$(basename $f .c).o
	hw_xfree86_x86emu_objs="$hw_xfree86_x86emu_objs $hw_xfree86_x86emu_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/x86emu \
		$f -o $hw_xfree86_x86emu_obj &
done
#===================================================================================================
hw_xfree86_int10_c_files="\
$src_dir/hw/xfree86/int10/vbe.c \
$src_dir/hw/xfree86/int10/vbeModes.c \
$src_dir/hw/xfree86/int10/helper_exec.c \
$src_dir/hw/xfree86/int10/xf86int10.c \
$src_dir/hw/xfree86/int10/xf86int10module.c \
$src_dir/hw/xfree86/int10/xf86x86emu.c \
$src_dir/hw/xfree86/int10/generic.c \
$src_dir/hw/xfree86/int10/x86emu.c \
"
mkdir -p $build_dir/hw/xfree86/int10
for f in $hw_xfree86_int10_c_files
do
	hw_xfree86_int10_obj=$build_dir/hw/xfree86/int10/$(basename $f .c).o
	hw_xfree86_int10_objs="$hw_xfree86_int10_objs $hw_xfree86_int10_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-D_PC -D_X86EMU -DNO_SYS_HEADERS \
		-I$src_dir/hw/xfree86/os-support/bus \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/i2c \
		-I$src_dir/hw/xfree86/parser \
		-I$src_dir/hw/xfree86/ddc \
		-I$src_dir/hw/xfree86/modes \
		-I$src_dir/hw/xfree86/common \
		-I$src_dir/hw/xfree86/x86emu \
		-I$src_dir/hw/xfree86/int10 \
		-I$src_dir/hw/xfree86/helper_mem \
		$f -o $hw_xfree86_int10_obj &
done
#===================================================================================================
hw_xfree86_os_support_bus_c_files="\
$src_dir/hw/xfree86/os-support/bus/Pci.c \
$src_dir/hw/xfree86/os-support/bus/nobus.c \
"
mkdir -p $build_dir/hw/xfree86/os-support/bus
for f in $hw_xfree86_os_support_bus_c_files
do
	hw_xfree86_os_support_bus_obj=$build_dir/hw/xfree86/os-support/bus/$(basename $f .c).o
	hw_xfree86_os_support_bus_objs="$hw_xfree86_os_support_bus_objs $hw_xfree86_os_support_bus_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		$f -o $hw_xfree86_os_support_bus_obj &
done
#===================================================================================================
hw_xfree86_os_support_linux_c_files="\
$src_dir/hw/xfree86/os-support/linux/lnx_init.c \
$src_dir/hw/xfree86/os-support/linux/lnx_video.c \
$src_dir/hw/xfree86/os-support/linux/lnx_kmod.c \
$src_dir/hw/xfree86/os-support/linux/lnx_bell.c \
$src_dir/hw/xfree86/os-support/shared/drm_platform.c \
$src_dir/hw/xfree86/os-support/shared/VTsw_usl.c \
$src_dir/hw/xfree86/os-support/shared/posix_tty.c \
$src_dir/hw/xfree86/os-support/shared/vidmem.c \
$src_dir/hw/xfree86/os-support/shared/sigio.c \
$src_dir/hw/xfree86/os-support/linux/lnx_acpi.c \
$src_dir/hw/xfree86/os-support/linux/lnx_apm.c \
$src_dir/hw/xfree86/os-support/linux/lnx_agp.c \
"
mkdir -p $build_dir/hw/xfree86/os-support/linux
for f in $hw_xfree86_os_support_linux_c_files
do
	hw_xfree86_os_support_linux_obj=$build_dir/hw/xfree86/os-support/linux/$(basename $f .c).o
	hw_xfree86_os_support_linux_objs="$hw_xfree86_os_support_linux_objs $hw_xfree86_os_support_linux_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-DHAVE_ACPI -DHAVE_APM \
		-DHAVE_SYSV_IPC \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_os_support_linux_obj &
done
#===================================================================================================
hw_xfree86_os_support_misc_c_files="\
$src_dir/hw/xfree86/os-support/misc/SlowBcopy.c \
"
mkdir -p $build_dir/hw/xfree86/os-support/misc
for f in $hw_xfree86_os_support_misc_c_files
do
	hw_xfree86_os_support_misc_obj=$build_dir/hw/xfree86/os-support/misc/$(basename $f .c).o
	hw_xfree86_os_support_misc_objs="$hw_xfree86_os_support_misc_objs $hw_xfree86_os_support_misc_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_os_support_misc_obj &
done
#===================================================================================================
hw_xfree86_parser_c_files="\
$src_dir/hw/xfree86/parser/Device.c \
$src_dir/hw/xfree86/parser/Files.c \
$src_dir/hw/xfree86/parser/Flags.c \
$src_dir/hw/xfree86/parser/Input.c \
$src_dir/hw/xfree86/parser/InputClass.c \
$src_dir/hw/xfree86/parser/OutputClass.c \
$src_dir/hw/xfree86/parser/Layout.c \
$src_dir/hw/xfree86/parser/Module.c \
$src_dir/hw/xfree86/parser/Video.c \
$src_dir/hw/xfree86/parser/Monitor.c \
$src_dir/hw/xfree86/parser/Pointer.c \
$src_dir/hw/xfree86/parser/Screen.c \
$src_dir/hw/xfree86/parser/Vendor.c \
$src_dir/hw/xfree86/parser/read.c \
$src_dir/hw/xfree86/parser/scan.c \
$src_dir/hw/xfree86/parser/write.c \
$src_dir/hw/xfree86/parser/DRI.c \
$src_dir/hw/xfree86/parser/Extensions.c \
"
mkdir -p $build_dir/hw/xfree86/parser
for f in $hw_xfree86_parser_c_files
do
	hw_xfree86_parser_obj=$build_dir/hw/xfree86/parser/$(basename $f .c).o
	hw_xfree86_parser_objs="$hw_xfree86_parser_objs $hw_xfree86_parser_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-DDATADIR=\"$datadir\" \
		-I$src_dir/hw/xfree86/parser \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_parser_obj &
done
#===================================================================================================
hw_xfree86_ramdac_c_files="\
$src_dir/hw/xfree86/ramdac/xf86CursorRD.c \
$src_dir/hw/xfree86/ramdac/xf86HWCurs.c \
"
mkdir -p $build_dir/hw/xfree86/ramdac
for f in $hw_xfree86_ramdac_c_files
do
	hw_xfree86_ramdac_obj=$build_dir/hw/xfree86/ramdac/$(basename $f .c).o
	hw_xfree86_ramdac_objs="$hw_xfree86_ramdac_objs $hw_xfree86_ramdac_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_ramdac_obj &
done
#===================================================================================================
hw_xfree86_vgahw_c_files="\
$src_dir/hw/xfree86/vgahw/vgaHW.c \
$src_dir/hw/xfree86/vgahw/vgaHWmodule.c \
"
mkdir -p $build_dir/hw/xfree86/vgahw
for f in $hw_xfree86_vgahw_c_files
do
	hw_xfree86_vgahw_obj=$build_dir/hw/xfree86/vgahw/$(basename $f .c).o
	hw_xfree86_vgahw_objs="$hw_xfree86_vgahw_objs $hw_xfree86_vgahw_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/os-support/bus \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/i2c \
		-I$src_dir/hw/xfree86/ddc \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_vgahw_obj &
done
#===================================================================================================
hw_xfree86_loader_c_files="\
$src_dir/hw/xfree86/loader/loader.c \
$src_dir/hw/xfree86/loader/loadmod.c \
"
mkdir -p $build_dir/hw/xfree86/loader
for f in $hw_xfree86_loader_c_files
do
	hw_xfree86_loader_obj=$build_dir/hw/xfree86/loader/$(basename $f .c).o
	hw_xfree86_loader_objs="$hw_xfree86_loader_objs $hw_xfree86_loader_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_loader_obj &
done
#===================================================================================================
hw_xfree86_modes_c_files="\
$src_dir/hw/xfree86/modes/xf86Crtc.c \
$src_dir/hw/xfree86/modes/xf86Cursors.c \
$src_dir/hw/xfree86/modes/xf86gtf.c \
$src_dir/hw/xfree86/modes/xf86EdidModes.c \
$src_dir/hw/xfree86/modes/xf86Modes.c \
$src_dir/hw/xfree86/modes/xf86RandR12.c \
$src_dir/hw/xfree86/modes/xf86Rotate.c \
"
mkdir -p $build_dir/hw/xfree86/modes
for f in $hw_xfree86_modes_c_files
do
	hw_xfree86_modes_obj=$build_dir/hw/xfree86/modes/$(basename $f .c).o
	hw_xfree86_modes_objs="$hw_xfree86_modes_objs $hw_xfree86_modes_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/ramdac \
		-I$src_dir/hw/xfree86/ddc \
		-I$src_dir/hw/xfree86/i2c \
		-I$src_dir/hw/xfree86/parser \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_modes_obj &
done
#===================================================================================================
hw_xfree86_dri_c_files="\
$src_dir/hw/xfree86/dri/dri.c \
$src_dir/hw/xfree86/dri/xf86dri.c \
"
mkdir -p $build_dir/hw/xfree86/dri
for f in $hw_xfree86_dri_c_files
do
	hw_xfree86_dri_obj=$build_dir/hw/xfree86/dri/$(basename $f .c).o
	hw_xfree86_dri_objs="$hw_xfree86_dri_objs $hw_xfree86_dri_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/i2c \
		-I$src_dir/hw/xfree86/ramdac \
		-I$src_dir/hw/xfree86/ddc \
		-I$src_dir/hw/xfree86/modes \
		-I$src_dir/hw/xfree86/parser \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_dri_obj &
done
#===================================================================================================
hw_xfree86_dri2_c_files="\
$src_dir/hw/xfree86/dri2/dri2.c \
$src_dir/hw/xfree86/dri2/dri2ext.c \
"
mkdir -p $build_dir/hw/xfree86/dri2
for f in $hw_xfree86_dri2_c_files
do
	hw_xfree86_dri2_obj=$build_dir/hw/xfree86/dri2/$(basename $f .c).o
	hw_xfree86_dri2_objs="$hw_xfree86_dri2_objs $hw_xfree86_dri2_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/dri2 \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_dri2_obj &
done
#===================================================================================================
hw_xfree86_dixmods_c_files="\
$src_dir/mi/miinitext.c \
"
mkdir -p $build_dir/hw/xfree86/dixmods
for f in $hw_xfree86_dixmods_c_files
do
	hw_xfree86_dixmods_obj=$build_dir/hw/xfree86/dixmods/$(basename $f .c).o
	hw_xfree86_dixmods_objs="$hw_xfree86_dixmods_objs $hw_xfree86_dixmods_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_dixmods_obj &
done
#===================================================================================================
hw_xfree86_i2c_c_files="\
$src_dir/hw/xfree86/i2c/xf86i2c.c \
"
mkdir -p $build_dir/hw/xfree86/i2c
for f in $hw_xfree86_i2c_c_files
do
	hw_xfree86_i2c_obj=$build_dir/hw/xfree86/i2c/$(basename $f .c).o
	hw_xfree86_i2c_objs="$hw_xfree86_i2c_objs $hw_xfree86_i2c_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_i2c_obj &
done
#===================================================================================================
hw_xfree86_xkb_c_files="\
$src_dir/hw/xfree86/xkb/xkbVT.c \
$src_dir/hw/xfree86/xkb/xkbPrivate.c \
$src_dir/hw/xfree86/xkb/xkbKillSrv.c \
"
mkdir -p $build_dir/hw/xfree86/xkb
for f in $hw_xfree86_xkb_c_files
do
	hw_xfree86_xkb_obj=$build_dir/hw/xfree86/xkb/$(basename $f .c).o
	hw_xfree86_xkb_objs="$hw_xfree86_xkb_objs $hw_xfree86_xkb_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_xkb_obj &
done
#===================================================================================================
hw_xfree86_dixmods_glxmodule_c_files="\
$src_dir/hw/xfree86/dixmods/glxmodule.c
"
mkdir -p $build_dir/hw/xfree86/dixmods
for f in $hw_xfree86_dixmods_glxmodule_c_files
do
	hw_xfree86_dixmods_glxmodule_obj=$build_dir/hw/xfree86/dixmods/$(basename $f .c).o
	hw_xfree86_dixmods_glxmodule_objs="$hw_xfree86_dixmods_glxmodule_objs $hw_xfree86_dixmods_glxmodule_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		-I$src_dir/glx \
		$f -o $hw_xfree86_dixmods_glxmodule_obj &
done
#===================================================================================================
hw_xfree86_dixmods_libwfb_c_files="\
$src_dir/hw/xfree86/dixmods/fbmodule.c
"
mkdir -p $build_dir/hw/xfree86/dixmods
for f in $hw_xfree86_dixmods_libwfb_c_files
do
	hw_xfree86_dixmods_libwfb_obj=$build_dir/hw/xfree86/dixmods/$(basename $f .c).o
	hw_xfree86_dixmods_libwfb_objs="$hw_xfree86_dixmods_libwfb_objs $hw_xfree86_dixmods_libwfb_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-DFB_ACCESS_WRAPPER \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_dixmods_libwfb_obj &
done
#===================================================================================================
hw_xfree86_dixmods_libshadow_c_files="\
$src_dir/hw/xfree86/dixmods/shmodule.c
"
mkdir -p $build_dir/hw/xfree86/dixmods
for f in $hw_xfree86_dixmods_libshadow_c_files
do
	hw_xfree86_dixmods_libshadow_obj=$build_dir/hw/xfree86/dixmods/$(basename $f .c).o
	hw_xfree86_dixmods_libshadow_objs="$hw_xfree86_dixmods_libshadow_objs $hw_xfree86_dixmods_libshadow_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_dixmods_libshadow_obj &
done
#===================================================================================================
hw_xfree86_fbdevhw_c_files="\
$src_dir/hw/xfree86/fbdevhw/fbdevhw.c
"
mkdir -p $build_dir/hw/xfree86/fbdevhw
for f in $hw_xfree86_fbdevhw_c_files
do
	hw_xfree86_fbdevhw_obj=$build_dir/hw/xfree86/fbdevhw/$(basename $f .c).o
	hw_xfree86_fbdevhw_objs="$hw_xfree86_fbdevhw_objs $hw_xfree86_fbdevhw_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/os-support/bus \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/parser \
		-I$src_dir/hw/xfree86/ddc \
		-I$src_dir/hw/xfree86/modes \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_fbdevhw_obj &
done
#===================================================================================================
hw_xfree86_shadowfb_libshadowfb_c_files="\
$src_dir/hw/xfree86/shadowfb/sfbmodule.c
$src_dir/hw/xfree86/shadowfb/shadowfb.c
"
mkdir -p $build_dir/hw/xfree86/shadowfb
for f in $hw_xfree86_shadowfb_libshadowfb_c_files
do
	hw_xfree86_shadowfb_libshadowfb_obj=$build_dir/hw/xfree86/shadowfb/$(basename $f .c).o
	hw_xfree86_shadowfb_libshadowfb_objs="$hw_xfree86_shadowfb_libshadowfb_objs $hw_xfree86_shadowfb_libshadowfb_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_shadowfb_libshadowfb_obj &
done
#===================================================================================================
hw_xfree86_exa_libexa_c_files="\
$src_dir/hw/xfree86/exa/examodule.c
"
mkdir -p $build_dir/hw/xfree86/exa
for f in $hw_xfree86_exa_libexa_c_files
do
	hw_xfree86_exa_libexa_obj=$build_dir/hw/xfree86/exa/$(basename $f .c).o
	hw_xfree86_exa_libexa_objs="$hw_xfree86_exa_libexa_objs $hw_xfree86_exa_libexa_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		-I$src_dir/exa \
		$f -o $hw_xfree86_exa_libexa_obj &
done
#===================================================================================================
glamor_libglamoregl_c_files="\
$src_dir/glamor/glamor_egl.c \
$src_dir/glamor/glamor_eglmodule.c \
$src_dir/hw/xfree86/glamor_egl/glamor_xf86_xv.c \
"
mkdir -p $build_dir/glamor
for f in $glamor_libglamoregl_c_files
do
	glamor_libglamoregl_obj=$build_dir/glamor/$(basename $f .c).o
	glamor_libglamoregl_objs="$glamor_libglamoregl_objs $glamor_libglamoregl_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		-I$src_dir/glamor \
		-I$src_dir/dri3 \
		$f -o $glamor_libglamoregl_obj &
done
#===================================================================================================
hw_xfree86_drivers_modesetting_c_files="\
$src_dir/hw/xfree86/drivers/modesetting/dri2.c \
$src_dir/hw/xfree86/drivers/modesetting/driver.c \
$src_dir/hw/xfree86/drivers/modesetting/drmmode_display.c \
$src_dir/hw/xfree86/drivers/modesetting/dumb_bo.c \
$src_dir/hw/xfree86/drivers/modesetting/present.c \
$src_dir/hw/xfree86/drivers/modesetting/vblank.c \
$src_dir/hw/xfree86/drivers/modesetting/pageflip.c \
"
mkdir -p $build_dir/hw/xfree86/drivers/modesetting
for f in $hw_xfree86_drivers_modesetting_c_files
do
	hw_xfree86_drivers_modesetting_obj=$build_dir/hw/xfree86/drivers/modesetting/$(basename $f .c).o
	hw_xfree86_drivers_modesetting_objs="$hw_xfree86_drivers_modesetting_objs $hw_xfree86_drivers_modesetting_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/dri2 \
		-I$src_dir/hw/xfree86/i2c \
		-I$src_dir/hw/xfree86/ramdac \
		-I$src_dir/hw/xfree86/parser \
		-I$src_dir/hw/xfree86/ddc \
		-I$src_dir/hw/xfree86/modes \
		-I$src_dir/hw/xfree86/os-support/bus \
		-I$src_dir/hw/xfree86/os-support \
		-I$src_dir/hw/xfree86/common \
		-I$src_dir/glamor \
		$f -o $hw_xfree86_drivers_modesetting_obj &
done
#===================================================================================================
hw_xfree86_drivers_inputtest_c_files="\
$src_dir/hw/xfree86/drivers/inputtest/xf86-input-inputtest.c \
"
mkdir -p $build_dir/hw/xfree86/drivers/inputtest
for f in $hw_xfree86_drivers_inputtest_c_files
do
	hw_xfree86_drivers_inputtest_obj=$build_dir/hw/xfree86/drivers/inputtest/$(basename $f .c).o
	hw_xfree86_drivers_inputtest_objs="$hw_xfree86_drivers_inputtest_objs $hw_xfree86_drivers_inputtest_obj"

	$cc	$cppflags \
		-DHAVE_XORG_CONFIG_H \
		-I$src_dir/hw/xfree86/common \
		$f -o $hw_xfree86_drivers_inputtest_obj &
done
####################################################################################################
####################################################################################################
####################################################################################################
wait
####################################################################################################
####################################################################################################
####################################################################################################
# building of archives for cherry picking during linking
$ar_rcs $build_dir/libxserver_glx.a $glx_objs &
$ar_rcs $build_dir/libglamor.a $glamor_objs &
wait
#===================================================================================================
# building of binaries
#===================================================================================================
(
mkdir -p $build_dir/install_root$bindir_store
mkdir -p $build_dir/install_root$bindir
$ccld \
	-o $build_dir/install_root$bindir_store/Xorg \
	\
	-Wl,--no-undefined \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	-Wl,--export-dynamic \
	\
	$dix_objs \
	$hw_xfree86_loader_objs \
	$hw_xfree86_common_objs \
	$config_objs \
	$hw_xfree86_os_support_bus_objs \
	$hw_xfree86_os_support_linux_objs \
	$hw_xfree86_os_support_misc_objs \
	$hw_xfree86_parser_objs \
	$hw_xfree86_dixmods_objs \
	$hw_xfree86_modes_objs \
	$hw_xfree86_ramdac_objs \
	$hw_xfree86_ddc_objs \
	$hw_xfree86_i2c_objs \
	$composite_objs \
	$xfixes_objs \
	$xext_objs \
	$dbe_objs \
	$record_objs \
	$randr_objs \
	$render_objs \
	$damageext_objs \
	$present_objs \
	$miext_damage_objs \
	$xi_objs \
	$xkb_objs \
	$hw_xfree86_xkb_objs \
	$hw_xfree86_dri_objs \
	$hw_xfree86_dri2_objs \
	$dri3_objs \
	$miext_sync_objs \
	$mi_objs \
	$os_objs \
	$xext_vidmod_objs \
	$fb_objs \
	$glxvnd_objs \
	\
	$pixman_ldflags \
	$libpciaccess_ldflags \
	$libxfont2_ldflags \
	$libdrm_ldflags \
	$libxshmfence_ldflags \
	$libudev_ldflags \
	$libxcvt_ldflags \
	$libxau_ldflags \
	$ssl_ldflags \
	-lm -ldl -lpthread \
	-Wl,--rpath-link=\
/nyan/libfontenc/current/lib:\
/nyan/freetype/current/lib:\
/nyan/libpng/current/lib

ln -sTf $bindir_store_virtual/Xorg $build_dir/install_root$bindir/Xorg
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath
mkdir -p $build_dir/install_root$module_store
module_name=libint10.so
$ccld \
	-o $build_dir/install_root$module_store/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$hw_xfree86_int10_objs \
	\
	$libpciaccess_ldflags

ln -sTf $module_store_virtual/$module_name $build_dir/install_root$defaultmodulepath/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath
mkdir -p $build_dir/install_root$module_store
module_name=libvgahw.so
$ccld \
	-o $build_dir/install_root$module_store/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$hw_xfree86_vgahw_objs \
	\
	$libpciaccess_ldflags

ln -sTf $module_store_virtual/$module_name $build_dir/install_root$defaultmodulepath/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath/extensions
mkdir -p $build_dir/install_root$module_store/extensions
module_name=libglx.so
$ccld \
	-o $build_dir/install_root$module_store/extensions/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$hw_xfree86_dixmods_glxmodule_objs \
	$glxdri_objs \
	-Wl,--whole-archive \
		$build_dir/libxserver_glx.a \
	-Wl,--no-whole-archive \
	$libglvnd_gl_ldflags \
	-ldl

ln -sTf $module_store_virtual/extensions/$module_name $build_dir/install_root$defaultmodulepath/extensions/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath
mkdir -p $build_dir/install_root$module_store
module_name=libwfb.so
$ccld \
	-o $build_dir/install_root$module_store/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$fbw_objs \
	\
	$hw_xfree86_dixmods_libwfb_objs \
	$pixman_ldflags

ln -sTf $module_store_virtual/$module_name $build_dir/install_root$defaultmodulepath/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath
mkdir -p $build_dir/install_root$module_store
module_name=libshadow.so
$ccld \
	-o $build_dir/install_root$module_store/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$miext_shadow_objs \
	\
	$hw_xfree86_dixmods_libshadow_objs

ln -sTf $module_store_virtual/$module_name $build_dir/install_root$defaultmodulepath/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath
mkdir -p $build_dir/install_root$module_store
module_name=libshadowfb.so
$ccld \
	-o $build_dir/install_root$module_store/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$hw_xfree86_shadowfb_libshadowfb_objs

ln -sTf $module_store_virtual/$module_name $build_dir/install_root$defaultmodulepath/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath
mkdir -p $build_dir/install_root$module_store
module_name=libexa.so
$ccld \
	-o $build_dir/install_root$module_store/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$exa_objs \
	\
	$hw_xfree86_exa_libexa_objs \
	\
	$pixman_ldflags

ln -sTf $module_store_virtual/$module_name $build_dir/install_root$defaultmodulepath/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath/drivers
mkdir -p $build_dir/install_root$module_store/drivers
module_name=modesetting_drv.so
$ccld \
	-o $build_dir/install_root$module_store/drivers/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$hw_xfree86_drivers_modesetting_objs \
	\
	$libudev_ldflags \
	$mesa_gbm_ldflags \
	$libdrm_ldflags

ln -sTf $module_store_virtual/drivers/$module_name $build_dir/install_root$defaultmodulepath/drivers/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath
mkdir -p $build_dir/install_root$module_store
module_name=libglamoregl.so
$ccld \
	-o $build_dir/install_root$module_store/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$glamor_libglamoregl_objs \
	\
	$build_dir/libglamor.a \
	$build_dir/libxserver_glx.a \
	\
	$libepoxy_ldflags \
	$mesa_gbm_ldflags \
	$libdrm_ldflags \
	$pixman_ldflags \
	-lm
ln -sTf $module_store_virtual/$module_name $build_dir/install_root$defaultmodulepath/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath
mkdir -p $build_dir/install_root$module_store
module_name=libfbdevhw.so
$ccld \
	-o $build_dir/install_root$module_store/$module_name \
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$hw_xfree86_fbdevhw_objs

ln -sTf $module_store_virtual/$module_name $build_dir/install_root$defaultmodulepath/$module_name
) &
#===================================================================================================
(
mkdir -p $build_dir/install_root$defaultmodulepath/input
mkdir -p $build_dir/install_root$module_store/input
module_name=inputtest_drv.so
$ccld \
	-o $build_dir/install_root$module_store/input/$module_name \
	\
	-shared \
	-Wl,-soname -Wl,$module_name \
	-static-libgcc \
	-ftls-model=global-dynamic -fpic -fPIC \
	-B/nyan/glibc/current/lib \
	-Wl,-s \
	\
	$hw_xfree86_drivers_inputtest_objs \
	\
	-lpthread

ln -sTf $module_store_virtual/input/$module_name $build_dir/install_root$defaultmodulepath/input/$module_name
) &
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
# installation finish
mkdir -p $build_dir/install_root$defaultlogdir &
#---------------------------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$datadir/X11/xorg.conf.d &
#---------------------------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$incdir_store/xorg
cp -f $src_dir/composite/compositeext.h		       				$build_dir/install_root$incdir_store/xorg/compositeext.h &
cp -f $src_dir/present/present.h                      				$build_dir/install_root$incdir_store/xorg/present.h &
cp -f $src_dir/present/presentext.h                   				$build_dir/install_root$incdir_store/xorg/presentext.h &
cp -f $src_dir/include/Xprintf.h                      				$build_dir/install_root$incdir_store/xorg/Xprintf.h &
cp -f $src_dir/include/callback.h                     				$build_dir/install_root$incdir_store/xorg/callback.h &
cp -f $src_dir/include/client.h                       				$build_dir/install_root$incdir_store/xorg/client.h &
cp -f $src_dir/include/closure.h                      				$build_dir/install_root$incdir_store/xorg/closure.h &
cp -f $src_dir/include/colormap.h                     				$build_dir/install_root$incdir_store/xorg/colormap.h &
cp -f $src_dir/include/colormapst.h                   				$build_dir/install_root$incdir_store/xorg/colormapst.h &
cp -f $src_dir/include/cursor.h                       				$build_dir/install_root$incdir_store/xorg/cursor.h &
cp -f $src_dir/include/cursorstr.h                    				$build_dir/install_root$incdir_store/xorg/cursorstr.h &
cp -f $src_dir/include/displaymode.h                  				$build_dir/install_root$incdir_store/xorg/displaymode.h &
cp -f $src_dir/include/dix.h                          				$build_dir/install_root$incdir_store/xorg/dix.h &
cp -f $src_dir/include/dixaccess.h                    				$build_dir/install_root$incdir_store/xorg/dixaccess.h &
cp -f $src_dir/include/dixevents.h                    				$build_dir/install_root$incdir_store/xorg/dixevents.h &
cp -f $src_dir/include/dixfont.h                      				$build_dir/install_root$incdir_store/xorg/dixfont.h &
cp -f $src_dir/include/dixfontstr.h                   				$build_dir/install_root$incdir_store/xorg/dixfontstr.h &
cp -f $src_dir/include/dixstruct.h                    				$build_dir/install_root$incdir_store/xorg/dixstruct.h &
cp -f $src_dir/include/exevents.h                     				$build_dir/install_root$incdir_store/xorg/exevents.h &
cp -f $src_dir/include/extension.h                    				$build_dir/install_root$incdir_store/xorg/extension.h &
cp -f $src_dir/include/extinit.h                      				$build_dir/install_root$incdir_store/xorg/extinit.h &
cp -f $src_dir/include/extnsionst.h                   				$build_dir/install_root$incdir_store/xorg/extnsionst.h &
cp -f $src_dir/include/events.h                       				$build_dir/install_root$incdir_store/xorg/events.h &
cp -f $src_dir/include/fourcc.h                       				$build_dir/install_root$incdir_store/xorg/fourcc.h &
cp -f $src_dir/include/gc.h                           				$build_dir/install_root$incdir_store/xorg/gc.h &
cp -f $src_dir/include/gcstruct.h                     				$build_dir/install_root$incdir_store/xorg/gcstruct.h &
cp -f $src_dir/include/globals.h                      				$build_dir/install_root$incdir_store/xorg/globals.h &
cp -f $src_dir/include/glx_extinit.h                  				$build_dir/install_root$incdir_store/xorg/glx_extinit.h &
cp -f $src_dir/include/glxvndabi.h                    				$build_dir/install_root$incdir_store/xorg/glxvndabi.h &
cp -f $src_dir/include/hotplug.h                      				$build_dir/install_root$incdir_store/xorg/hotplug.h &
cp -f $src_dir/include/input.h                        				$build_dir/install_root$incdir_store/xorg/input.h &
cp -f $src_dir/include/inputstr.h                     				$build_dir/install_root$incdir_store/xorg/inputstr.h &
cp -f $src_dir/include/list.h                         				$build_dir/install_root$incdir_store/xorg/list.h &
cp -f $src_dir/include/misc.h                         				$build_dir/install_root$incdir_store/xorg/misc.h &
cp -f $src_dir/include/miscstruct.h                   				$build_dir/install_root$incdir_store/xorg/miscstruct.h &
cp -f $src_dir/include/nonsdk_extinit.h               				$build_dir/install_root$incdir_store/xorg/nonsdk_extinit.h &
cp -f $src_dir/include/opaque.h                       				$build_dir/install_root$incdir_store/xorg/opaque.h &
cp -f $src_dir/include/optionstr.h                    				$build_dir/install_root$incdir_store/xorg/optionstr.h &
cp -f $src_dir/include/os.h                           				$build_dir/install_root$incdir_store/xorg/os.h &
cp -f $src_dir/include/pixmap.h                       				$build_dir/install_root$incdir_store/xorg/pixmap.h &
cp -f $src_dir/include/pixmapstr.h                    				$build_dir/install_root$incdir_store/xorg/pixmapstr.h &
cp -f $src_dir/include/privates.h                     				$build_dir/install_root$incdir_store/xorg/privates.h &
cp -f $src_dir/include/property.h                     				$build_dir/install_root$incdir_store/xorg/property.h &
cp -f $src_dir/include/propertyst.h                   				$build_dir/install_root$incdir_store/xorg/propertyst.h &
cp -f $src_dir/include/ptrveloc.h                     				$build_dir/install_root$incdir_store/xorg/ptrveloc.h &
cp -f $src_dir/include/region.h                       				$build_dir/install_root$incdir_store/xorg/region.h &
cp -f $src_dir/include/regionstr.h                    				$build_dir/install_root$incdir_store/xorg/regionstr.h &
cp -f $src_dir/include/resource.h                     				$build_dir/install_root$incdir_store/xorg/resource.h &
cp -f $src_dir/include/rgb.h                          				$build_dir/install_root$incdir_store/xorg/rgb.h &
cp -f $src_dir/include/screenint.h                    				$build_dir/install_root$incdir_store/xorg/screenint.h &
cp -f $src_dir/include/scrnintstr.h                   				$build_dir/install_root$incdir_store/xorg/scrnintstr.h &
cp -f $src_dir/include/selection.h                    				$build_dir/install_root$incdir_store/xorg/selection.h &
cp -f $src_dir/include/servermd.h                     				$build_dir/install_root$incdir_store/xorg/servermd.h &
cp -f $src_dir/include/validate.h                     				$build_dir/install_root$incdir_store/xorg/validate.h &
cp -f $src_dir/include/window.h                       				$build_dir/install_root$incdir_store/xorg/window.h &
cp -f $src_dir/include/windowstr.h                    				$build_dir/install_root$incdir_store/xorg/windowstr.h &
cp -f $src_dir/include/xkbrules.h                     				$build_dir/install_root$incdir_store/xorg/xkbrules.h &
cp -f $src_dir/include/xkbsrv.h                       				$build_dir/install_root$incdir_store/xorg/xkbsrv.h &
cp -f $src_dir/include/xkbstr.h                       				$build_dir/install_root$incdir_store/xorg/xkbstr.h &
cp -f $src_dir/include/xserver-properties.h           				$build_dir/install_root$incdir_store/xorg/xserver-properties.h &
cp -f $src_dir/miext/damage/damage.h                       			$build_dir/install_root$incdir_store/xorg/damage.h &
cp -f $src_dir/miext/damage/damagestr.h                    			$build_dir/install_root$incdir_store/xorg/damagestr.h &
cp -f $src_dir/miext/shadow/shadow.h                       			$build_dir/install_root$incdir_store/xorg/shadow.h &
cp -f $src_dir/miext/sync/misync.h                       			$build_dir/install_root$incdir_store/xorg/misync.h &
cp -f $src_dir/miext/sync/misyncfd.h                     			$build_dir/install_root$incdir_store/xorg/misyncfd.h &
cp -f $src_dir/miext/sync/misyncshm.h                    			$build_dir/install_root$incdir_store/xorg/misyncshm.h &
cp -f $src_dir/miext/sync/misyncstr.h                    			$build_dir/install_root$incdir_store/xorg/misyncstr.h &
cp -f $src_dir/dbe/dbestruct.h                    				$build_dir/install_root$incdir_store/xorg/dbestruct.h &
cp -f $src_dir/dri3/dri3.h                         				$build_dir/install_root$incdir_store/xorg/dri3.h &
cp -f $src_dir/exa/exa.h                          				$build_dir/install_root$incdir_store/xorg/exa.h &
cp -f $src_dir/fb/fb.h                           				$build_dir/install_root$incdir_store/xorg/fb.h &
cp -f $src_dir/fb/fboverlay.h                    				$build_dir/install_root$incdir_store/xorg/fboverlay.h &
cp -f $src_dir/fb/fbpict.h                       				$build_dir/install_root$incdir_store/xorg/fbpict.h &
cp -f $src_dir/fb/fbrop.h                        				$build_dir/install_root$incdir_store/xorg/fbrop.h &
cp -f $src_dir/fb/wfbrename.h                    				$build_dir/install_root$incdir_store/xorg/wfbrename.h &
cp -f $src_dir/Xext/geext.h                        				$build_dir/install_root$incdir_store/xorg/geext.h &
cp -f $src_dir/Xext/geint.h                        				$build_dir/install_root$incdir_store/xorg/geint.h &
cp -f $src_dir/Xext/panoramiX.h                    				$build_dir/install_root$incdir_store/xorg/panoramiX.h &
cp -f $src_dir/Xext/panoramiXsrv.h                 				$build_dir/install_root$incdir_store/xorg/panoramiXsrv.h &
cp -f $src_dir/Xext/shmint.h                       				$build_dir/install_root$incdir_store/xorg/shmint.h &
cp -f $src_dir/Xext/syncsdk.h                      				$build_dir/install_root$incdir_store/xorg/syncsdk.h &
cp -f $src_dir/Xext/xace.h                         				$build_dir/install_root$incdir_store/xorg/xace.h &
cp -f $src_dir/Xext/xacestr.h                      				$build_dir/install_root$incdir_store/xorg/xacestr.h &
cp -f $src_dir/Xext/xvdix.h                        				$build_dir/install_root$incdir_store/xorg/xvdix.h &
cp -f $src_dir/Xext/xvmcext.h                      				$build_dir/install_root$incdir_store/xorg/xvmcext.h &
cp -f $src_dir/glamor/glamor.h                       				$build_dir/install_root$incdir_store/xorg/glamor.h &
cp -f $src_dir/render/glyphstr.h                     				$build_dir/install_root$incdir_store/xorg/glyphstr.h &
cp -f $src_dir/render/mipict.h                       				$build_dir/install_root$incdir_store/xorg/mipict.h &
cp -f $src_dir/render/picture.h                      				$build_dir/install_root$incdir_store/xorg/picture.h &
cp -f $src_dir/render/picturestr.h                   				$build_dir/install_root$incdir_store/xorg/picturestr.h &
cp -f $src_dir/mi/mi.h                           				$build_dir/install_root$incdir_store/xorg/mi.h &
cp -f $src_dir/mi/micmap.h                       				$build_dir/install_root$incdir_store/xorg/micmap.h &
cp -f $src_dir/mi/micoord.h                      				$build_dir/install_root$incdir_store/xorg/micoord.h &
cp -f $src_dir/mi/migc.h                         				$build_dir/install_root$incdir_store/xorg/migc.h &
cp -f $src_dir/mi/miline.h                       				$build_dir/install_root$incdir_store/xorg/miline.h &
cp -f $src_dir/mi/mipointer.h                    				$build_dir/install_root$incdir_store/xorg/mipointer.h &
cp -f $src_dir/mi/mipointrst.h                   				$build_dir/install_root$incdir_store/xorg/mipointrst.h &
cp -f $src_dir/mi/mistruct.h                     				$build_dir/install_root$incdir_store/xorg/mistruct.h &
cp -f $src_dir/mi/mizerarc.h                     				$build_dir/install_root$incdir_store/xorg/mizerarc.h &
cp -f $src_dir/randr/randrstr.h                     				$build_dir/install_root$incdir_store/xorg/randrstr.h &
cp -f $src_dir/randr/rrtransform.h                  				$build_dir/install_root$incdir_store/xorg/rrtransform.h &
cp -f $src_dir/glx/vndserver.h                    				$build_dir/install_root$incdir_store/xorg/vndserver.h &
cp -f $src_dir/hw/xfree86/common/compiler.h	    				$build_dir/install_root$incdir_store/xorg/compiler.h &
cp -f $src_dir/hw/xfree86/common/xaarop.h                       		$build_dir/install_root$incdir_store/xorg/xaarop.h &
cp -f $src_dir/hw/xfree86/common/xf86.h                         		$build_dir/install_root$incdir_store/xorg/xf86.h &
cp -f $src_dir/hw/xfree86/common/xf86MatchDrivers.h             		$build_dir/install_root$incdir_store/xorg/xf86MatchDrivers.h &
cp -f $src_dir/hw/xfree86/common/xf86Module.h                   		$build_dir/install_root$incdir_store/xorg/xf86Module.h &
cp -f $src_dir/hw/xfree86/common/xf86Opt.h                      		$build_dir/install_root$incdir_store/xorg/xf86Opt.h &
cp -f $src_dir/hw/xfree86/common/xf86Optionstr.h                		$build_dir/install_root$incdir_store/xorg/xf86Optionstr.h &
cp -f $src_dir/hw/xfree86/common/xf86PciInfo.h                  		$build_dir/install_root$incdir_store/xorg/xf86PciInfo.h &
cp -f $src_dir/hw/xfree86/common/xf86Priv.h                     		$build_dir/install_root$incdir_store/xorg/xf86Priv.h &
cp -f $src_dir/hw/xfree86/common/xf86Privstr.h                  		$build_dir/install_root$incdir_store/xorg/xf86Privstr.h &
cp -f $src_dir/hw/xfree86/common/xf86VGAarbiter.h               		$build_dir/install_root$incdir_store/xorg/xf86VGAarbiter.h &
cp -f $src_dir/hw/xfree86/common/xf86Xinput.h                   		$build_dir/install_root$incdir_store/xorg/xf86Xinput.h &
cp -f $src_dir/hw/xfree86/common/xf86cmap.h                     		$build_dir/install_root$incdir_store/xorg/xf86cmap.h &
cp -f $src_dir/hw/xfree86/common/xf86fbman.h                    		$build_dir/install_root$incdir_store/xorg/xf86fbman.h &
cp -f $src_dir/hw/xfree86/common/xf86platformBus.h              		$build_dir/install_root$incdir_store/xorg/xf86platformBus.h &
cp -f $src_dir/hw/xfree86/common/xf86sbusBus.h                  		$build_dir/install_root$incdir_store/xorg/xf86sbusBus.h &
cp -f $src_dir/hw/xfree86/common/xf86str.h                      		$build_dir/install_root$incdir_store/xorg/xf86str.h &
cp -f $src_dir/hw/xfree86/common/xf86xv.h                       		$build_dir/install_root$incdir_store/xorg/xf86xv.h &
cp -f $src_dir/hw/xfree86/common/xf86xvmc.h                     		$build_dir/install_root$incdir_store/xorg/xf86xvmc.h &
cp -f $src_dir/hw/xfree86/common/xf86xvpriv.h                   		$build_dir/install_root$incdir_store/xorg/xf86xvpriv.h &
cp -f $src_dir/hw/xfree86/common/xisb.h                         		$build_dir/install_root$incdir_store/xorg/xisb.h &
cp -f $src_dir/hw/xfree86/common/xorgVersion.h                  		$build_dir/install_root$incdir_store/xorg/xorgVersion.h &
cp -f $src_dir/hw/xfree86/ddc/edid.h                   				$build_dir/install_root$incdir_store/xorg/edid.h &
cp -f $src_dir/hw/xfree86/ddc/xf86DDC.h		                      		$build_dir/install_root$incdir_store/xorg/xf86DDC.h &
cp -f $src_dir/hw/xfree86/dri/dri.h                   				$build_dir/install_root$incdir_store/xorg/dri.h &
cp -f $src_dir/hw/xfree86/dri/dristruct.h             				$build_dir/install_root$incdir_store/xorg/dristruct.h &
cp -f $src_dir/hw/xfree86/dri/sarea.h                  				$build_dir/install_root$incdir_store/xorg/sarea.h &
cp -f $src_dir/hw/xfree86/dri2/dri2.h                  				$build_dir/install_root$incdir_store/xorg/dri2.h &
cp -f $src_dir/hw/xfree86/drivers/inputtest/xf86-input-inputtest-protocol.h	$build_dir/install_root$incdir_store/xorg/xf86-input-inputtest-protocol.h &
cp -f $src_dir/hw/xfree86/fbdevhw/fbdevhw.h                      		$build_dir/install_root$incdir_store/xorg/fbdevhw.h &
cp -f $src_dir/hw/xfree86/i2c/i2c_def.h                				$build_dir/install_root$incdir_store/xorg/i2c_def.h &
cp -f $src_dir/hw/xfree86/i2c/xf86i2c.h		                      		$build_dir/install_root$incdir_store/xorg/xf86i2c.h &
cp -f $src_dir/hw/xfree86/int10/vbe.h                  				$build_dir/install_root$incdir_store/xorg/vbe.h &
cp -f $src_dir/hw/xfree86/int10/vbeModes.h                     			$build_dir/install_root$incdir_store/xorg/vbeModes.h &
cp -f $src_dir/hw/xfree86/int10/xf86int10.h 	                   		$build_dir/install_root$incdir_store/xorg/xf86int10.h &
cp -f $src_dir/hw/xfree86/modes/xf86Crtc.h  	                   		$build_dir/install_root$incdir_store/xorg/xf86Crtc.h &
cp -f $src_dir/hw/xfree86/modes/xf86Modes.h                    			$build_dir/install_root$incdir_store/xorg/xf86Modes.h &
cp -f $src_dir/hw/xfree86/modes/xf86RandR12.h 	                 		$build_dir/install_root$incdir_store/xorg/xf86RandR12.h &
cp -f $src_dir/hw/xfree86/os-support/xf86_OSlib.h                   		$build_dir/install_root$incdir_store/xorg/xf86_OSlib.h &
cp -f $src_dir/hw/xfree86/os-support/xf86_OSproc.h                  		$build_dir/install_root$incdir_store/xorg/xf86_OSproc.h &
cp -f $src_dir/hw/xfree86/os-support/bus/xf86Pci.h                     		$build_dir/install_root$incdir_store/xorg/xf86Pci.h &
cp -f $src_dir/hw/xfree86/parser/xf86Optrec.h                   		$build_dir/install_root$incdir_store/xorg/xf86Optrec.h &
cp -f $src_dir/hw/xfree86/parser/xf86Parser.h                   		$build_dir/install_root$incdir_store/xorg/xf86Parser.h &
cp -f $src_dir/hw/xfree86/ramdac/xf86Cursor.h                   		$build_dir/install_root$incdir_store/xorg/xf86Cursor.h &
cp -f $src_dir/hw/xfree86/shadowfb/shadowfb.h          				$build_dir/install_root$incdir_store/xorg/shadowfb.h &
cp -f $src_dir/hw/xfree86/vgahw/vgaHW.h                				$build_dir/install_root$incdir_store/xorg/vgaHW.h &
#---------------------------------------------------------------------------------------------------
sed	-e "s:@defaultfontdir@:$defaultfontdir:g" \
	-e "s:@version_0@:$version_0:g" \
	-e "s:@version_1@:$version_1:g" \
	-e "s:@version_2@:$version_2:g" \
	<$src_dir/contrib/xorg-server.h.in \
	>$build_dir/install_root$incdir_store/xorg/xorg-server.h &
#---------------------------------------------------------------------------------------------------
#TODO: to remove
mkdir -p $build_dir/install_root$defaultlibrarypath/pkgconfig
sed	-e "s:@projectroot@:$projectroot:g" \
	-e "s:@defaultmodulepath@:$defaultmodulepath:g" \
	-e "s:@sysconfigdir@:$datadir/X11/xorg.conf.d:g" \
	-e "s:@version_0@:$version_0:g" \
	-e "s:@version_1@:$version_1:g" \
	-e "s:@version_2@:$version_2:g" \
	-e "s:@version_3@:$version_3:g" \
	<$src_dir/contrib/xorg-server.pc.in \
	>$build_dir/install_root$defaultlibrarypath/pkgconfig/xorg-server.pc &
#---------------------------------------------------------------------------------------------------
# INFO: our configured version of xorg does not use the registry hence no 'protocol.txt'
wait
