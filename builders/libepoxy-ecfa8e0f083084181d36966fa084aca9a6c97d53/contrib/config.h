/* hardcoded copy of config.h */

#define ENABLE_EGL 1

#define ENABLE_GLX 1

#define EPOXY_PUBLIC __attribute__((visibility("default"))) extern

#define HAVE_KHRPLATFORM_H

#define PACKAGE_DATADIR "share"

#define PACKAGE_LIBDIR "lib"

#define PACKAGE_LIBEXECDIR "libexec"

#define PACKAGE_LOCALEDIR "share/locale"

#define PACKAGE_NAME "libepoxy"

#define PACKAGE_STRING "libepoxy-1.5.3"

#define PACKAGE_VERSION "1.5.3"

