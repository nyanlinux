echo "Running AMD GPU code generators-->"
#===============================================================================
# common
mkdir $build_dir/src
mkdir $build_dir/src/amd
mkdir $build_dir/src/amd/common

$python3 \
$src_dir/src/amd/common/sid_tables.py \
$src_dir/src/amd/common/sid.h \
$src_dir/src/amd/registers/gfx6.json \
$src_dir/src/amd/registers/gfx7.json \
$src_dir/src/amd/registers/gfx8.json \
$src_dir/src/amd/registers/gfx81.json \
$src_dir/src/amd/registers/gfx9.json \
$src_dir/src/amd/registers/gfx940.json \
$src_dir/src/amd/registers/gfx10.json \
$src_dir/src/amd/registers/gfx10-rsrc.json \
$src_dir/src/amd/registers/gfx11.json \
$src_dir/src/amd/registers/gfx115.json \
$src_dir/src/amd/registers/gfx11-rsrc.json \
$src_dir/src/amd/registers/gfx12.json \
$src_dir/src/amd/registers/gfx12-rsrc.json \
$src_dir/src/amd/registers/gfx103.json \
$src_dir/src/amd/registers/pkt3.json \
$src_dir/src/amd/registers/registers-manually-defined.json \
>$build_dir/src/amd/common/sid_tables.h &

$python3 \
$src_dir/src/amd/registers/makeregheader.py \
$src_dir/src/amd/registers/gfx6.json \
$src_dir/src/amd/registers/gfx7.json \
$src_dir/src/amd/registers/gfx8.json \
$src_dir/src/amd/registers/gfx81.json \
$src_dir/src/amd/registers/gfx9.json \
$src_dir/src/amd/registers/gfx940.json \
$src_dir/src/amd/registers/gfx10.json \
$src_dir/src/amd/registers/gfx10-rsrc.json \
$src_dir/src/amd/registers/gfx103.json \
$src_dir/src/amd/registers/gfx11.json \
$src_dir/src/amd/registers/gfx115.json \
$src_dir/src/amd/registers/gfx11-rsrc.json \
$src_dir/src/amd/registers/gfx12.json \
$src_dir/src/amd/registers/gfx12-rsrc.json \
$src_dir/src/amd/registers/pkt3.json \
$src_dir/src/amd/registers/registers-manually-defined.json \
--sort address \
--guard AMDGFXREGS_H \
>$build_dir/src/amd/common/amdgfxregs.h &
#===============================================================================
# aco
mkdir $build_dir/src/amd/compiler

export PYTHONPATH=$mako
$python3 $src_dir/src/amd/compiler/aco_opcodes_h.py \
	>$build_dir/src/amd/compiler/aco_opcodes.h &
$python3 $src_dir/src/amd/compiler/aco_opcodes_cpp.py \
	>$build_dir/src/amd/compiler/aco_opcodes.cpp &
$python3 $src_dir/src/amd/compiler/aco_builder_h.py \
	>$build_dir/src/amd/compiler/aco_builder.h &
unset PYTHONPATH
#===============================================================================
# gallium driver
mkdir $build_dir/src/gallium
mkdir $build_dir/src/gallium/drivers
mkdir $build_dir/src/gallium/drivers/radeonsi

export PYTHONPATH=$mako:$yaml
$python3 $src_dir/src/amd/common/gfx10_format_table.py \
$src_dir/src/util/format/u_format.yaml \
$src_dir/src/amd/registers/gfx10-rsrc.json \
$src_dir/src/amd/registers/gfx11-rsrc.json \
>$build_dir/src/gallium/drivers/radeonsi/gfx10_format_table.c &
unset PYTHONPATH
#-------------------------------------------------------------------------------
# do remove the radeonsi pipe loader fallback from (amdgpu/drm winsys) to
# (radeon/drm winsys)
cp $src_dir/src/gallium/drivers/radeonsi/si_pipe.c $build_dir/src/gallium/drivers/radeonsi/
patch -i $script_dir/si_pipe.c.patch $build_dir/src/gallium/drivers/radeonsi/si_pipe.c

# uvd block is crap, avoid any non-critical code related to it
cp $script_dir/si_uvd.c $build_dir/src/gallium/drivers/radeonsi/si_uvd.c

# not [generation&compile]-able out, trash work, again
cp $script_dir/si_tracepoints.h $build_dir/src/gallium/drivers/radeonsi
cp $script_dir/si_utrace.h $build_dir/src/gallium/drivers/radeonsi
#===============================================================================
echo "<--AMD GPU code generation done"
