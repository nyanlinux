src_name=nyanblkid
git_commit=7af80f73e523cb74ad26b31f63c7176a430d5105
git_url0=git://github.com/sylware/$src_name

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

cd $pkg_dir

git checkout --force $git_commit
git reset --hard

build_dir=$builds_dir_root/$pkg_name-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

# let pkg-config properly configure the use of the cross-compiled libuuid
export PKG_CONFIG_LIBDIR=$target_sysroot/nyan/nyanuuid/0/lib/pkgconfig
export PKG_CONFIG_SYSROOT_DIR=$target_sysroot

# we cheat, we cross-build a dynamic binary using the static lib
# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
$pkg_dir/make																								\
	--prefix=/nyan/nyanblkid/0																					\
	--disable-dynamic																						\
	"--bin-cc=$target_gnu_triple-gcc -O2 -pipe -fPIC -c"																		\
	"--bin-ccld=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"	\
	"--libblkid-cc=$target_gnu_triple-gcc -O2 -pipe -fPIC -c"																	\
	"--libblkid-ar=$target_gnu_triple-ar rcs"

cp -r $build_dir/fake_root/* $target_sysroot

# cleanup and tidying
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/bin/blkid

rm -Rf $build_dir $pkg_dir
export PATH=$OLD_PATH
