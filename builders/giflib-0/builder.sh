src_name=giflib
version=5.1.4
archive_name=$src_name-$version.tar.bz2
url0=https://sourceforge.net/projects/$src_name/files/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export 'CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc'
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--prefix=/nyan/giflib/0		\
	--disable-shared		\
	--enable-static
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup and tidying
rm -f /nyan/$src_name/0/lib/*.la
strip -s /nyan/$src_name/0/bin/* || true

rm -Rf $build_dir $src_dir
