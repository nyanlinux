src_name=libXft
mkdir /nyan/$src_name
version=${pkg_name##*-}
slot=$version
mkdir /nyan/$src_name/$slot

archive_name=$src_name-$version.tar.xz
url0=http://xorg.freedesktop.org/releases/individual/lib/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
$PATH\
"

# install our build system
cp -r $nyan_root/builders/$pkg_name/contrib $pkg_dir

$pkg_dir/contrib/binutils-gcc-glibc.sh

rm -Rf /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/include
mkdir /nyan/$src_name/$slot/include/X11
mkdir /nyan/$src_name/$slot/include/X11/Xft
cp -f $pkg_dir/include/X11/Xft/Xft.h /nyan/$src_name/$slot/include/X11/Xft/Xft.h
cp -f $pkg_dir/include/X11/Xft/XftCompat.h /nyan/$src_name/$slot/include/X11/Xft/XftCompat.h

rm -Rf /nyan/$src_name/$slot/lib
mkdir /nyan/$src_name/$slot/lib
cp -f $build_dir/libXft.so.2.3.8 /nyan/$src_name/$slot/lib/libXft.so.2.3.8
ln -s libXft.so.2.3.8 /nyan/$src_name/$slot/lib/libXft.so
ln -s libXft.so.2.3.8 /nyan/$src_name/$slot/lib/libXft.so.2

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
