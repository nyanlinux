#!/bin/sh
# TODO: may have to move the patch version to the minor version place-holder
# to please that thing which is pressure-vessel and similars... god...
# in a fair world, namely with the same amount of resources than upstream:
# should have scripts to build everything and specialized others, like this one
# which is specialized for amdgpu
# libdrm_radeon is for legacy mesa GL.
#===============================================================================
# build dir and src dir
build_dir=$(readlink -f .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(readlink -f $(dirname $0)/..)
echo "src_dir=$src_dir"
#===============================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===============================================================================
# directory configuration
#-------------------------------------------------------------------------------
if test "${runtime_data_dir-unset}" = unset; then
runtime_data_dir='/usr/share'
fi
if test "${runtime_data_store_dir-unset}" = unset; then
runtime_data_store_dir='/usr/store/libdrm/git/share'
fi
if test "${runtime_data_store_virtual_dir-unset}" = unset; then
runtime_data_store_virtual_dir='/usr/store/libdrm/current/share'
fi
#-------------------------------------------------------------------------------
if test "${lib_store_dir-unset}" = unset; then
lib_store_dir='/usr/store/libdrm/git/lib'
fi
if test "${lib_store_virtual_dir-unset}" = unset; then
lib_store_virtual_dir='/usr/store/libdrm/current/lib'
fi
#-------------------------------------------------------------------------------
if test "${runtime_lib_dir-unset}" = unset; then
runtime_lib_dir='/usr/lib'
fi
#-------------------------------------------------------------------------------
if test "${inc_store_dir-unset}" = unset; then
inc_store_dir='/usr/store/libdrm/git/include'
fi
if test "${inc_store_virtual_dir-unset}" = unset; then
inc_store_virtual_dir='/usr/store/libdrm/current/include'
fi
#===============================================================================
gcc_cpp_flags="\
-DHAVE_VISIBILITY \
-DHAVE_LIBDRM_ATOMIC_PRIMITIVES"

if test "${slib_gcc-unset}" = unset; then
slib_gcc='gcc -pipe -fPIC -O2 -c -static-libgcc -fvisibility=hidden -ftls-model=global-dynamic -fpic'
fi
#===============================================================================
glibc_cpp_flags="\
-DMAJOR_IN_SYSMACROS \
-D_GNU_SOURCE \
"
#===============================================================================
# some inappropriate code gen generator, should be simple and plain C coded
# table generators, in the worst case.
# updated
python3 $src_dir/gen_table_fourcc.py \
	$src_dir/include/drm/drm_fourcc.h \
	$build_dir/generated_static_table_fourcc.h
#===============================================================================
libdrm_so_major=2
libdrm_so_minor=4
libdrm_so_patch=122

libdrm_c_src_files="\
$src_dir/xf86drm.c \
$src_dir/xf86drmHash.c \
$src_dir/xf86drmRandom.c \
$src_dir/xf86drmSL.c \
$src_dir/xf86drmMode.c \
"

libdrm_cpp_flags="\
-I$build_dir \
-I$src_dir/include/drm \
-I$src_dir \
$gcc_cpp_flags \
$glibc_cpp_flags"
#-------------------------------------------------------------------------------
for f in $libdrm_c_src_files
do
	libdrm_c_obj=$build_dir/$(basename $f .c).o 
	libdrm_so="$libdrm_so $libdrm_c_obj"

	printf "SLIB_GCC $f\n"
	eval $slib_gcc $libdrm_cpp_flags -o $libdrm_c_obj $f &
done
#-------------------------------------------------------------------------------
wait

mkdir -p $build_dir/install_root$lib_store_dir
mkdir -p $build_dir/install_root$runtime_lib_dir

if test "${libdrm_slib_gccld-unset}" = unset; then
libdrm_slib_gccld="gcc \
-o $build_dir/install_root$lib_store_dir/libdrm.so.$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch \
-Wl,-soname=libdrm.so.$libdrm_so_major \
-pipe -shared -static-libgcc \
-Wl,--no-undefined,--gc-sections \
-Wl,-s \
$libdrm_so \
-lm"
fi

printf "SLIB_GCCLD libdrm.so.$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch\n"
eval $libdrm_slib_gccld &
ln -sTf $lib_store_virtual_dir/libdrm.so.$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch \
	$build_dir/install_root$runtime_lib_dir/libdrm.so.$libdrm_so_major
ln -sTf libdrm.so.$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch  $build_dir/install_root$lib_store_dir/libdrm.so
#-------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$inc_store_dir
cp $src_dir/libsync.h $src_dir/xf86drm.h $src_dir/xf86drmMode.h \
$build_dir/install_root$inc_store_dir &

mkdir -p $build_dir/install_root$inc_store_dir/libdrm
cp $src_dir/include/drm/drm.h $src_dir/include/drm/drm_fourcc.h \
$src_dir/include/drm/drm_mode.h $src_dir/include/drm/drm_sarea.h \
$src_dir/include/drm/i915_drm.h $src_dir/include/drm/mach64_drm.h \
$src_dir/include/drm/mga_drm.h $src_dir/include/drm/msm_drm.h \
$src_dir/include/drm/nouveau_drm.h $src_dir/include/drm/qxl_drm.h \
$src_dir/include/drm/r128_drm.h $src_dir/include/drm/radeon_drm.h \
$src_dir/include/drm/amdgpu_drm.h $src_dir/include/drm/savage_drm.h \
$build_dir/install_root$inc_store_dir/libdrm &
#-------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$lib_store_dir/pkgconfig
cp $src_dir/contrib/pkgconfig/libdrm.pc.in \
$build_dir/install_root$lib_store_dir/pkgconfig/libdrm.pc

sed -i "\
s:@libdir@:$lib_store_virtual_dir:;\
s:@incdir@:$inc_store_virtual_dir:;\
s:@PACKAGE_VERSION@:$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch:;\
" $build_dir/install_root$lib_store_dir/pkgconfig/libdrm.pc &
#===============================================================================
libdrm_amdgpu_so_major=1
libdrm_amdgpu_so_minor=0
libdrm_amdgpu_so_patch=0

libdrm_amdgpu_c_src_files="\
$src_dir/amdgpu/amdgpu_asic_id.c \
$src_dir/amdgpu/amdgpu_bo.c \
$src_dir/amdgpu/amdgpu_cs.c \
$src_dir/amdgpu/amdgpu_device.c \
$src_dir/amdgpu/amdgpu_gpu_info.c \
$src_dir/amdgpu/amdgpu_vamgr.c \
$src_dir/amdgpu/amdgpu_vm.c \
$src_dir/amdgpu/handle_table.c \
"

libdrm_amdgpu_cpp_flags="\
-DAMDGPU_ASIC_ID_TABLE=\\\"$runtime_data_dir/libdrm/amdgpu.ids\\\" \
-I$build_dir \
-I$src_dir/include/drm \
-I$src_dir \
$gcc_cpp_flags \
$glibc_cpp_flags"
#-------------------------------------------------------------------------------
for f in $libdrm_amdgpu_c_src_files
do
	libdrm_amdgpu_c_obj=$build_dir/$(basename $f .c).o 
	libdrm_amdgpu_so="$libdrm_amdgpu_so $libdrm_amdgpu_c_obj"

	printf "SLIB_GCC $f\n"
	eval $slib_gcc $libdrm_amdgpu_cpp_flags -o $libdrm_amdgpu_c_obj $f &
done
#-------------------------------------------------------------------------------
wait

mkdir -p $build_dir/install_root$lib_store_dir
mkdir -p $build_dir/install_root$runtime_lib_dir

if test "${libdrm_amdgpu_slib_gccld-unset}" = unset; then
libdrm_amdgpu_slib_gccld="gcc \
-o $build_dir/install_root$lib_store_dir/libdrm_amdgpu.so.$libdrm_amdgpu_so_major.$libdrm_amdgpu_so_minor.$libdrm_amdgpu_so_patch \
-Wl,-soname=libdrm_amdgpu.so.$libdrm_amdgpu_so_major \
-pipe -shared -static-libgcc \
-Wl,--no-undefined,--gc-sections \
-Wl,-s \
$libdrm_amdgpu_so \
$build_dir/install_root$lib_store_dir/libdrm.so.$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch"
fi

printf "SLIB_GCCLD libdrm_amdgpu.so.$libdrm_amdgpu_so_major.$libdrm_amdgpu_so_minor.$libdrm_amdgpu_so_patch\n"
eval $libdrm_amdgpu_slib_gccld &
ln -sTf $lib_store_virtual_dir/libdrm_amdgpu.so.$libdrm_amdgpu_so_major.$libdrm_amdgpu_so_minor.$libdrm_amdgpu_so_patch \
	$build_dir/install_root$runtime_lib_dir/libdrm_amdgpu.so.$libdrm_amdgpu_so_major
ln -sTf libdrm_amdgpu.so.$libdrm_amdgpu_so_major.$libdrm_amdgpu_so_minor.$libdrm_amdgpu_so_patch \
	$build_dir/install_root$lib_store_dir/libdrm_amdgpu.so
#-------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$runtime_data_store_dir/libdrm
cp $src_dir/data/amdgpu.ids $build_dir/install_root$runtime_data_store_dir/libdrm &
mkdir -p $build_dir/install_root$runtime_data_dir/libdrm
ln -sTf $runtime_data_store_virtual_dir/libdrm/amdgpu.ids \
	$build_dir/install_root$runtime_data_dir/libdrm/amdgpu.ids
#-------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$inc_store_dir/libdrm
cp $src_dir/amdgpu/amdgpu.h $build_dir/install_root$inc_store_dir/libdrm &
#-------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$lib_store_dir/pkgconfig
cp $src_dir/contrib/pkgconfig/libdrm_amdgpu.pc.in \
$build_dir/install_root$lib_store_dir/pkgconfig/libdrm_amdgpu.pc

# the PACKAGE_VERSION is libdrm one
sed -i "\
s:@libdir@:$lib_store_virtual_dir:;\
s:@incdir@:$inc_store_virtual_dir:;\
s:@PACKAGE_VERSION@:$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch:;\
" $build_dir/install_root$lib_store_dir/pkgconfig/libdrm_amdgpu.pc &
#===============================================================================
libdrm_radeon_so_major=1
libdrm_radeon_so_minor=0
libdrm_radeon_so_patch=1

libdrm_radeon_c_src_files="\
$src_dir/radeon/radeon_bo_gem.c \
$src_dir/radeon/radeon_cs_gem.c \
$src_dir/radeon/radeon_cs_space.c \
$src_dir/radeon/radeon_bo.c \
$src_dir/radeon/radeon_cs.c \
$src_dir/radeon/radeon_surface.c \
"

libdrm_radeon_cpp_flags="\
-I$build_dir \
-I$src_dir/radeon \
-I$src_dir/include/drm \
-I$src_dir \
$gcc_cpp_flags \
$glibc_cpp_flags"
#-------------------------------------------------------------------------------
for f in $libdrm_radeon_c_src_files
do
	libdrm_radeon_c_obj=$build_dir/$(basename $f .c).o 
	libdrm_radeon_so="$libdrm_radeon_so $libdrm_radeon_c_obj"

	printf "SLIB_GCC $f\n"
	eval $slib_gcc $libdrm_radeon_cpp_flags -o $libdrm_radeon_c_obj $f &
done
#-------------------------------------------------------------------------------
wait

mkdir -p $build_dir/install_root$lib_store_dir

if test "${libdrm_radeon_slib_gccld-unset}" = unset; then
libdrm_radeon_slib_gccld="gcc \
-o $build_dir/install_root$lib_store_dir/libdrm_radeon.so.$libdrm_radeon_so_major.$libdrm_radeon_so_minor.$libdrm_radeon_so_patch \
-Wl,-soname=libdrm_radeon.so.$libdrm_radeon_so_major \
-pipe -shared -static-libgcc \
-Wl,--no-undefined,--gc-sections \
-Wl,-s \
$libdrm_radeon_so \
$build_dir/install_root$lib_store_dir/libdrm.so.$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch"
fi

printf "SLIB_GCCLD libdrm_radeon.so.$libdrm_radeon_so_major.$libdrm_radeon_so_minor.$libdrm_radeon_so_patch\n"
eval $libdrm_radeon_slib_gccld &
ln -sTf $lib_store_virtual_dir/libdrm_radeon.so.$libdrm_radeon_so_major.$libdrm_radeon_so_minor.$libdrm_radeon_so_patch \
	$build_dir/install_root$runtime_lib_dir/libdrm_radeon.so.$libdrm_radeon_so_major
ln -sTf libdrm_radeon.so.$libdrm_radeon_so_major.$libdrm_radeon_so_minor.$libdrm_radeon_so_patch \
	$build_dir/install_root$lib_store_dir/libdrm_radeon.so
#-------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$inc_store_dir/libdrm
cp $src_dir/radeon/radeon_bo.h $src_dir/radeon/radeon_cs.h \
$src_dir/radeon/radeon_surface.h $src_dir/radeon/radeon_bo_gem.h \
$src_dir/radeon/radeon_cs_gem.h $src_dir/radeon/radeon_bo_int.h \
$src_dir/radeon/radeon_cs_int.h $src_dir/radeon/r600_pci_ids.h \
$build_dir/install_root$inc_store_dir/libdrm &
#-------------------------------------------------------------------------------
mkdir -p $build_dir/install_root$lib_store_dir/pkgconfig
cp $src_dir/contrib/pkgconfig/libdrm_radeon.pc.in \
$build_dir/install_root$lib_store_dir/pkgconfig/libdrm_radeon.pc

# the PACKAGE_VERSION is libdrm one
sed -i "\
s:@libdir@:$lib_store_virtual_dir:;\
s:@incdir@:$inc_store_virtual_dir:;\
s:@PACKAGE_VERSION@:$libdrm_so_major.$libdrm_so_minor.$libdrm_so_patch:;\
" $build_dir/install_root$lib_store_dir/pkgconfig/libdrm_radeon.pc &
#===============================================================================
wait
