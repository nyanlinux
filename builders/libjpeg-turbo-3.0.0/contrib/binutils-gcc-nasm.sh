#!/bin/sh

# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the script at the top of the source tree, create somewhere else
# a build directory, cd into it, and call from there this script.

# XXX: the defaults are for our custom distro
#===================================================================================================
# build dir and src dir
build_dir=$(readlink -f .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(readlink -f $(dirname $0)/..)
echo "src_dir=$src_dir"
#===================================================================================================
# the current configur-able variables may be individually overridden with the
# content of the file $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f $build_dir/local_conf.sh; then
	. $build_dir/local_conf.sh
fi
#===================================================================================================
VERSION=3.0.0
#===================================================================================================
# Only for the pkgconfig file.
if test "${prefix-unset}" = unset; then
prefix=/usr
fi
#===================================================================================================
if test "${cpp-unset}" = unset; then
cpp="/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
	-E -nostdinc -undef -traditional-cpp -x c"
fi
#===================================================================================================
if test "${cc-unset}" = unset; then
cc="/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc -c \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-pipe -fPIC -O2 \
	-static-libgcc"
fi
# This is very bad, should _NEVER_ use in-syntax thread local support, always the platform thread
# lib... UNLESS the compiler is generating calls to the platform threading lib... (was this ever
# a thing?)
if test "${THREAD_LOCAL-unset}" = unset; then
THREAD_LOCAL=__thread
fi
if test "${SIZE_T-unset}" = unset; then
SIZE_T=8
fi
if test "${INLINE-unset}" = unset; then
INLINE=inline
fi
if test "${HAVE_BUILTIN_CTZL_AND_SIZE_T_MATCHES_ULONG_SZ-unset}" = unset; then
HAVE_BUILTIN_CTZL_AND_SIZE_T_MATCHES_ULONG_SZ="define HAVE_BUILTIN_CTZL"
fi
#===================================================================================================
if test "${nasm-unset}" = unset; then
nasm=/nyan/nasm/current/bin/nasm
fi
#===================================================================================================
if test "${ar-unset}" = unset; then
ar=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-ar
fi
#===================================================================================================
# Integer version seems to be: MAJOR*1000 000+MINOR*1000+MICRO.
sed -E -e "\
s:@JPEG_LIB_VERSION@:62:;\
s:@VERSION@:${VERSION}:;\
s:@LIBJPEG_TURBO_VERSION_NUMBER@:3000000:;\
s:cmakedefine C_ARITH_CODING_SUPPORTED:define C_ARITH_CODING_SUPPORTED:;\
s:cmakedefine D_ARITH_CODING_SUPPORTED:define D_ARITH_CODING_SUPPORTED:;\
s:cmakedefine WITH_SIMD:define WITH_SIMD:;\
s:cmakedefine RIGHT_SHIFT_IS_UNSIGNED 1:undef RIGHT_SHIFT_IS_UNSIGNED:" \
$src_dir/jconfig.h.in \
>$build_dir/jconfig.h &
#---------------------------------------------------------------------------------------------------
sed -E -e "\
s:@BUILD@:$(date +%Y%m%d):;\
s:@THREAD_LOCAL@:${THREAD_LOCAL}:;\
s:@INLINE@:${INLINE}:;\
s:@SIZE_T@:${SIZE_T}:;\
s:@CMAKE_PROJECT_NAME@:libjpeg-turbo C:;\
s:@VERSION@:${VERSION}:;\
s:cmakedefine HAVE_INTRIN_H:undef HAVE_INTRIN_H:;\
s:cmakedefine HAVE_BUILTIN_CTZL:${HAVE_BUILTIN_CTZL_AND_SIZE_T_MATCHES_ULONG_SZ}:;\
s:cmakedefine C_ARITH_CODING_SUPPORTED:define C_ARITH_CODING_SUPPORTED:;\
s:cmakedefine D_ARITH_CODING_SUPPORTED:define D_ARITH_CODING_SUPPORTED:;\
s:cmakedefine WITH_SIMD:define WITH_SIMD:" \
$src_dir/jconfigint.h.in \
>$build_dir/jconfigint.h &
#---------------------------------------------------------------------------------------------------
sed -E -e "\
s:@COPYRIGHT_YEAR@:$(date +%Y):" \
$src_dir/jversion.h.in \
>$build_dir/jversion.h &
#---------------------------------------------------------------------------------------------------
# simd
mkdir -p $build_dir/simd/nasm
# There could be a pre-generated jsimdcfg.inc in the source tree, remove it just to be sure.
rm -f $src_dir/simd/nasm/jsimdcfg.inc

# XPP = C preprocessor AND nasm preprocessor
XPP_DEFINES_COMMON="\
-D__x86_64__ \
-DPIC \
-DELF"

XPP_INCLUDE_DIRS_COMMON="\
-I$build_dir/simd/x86_64 \
-I$src_dir/simd/x86_64 \
-I$build_dir/simd/nasm \
-I$src_dir/simd/nasm \
-I$build_dir/simd \
-I$src_dir/simd \
-I$build_dir \
-I$src_dir"

wait
$cpp \
	$XPP_DEFINES_COMMON \
	$XPP_INCLUDE_DIRS_COMMON \
	$src_dir/simd/nasm/jsimdcfg.inc.h \
		| grep -E '^[\;%]|^\ %' \
		| sed 's%_cpp_protection_%%' \
		| sed 's@% define@%define@g' \
	>$build_dir/simd/nasm/jsimdcfg.inc

simd_nasm_files="\
simd/x86_64/jsimdcpu.asm \
simd/x86_64/jfdctflt-sse.asm \
simd/x86_64/jccolor-sse2.asm \
simd/x86_64/jcgray-sse2.asm \
simd/x86_64/jchuff-sse2.asm \
simd/x86_64/jcphuff-sse2.asm \
simd/x86_64/jcsample-sse2.asm \
simd/x86_64/jdcolor-sse2.asm \
simd/x86_64/jdmerge-sse2.asm \
simd/x86_64/jdsample-sse2.asm \
simd/x86_64/jfdctfst-sse2.asm \
simd/x86_64/jfdctint-sse2.asm \
simd/x86_64/jidctflt-sse2.asm \
simd/x86_64/jidctfst-sse2.asm \
simd/x86_64/jidctint-sse2.asm \
simd/x86_64/jidctred-sse2.asm \
simd/x86_64/jquantf-sse2.asm \
simd/x86_64/jquanti-sse2.asm \
simd/x86_64/jccolor-avx2.asm \
simd/x86_64/jcgray-avx2.asm \
simd/x86_64/jcsample-avx2.asm \
simd/x86_64/jdcolor-avx2.asm \
simd/x86_64/jdmerge-avx2.asm \
simd/x86_64/jdsample-avx2.asm \
simd/x86_64/jfdctint-avx2.asm \
simd/x86_64/jidctint-avx2.asm \
simd/x86_64/jquanti-avx2.asm"

mkdir -p $build_dir/simd/x86_64
for f in $simd_nasm_files
do
	o=$(dirname $f)/$(basename $f .asm).o
	simd_a="$simd_a $build_dir/$o"
	$nasm \
		$XPP_DEFINES_COMMON \
		$XPP_INCLUDE_DIRS_COMMON \
		-felf64 \
		$src_dir/$f \
		-o $build_dir/$o &
	
done

$cc \
	$XPP_DEFINES_COMMON \
	$XPP_INCLUDE_DIRS_COMMON \
	$src_dir/simd/x86_64/jsimd.c \
	-o $build_dir/simd/x86_64/jsimd.o &
simd_a="$simd_a $build_dir/simd/x86_64/jsimd.o"
#---------------------------------------------------------------------------------------------------
jpeg16_src_c_files="\
jcapistd.c \
jccolor.c \
jcdiffct.c \
jclossls.c \
jcmainct.c \
jcprepct.c \
jcsample.c \
jdapistd.c \
jdcolor.c \
jddiffct.c \
jdlossls.c \
jdmainct.c \
jdpostct.c \
jdsample.c \
jutils.c"

mkdir -p $build_dir/16bits
for f in $jpeg16_src_c_files
do
	o=$(basename $f .c).o
	$cc \
		-DBITS_IN_JSAMPLE=16 \
		$XPP_DEFINES_COMMON \
		$XPP_INCLUDE_DIRS_COMMON \
		$src_dir/$f \
		-o $build_dir/16bits/$o &
	jpeg_a="$jpeg_a $build_dir/16bits/$o"
done

jpeg12_src_c_files="\
$jpeg16_src_c_files \
jccoefct.c \
jcdctmgr.c \
jdcoefct.c \
jddctmgr.c \
jdmerge.c \
jfdctfst.c \
jfdctint.c \
jidctflt.c \
jidctfst.c \
jidctint.c \
jidctred.c \
jquant1.c \
jquant2.c"

mkdir -p $build_dir/12bits
for f in $jpeg12_src_c_files
do
	o=$(basename $f .c).o
	$cc \
		-DBITS_IN_JSAMPLE=12 \
		$XPP_DEFINES_COMMON \
		$XPP_INCLUDE_DIRS_COMMON \
		$src_dir/$f \
		-o $build_dir/12bits/$o &
	jpeg_a="$jpeg_a $build_dir/12bits/$o"
done

jpeg_src_c_files="\
$jpeg12_src_c_files \
jcapimin.c \
jchuff.c \
jcicc.c \
jcinit.c \
jclhuff.c \
jcmarker.c \
jcmaster.c \
jcomapi.c \
jcparam.c \
jcphuff.c \
jctrans.c \
jdapimin.c \
jdatadst.c \
jdatasrc.c \
jdhuff.c \
jdicc.c \
jdinput.c \
jdlhuff.c \
jdmarker.c \
jdmaster.c \
jdphuff.c \
jdtrans.c \
jerror.c \
jfdctflt.c \
jmemmgr.c \
jmemnobs.c \
\
jaricom.c \
\
jcarith.c \
\
jdarith.c"

for f in $jpeg_src_c_files
do
	o=$(basename $f .c).o
	$cc \
		$XPP_DEFINES_COMMON \
		$XPP_INCLUDE_DIRS_COMMON \
		$src_dir/$f \
		-o $build_dir/$o &
	jpeg_a="$jpeg_a $build_dir/$o"
done

cat >$build_dir/libjpeg.pc <<EOF
prefix=${prefix}
exec_prefix=\${prefix}
libdir=\${prefix}/lib
includedir=\${prefix}/include

Name: libjpeg
Description: A SIMD-accelerated JPEG codec that provides the libjpeg API
Version: ${VERSION}
Libs: -L\${libdir} -ljpeg
Cflags: -I\${includedir}
EOF

wait
$ar rcs $build_dir/libjpeg.a $jpeg_a $simd_a

# include files to install are:
#	$build_dir/jconfig.h
#	$src_dir/jerror.h
#	$src_dir/jmorecfg.h
#	$src_dir/jpeglib.h
