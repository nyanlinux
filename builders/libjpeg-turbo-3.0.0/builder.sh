src_name=libjpeg-turbo
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=https://sourceforge.net/projects/$src_name/files/$version/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

# install our build system to work around that cmake trash.
cp -r $nyan_root/builders/$pkg_name/contrib $pkg_dir

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

cat >$build_dir/local_conf.sh <<EOF
prefix=/nyan/$src_name/$slot
EOF

$pkg_dir/contrib/binutils-gcc-nasm.sh

mkdir -p /nyan/$src_name/$slot/lib/pkgconfig
cp -f $build_dir/libjpeg.a /nyan/$src_name/$slot/lib
cp -f $build_dir/libjpeg.pc /nyan/$src_name/$slot/lib/pkgconfig
mkdir -p /nyan/$src_name/$slot/include
# The list of header files is in the contrib build script at the end.
cp -f \
	$build_dir/jconfig.h \
	$pkg_dir/jerror.h \
	$pkg_dir/jmorecfg.h \
	$pkg_dir/jpeglib.h \
\
	/nyan/$src_name/$slot/include

rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
