src_name=libevent
version=2.1.8
archive_name=$src_name-$version-stable.tar.gz
url0=https://github.com/$src_name/$src_name/releases/download/release-$version-stable/$archive_name

src_dir=$src_dir_root/$src_name-$version-stable
rm -Rf $src_dir
cd $src_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export 'CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc'
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--prefix=/nyan/libevent/0	\
	--enable-thread-support		\
	--disable-malloc-replacement	\
	--disable-openssl		\
	--disable-debug-mode		\
	--enable-static			\
	--disable-shared
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup and tidying
rm -f /nyan/$src_name/0/lib/*.la

rm -Rf $build_dir $src_dir
