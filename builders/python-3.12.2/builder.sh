src_name=Python
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=https://www.python.org/ftp/python/$version/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

cd $pkg_dir

#===================================================================================================
# fix the usual hardcoded trashy use of openssl
#---------------------------------------------------------------------------------------------------
# make the configure sh script behave
sed -i ./configure -e '28307 c\
    '
sed -i ./configure -e '28308 c\
    '
#---------------------------------------------------------------------------------------------------
# copy the libressl-ized (and fixed...) hashlib module
cp -f $nyan_root/builders/$pkg_name/_hashopenssl.c ./Modules/_hashopenssl.c
#===================================================================================================

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
$PATH\
"

export OPT=-O2
export 'CFLAGS=-pipe -fpic -fPIC'
export LDFLAGS="\
-L/nyan/zlib/current/lib -lz \
-L/nyan/expat/current/lib -lexpat \
-L/nyan/libffi/current/lib64 -lffi"
export "CC=gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-I/nyan/zlib/current/include \
	-I/nyan/expat/current/include \
	-I/nyan/libffi/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
$pkg_dir/configure \
	--prefix=/nyan/python/$slot \
	--disable-shared \
	--disable-ipv6 \
	--with-system-expat \
	--without-system-libmpdec \
	--with-openssl=/nyan/libressl/current
unset OPT
unset CFLAGS
unset LDFLAGS
unset CC

make -j $threads_n 
make install

rm -Rf /nyan/python/$slot/share

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
