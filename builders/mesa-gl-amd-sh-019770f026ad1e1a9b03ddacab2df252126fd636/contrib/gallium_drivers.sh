printf "\tbuilding drivers sub-components-->\n"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/gallium
mkdir $build_dir/src/gallium/drivers
mkdir $build_dir/src/gallium/drivers/radeonsi
#-------------------------------------------------------------------------------
libradeonsi_c_pathnames="\
$build_dir/src/gallium/drivers/radeonsi/gfx10_format_table.c \
$src_dir/src/gallium/drivers/radeonsi/gfx11_query.c \
$src_dir/src/gallium/drivers/radeonsi/gfx10_shader_ngg.c \
\
$src_dir/src/gallium/drivers/radeonsi/si_barrier.c \
$src_dir/src/gallium/drivers/radeonsi/si_blit.c \
$src_dir/src/gallium/drivers/radeonsi/si_buffer.c \
$src_dir/src/gallium/drivers/radeonsi/si_clear.c \
$src_dir/src/gallium/drivers/radeonsi/si_compute.c \
$src_dir/src/gallium/drivers/radeonsi/si_compute_blit.c \
$src_dir/src/gallium/drivers/radeonsi/si_cp_dma.c \
$src_dir/src/gallium/drivers/radeonsi/si_cp_reg_shadowing.c \
$src_dir/src/gallium/drivers/radeonsi/si_cp_utils.c \
$src_dir/src/gallium/drivers/radeonsi/si_debug.c \
$src_dir/src/gallium/drivers/radeonsi/si_descriptors.c \
$src_dir/src/gallium/drivers/radeonsi/si_sdma_copy_image.c \
$src_dir/src/gallium/drivers/radeonsi/si_fence.c \
$src_dir/src/gallium/drivers/radeonsi/si_get.c \
$src_dir/src/gallium/drivers/radeonsi/si_gfx_cs.c \
$src_dir/src/gallium/drivers/radeonsi/si_gpu_load.c \
$src_dir/src/gallium/drivers/radeonsi/si_nir_lower_abi.c \
$src_dir/src/gallium/drivers/radeonsi/si_nir_lower_resource.c \
$src_dir/src/gallium/drivers/radeonsi/si_nir_lower_vs_inputs.c \
$src_dir/src/gallium/drivers/radeonsi/si_nir_optim.c \
$src_dir/src/gallium/drivers/radeonsi/si_perfcounter.c \
$build_dir/src/gallium/drivers/radeonsi/si_pipe.c \
$src_dir/src/gallium/drivers/radeonsi/si_pm4.c \
$src_dir/src/gallium/drivers/radeonsi/si_query.c \
$src_dir/src/gallium/drivers/radeonsi/si_shader.c \
$src_dir/src/gallium/drivers/radeonsi/si_shader_aco.c \
$src_dir/src/gallium/drivers/radeonsi/si_shader_info.c \
$src_dir/src/gallium/drivers/radeonsi/si_shaderlib_nir.c \
$src_dir/src/gallium/drivers/radeonsi/si_shader_nir.c \
$src_dir/src/gallium/drivers/radeonsi/si_sqtt.c \
$src_dir/src/gallium/drivers/radeonsi/si_state.c \
$src_dir/src/gallium/drivers/radeonsi/si_state_binning.c \
$src_dir/src/gallium/drivers/radeonsi/si_state_msaa.c \
$src_dir/src/gallium/drivers/radeonsi/si_state_streamout.c \
$src_dir/src/gallium/drivers/radeonsi/si_state_viewport.c \
$src_dir/src/gallium/drivers/radeonsi/si_test_blit_perf.c \
$src_dir/src/gallium/drivers/radeonsi/si_test_dma_perf.c \
$src_dir/src/gallium/drivers/radeonsi/si_test_image_copy_region.c \
$src_dir/src/gallium/drivers/radeonsi/si_texture.c \
$build_dir/src/gallium/drivers/radeonsi/si_uvd.c \
\
$src_dir/src/gallium/drivers/radeonsi/radeon_uvd.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn_enc_1_2.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn_enc_2_0.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn_enc_3_0.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn_enc_4_0.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn_enc_5_0.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn_enc.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn_dec_jpeg.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vcn_dec.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_uvd_enc_1_1.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_uvd_enc.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vce_40_2_2.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vce_50.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vce_52.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_vce.c \
$src_dir/src/gallium/drivers/radeonsi/radeon_video.c \
"
# some c++ diarrhea did slip in
libradeonsi_cxx_pathnames="\
$src_dir/src/gallium/drivers/radeonsi/si_state_shaders.cpp \
"
#-------------------------------------------------------------------------------
for src_pathname in $libradeonsi_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/drivers/radeonsi/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/drivers/radeonsi/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src/amd/compiler \
		-I$src_dir/src/amd/compiler \
		-I$build_dir/src/amd/common \
		-I$src_dir/src/amd/common \
		-I$build_dir/src/amd \
		-I$src_dir/src/amd \
		-I$build_dir/src/gallium/drivers/radeonsi \
		-I$src_dir/src/gallium/drivers/radeonsi \
		-I$build_dir/src/gallium/drivers \
		-I$src_dir/src/gallium/drivers \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libradeonsi_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/drivers/radeonsi/$cpp_filename --> $build_dir/src/gallium/drivers/radeonsi/$asm_filename\n"
	$cc_s $build_dir/src/gallium/drivers/radeonsi/$cpp_filename -o $build_dir/src/gallium/drivers/radeonsi/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libradeonsi_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/drivers/radeonsi/$o_filename"
	printf "AS $build_dir/src/gallium/drivers/radeonsi/$asm_filename --> $build_dir/src/gallium/drivers/radeonsi/$o_filename\n"
	$as $build_dir/src/gallium/drivers/radeonsi/$asm_filename -o $build_dir/src/gallium/drivers/radeonsi/$o_filename &
done
#------------------------------------------------------------------------------
for src_pathname in $libradeonsi_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	printf "CXXPP $src_pathname --> $build_dir/src/gallium/drivers/radeonsi/$cxxpp_filename\n"
	$cxxpp $src_pathname -o $build_dir/src/gallium/drivers/radeonsi/$cxxpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$cxx_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src/amd/compiler \
		-I$src_dir/src/amd/compiler \
		-I$build_dir/src/amd/common \
		-I$src_dir/src/amd/common \
		-I$build_dir/src/amd \
		-I$src_dir/src/amd \
		-I$build_dir/src/gallium/drivers/radeonsi \
		-I$src_dir/src/gallium/drivers/radeonsi \
		-I$build_dir/src/gallium/drivers \
		-I$src_dir/src/gallium/drivers \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libradeonsi_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	printf "CXX_S $build_dir/src/gallium/drivers/radeonsi/$cxxpp_filename --> $build_dir/src/gallium/drivers/radeonsi/$asm_filename\n"
	$cxx_s $build_dir/src/gallium/drivers/radeonsi/$cxxpp_filename -o $build_dir/src/gallium/drivers/radeonsi/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
# reuse os
for src_pathname in $libradeonsi_cxx_pathnames
do
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	o_filename=$(basename $src_pathname .cpp).cpp.cxx.s.o
	os="$os $build_dir/src/gallium/drivers/radeonsi/$o_filename"
	printf "AS $build_dir/src/gallium/drivers/radeonsi/$asm_filename --> $build_dir/src/gallium/drivers/radeonsi/$o_filename\n"
	$as $build_dir/src/gallium/drivers/radeonsi/$asm_filename -o $build_dir/src/gallium/drivers/radeonsi/$o_filename &
done
#------------------------------------------------------------------------------
wait
#-------------------------------------------------------------------------------
printf "AR RCS $build_dir/libradeonsi.a $os\n"
$ar_rcs $build_dir/libradeonsi.a $os
#===============================================================================
# this so much filthy, there is no words for it
libradeonsi_gfx_cxx_pathnames="\
$src_dir/src/gallium/drivers/radeonsi/si_state_draw.cpp \
"
for g in 6 7 8 9 10 103 11 115 12
do
	for src_pathname in $libradeonsi_gfx_cxx_pathnames
	do
		cxxpp_filename=$(basename $src_pathname .cpp).$g.cpp.cxx
		printf "CXXPP $src_pathname --> $build_dir/src/gallium/drivers/radeonsi/$cxxpp_filename\n"
		$cxxpp $src_pathname -o $build_dir/src/gallium/drivers/radeonsi/$cxxpp_filename \
			-DGFX_VER=$g \
			\
			-I$cc_internal_fixed_incdir \
			-I$cc_internal_incdir \
			-I$cxx_internal_incdir \
			-I$linux_incdir \
			-I$syslib_incdir \
			\
			-I$build_dir/src/util \
			-I$src_dir/src/util \
			-I$build_dir/src/amd/compiler \
			-I$src_dir/src/amd/compiler \
			-I$build_dir/src/amd/common \
			-I$src_dir/src/amd/common \
			-I$build_dir/src/amd \
			-I$src_dir/src/amd \
			-I$build_dir/src/gallium/drivers/radeonsi \
			-I$src_dir/src/gallium/drivers/radeonsi \
			-I$build_dir/src/gallium/drivers \
			-I$src_dir/src/gallium/drivers \
			-I$build_dir/src/gallium/auxiliary \
			-I$src_dir/src/gallium/auxiliary \
			-I$build_dir/src/gallium/include \
			-I$src_dir/src/gallium/include \
			-I$build_dir/src/compiler/nir \
			-I$src_dir/src/compiler/nir \
			-I$build_dir/src/compiler \
			-I$src_dir/src/compiler \
			-I$build_dir/src \
			-I$src_dir/src \
			-I$build_dir/include \
			-I$src_dir/include \
			\
			$syslib_cpp_flags_defs \
			$linux_cpp_flags_defs \
			$cc_builtins_cpp_flags_defs \
			$cc_attributes_cpp_flags_defs \
			$mesa_cpp_flags_defs \
			\
			$external_deps_cpp_flags &
	done
	#-----------------------------------------------------------------------
	wait
	#-----------------------------------------------------------------------
	for src_pathname in $libradeonsi_gfx_cxx_pathnames
	do
		cxxpp_filename=$(basename $src_pathname .cpp).$g.cpp.cxx
		asm_filename=$(basename $src_pathname .cpp).$g.cpp.cxx.s
		printf "CXX_S $build_dir/src/gallium/drivers/radeonsi/$cxxpp_filename --> $build_dir/src/gallium/drivers/radeonsi/$asm_filename\n"
		$cxx_s $build_dir/src/gallium/drivers/radeonsi/$cxxpp_filename -o $build_dir/src/gallium/drivers/radeonsi/$asm_filename &
	done
	#-----------------------------------------------------------------------
	wait
	#-----------------------------------------------------------------------
	libradeonsi_gfx_a=
	for src_pathname in $libradeonsi_gfx_cxx_pathnames
	do
		asm_filename=$(basename $src_pathname .cpp).$g.cpp.cxx.s
		o_filename=$(basename $src_pathname .cpp).$g.cpp.cxx.s.o
		libradeonsi_gfx_a="$libradeonsi_gfx_a $build_dir/src/gallium/drivers/radeonsi/$o_filename"
		printf "AS $build_dir/src/gallium/drivers/radeonsi/$asm_filename --> $build_dir/src/gallium/drivers/radeonsi/$o_filename\n"
		$as $build_dir/src/gallium/drivers/radeonsi/$asm_filename -o $build_dir/src/gallium/drivers/radeonsi/$o_filename &
	done
	#------------------------------------------------------------------------------
	wait
	#-------------------------------------------------------------------------------
	printf "AR RCS $build_dir/libradeonsi_gfx$g.a $libradeonsi_gfx_a\n"
	$ar_rcs $build_dir/libradeonsi_gfx$g.a $libradeonsi_gfx_a
	# will use this variable in dri driver linking command later on
	libradeonsi_gfx_libs="$libradeonsi_gfx_libs $build_dir/libradeonsi_gfx$g.a"
done
#===============================================================================
printf "\t<--drivers sub-components built\n"
