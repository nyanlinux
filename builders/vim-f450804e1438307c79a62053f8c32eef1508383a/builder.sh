src_name=vim
git_commit=${pkg_name##*-}
slot=$git_commit
url0=https://github.com/vim/vim.git

pkg_dir=$pkgs_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -Hr $src_dir_root/$src_name $pkgs_dir_root

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
/nyan/git/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

cd $pkg_dir

if test x$git_commit != x; then
	git reset --hard
	git checkout $git_commit
fi

export CPPFLAGS="\
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-I/nyan/ncurses/current/include"
export 'CFLAGS=-O2 -pipe -fPIC -static-libgcc'
# to find tinfow
export "LDFLAGS=\
	-L/nyan/ncurses/current/lib \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export CC=$target_gnu_triple-gcc
export LIBS=-lpthread
./configure \
	--prefix=/nyan/$src_name/$slot \
	--disable-xsmp \
	--disable-xsmp-interact \
	--enable-multibyte \
	--disable-acl \
	--disable-nls \
	--without-x \
	--with-tlib=tinfo
unset CPPFLAGS
unset CFLAGS
unset LDFLAGS
unset CC
unset LIBS

make -j $threads_s
make install

rm -Rf /nyan/$src_name/$slot/share/man

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $build_dir $pkg_dir
