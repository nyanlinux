src_name=readline
version=7.0
archive_name=$src_name-$version.tar.gz
url0=http://ftpmirror.gnu.org/$src_name/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

# applying patches
cd $src_dir
for p in 001 002 003
do
	cp $nyan_root/src/${src_name}70-$p ./
	patch -p0 -i ./${src_name}70-$p
done

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

# help readline configuration find the right tinfo
ln -fs libtinfow.a $target_sysroot/nyan/ncurses/0/lib/libtinfo.a

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CPPFLAGS=-I$target_sysroot/nyan/ncurses/0/include"
export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
export "LDFLAGS=-L$target_sysroot/nyan/ncurses/0/lib"
$src_dir/configure			\
	--build=$build_gnu_triple	\
	--host=$target_gnu_triple	\
	--prefix=/nyan/readline/0	\
	--disable-shared		\
	--enable-multibyte		\
	--with-curses
unset LDFLAGS
unset CFLAGS
unset CC
unset CPPFLAGS

make -j $threads_n
make install DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/$src_name/0/share/doc
rm -Rf $target_sysroot/nyan/$src_name/0/share/info
rm -Rf $target_sysroot/nyan/$src_name/0/share/man

rm -f $target_sysroot/nyan/ncurses/0/lib/libtinfo.a

rm -Rf $build_dir $src_dir
export PATH=$OLD_PATH
