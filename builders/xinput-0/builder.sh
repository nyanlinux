src_name=xinput
version=1.6.2
archive_name=$src_name-$version.tar.bz2
url0=http://xorg.freedesktop.org/releases/individual/app/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export "PKG_CONFIG_PATH=\
/nyan/libpthread-stubs/current/lib/pkgconfig:\
/nyan/libXau/current/lib/pkgconfig:\
/nyan/libXext/current/lib/pkgconfig:\
/nyan/libxcb/current/lib/pkgconfig:\
/nyan/libX11/current/lib/pkgconfig:\
/nyan/libXfixes/current/lib/pkgconfig:\
/nyan/libXrender/current/lib/pkgconfig:\
/nyan/libXrandr/current/lib/pkgconfig:\
/nyan/libXi/current/lib/pkgconfig:\
/nyan/libXinerama/current/lib/pkgconfig:\
/nyan/xorgproto/current/share/pkgconfig:\
/nyan/util-macro/current/share/pkgconfig"

export "CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -static-libgcc \
-Wl,-rpath-link,\
/nyan/libxcb/current/lib:\
/nyan/libXrender/current/lib:\
/nyan/libXau/current/lib:\
/nyan/glibc/current/lib"
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure --prefix=/nyan/xinput/0
unset CFLAGS
unset CC
unset PKG_CONFIG_PATH

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/0/share
strip -s /nyan/$src_name/0/bin/$src_name

rm -Rf $build_dir $src_dir
