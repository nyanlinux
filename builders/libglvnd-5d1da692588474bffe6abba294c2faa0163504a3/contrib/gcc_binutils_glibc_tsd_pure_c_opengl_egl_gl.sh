#!/bin/sh

#===============================================================================
# build dir, src dir and script dir
build_dir_pathname=$(readlink -f .)
printf "build_dir_pathname=$build_dir_pathname\n"

if test "x$LIBGLVND_SRC_DIR_PATHNAME" != "x"; then
src_dir_pathname=$LIBGLVND_SRC_DIR_PATHNAME
else
# we are in "contrib" directory
src_dir_pathname=$(readlink -f $(dirname $0)/..)
fi
printf "src_dir_pathname=$src_dir_pathname\n"

# sdk location
sdk_dir_pathname=$(readlink -f $(dirname $0))
printf "sdk_dir_pathname=$sdk_dir_pathname\n"
#===============================================================================


#===============================================================================
# the current configur-able variables may be individually overridden with the
# content of $build_dir/local_conf.sh. Look for "unset" in those scripts to find
# what you can override to tune the build.
if test -f $build_dir_pathname/local_conf.sh; then
	. $build_dir_pathname/local_conf.sh
fi
#===============================================================================


#===============================================================================
# directory configuration
#-------------------------------------------------------------------------------
if test "${lib_dir_pathname-unset}" = unset; then
lib_dir_pathname=/usr/lib
fi
#-------------------------------------------------------------------------------
# This will be used in shared/public pathnames
if test "${sysconf_dir_pathname-unset}" = unset; then
sysconf_dir_pathname=/etc
fi
#-------------------------------------------------------------------------------
# This will be used in shared/public pathnames
if test "${data_dir_pathname-unset}" = unset; then
data_dir_pathname=/usr/share
fi
#-------------------------------------------------------------------------------
if test "${inc_dir_pathname-unset}" = unset; then
inc_dir_pathname=/usr/include
fi
#-------------------------------------------------------------------------------
#===============================================================================


################################################################################
# toolchain
# XXX: we avoid as much as we can that brain fuckage which is the compiler
#      driver
#===============================================================================
if test "${cpp-unset}" = unset; then
cpp="/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc
	-E -nostdinc"
fi
if test "${cc-unset}" = unset; then
cc="/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc \
	-O2 -pipe -fPIC -ftls-model=global-dynamic -static-libgcc -fpic \
	-fvisibility=hidden"
fi
#-------------------------------------------------------------------------------
# compiler internal cpp files. In the case of gcc, they require gcc cpp
# preprocessor... jez...
if test "${cc_cpp_flags_inc-unset}" = unset; then
cc_cpp_flags_inc="\
-I/opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include \
"
fi
if test "${cc_cpp_flags_defs-unset}" = unset; then
# XXX: CURRENT GLDISPATCH.C IMPLICIT INITIALIZATION IS CURRENTLY DONE AT
# SHARED LIBRARY LOADING TIME BY THE ELF LOADER WHICH IMPLIES COMPILER SUPPORT.
# (AND THE C RUNTIME BEING INITIALIZED).
# XXX: IT SHOULD EXIST A "AT FIRST USAGE" MODE, THAT TO RELAX COMPILER
# REQUIREMENTS AND SHOULD HAVE FOR SURE AN INITIALIZED C RUNTIME.
# 20231130: tinycc has the constructor/destructor attribute, but not cproc/qbe
# namely we would have to add an assembler trampoline, or patch the assembly
# generated file from qbe (probably the way to go). Did not check simple-cc.
# XXX:expect gcc inline assembly *OR* gcc __sync builtin function. Needs
# binutils gas assembly files to decouple the code from gcc.
cc_cpp_flags_defs="\
-DHAVE_TYPEOF \
-DUSE_ATTRIBUTE_CONSTRUCTOR \
-DHAVE_SYNC_INTRINSICS \
"
fi
#-------------------------------------------------------------------------------
# Compiler generic support library. In the case of gcc it is libgcc, for tinycc
# it is libtcc1, etc.
# We want a full pathname. With gcc, you can discover the pathname with:
# "gcc -print-libgcc-file-name"
if test "${cc_ld_support_lib-unset}" = unset; then
eval cc_ld_support_lib=$($cc -print-libgcc-file-name)
fi
#-------------------------------------------------------------------------------
# Compiler specific start/end files for linking a Shared LIBrary (slib), they
# appear after/before the syslib start/end files while linking. Here, as an
# example, look at the compiler driver $gcc_src/gcc/config/gnu-user.h (13.2.0)
# if you want to understand a bit more in the case of glibc, shared libs do fall
# in the "shared" category.
# With gcc, their pathname can be discovered with:
# "gcc -print-file-name=crtbeginS.o" and "gcc -print-file-name=crtendS.o"
if test "${cc_ld_slib_start_files-unset}" = unset; then
eval cc_ld_slib_start_files=$($cc -print-file-name=crtbeginS.o)
fi
if test "${cc_ld_slib_end_files-unset}" = unset; then
eval cc_ld_slib_end_files=$($cc -print-file-name=crtendS.o)
fi
#===============================================================================
# if your use another linker, it must be "added".
if test "${binutils_ld-unset}" = unset; then
binutils_ld=/opt/toolchains/x64/elf/binutils-gcc/current/bin/ld
fi
#===============================================================================
if test "${ar-unset}" = unset; then
ar=/opt/toolchains/x64/elf/binutils-gcc/current/bin/ar
fi
#===============================================================================
# toolchain
################################################################################


#===============================================================================
# syslib
if test "${syslib_cpp_flags_inc-unset}" = unset; then
syslib_cpp_flags_inc="\
-I/nyan/linux-headers/current/include \
-I/nyan/glibc/current/include \
"
fi
if test "${syslib_cpp_flags_defs-unset}" = unset; then
syslib_cpp_flags_defs="\
-DHAVE_PTHREAD_RWLOCK_T \
-DHAVE_RTLD_NOLOAD \
"
fi
#-------------------------------------------------------------------------------
if test "${syslib_libc_ld_flags-unset}" = unset; then
syslib_libc_ld_flags="-L/nyan/glibc/current/lib -lc"
fi
#-------------------------------------------------------------------------------
if test "${syslib_libdl_ld_flags-unset}" = unset; then
syslib_libdl_ld_flags="-L/nyan/glibc/current/lib -ldl"
fi
#-------------------------------------------------------------------------------
if test "${syslib_libm_ld_flags-unset}" = unset; then
syslib_libm_ld_flags="-L/nyan/glibc/current/lib -lm"
fi
#-------------------------------------------------------------------------------
# syslib specific start/end files for linking a Shared LIBrary (slib), they
# appear before/after the compiler start/end files while linking. Here, as an
# example, look at the compiler driver $gcc_src/gcc/config/gnu-user.h (13.2.0)
# if you want to understand a bit more in the case of glibc, shared libs do fall
# in the "shared" category (there, you can see the object requirement of PIE
# static-PIE, etc etc).
# GNU toolchain have a circular dependency between the gcc support library,
# libgcc, and the glibc itself, namely you have to handle that with your linker
# (binutils ld has --{start,end}-group, or YOU USUALLY HAVE TO REPEAT THE
# LIBRARY FILES (XXX: DOES NOT WORK WITH OBJECT FILES) ON THE LINK COMMAND LINE
# UNTIL YOU BREAK THE CIRCULAR DEPENDENCY.
# We want full pathnames.
if test "${syslib_ld_slib_start_files-unset}" = unset; then
syslib_ld_slib_start_files=/nyan/glibc/current/lib/crti.o
fi
if test "${syslib_ld_slib_end_files-unset}" = unset; then
syslib_ld_slib_end_files=/nyan/glibc/current/lib/crtn.o
fi
# syslib
#===============================================================================


#===============================================================================
if test "${xorgproto_inc_dir_pathname-unset}" = unset; then
xorgproto_inc_dir_pathname=/nyan/xorgproto/current/include
fi
#===============================================================================


#===============================================================================
if test "${libx11_inc_dir_pathname-unset}" = unset; then
libx11_inc_dir_pathname=/nyan/libX11/current/include
fi
if test "${libx11_lib_dir_pathname-unset}" = unset; then
libx11_lib_dir_pathname=/nyan/libX11/current/lib
fi
#===============================================================================


#===============================================================================
# python/perl/ruby/javascript/lua/etc whatever...
if test "${python3-unset}" = unset; then
python3=/nyan/python/current/bin/python3
fi
#===============================================================================


#===============================================================================
# libglvnd configuration
libglvnd_cpp_flags="\
"
#===============================================================================


################################################################################
# install_root
mkdir -p $build_dir_pathname/install_root$lib_dir_pathname
# install_root
################################################################################


################################################################################
# generators
#-------------------------------------------------------------------------------
mkdir -p $build_dir_pathname/src/generate
#-------------------------------------------------------------------------------
printf "GENERATOR:PYTHON3: $build_dir_pathname/src/generate/glapi_mapi_tmp.h\n"
$python3 $src_dir_pathname/src/generate/gen_gldispatch_mapi.py \
	gldispatch \
	$src_dir_pathname/src/generate/xml/gl.xml \
	$src_dir_pathname/src/generate/xml/gl_other.xml \
		>$build_dir_pathname/src/generate/glapi_mapi_tmp.h &
#-------------------------------------------------------------------------------
printf "GENERATOR:PYTHON3: $build_dir_pathname/src/generate/g_glapi_mapi_opengl_tmp.h\n"
$python3 $src_dir_pathname/src/generate/gen_gldispatch_mapi.py \
	opengl \
	$src_dir_pathname/src/generate/xml/gl.xml \
	$src_dir_pathname/src/generate/xml/gl_other.xml \
		>$build_dir_pathname/src/generate/g_glapi_mapi_opengl_tmp.h &
#-------------------------------------------------------------------------------
printf "GENERATOR:PYTHON3: $build_dir_pathname/src/generate/g_glapi_mapi_gl_tmp.h\n"
$python3 $src_dir_pathname/src/generate/gen_gldispatch_mapi.py \
	$src_dir_pathname/src/GL/gl.symbols \
	$src_dir_pathname/src/generate/xml/gl.xml \
	$src_dir_pathname/src/generate/xml/gl_other.xml \
		>$build_dir_pathname/src/generate/g_glapi_mapi_gl_tmp.h &
#-------------------------------------------------------------------------------
printf "GENERATOR:PYTHON3: $build_dir_pathname/src/generate/g_egldispatchstubs.h\n"
$python3 $src_dir_pathname/src/generate/gen_egl_dispatch.py \
	header \
	$src_dir_pathname/src/generate/xml/egl.xml \
		>$build_dir_pathname/src/generate/g_egldispatchstubs.h &
#-------------------------------------------------------------------------------
printf "GENERATOR:PYTHON3: $build_dir_pathname/src/generate/g_egldispatchstubs.c\n"
$python3 $src_dir_pathname/src/generate/gen_egl_dispatch.py \
	source \
	$src_dir_pathname/src/generate/xml/egl.xml \
		>$build_dir_pathname/src/generate/g_egldispatchstubs.c &
#-------------------------------------------------------------------------------
printf "GENERATOR:PYTHON3: $build_dir_pathname/src/generate/g_libglglxwrapper.c\n"
$python3 $src_dir_pathname/src/generate/gen_libgl_glxstubs.py \
	$src_dir_pathname/src/generate/xml/glx.xml \
	$src_dir_pathname/src/generate/xml/glx_other.xml \
		>$build_dir_pathname/src/generate/g_libglglxwrapper.c &
#-------------------------------------------------------------------------------
printf "GENERATOR:PYTHON3: $build_dir_pathname/src/generate/g_glx_dispatch_stub_list.h\n"
$python3 $src_dir_pathname/src/GLX/gen_glx_stubs.py \
		>$build_dir_pathname/src/generate/g_glx_dispatch_stub_list.h &
#-------------------------------------------------------------------------------
# generators
################################################################################

wait

################################################################################
# util
# we fused the build, but should be independently built
src_c_subpathnames="\
src/util/glvnd_pthread.c \
src/util/app_error_check.c \
src/util/utils_misc.c \
src/util/trace.c \
src/util/winsys_dispatch.c \
src/util/cJSON.c
"

for src_c_subpathname in $src_c_subpathnames
do
	cpp_filename=$(basename $src_c_subpathname .c).cpp.c
	subpath=$(dirname $src_c_subpathname)

	mkdir -p $build_dir_pathname/$subpath

	printf "CPP $src_dir_pathname/$src_c_subpathname --> $build_dir_pathname/$subpath/$cpp_filename\n"
	$cpp -o $build_dir_pathname/$subpath/$cpp_filename $src_dir_pathname/$src_c_subpathname \
		$syslib_cpp_flags_inc \
		$cc_cpp_flags_inc \
		-I$build_dir_pathname/src/util/uthash/include \
		-I$src_dir_pathname/src/util/uthash/include \
		-I$build_dir_pathname/src/util/ \
		-I$src_dir_pathname/src/util/ \
		-I$build_dir_pathname/include \
		-I$src_dir_pathname/include \
		$cc_cpp_flags_defs \
		$syslib_cpp_flags_defs \
		$libglvnd_cpp_flags &
done

wait

for src_c_subpathname in $src_c_subpathnames
do
	o_filename=$(basename $src_c_subpathname .c).o
	cpp_filename=$(basename $src_c_subpathname .c).cpp.c
	subpath=$(dirname $src_c_subpathname)

	mkdir -p $build_dir_pathname/$subpath

	printf "CC $build_dir_pathname/$subpath/$cpp_filename --> $build_dir_pathname/$subpath/$o_filename\n"
	$cc -c -o $build_dir_pathname/$subpath/$o_filename $build_dir_pathname/$subpath/$cpp_filename &
done
# util
################################################################################

wait

################################################################################
# GLdispatch/vnd-glapi
mkdir -p $build_dir_pathname/src/GLdispatch/vnd-glapi
#==============================================================================
# libglapi
(
src_c_pathnames="\
$src_dir_pathname/src/GLdispatch/vnd-glapi/mapi_glapi.c \
$src_dir_pathname/src/GLdispatch/vnd-glapi/stub.c \
$src_dir_pathname/src/GLdispatch/vnd-glapi/table.c \
$src_dir_pathname/src/GLdispatch/vnd-glapi/u_current_tsd.c \
$src_dir_pathname/src/GLdispatch/vnd-glapi/entry_pure_c.c \
"
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c

	printf "CPP GLAPI $src_c_pathname --> $build_dir_pathname/src/GLdispatch/vnd-glapi/$cpp_filename\n"
	$cpp -o $build_dir_pathname/src/GLdispatch/vnd-glapi/$cpp_filename $src_c_pathname \
		$syslib_cpp_flags_inc \
		$cc_cpp_flags_inc \
		-I$build_dir_pathname/src/util \
		-I$src_dir_pathname/src/util \
		-I$build_dir_pathname/include \
		-I$src_dir_pathname/include \
		$cc_cpp_flags_defs \
		$syslib_cpp_flags_defs \
		$libglvnd_cpp_flags \
		-DMAPI_ABI_HEADER=\"$build_dir_pathname/src/generate/glapi_mapi_tmp.h\" &
done

wait

for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c
	o_filename=$(basename $src_c_pathname .c).o
	libglapi_a="$libglapi_a $build_dir_pathname/src/GLdispatch/vnd-glapi/$o_filename"

	printf "CC GLAPI $build_dir_pathname/src/GLdispatch/vnd-glapi/$cpp_filename --> $build_dir_pathname/src/GLdispatch/vnd-glapi/$o_filename\n"
	$cc -c -o $build_dir_pathname/src/GLdispatch/vnd-glapi/$o_filename $build_dir_pathname/src/GLdispatch/vnd-glapi/$cpp_filename &
done

wait

# XXX: you must create an archive in order to get most linkers resolve circular
# dependencies between objects in the archive.
rm -f $build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi.a 
printf "AR $build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi.a $libglapi_a\n"
$ar rcs $build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi.a $libglapi_a
) &
# libglapi
#==============================================================================
# glapi_{gl,opengl} ({glesv1,glesv2})
for api in gl opengl
do
(
	src_c_pathnames="\
	$src_dir_pathname/src/GLdispatch/vnd-glapi/stub.c \
	$src_dir_pathname/src/GLdispatch/vnd-glapi/entry_pure_c.c \
	"
	for src_c_pathname in $src_c_pathnames
	do
		cpp_filename=$(basename $src_c_pathname .c).$api.cpp.c
	
		printf "CPP GLAPI($api) $src_c_pathname --> $build_dir_pathname/src/GLdispatch/vnd-glapi/$cpp_filename\n"
		$cpp -o $build_dir_pathname/src/GLdispatch/vnd-glapi/$cpp_filename $src_c_pathname \
			$syslib_cpp_flags_inc \
			$cc_cpp_flags_inc \
			-I$build_dir_pathname/src/util \
			-I$src_dir_pathname/src/util \
			-I$build_dir_pathname/include \
			-I$src_dir_pathname/include \
			$cc_cpp_flags_defs \
			$syslib_cpp_flags_defs \
			$libglvnd_cpp_flags \
			-DSTATIC_DISPATCH_ONLY \
			-DMAPI_ABI_HEADER=\"$build_dir_pathname/src/generate/g_glapi_mapi_${api}_tmp.h\"
	done

	wait

	os=
	for src_c_pathname in $src_c_pathnames
	do
		cpp_filename=$(basename $src_c_pathname .c).$api.cpp.c
		o_filename=$(basename $src_c_pathname .c).$api.o
		os="$os $build_dir_pathname/src/GLdispatch/vnd-glapi/$o_filename"
	
		printf "CC GLAPI($api) $build_dir_pathname/src/GLdispatch/vnd-glapi/$cpp_filename --> $build_dir_pathname/src/GLdispatch/vnd-glapi/$o_filename\n"
		$cc -c -o $build_dir_pathname/src/GLdispatch/vnd-glapi/$o_filename $build_dir_pathname/src/GLdispatch/vnd-glapi/$cpp_filename &
	done
	eval glapi_${api}_a=\"$os\"
	# XXX: you must create an archive in order to get most linkers resolve circular
	# dependencies between objects in the archive.
	rm -f $build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi_${api}.a 

	wait

	eval printf \"AR $build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi_${api}.a \$glapi_${api}_a\\n\"
	eval $ar rcs $build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi_${api}.a \$glapi_${api}_a
) &
done
# glapi_{gl,opengl} ({glesv1,glesv2}
#==============================================================================
# GLdispatch/vnd-glapi
################################################################################

wait

################################################################################
# GLdispatch
#-------------------------------------------------------------------------------
printf "CPP $src_dir_pathname/src/GLdispatch/GLdispatch.c --> $build_dir_pathname/src/GLdispatch/GLdispatch.cpp.c\n"
$cpp -o $build_dir_pathname/src/GLdispatch/GLdispatch.cpp.c $src_dir_pathname/src/GLdispatch/GLdispatch.c \
	$syslib_cpp_flags_inc \
	$cc_cpp_flags_inc \
	-I$build_dir_pathname/src/GLdispatch/vnd-glapi \
	-I$src_dir_pathname/src/GLdispatch/vnd-glapi \
	-I$build_dir_pathname/src/util \
	-I$src_dir_pathname/src/util \
	-I$build_dir_pathname/include \
	-I$src_dir_pathname/include \
	$cc_cpp_flags_defs \
	$syslib_cpp_flags_defs \
	$libglvnd_cpp_flags

printf "CC $build_dir_pathname/src/GLdispatch/GLdispatch.cpp.c --> $build_dir_pathname/src/GLdispatch/GLdispatch.o\n"
$cc -c -o $build_dir_pathname/src/GLdispatch/GLdispatch.o $build_dir_pathname/src/GLdispatch/GLdispatch.cpp.c \
#-------------------------------------------------------------------------------
printf "BINUTILS LD SLIB $build_dir_pathname/install_root$lib_dir_pathname/libGLdispatch.so.0.0.0\n"
$binutils_ld -o $build_dir_pathname/install_root$lib_dir_pathname/libGLdispatch.so.0.0.0 \
	-soname libGLdispatch.so.0 \
	--version-script=$src_dir_pathname/src/GLdispatch/export_list_tsd.ver \
	-shared --no-undefined -s \
	$syslib_ld_slib_start_files \
	$cc_ld_slib_start_files \
		$build_dir_pathname/src/GLdispatch/GLdispatch.o \
		$build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi.a \
		$build_dir_pathname/src/util/trace.o \
		$build_dir_pathname/src/util/glvnd_pthread.o \
		$build_dir_pathname/src/util/app_error_check.o \
		$build_dir_pathname/src/util/utils_misc.o \
		$syslib_libdl_ld_flags \
		$syslib_libc_ld_flags \
	$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files &
ln -sTf libGLdispatch.so.0.0.0 $build_dir_pathname/install_root$lib_dir_pathname/libGLdispatch.so.0
# GLdispatch
################################################################################

wait

################################################################################
# openGL (without GLX)
#-------------------------------------------------------------------------------
(
mkdir -p $build_dir_pathname/src/OpenGL
#-------------------------------------------------------------------------------
printf "CPP $src_dir_pathname/src/OpenGL/libopengl.c --> $build_dir_pathname/src/OpenGL/libopengl.cpp.c\n"
$cpp -o $build_dir_pathname/src/OpenGL/libopengl.cpp.c $src_dir_pathname/src/OpenGL/libopengl.c \
	$syslib_cpp_flags_inc \
	$cc_cpp_flags_inc \
	-I$build_dir_pathname/src/GLdispatch/vnd-glapi \
	-I$src_dir_pathname/src/GLdispatch/vnd-glapi \
	-I$build_dir_pathname/src/GLdispatch \
	-I$src_dir_pathname/src/GLdispatch \
	-I$build_dir_pathname/src/util \
	-I$src_dir_pathname/src/util \
	-I$build_dir_pathname/include \
	-I$src_dir_pathname/include \
	$cc_cpp_flags_defs \
	$syslib_cpp_flags_defs \
	$libglvnd_cpp_flags

printf "CC $build_dir_pathname/src/OpenGL/libopengl.cpp.c --> $build_dir_pathname/src/OpenGL/libopengl.o\n"
$cc -c -o $build_dir_pathname/src/OpenGL/libopengl.o $build_dir_pathname/src/OpenGL/libopengl.cpp.c
#-------------------------------------------------------------------------------
printf "BINUTILS LD SLIB $build_dir_pathname/install_root$lib_dir_pathname/libOpenGL.so.0.0.0\n"
# FIXME: This is not using a version script, must force inclusion with
# --[no-]whole-archive or we could miss some public symbols. There is
# $src_dir_pathname/src/OpenGL/ogl.symbols text file to check we have all
# symbols.
$binutils_ld -o $build_dir_pathname/install_root$lib_dir_pathname/libOpenGL.so.0.0.0 \
	-soname libOpenGL.so.0 \
	-shared --no-undefined -s \
	$syslib_ld_slib_start_files \
	$cc_ld_slib_start_files \
		--whole-archive \
			$build_dir_pathname/src/OpenGL/libopengl.o \
			$build_dir_pathname/src/util/utils_misc.o \
			$build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi_opengl.a \
		--no-whole-archive \
		$build_dir_pathname/install_root$lib_dir_pathname/libGLdispatch.so.0.0.0 \
		$syslib_libc_ld_flags \
	$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
# libOpenGL is expected to be dynamically loaded
ln -sTf libOpenGL.so.0.0.0 $build_dir_pathname/install_root$lib_dir_pathname/libOpenGL.so.0
) &
# openGL (without GLX)
################################################################################


################################################################################
# EGL (legacy xorg/xwayland glamor on on drm/gbm platform)
#-------------------------------------------------------------------------------
(
mkdir -p $build_dir_pathname/src/EGL
#-------------------------------------------------------------------------------
src_c_pathnames="\
$src_dir_pathname/src/EGL/egldispatchstubs.c \
$build_dir_pathname/src/generate/g_egldispatchstubs.c \
"

for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c

	printf "CPP $src_c_pathname --> $build_dir_pathname/src/EGL/$cpp_filename\n"
	$cpp -o $build_dir_pathname/src/EGL/$cpp_filename $src_c_pathname \
		$syslib_cpp_flags_inc \
		$cc_cpp_flags_inc \
		-I$build_dir_pathname/src/EGL \
		-I$src_dir_pathname/src/EGL \
		-I$build_dir_pathname/src/generate \
		-I$src_dir_pathname/src/generate \
		-I$build_dir_pathname/include \
		-I$src_dir_pathname/include \
		$cc_cpp_flags_defs \
		$syslib_cpp_flags_defs \
		$libglvnd_cpp_flags &
done

wait

for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c
	o_filename=$(basename $src_c_pathname .c).o

	printf "CC $build_dir_pathname/src/EGL/$cpp_filename --> $build_dir_pathname/src/EGL/$o_filename\n"
	$cc -c -o $build_dir_pathname/src/EGL/$o_filename $build_dir_pathname/src/EGL/$cpp_filename &
done
#-------------------------------------------------------------------------------
src_c_pathnames="\
$src_dir_pathname/src/EGL/libegl.c \
$src_dir_pathname/src/EGL/libeglcurrent.c \
$src_dir_pathname/src/EGL/libeglmapping.c \
$src_dir_pathname/src/EGL/libeglvendor.c \
$src_dir_pathname/src/EGL/libeglerror.c \
"
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c

	printf "CPP $src_c_pathname --> $build_dir_pathname/src/EGL/$cpp_filename\n"
	$cpp -o $build_dir_pathname/src/EGL/$cpp_filename $src_c_pathname \
		$syslib_cpp_flags_inc \
		$cc_cpp_flags_inc \
		-I$build_dir_pathname/src/GLdispatch \
		-I$src_dir_pathname/src/GLdispatch \
		-I$build_dir_pathname/src/util/uthash/include \
		-I$src_dir_pathname/src/util/uthash/include \
		-I$build_dir_pathname/src/util \
		-I$src_dir_pathname/src/util \
		-I$build_dir_pathname/include \
		-I$src_dir_pathname/include \
		$cc_cpp_flags_defs \
		$syslib_cpp_flags_defs \
		$libglvnd_cpp_flags \
		-DDEFAULT_EGL_VENDOR_CONFIG_DIRS=\"$sysconf_dir_pathname/glvnd/egl_vendor.d:$data_dir_pathname/glvnd/egl_vendor.d\" &
done

wait

os=
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c
	o_filename=$(basename $src_c_pathname .c).o
	os="$os $build_dir_pathname/src/EGL/$o_filename"

	printf "CC $build_dir_pathname/src/EGL/$cpp_filename --> $build_dir_pathname/src/EGL/$o_filename\n"
	$cc -c -o $build_dir_pathname/src/EGL/$o_filename $build_dir_pathname/src/EGL/$cpp_filename &
done

wait

rm -f $build_dir_pathname/src/EGL/libEGL_static.a 
printf "AR $build_dir_pathname/src/EGL/libEGL_static.a $os\n"
$ar rcs $build_dir_pathname/src/EGL/libEGL_static.a $os
#-------------------------------------------------------------------------------
printf "BINUTILS LD SLIB $build_dir_pathname/install_root$lib_dir_pathname/libEGL.so.1.1.0\n"
# FIXME: This is not using a version script, must force inclusion with
# --[no-]whole-archive or we could miss some public symbols. There is
# $src_dir_pathname/src/EGL/egl.symbols text file to check we have all
# symbols.
$binutils_ld -o $build_dir_pathname/install_root$lib_dir_pathname/libEGL.so.1.1.0 \
	-soname libEGL.so.1 \
	-shared --no-undefined -s \
	-Bsymbolic \
	$syslib_ld_slib_start_files \
	$cc_ld_slib_start_files \
		--whole-archive \
			$build_dir_pathname/src/EGL/libEGL_static.a \
			$build_dir_pathname/src/EGL/egldispatchstubs.o \
			$build_dir_pathname/src/EGL/g_egldispatchstubs.o \
		--no-whole-archive \
		$build_dir_pathname/src/util/trace.o \
		$build_dir_pathname/src/util/utils_misc.o \
		$build_dir_pathname/src/util/cJSON.o \
		$build_dir_pathname/src/util/glvnd_pthread.o \
		$build_dir_pathname/src/util/winsys_dispatch.o \
		$build_dir_pathname/install_root$lib_dir_pathname/libGLdispatch.so.0.0.0 \
		$syslib_libdl_ld_flags \
		$syslib_libc_ld_flags \
	$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
# It is supposed to be dynamically loaded.
ln -sTf libEGL.so.1.1.0 $build_dir_pathname/install_root$lib_dir_pathname/libEGL.so.1
) &
# EGL
################################################################################


################################################################################
# GLX
# FIXME:requiring libX11 instead of xcb?
#-------------------------------------------------------------------------------
(
mkdir -p $build_dir_pathname/src/GLX
#-------------------------------------------------------------------------------
src_c_pathnames="\
$src_dir_pathname/src/GLX/libglx.c \
$src_dir_pathname/src/GLX/libglxmapping.c \
$src_dir_pathname/src/GLX/libglxproto.c \
$src_dir_pathname/src/GLX/glvnd_genentry.c \
"
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c

	printf "CPP $src_c_pathname --> $build_dir_pathname/src/GLX/$cpp_filename\n"
	$cpp -o $build_dir_pathname/src/GLX/$cpp_filename $src_c_pathname \
		$syslib_cpp_flags_inc \
		$cc_cpp_flags_inc \
		-I$build_dir_pathname/src/GLdispatch \
		-I$src_dir_pathname/src/GLdispatch \
		-I$build_dir_pathname/src/util/uthash/include \
		-I$src_dir_pathname/src/util/uthash/include \
		-I$build_dir_pathname/src/util \
		-I$src_dir_pathname/src/util \
		-I$build_dir_pathname/include \
		-I$src_dir_pathname/include \
		-I$libx11_inc_dir_pathname \
		-I$xorgproto_inc_dir_pathname \
		$cc_cpp_flags_defs \
		$syslib_cpp_flags_defs \
		$libglvnd_cpp_flags &
done

wait

os=
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c
	o_filename=$(basename $src_c_pathname .c).o
	os="$os $build_dir_pathname/src/GLX/$o_filename"

	printf "CC $build_dir_pathname/src/GLX/$cpp_filename --> $build_dir_pathname/src/GLX/$o_filename\n"
	$cc -c -o $build_dir_pathname/src/GLX/$o_filename $build_dir_pathname/src/GLX/$cpp_filename &
done

wait

rm -f $build_dir_pathname/src/GLX/libglx_static.a 
printf "AR $build_dir_pathname/src/GLX/libglx_static.a $os\n"
$ar rcs $build_dir_pathname/src/GLX/libglx_static.a $os
#-------------------------------------------------------------------------------
printf "BINUTILS LD SLIB $build_dir_pathname/install_root$lib_dir_pathname/libGLX.so.0.0.0\n"
# FIXME:This is not using a version script, must force inclusion with
# --[no-]whole-archive or we could miss some public symbols. There is
# $src_dir_pathname/src/GLX/glx.symbols text file to check we have all
# symbols.
$binutils_ld -o $build_dir_pathname/install_root$lib_dir_pathname/libGLX.so.0.0.0 \
	-soname libGLX.so.0 \
	-shared --no-undefined -s \
	-Bsymbolic \
	$syslib_ld_slib_start_files \
	$cc_ld_slib_start_files \
		--whole-archive \
			$build_dir_pathname/src/GLX/libglx_static.a \
		--no-whole-archive \
		$build_dir_pathname/src/util/winsys_dispatch.o \
		$build_dir_pathname/src/util/trace.o \
		$build_dir_pathname/src/util/utils_misc.o \
		$build_dir_pathname/src/util/app_error_check.o \
		$build_dir_pathname/src/util/glvnd_pthread.o \
		$build_dir_pathname/install_root$lib_dir_pathname/libGLdispatch.so.0.0.0 \
		-L$libx11_lib_dir_pathname -lX11 \
		$syslib_libdl_ld_flags \
		$syslib_libc_ld_flags \
	$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
# TODO:private?
ln -sTf libGLX.so.0.0.0 $build_dir_pathname/install_root$lib_dir_pathname/libGLX.so.0
) &
# GLX
################################################################################

wait

################################################################################
# GL (legacy OpenGL and GLX)
# FIXME: using libX11 instead of xcb?
#-------------------------------------------------------------------------------
(
mkdir -p $build_dir_pathname/src/GL
#-------------------------------------------------------------------------------
src_c_pathnames="\
$build_dir_pathname/src/generate/g_libglglxwrapper.c \
$src_dir_pathname/src/GL/libgl.c \
"
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c

	printf "CPP $src_c_pathname --> $build_dir_pathname/src/GL/$cpp_filename\n"
	$cpp -o $build_dir_pathname/src/GL/$cpp_filename $src_c_pathname \
		$syslib_cpp_flags_inc \
		$cc_cpp_flags_inc \
		-I$build_dir_pathname/src/GLX \
		-I$src_dir_pathname/src/GLX \
		-I$build_dir_pathname/src/GLdispatch/vnd-glapi \
		-I$src_dir_pathname/src/GLdispatch/vnd-glapi \
		-I$build_dir_pathname/src/GLdispatch \
		-I$src_dir_pathname/src/GLdispatch \
		-I$build_dir_pathname/src/util \
		-I$src_dir_pathname/src/util \
		-I$build_dir_pathname/include \
		-I$src_dir_pathname/include \
		-I$libx11_inc_dir_pathname \
		-I$xorgproto_inc_dir_pathname \
		$cc_cpp_flags_defs \
		$syslib_cpp_flags_defs \
		$libglvnd_cpp_flags &
done

wait

os=
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c
	o_filename=$(basename $src_c_pathname .c).o
	os="$os $build_dir_pathname/src/GL/$o_filename"

	printf "CC $build_dir_pathname/src/GL/$cpp_filename --> $build_dir_pathname/src/GL/$o_filename\n"
	$cc -c -o $build_dir_pathname/src/GL/$o_filename $build_dir_pathname/src/GL/$cpp_filename &
done

wait

rm -f $build_dir_pathname/src/GL/libgl_static.a 
printf "AR $build_dir_pathname/src/GL/libgl_static.a $os\n"
$ar rcs $build_dir_pathname/src/GL/libgl_static.a $os
#-------------------------------------------------------------------------------
printf "BINUTILS LD SLIB $build_dir_pathname/install_root$lib_dir_pathname/libGL.so.1.7.0\n"
# FIXME:This is not using a version script, must force inclusion with
# --[no-]whole-archive or we could miss some public symbols. There is
# $src_dir_pathname/src/GL/gl.symbols text file to check we have all
# symbols.
$binutils_ld -o $build_dir_pathname/install_root$lib_dir_pathname/libGL.so.1.7.0 \
	-soname libGL.so.1 \
	-shared --no-undefined -s \
	-Bsymbolic \
	$syslib_ld_slib_start_files \
	$cc_ld_slib_start_files \
		--whole-archive \
			$build_dir_pathname/src/GL/libgl_static.a \
		--no-whole-archive \
		$build_dir_pathname/src/GLdispatch/vnd-glapi/libglapi_gl.a \
		$build_dir_pathname/src/util/utils_misc.o \
		$build_dir_pathname/install_root$lib_dir_pathname/libGLdispatch.so.0.0.0 \
		$build_dir_pathname/install_root$lib_dir_pathname/libGLX.so.0.0.0 \
		-L$libx11_lib_dir_pathname -lX11 \
		$syslib_libdl_ld_flags \
		$syslib_libc_ld_flags \
	$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
# Should not be use anymore. Only legacy binaries with static loading.
ln -sTf libGL.so.1.7.0 $build_dir_pathname/install_root$lib_dir_pathname/libGL.so.1
) &
# GL (legacy OpenGL and GLX)
################################################################################


################################################################################
# glvnd include files
rm -Rf $build_dir_pathname/install_root$inc_dir_pathname/glvnd
mkdir -p $build_dir_pathname/install_root$inc_dir_pathname/glvnd
cp -rf 	$src_dir_pathname/include/glvnd/GLdispatchABI.h \
	$src_dir_pathname/include/glvnd/libeglabi.h \
	$src_dir_pathname/include/glvnd/libglxabi.h \
		$build_dir_pathname/install_root$inc_dir_pathname/glvnd
# glvnd include files
################################################################################
wait
