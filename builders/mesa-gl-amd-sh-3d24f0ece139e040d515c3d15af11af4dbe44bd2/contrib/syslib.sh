# syslib configuration here
#-------------------------------------------------------------------------------
if test "${syslib_incdir-unset}" = unset; then
syslib_incdir=/nyan/glibc/current/include
fi
#-------------------------------------------------------------------------------
# for a glibc
syslib_cpp_flags_defs="\
-D_GNU_SOURCE=1 \
-DHAVE_SYS_SYSCTL_H=1 \
-DHAVE_ENDIAN_H=1 \
-DHAVE_DLFCN_H=1 \
-DHAVE_UNISTD_H=1 \
\
-DMAJOR_IN_SYSMACROS=1 \
-DHAVE_STRTOF=1 \
-DHAVE_MKOSTEMP=1 \
-DHAVE_TIMESPEC_GET=1 \
-DHAVE_STRTOD_L=1 \
-DHAVE_DLADDR=1 \
-DHAVE_DL_ITERATE_PHDR=1 \
-DHAVE_PTHREAD=1 \
-DHAVE_PTHREAD_SETAFFINITY=1 \
-DHAVE_TIMESPEC_GET=1 \
-DHAVE_STRUCT_TIMESPEC=1 \
-DHAVE_REALLOCARRAY=1 \
-DHAVE_SECURE_GETENV=1 \
\
-DHAVE_PROGRAM_INVOCATION_NAME=1 \
-DHAVE_FLOCK=1 \
\
-DHAVE_GNU_QSORT_R=1 \
-DHAVE_RANDOM_R=1 \
-DHAVE_STRTOK_R=1 \
-DHAVE_GETRANDOM=1 \
\
-DHAVE_POSIX_FALLOCATE=1 \
-DHAVE_POSIX_MEMALIGN=1 \
\
-DHAVE_ISSIGNALING=1 \
\
-DHAVE_DIRENT_D_TYPE=1 \
"
#-------------------------------------------------------------------------------
# syslib specific start/end files for linking a Shared LIBrary (slib), they
# appear before/after the compiler start/end files while linking. Here, as an
# example, look at the compiler driver $gcc_src/gcc/config/gnu-user.h (13.2.0)
# if you want to understand a bit more in the case of glibc, shared libs do fall
# in the "shared" category (there, you can see the object requirement of PIE
# static-PIE, etc etc).
# GNU toolchain have a circular dependency between the gcc support library,
# libgcc, and the glibc itself, namely you have to handle that with your linker
# (binutils ld has --{start,end}-group, or YOU USUALLY HAVE TO REPEAT THE
# LIBRARY FILES (XXX: DOES NOT WORK WITH OBJECT FILES) ON THE LINK COMMAND LINE
# UNTIL YOU BREAK THE CIRCULAR DEPENDENCY.
# We want full pathnames.
if test "${syslib_ld_slib_start_files-unset}" = unset; then
syslib_ld_slib_start_files=/nyan/glibc/current/lib/crti.o
fi
if test "${syslib_ld_slib_end_files-unset}" = unset; then
syslib_ld_slib_end_files=/nyan/glibc/current/lib/crtn.o
fi
#-------------------------------------------------------------------------------
if test "${syslibs_libc_ld_flags-unset}" = unset; then
syslibs_libc_ld_flags='-L/nyan/glibc/current/lib -lc'
fi
if test "${syslibs_libdl_ld_flags-unset}" = unset; then
syslibs_libdl_ld_flags='-L/nyan/glibc/current/lib -ldl'
fi
if test "${syslibs_libpthread_ld_flags-unset}" = unset; then
syslibs_libpthread_ld_flags='-L/nyan/glibc/current/lib -lpthread'
fi
# libm.a was not compiled with -fPIC -fpic by glibc sdk...
if test "${syslibs_libm_ld_flags-unset}" = unset; then
syslibs_libm_ld_flags='-L/nyan/glibc/current/lib -lm'
fi
#-------------------------------------------------------------------------------
