printf "Running egl code generators-->\n"
mkdir $build_dir/src
mkdir $build_dir/src/egl
mkdir $build_dir/src/egl/generate
mkdir $build_dir/src/egl/drivers
mkdir $build_dir/src/egl/drivers/dri2
#===================================================================================================
# glvnd
#---------------------------------------------------------------------------------------------------
$python3 $src_dir/src/egl/generate/gen_egl_dispatch.py \
	source \
	$src_dir/src/egl/generate/egl.xml \
	$src_dir/src/egl/generate/egl_other.xml \
	>$build_dir/src/egl/generate/g_egldispatchstubs.c &
#---------------------------------------------------------------------------------------------------
$python3 $src_dir/src/egl/generate/gen_egl_dispatch.py \
	header \
	$src_dir/src/egl/generate/egl.xml \
	$src_dir/src/egl/generate/egl_other.xml \
	>$build_dir/src/egl/generate/g_egldispatchstubs.h &
#---------------------------------------------------------------------------------------------------
printf "<--egl code generation done\n"
