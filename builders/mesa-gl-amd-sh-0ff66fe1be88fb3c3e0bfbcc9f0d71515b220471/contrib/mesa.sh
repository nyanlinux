echo "building opengl related common components-->"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/mesa
#-------------------------------------------------------------------------------
# filenames should be different since we build in one directory
libmesa_common_c_pathnames="\
$build_dir/src/mesa/format_fallback.c \
$build_dir/src/mesa/program/lex.yy.c \
$build_dir/src/mesa/program/program_parse.tab.c \
$build_dir/src/mapi/glapi/api_exec_init.c \
$build_dir/src/mapi/glapi/enums.c \
$build_dir/src/mapi/glapi/marshal_generated0.c \
$build_dir/src/mapi/glapi/marshal_generated1.c \
$build_dir/src/mapi/glapi/marshal_generated2.c \
$build_dir/src/mapi/glapi/marshal_generated3.c \
$build_dir/src/mapi/glapi/marshal_generated4.c \
$build_dir/src/mapi/glapi/marshal_generated5.c \
$build_dir/src/mapi/glapi/marshal_generated6.c \
$build_dir/src/mapi/glapi/marshal_generated7.c \
$build_dir/src/mapi/glapi/unmarshal_table.c \
$src_dir/src/mesa/program/arbprogparse.c \
$src_dir/src/mesa/program/prog_cache.c \
$src_dir/src/mesa/program/prog_instruction.c \
$src_dir/src/mesa/program/prog_parameter.c \
$src_dir/src/mesa/program/prog_parameter_layout.c \
$src_dir/src/mesa/program/prog_print.c \
$src_dir/src/mesa/program/program.c \
$src_dir/src/mesa/program/program_parse_extra.c \
$src_dir/src/mesa/program/prog_statevars.c \
$src_dir/src/mesa/program/symbol_table.c \
$src_dir/src/mesa/program/prog_to_nir.c \
$src_dir/src/mesa/main/accum.c \
$src_dir/src/mesa/main/api_arrayelt.c \
$src_dir/src/mesa/main/arbprogram.c \
$src_dir/src/mesa/main/arrayobj.c \
$src_dir/src/mesa/main/atifragshader.c \
$src_dir/src/mesa/main/attrib.c \
$src_dir/src/mesa/main/barrier.c \
$src_dir/src/mesa/main/bbox.c \
$src_dir/src/mesa/main/blend.c \
$src_dir/src/mesa/main/blit.c \
$src_dir/src/mesa/main/bufferobj.c \
$src_dir/src/mesa/main/buffers.c \
$src_dir/src/mesa/main/clear.c \
$src_dir/src/mesa/main/clip.c \
$src_dir/src/mesa/main/compute.c \
$src_dir/src/mesa/main/condrender.c \
$src_dir/src/mesa/main/conservativeraster.c \
$src_dir/src/mesa/main/context.c \
$src_dir/src/mesa/main/copyimage.c \
$src_dir/src/mesa/main/debug.c \
$src_dir/src/mesa/main/debug_output.c \
$src_dir/src/mesa/main/depth.c \
$src_dir/src/mesa/main/dlist.c \
$src_dir/src/mesa/main/draw.c \
$src_dir/src/mesa/main/drawpix.c \
$src_dir/src/mesa/main/drawtex.c \
$src_dir/src/mesa/main/draw_validate.c \
$src_dir/src/mesa/main/enable.c \
$src_dir/src/mesa/main/errors.c \
$src_dir/src/mesa/main/eval.c \
$src_dir/src/mesa/main/extensions.c \
$src_dir/src/mesa/main/extensions_table.c \
$src_dir/src/mesa/main/externalobjects.c \
$src_dir/src/mesa/main/fbobject.c \
$src_dir/src/mesa/main/feedback.c \
$src_dir/src/mesa/main/ff_fragment_shader.c \
$src_dir/src/mesa/main/ffvertex_prog.c \
$src_dir/src/mesa/main/fog.c \
$src_dir/src/mesa/main/formatquery.c \
$src_dir/src/mesa/main/formats.c \
$src_dir/src/mesa/main/format_utils.c \
$src_dir/src/mesa/main/framebuffer.c \
$src_dir/src/mesa/main/get.c \
$src_dir/src/mesa/main/genmipmap.c \
$src_dir/src/mesa/main/getstring.c \
$src_dir/src/mesa/main/glformats.c \
$src_dir/src/mesa/main/glspirv.c \
$src_dir/src/mesa/main/glthread.c \
$src_dir/src/mesa/main/glthread_bufferobj.c \
$src_dir/src/mesa/main/glthread_draw.c \
$src_dir/src/mesa/main/glthread_draw_unroll.c \
$src_dir/src/mesa/main/glthread_get.c \
$src_dir/src/mesa/main/glthread_list.c \
$src_dir/src/mesa/main/glthread_pixels.c \
$src_dir/src/mesa/main/glthread_shaderobj.c \
$src_dir/src/mesa/main/glthread_varray.c \
$src_dir/src/mesa/main/hash.c \
$src_dir/src/mesa/main/hint.c \
$src_dir/src/mesa/main/image.c \
$src_dir/src/mesa/main/light.c \
$src_dir/src/mesa/main/lines.c \
$src_dir/src/mesa/main/matrix.c \
$src_dir/src/mesa/main/mipmap.c \
$src_dir/src/mesa/main/multisample.c \
$src_dir/src/mesa/main/objectlabel.c \
$src_dir/src/mesa/main/pack.c \
$src_dir/src/mesa/main/pbo.c \
$src_dir/src/mesa/main/performance_monitor.c \
$src_dir/src/mesa/main/performance_query.c \
$src_dir/src/mesa/main/pipelineobj.c \
$src_dir/src/mesa/main/pixel.c \
$src_dir/src/mesa/main/pixelstore.c \
$src_dir/src/mesa/main/pixeltransfer.c \
$src_dir/src/mesa/main/points.c \
$src_dir/src/mesa/main/polygon.c \
$src_dir/src/mesa/main/program_binary.c \
$src_dir/src/mesa/main/program_resource.c \
$src_dir/src/mesa/main/querymatrix.c \
$src_dir/src/mesa/main/queryobj.c \
$src_dir/src/mesa/main/rastpos.c \
$src_dir/src/mesa/main/readpix.c \
$src_dir/src/mesa/main/renderbuffer.c \
$src_dir/src/mesa/main/robustness.c \
$src_dir/src/mesa/main/samplerobj.c \
$src_dir/src/mesa/main/scissor.c \
$src_dir/src/mesa/main/shaderapi.c \
$src_dir/src/mesa/main/shaderimage.c \
$src_dir/src/mesa/main/shaderobj.c \
$src_dir/src/mesa/main/shared.c \
$src_dir/src/mesa/main/spirv_capabilities.c \
$src_dir/src/mesa/main/spirv_extensions.c \
$src_dir/src/mesa/main/state.c \
$src_dir/src/mesa/main/stencil.c \
$src_dir/src/mesa/main/syncobj.c \
$src_dir/src/mesa/main/texcompress.c \
$src_dir/src/mesa/main/texcompress_bptc.c \
$src_dir/src/mesa/main/texcompress_cpal.c \
$src_dir/src/mesa/main/texcompress_etc.c \
$src_dir/src/mesa/main/texcompress_fxt1.c \
$src_dir/src/mesa/main/texcompress_rgtc.c \
$src_dir/src/mesa/main/texcompress_s3tc.c \
$src_dir/src/mesa/main/texenv.c \
$src_dir/src/mesa/main/texenvprogram.h \
$src_dir/src/mesa/main/texgen.c \
$src_dir/src/mesa/main/texgetimage.c \
$src_dir/src/mesa/main/teximage.c \
$src_dir/src/mesa/main/texobj.c \
$src_dir/src/mesa/main/texparam.c \
$src_dir/src/mesa/main/texstate.c \
$src_dir/src/mesa/main/texstorage.c \
$src_dir/src/mesa/main/texstore.c \
$src_dir/src/mesa/main/texturebindless.c \
$src_dir/src/mesa/main/textureview.c \
$src_dir/src/mesa/main/transformfeedback.c \
$src_dir/src/mesa/main/uniforms.c \
$src_dir/src/mesa/main/varray.c \
$src_dir/src/mesa/main/vdpau.c \
$src_dir/src/mesa/main/version.c \
$src_dir/src/mesa/main/viewport.c \
$src_dir/src/mesa/main/es1_conversion.c \
$src_dir/src/mesa/math/m_eval.c \
$src_dir/src/mesa/math/m_matrix.c \
$src_dir/src/mesa/vbo/vbo_context.c \
$src_dir/src/mesa/vbo/vbo_exec_api.c \
$src_dir/src/mesa/vbo/vbo_exec.c \
$src_dir/src/mesa/vbo/vbo_exec_draw.c \
$src_dir/src/mesa/vbo/vbo_exec_eval.c \
$src_dir/src/mesa/vbo/vbo_minmax_index.c \
$src_dir/src/mesa/vbo/vbo_noop.c \
$src_dir/src/mesa/vbo/vbo_save_api.c \
$src_dir/src/mesa/vbo/vbo_save.c \
$src_dir/src/mesa/vbo/vbo_save_draw.c \
$src_dir/src/mesa/vbo/vbo_save_loopback.c \
"
# from the trashiest and toxiciest coders
libmesa_common_cxx_pathnames="\
$src_dir/src/mesa/main/shader_query.cpp \
$src_dir/src/mesa/main/texcompress_astc.cpp \
$src_dir/src/mesa/main/uniform_query.cpp \
"
libmesa_gallium_c_pathnames="\
$src_dir/src/mesa/state_tracker/st_atifs_to_nir.c \
$src_dir/src/mesa/state_tracker/st_atom_atomicbuf.c \
$src_dir/src/mesa/state_tracker/st_atom_blend.c \
$src_dir/src/mesa/state_tracker/st_atom_clip.c \
$src_dir/src/mesa/state_tracker/st_atom_constbuf.c \
$src_dir/src/mesa/state_tracker/st_atom_depth.c \
$src_dir/src/mesa/state_tracker/st_atom_framebuffer.c \
$src_dir/src/mesa/state_tracker/st_atom_image.c \
$src_dir/src/mesa/state_tracker/st_atom_msaa.c \
$src_dir/src/mesa/state_tracker/st_atom_pixeltransfer.c \
$src_dir/src/mesa/state_tracker/st_atom_rasterizer.c \
$src_dir/src/mesa/state_tracker/st_atom_sampler.c \
$src_dir/src/mesa/state_tracker/st_atom_scissor.c \
$src_dir/src/mesa/state_tracker/st_atom_shader.c \
$src_dir/src/mesa/state_tracker/st_atom_stipple.c \
$src_dir/src/mesa/state_tracker/st_atom_storagebuf.c \
$src_dir/src/mesa/state_tracker/st_atom_tess.c \
$src_dir/src/mesa/state_tracker/st_atom_texture.c \
$src_dir/src/mesa/state_tracker/st_atom_viewport.c \
$src_dir/src/mesa/state_tracker/st_cb_bitmap.c \
$src_dir/src/mesa/state_tracker/st_cb_clear.c \
$src_dir/src/mesa/state_tracker/st_cb_copyimage.c \
$src_dir/src/mesa/state_tracker/st_cb_drawpixels.c \
$src_dir/src/mesa/state_tracker/st_cb_drawtex.c \
$src_dir/src/mesa/state_tracker/st_cb_eglimage.c \
$src_dir/src/mesa/state_tracker/st_cb_feedback.c \
$src_dir/src/mesa/state_tracker/st_cb_flush.c \
$src_dir/src/mesa/state_tracker/st_cb_rasterpos.c \
$src_dir/src/mesa/state_tracker/st_cb_readpixels.c \
$src_dir/src/mesa/state_tracker/st_cb_texture.c \
$src_dir/src/mesa/state_tracker/st_context.c \
$src_dir/src/mesa/state_tracker/st_copytex.c \
$src_dir/src/mesa/state_tracker/st_debug.c \
$src_dir/src/mesa/state_tracker/st_draw.c \
$src_dir/src/mesa/state_tracker/st_draw_feedback.c \
$src_dir/src/mesa/state_tracker/st_draw_hw_select.c \
$src_dir/src/mesa/state_tracker/st_extensions.c \
$src_dir/src/mesa/state_tracker/st_format.c \
$src_dir/src/mesa/state_tracker/st_gen_mipmap.c \
$src_dir/src/mesa/state_tracker/st_interop.c \
$src_dir/src/mesa/state_tracker/st_manager.c \
$src_dir/src/mesa/state_tracker/st_nir_builtins.c \
$src_dir/src/mesa/state_tracker/st_nir_lower_builtin.c \
$src_dir/src/mesa/state_tracker/st_nir_lower_fog.c \
$src_dir/src/mesa/state_tracker/st_nir_lower_position_invariant.c \
$src_dir/src/mesa/state_tracker/st_nir_lower_tex_src_plane.c \
$src_dir/src/mesa/state_tracker/st_nir_unlower_io_to_vars.c \
$src_dir/src/mesa/state_tracker/st_pbo.c \
$src_dir/src/mesa/state_tracker/st_pbo_compute.c \
$src_dir/src/mesa/state_tracker/st_program.c \
$src_dir/src/mesa/state_tracker/st_sampler_view.c \
$src_dir/src/mesa/state_tracker/st_scissor.c \
$src_dir/src/mesa/state_tracker/st_shader_cache.c \
$src_dir/src/mesa/state_tracker/st_texcompress_compute.c \
$src_dir/src/mesa/state_tracker/st_texture.c \
$src_dir/src/mesa/state_tracker/st_vdpau.c \
"
# more c++ diarrhea
libmesa_gallium_cxx_pathnames="\
$src_dir/src/mesa/state_tracker/st_atom_array.cpp \
$src_dir/src/mesa/state_tracker/st_glsl_to_nir.cpp \
"
#-------------------------------------------------------------------------------
for src_pathname in $libmesa_common_c_pathnames $libmesa_gallium_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/mesa/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/mesa/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mesa/main \
		-I$src_dir/src/mesa/main \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/mapi/glapi \
		-I$src_dir/src/mapi/glapi \
		-I$build_dir/src/mapi \
		-I$src_dir/src/mapi \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src/ \
		-I$src_dir/src/ \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libmesa_common_c_pathnames $libmesa_gallium_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/mesa/$cpp_filename --> $build_dir/src/mesa/$asm_filename\n"
	$cc_s $build_dir/src/mesa/$cpp_filename -o $build_dir/src/mesa/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libmesa_common_c_pathnames $libmesa_gallium_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/mesa/$o_filename"
	printf "AS $build_dir/src/mesa/$asm_filename --> $build_dir/src/mesa/$o_filename\n"
	$as $build_dir/src/mesa/$asm_filename -o $build_dir/src/mesa/$o_filename &
done
#===============================================================================
for src_pathname in $libmesa_common_cxx_pathnames $libmesa_gallium_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	printf "CXXPP $src_pathname --> $build_dir/src/mesa/$cxxpp_filename\n"
	$cxxpp $src_pathname -o $build_dir/src/mesa/$cxxpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$cxx_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/compiler/glsl \
		-I$src_dir/src/compiler/glsl \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mapi/glapi \
		-I$src_dir/src/mapi/glapi \
		-I$build_dir/src/mapi \
		-I$src_dir/src/mapi \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libmesa_common_cxx_pathnames $libmesa_gallium_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	printf "CXX_S $build_dir/src/mesa/$cxxpp_filename --> $build_dir/src/mesa/$asm_filename\n"
	$cxx_s $build_dir/src/mesa/$cxxpp_filename -o $build_dir/src/mesa/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
# reuse os
for src_pathname in $libmesa_common_cxx_pathnames $libmesa_gallium_cxx_pathnames
do
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	o_filename=$(basename $src_pathname .cpp).cpp.cxx.s.o
	os="$os $build_dir/src/mesa/$o_filename"
	printf "AS $build_dir/src/mesa/$asm_filename --> $build_dir/src/mesa/$o_filename\n"
	$as $build_dir/src/mesa/$asm_filename -o $build_dir/src/mesa/$o_filename &
done
#------------------------------------------------------------------------------
#===============================================================================
wait
#===============================================================================
printf "AR RCS $build_dir/libmesa_gallium.a $os\n"
$ar_rcs $build_dir/libmesa_gallium.a $os &
#===============================================================================
echo "<--opengl related common components built"
