printf "\tbuilding winsys sub-components-->\n"
####################################################################################################
# we will put the compilation objects of all winsys-es there:
mkdir $build_dir/src
mkdir $build_dir/src/gallium
mkdir $build_dir/src/gallium/winsys
#---------------------------------------------------------------------------------------------------
libgallium_winsys_amdgpu_drm_c_pathnames="\
$src_dir/src/gallium/winsys/amdgpu/drm/amdgpu_bo.c \
$src_dir/src/gallium/winsys/amdgpu/drm/amdgpu_winsys.c \
$src_dir/src/gallium/winsys/amdgpu/drm/amdgpu_surface.c \
$src_dir/src/gallium/winsys/amdgpu/drm/amdgpu_userq.c \
"
# Blame the intellectual masturbator for that crap of c++ template (c11 generic
# is same same though) tantrum about code factorization at the price of pulling
# a c++ compiler:
# Marek Olšák <maraeo@gmail.com> (if he keeps doing that and does not fix it, 
# should be fired from amd, if still over there though since this is a red flag)
# The initial crappification did happen there:
# f933536517c98f7baf60d0fd5d8d0b0e49cb4592
# 3e118c6d2f7eb7aa138b96305d828bd9cc4e9e5b
libgallium_winsys_amdgpu_drm_cxx_pathnames="\
$src_dir/src/gallium/winsys/amdgpu/drm/amdgpu_cs.cpp \
"
#---------------------------------------------------------------------------------------------------
for src_pathname in $libgallium_winsys_amdgpu_drm_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/winsys/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/winsys/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/amd/common \
		-I$src_dir/src/amd/common \
		-I$build_dir/src/amd \
		-I$src_dir/src/amd \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
# we don't add cxx_internal_incdir because the crappification was "pure c++"
for src_pathname in $libgallium_winsys_amdgpu_drm_cxx_pathnames
do
	cpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	printf "CXXPP $src_pathname --> $build_dir/src/gallium/winsys/$cpp_filename\n"
	$cxxpp $src_pathname -o $build_dir/src/gallium/winsys/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/amd/common \
		-I$src_dir/src/amd/common \
		-I$build_dir/src/amd \
		-I$src_dir/src/amd \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
for src_pathname in $libgallium_winsys_amdgpu_drm_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/winsys/$cpp_filename --> $build_dir/src/gallium/winsys/$asm_filename\n"
	$cc_s $build_dir/src/gallium/winsys/$cpp_filename -o $build_dir/src/gallium/winsys/$asm_filename &
done

for src_pathname in $libgallium_winsys_amdgpu_drm_cxx_pathnames
do
	cpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	printf "CXX_S $build_dir/src/gallium/winsys/$cpp_filename --> $build_dir/src/gallium/winsys/$asm_filename\n"
	$cxx_s $build_dir/src/gallium/winsys/$cpp_filename -o $build_dir/src/gallium/winsys/$asm_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
os=
for src_pathname in $libgallium_winsys_amdgpu_drm_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/winsys/$o_filename"
	printf "AS $build_dir/src/gallium/winsys/$asm_filename --> $build_dir/src/gallium/winsys/$o_filename\n"
	$as $build_dir/src/gallium/winsys/$asm_filename -o $build_dir/src/gallium/winsys/$o_filename &
done

for src_pathname in $libgallium_winsys_amdgpu_drm_cxx_pathnames
do
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	o_filename=$(basename $src_pathname .cpp).cpp.cxx.s.o
	os="$os $build_dir/src/gallium/winsys/$o_filename"
	printf "AS $build_dir/src/gallium/winsys/$asm_filename --> $build_dir/src/gallium/winsys/$o_filename\n"
	$as $build_dir/src/gallium/winsys/$asm_filename -o $build_dir/src/gallium/winsys/$o_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
printf "AR RCS $build_dir/libgallium_winsys_amdgpu_drm.a $os\n"
$ar_rcs $build_dir/libgallium_winsys_amdgpu_drm.a $os
####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
# XXX: We add the additional gallium sw winsys-s even though we have no drivers using any of them:
# This is just a compile thing because they were unable to make them cleanly compile-able out as
# they should be. With proper C preprocessor namespaces all binaries should be One Compilation Units
# anyway...
#===================================================================================================
# the pure sw dri winsys
libgallium_winsys_sw_dri_c_pathnames="\
$src_dir/src/gallium/winsys/sw/dri/dri_sw_winsys.c \
"
for src_pathname in $libgallium_winsys_sw_dri_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/winsys/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/winsys/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
for src_pathname in $libgallium_winsys_sw_dri_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/winsys/$cpp_filename --> $build_dir/src/gallium/winsys/$asm_filename\n"
	$cc_s $build_dir/src/gallium/winsys/$cpp_filename -o $build_dir/src/gallium/winsys/$asm_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
os=
for src_pathname in $libgallium_winsys_sw_dri_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/winsys/$o_filename"
	printf "AS $build_dir/src/gallium/winsys/$asm_filename --> $build_dir/src/gallium/winsys/$o_filename\n"
	$as $build_dir/src/gallium/winsys/$asm_filename -o $build_dir/src/gallium/winsys/$o_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
printf "AR RCS $build_dir/libgallium_winsys_sw_dri.a $os\n"
$ar_rcs $build_dir/libgallium_winsys_sw_dri.a $os
#===================================================================================================
# the sw kms dri winsys
libgallium_winsys_sw_kms_dri_c_pathnames="\
$src_dir/src/gallium/winsys/sw/kms-dri/kms_dri_sw_winsys.c \
"
for src_pathname in $libgallium_winsys_sw_kms_dri_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/winsys/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/winsys/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
for src_pathname in $libgallium_winsys_sw_kms_dri_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/winsys/$cpp_filename --> $build_dir/src/gallium/winsys/$asm_filename\n"
	$cc_s $build_dir/src/gallium/winsys/$cpp_filename -o $build_dir/src/gallium/winsys/$asm_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
os=
for src_pathname in $libgallium_winsys_sw_kms_dri_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/winsys/$o_filename"
	printf "AS $build_dir/src/gallium/winsys/$asm_filename --> $build_dir/src/gallium/winsys/$o_filename\n"
	$as $build_dir/src/gallium/winsys/$asm_filename -o $build_dir/src/gallium/winsys/$o_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
printf "AR RCS $build_dir/libgallium_winsys_sw_kms_dri.a $os\n"
$ar_rcs $build_dir/libgallium_winsys_sw_kms_dri.a $os
#===================================================================================================
# the sw null winsys
libgallium_winsys_sw_null_c_pathnames="\
$src_dir/src/gallium/winsys/sw/null/null_sw_winsys.c \
"
for src_pathname in $libgallium_winsys_sw_null_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/winsys/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/winsys/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
for src_pathname in $libgallium_winsys_sw_null_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/winsys/$cpp_filename --> $build_dir/src/gallium/winsys/$asm_filename\n"
	$cc_s $build_dir/src/gallium/winsys/$cpp_filename -o $build_dir/src/gallium/winsys/$asm_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
os=
for src_pathname in $libgallium_winsys_sw_null_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/winsys/$o_filename"
	printf "AS $build_dir/src/gallium/winsys/$asm_filename --> $build_dir/src/gallium/winsys/$o_filename\n"
	$as $build_dir/src/gallium/winsys/$asm_filename -o $build_dir/src/gallium/winsys/$o_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
printf "AR RCS $build_dir/libgallium_winsys_sw_null.a $os\n"
$ar_rcs $build_dir/libgallium_winsys_sw_null.a $os
#===================================================================================================
# the sw wrapper winsys
libgallium_winsys_sw_wrapper_c_pathnames="\
$src_dir/src/gallium/winsys/sw/wrapper/wrapper_sw_winsys.c \
"
for src_pathname in $libgallium_winsys_sw_wrapper_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/winsys/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/winsys/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
for src_pathname in $libgallium_winsys_sw_wrapper_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/winsys/$cpp_filename --> $build_dir/src/gallium/winsys/$asm_filename\n"
	$cc_s $build_dir/src/gallium/winsys/$cpp_filename -o $build_dir/src/gallium/winsys/$asm_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
os=
for src_pathname in $libgallium_winsys_sw_wrapper_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/winsys/$o_filename"
	printf "AS $build_dir/src/gallium/winsys/$asm_filename --> $build_dir/src/gallium/winsys/$o_filename\n"
	$as $build_dir/src/gallium/winsys/$asm_filename -o $build_dir/src/gallium/winsys/$o_filename &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
printf "AR RCS $build_dir/libgallium_winsys_sw_wrapper.a $os\n"
$ar_rcs $build_dir/libgallium_winsys_sw_wrapper.a $os
####################################################################################################
printf "\t<--winsys sub-components built\n"
