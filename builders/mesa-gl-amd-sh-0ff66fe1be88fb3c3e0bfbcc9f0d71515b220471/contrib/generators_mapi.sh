printf "Running mapi code generators-->\n"
#===============================================================================
# you have 2 versions of mapi_tmp.h: one for the "shared glapi" static library
# and one for the "static glapi" static library (which is limited to the
# "bridge glapi" in our case).
# XXX: "static glapi" and "shared glapi" are both static libraries now.
mkdir $build_dir/src
mkdir $build_dir/src/mapi
mkdir $build_dir/src/mapi/glapi

$python3 $src_dir/src/mapi/mapi_abi.py \
--printer shared-glapi \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
>$build_dir/src/mapi/glapi/shared_glapi_mapi_tmp.h &


$python3 $src_dir/src/mapi/mapi_abi.py \
--printer glapi \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
>$build_dir/src/mapi/glapi/static_glapi_mapi_tmp.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/gl_enums.py -f \
$src_dir/src/mapi/glapi/registry/gl.xml \
>$build_dir/src/mapi/glapi/enums.c &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/api_exec_init.py -f \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
>$build_dir/src/mapi/glapi/api_exec_init.c &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/api_exec_decl_h.py -f \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
>$build_dir/src/mapi/glapi/api_exec_decl.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/api_save_init_h.py -f \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
>$build_dir/src/mapi/glapi/api_save_init.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/api_save_h.py -f \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
>$build_dir/src/mapi/glapi/api_save.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/api_beginend_init_h.py -f \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
>$build_dir/src/mapi/glapi/api_beginend_init.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/api_hw_select_init_h.py -f \
$src_dir/src/mapi/glapi/gen/gl_API.xml \
>$build_dir/src/mapi/glapi/api_hw_select_init.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/gl_marshal.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
0 8 8 \
>$build_dir/src/mapi/glapi/marshal_generated0.c &

$python3 $src_dir/src/mapi/glapi/gen/gl_marshal.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
1 8 8 \
>$build_dir/src/mapi/glapi/marshal_generated1.c &

$python3 $src_dir/src/mapi/glapi/gen/gl_marshal.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
2 8 8 \
>$build_dir/src/mapi/glapi/marshal_generated2.c &

$python3 $src_dir/src/mapi/glapi/gen/gl_marshal.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
3 8 8 \
>$build_dir/src/mapi/glapi/marshal_generated3.c &

$python3 $src_dir/src/mapi/glapi/gen/gl_marshal.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
4 8 8 \
>$build_dir/src/mapi/glapi/marshal_generated4.c &

$python3 $src_dir/src/mapi/glapi/gen/gl_marshal.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
5 8 8 \
>$build_dir/src/mapi/glapi/marshal_generated5.c &

$python3 $src_dir/src/mapi/glapi/gen/gl_marshal.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
6 8 8 \
>$build_dir/src/mapi/glapi/marshal_generated6.c &

$python3 $src_dir/src/mapi/glapi/gen/gl_marshal.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml \
7 8 8 \
>$build_dir/src/mapi/glapi/marshal_generated7.c &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/gl_unmarshal_table.py \
$src_dir/src/mapi/glapi/gen/gl_and_es_API.xml 8 \
>$build_dir/src/mapi/glapi/unmarshal_table.c &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/glX_proto_send.py -f \
$src_dir/src/mapi/glapi/gen/gl_API.xml \
-m proto \
>$build_dir/src/mapi/glapi/indirect.c &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/glX_proto_send.py -f \
$src_dir/src/mapi/glapi/gen/gl_API.xml \
-m init_h \
>$build_dir/src/mapi/glapi/indirect.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/glX_proto_send.py -f \
$src_dir/src/mapi/glapi/gen/gl_API.xml \
-m init_c \
>$build_dir/src/mapi/glapi/indirect_init.c &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/glX_proto_size.py -f \
$src_dir/src/mapi/glapi/gen/gl_API.xml \
--only-set -m size_h --header-tag _INDIRECT_SIZE_H_ \
>$build_dir/src/mapi/glapi/indirect_size.h &
#-------------------------------------------------------------------------------
$python3 $src_dir/src/mapi/glapi/gen/glX_proto_size.py -f \
$src_dir/src/mapi/glapi/gen/gl_API.xml \
--only-set -m size_c \
>$build_dir/src/mapi/glapi/indirect_size.c &
#===============================================================================
printf "<--mapi code generation done\n"
