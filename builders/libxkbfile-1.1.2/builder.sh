src_name=libxkbfile
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://xorg.freedesktop.org/releases/individual/lib/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# XXX: for all X11 based header we do not define MALLOC_0_RETURNS_NULL for glibc allocator

mkdir -p $build_dir/bin
cat >$build_dir/bin/cc <<EOF
#!/bin/sh
eval \
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
\
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
\
-I/nyan/libXau/current/include \
-I/nyan/libxcb/current/include \
-I/nyan/libX11/current/include \
-I/nyan/libXext/current/include \
-I/nyan/libXfixes/current/include \
-I/nyan/xorgproto/current/include \
\
-O2 -pipe -fPIC -static-libgcc -ftls-model=global-dynamic -fpic \
\
-B/nyan/glibc/current/lib \
\
-L/nyan/libXau/current/lib \
-L/nyan/libxcb/current/lib \
-L/nyan/libX11/current/lib \
-L/nyan/libXext/current/lib \
-L/nyan/libXfixes/current/lib \
-L/nyan/glibc/current/lib \
\
\
"\$@"
EOF
chmod +x $build_dir/bin/cc
export PATH_SAVED=$PATH
export PATH="\
$build_dir/bin:\
$PATH\
"

libxkbfile_src_files="\
cout.c \
maprules.c \
srvmisc.c \
xkbatom.c \
xkbbells.c \
xkbconfig.c \
xkbdraw.c \
xkberrs.c \
xkbmisc.c \
xkbout.c \
xkbtext.c \
xkmout.c \
xkmread.c \
"
#---------------------------------------------------------------------------------------------------
# TODO: we are still using the compiler driver, bad
for f in $libxkbfile_src_files
do
	o=$(basename $f .c).o
	os="$os $o"

	printf "CC $f->$o\n"
	cc -c $pkg_dir/src/$f -o $build_dir/$o \
		-DHAVE_UNLOCKED_STDIO \
		-DHAVE_STRCASECMP \
		-DHAVE_STRNDUP \
		-I$pkg_dir/include/X11/extensions \
		-I$pkg_dir/include &
done
#---------------------------------------------------------------------------------------------------
wait
#---------------------------------------------------------------------------------------------------
# TODO: we are still using the compiler driver, bad
# no version file and it expects all symbols to end up in dynamic table, aka must not use default
# hidden visibility
printf "CCLD libxkbfile.so.1.0.2\n"
cc -o $build_dir/libxkbfile.so.1.0.2 \
	-shared -Wl,--soname=libxkbfile.so.1 -Wl,-s -Wl,-no-undefined \
	$os \
	-lX11
#---------------------------------------------------------------------------------------------------
mkdir -p /nyan/$src_name/$slot/lib
rm -Rf /nyan/$src_name/$slot/include/X11/extensions
mkdir -p /nyan/$src_name/$slot/include/X11

cp -r $pkg_dir/include/X11/extensions /nyan/$src_name/$slot/include/X11
cp -f $build_dir/libxkbfile.so.1.0.2 /nyan/$src_name/$slot/lib

ln -sTf libxkbfile.so.1.0.2 /nyan/$src_name/$slot/lib/libxkbfile.so
ln -sTf libxkbfile.so.1.0.2 /nyan/$src_name/$slot/lib/libxkbfile.so.1
#---------------------------------------------------------------------------------------------------

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
