src_name=gettext
version=0.19.8.1
archive_name=$src_name-$version.tar.xz
url0=http://ftpmirror.gnu.org/$src_name/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# it wants sh in path
OLD_PATH=$PATH
export PATH=$PATH:/bin

export 'CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc'
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--prefix=/nyan/gettext/0	\
	--disable-java			\
	--disable-native-java		\
	--enable-threads=posix		\
	--disable-shared		\
	--enable-static			\
	--disable-nls			\
	--disable-c++			\
	--disable-openmp		\
	--disable-acl			\
	--disable-curses		\
	--with-gnu-ld			\
	--with-included-gettext		\
	--with-included-glib		\
	--with-included-libcroco	\
	--with-included-libunistring	\
	--with-included-libxml		\
	--without-included-regex	\
	--without-emacs
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/0/share/doc
rm -Rf /nyan/$src_name/0/share/info
rm -Rf /nyan/$src_name/0/share/man
find /nyan/$src_name/0 -name '*.la' | xargs rm -f
find /nyan/$src_name/0 -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then strip -s $f; fi; done

rm -Rf $build_dir $src_dir
export PATH=$OLD_PATH
