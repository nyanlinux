src_name=xserver
git_url0=git://anongit.freedesktop.org/xorg/$src_name

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir
#-------------------------------------------------------------------------------
cd $pkg_dir

if test x$git_commit != x; then
	/nyan/git/current/bin/git checkout --force $git_commit
	/nyan/git/current/bin/git reset --hard
fi
# copy the canonical lean build scripts
cp -r $nyan_root/builders/$pkg_name/contrib .
#-------------------------------------------------------------------------------
build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir
#-------------------------------------------------------------------------------
# create the local configuration file, carefull libxcvt is tcc compiled and
# needs internal tcc libs
cat >$build_dir/local_conf.sh <<EOF
cc='/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc \
-static-libgcc \
-ftls-model=global-dynamic -fpic \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
-fvisibility=hidden \
-fno-strict-aliasing \
-O2 -pipe -fPIC -DPIC -std=c99 \
-c \
'
ar_rcs='/opt/toolchains/x64/elf/binutils-gcc/current/bin/ar rcs'
ccld=/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc
cpp='/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc -E'
awk='/nyan/busybox/current/bin/awk'

xorgproto_cppflags='\
-I/nyan/xorgproto/current/include/X11/dri \
-I/nyan/xorgproto/current/include \
'
xtrans_cppflags=-I/nyan/xtrans/current/include
libxcvt_cppflags=-I/nyan/libxcvt/current/include
libxcvt_ldflags='-L/nyan/libxcvt/current/lib -lxcvt -L/nyan/toolchains/binutils-tcc/current/lib/tcc -ltcc1'
mesa_gl_cppflags=-I/nyan/mesa-gl/current/include
mesa_gbm_ldflags='-L/nyan/mesa-gl/current -lgbm'
libglvnd_cppflags=-I/nyan/libglvnd/current/include
# actually, we should do that for nearly everything
libglvnd_gl_ldflags=/usr/lib/libGL.so.1
libxkbfile_cppflags=-I/nyan/libxkbfile/current/include
pixman_cppflags=-I/nyan/pixman/current/include/pixman-1
pixman_ldflags='-L/nyan/pixman/current/lib -lpixman-1'
libx11_cppflags=-I/nyan/libX11/current/include
libxcb_cppflags=-I/nyan/libxcb/current/include
libxau_cppflags=-I/nyan/libXau/current/include
libxau_ldflags='-L/nyan/libXau/current/lib -lXau'
libxfont2_cppflags=-I/nyan/libXfont2/current/include
libxfont2_ldflags='-L/nyan/libXfont2/current/lib -lXfont2'
libfontenc_cppflags=-I/nyan/libfontenc/current/include
freetype_cppflags=-I/nyan/freetype/current/include/freetype2
zlib_cppflags=-I/nyan/zlib/current/include
libpng_cppflags=-I/nyan/libpng/current/include/libpng16
libxshmfence_cppflags=-I/nyan/libxshmfence/current/include
libxshmfence_ldflags='-L/nyan/libxshmfence/current/lib -lxshmfence'
ssl_cppflags=-I/nyan/libressl/current/include
ssl_ldflags='-L/nyan/libressl/current/lib -lssl -lcrypto'
libdrm_cppflags='-I/nyan/drm/current/include/libdrm -I/nyan/drm/current/include'
libdrm_ldflags='-L/nyan/drm/current/lib -ldrm'
libepoxy_cppflags=-I/nyan/libepoxy/current/include
libepoxy_ldflags='-L/nyan/libepoxy/current/lib -lepoxy'
libudev_cppflags=-I/nyan/mudev/current/include
libudev_ldflags='-L/nyan/mudev/current/lib -ludev'
libpciaccess_cppflags=-I/nyan/libpciaccess/current/include
libpciaccess_ldflags='-L/nyan/libpciaccess/current/lib -lpciaccess'

xkbbindir=/nyan/xkbcomp/current/bin
defaultlogdir=/nyan/xserver/$slot/var/log

module_store=/nyan/xserver/$slot/modules
module_store_virtual=/nyan/xserver/current/modules

bindir_store=/nyan/xserver/$slot/bin
bindir_store_virtual=/nyan/xserver/current/bin

incdir_store=/nyan/xserver/$slot/include
incdir_store_virtual=/nyan/xserver/current/include
EOF

#-------------------------------------------------------------------------------
$pkg_dir/contrib/linux-glibc.sh
#-------------------------------------------------------------------------------

rm -Rf /nyan/$src_name/$slot
mkdir -p /nyan/$src_name/$slot
cp -rf $build_dir/install_root/nyan/xserver/$slot/* /nyan/$src_name/$slot
mkdir -p \
	/usr/bin \
	/usr/lib/xorg/modules/drivers \
	/usr/lib/xorg/modules/extensions \
	/usr/lib/xorg/modules/input \
	/usr/share/X11/xorg.conf.d

cp -Pf $build_dir/install_root/usr/bin/Xorg /usr/bin

cp -Pf	$build_dir/install_root/usr/lib/xorg/modules/libexa.so \
	$build_dir/install_root/usr/lib/xorg/modules/libfbdevhw.so \
	$build_dir/install_root/usr/lib/xorg/modules/libglamoregl.so \
	$build_dir/install_root/usr/lib/xorg/modules/libint10.so \
	$build_dir/install_root/usr/lib/xorg/modules/libshadow.so \
	$build_dir/install_root/usr/lib/xorg/modules/libshadowfb.so \
	$build_dir/install_root/usr/lib/xorg/modules/libvgahw.so \
	$build_dir/install_root/usr/lib/xorg/modules/libwfb.so \
		/usr/lib/xorg/modules/

cp -Pf $build_dir/install_root/usr/lib/xorg/modules/drivers/modesetting_drv.so \
		/usr/lib/xorg/modules/drivers

cp -Pf $build_dir/install_root/usr/lib/xorg/modules/extensions/libglx.so \
		/usr/lib/xorg/modules/extensions

cp -Pf $build_dir/install_root/usr/lib/xorg/modules/input/inputtest_drv.so \
		/usr/lib/xorg/modules/input/inputtest_drv.so
	
# /usr/share/X11/xkb may be installed as a symbol link later, do not create the full path.
# xorg won't run without the "compiled" dir (compiled keymaps will end up here), configured above
# with xkmoutputdir, xkb data should have been installed before.
# The xorg server may run without root priviledges.
if test -d /usr/share/X11/xkb/compiled; then
	chmod 777 /usr/share/X11/xkb/compiled
else
	printf '****** WARNING:/usr/share/X11/xkb/compiled does not exist, the server may probably not run\n'
fi
rm -Rf $build_dir $pkg_dir
