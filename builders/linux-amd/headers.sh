#!/bin/sh

# this is deep core, update will happen only manually

builder_dir="$(realpath $(dirname $0))"
build_dir=/run/build-linux-headers
mkdir $build_dir
nyan_root="$(realpath $builder_dir/../..)"
src_name=linux-amd
# better do that manually
slot=6.10.0

printf "builder_dir=$builder_dir\n"
printf "nyan_root=$nyan_root\n"

# genius at the linux fondation:
mkdir $build_dir/bin
cat <<EOF >$build_dir/bin/rsync
#!/bin/sh
printf "RSYNC:\$*\n"
EOF
chmod 100 $build_dir/bin/rsync

export PATH_SAVED="$PATH"
# you need gcc...
export PATH="\
$build_dir/bin:\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
$PATH\
"

cd $build_dir

# Using that horrible compiler driver, the abomination...
# to "generate" source code number definitions for syscalls... really... super genius
export HOSTCC=gcc
export "HOSTCFLAGS=\
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
-O2 -pipe -fPIC -static-libgcc \
"
export "HOSTLDFLAGS=\
-B/nyan/glibc/current/lib \
-L/nyan/glibc/current/lib \
-static-libgcc -static \
"
make -C	$nyan_root/src/$src_name headers_install \
	"INSTALL_HDR_PATH=/run/$src_name-headers-$slot" \
	ARCH=x86_64 \
	O=$build_dir V=1
unset HOSTCC
unset HOSTGFLAGS
unset HOSTLDFLAGS

# bit of cleanup
rm $(find -name '.*.cmd')
# same filesystem->rename
rm -Rf /run/$src_name-headers-$slot/include
# install the base header files (where 
mv usr/include /run/$src_name-headers-$slot/include
# overlay with the arch asm headers
for f in arch/x86/include/generated/uapi/asm/*.h
do
	cp $f /run/$src_name-headers-$slot/include/asm/$(basename "$f")
done
# the linux version file
cp include/generated/uapi/linux/version.h /run/$src_name-headers-$slot/include/linux/version.h

export PATH="$PATH_SAVED"
