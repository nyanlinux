#!/bin/sh
# SPDX-License-Identifier: GPL-2.0

# Planned obsolescence 101 directly from linux devs with commit access.

cflags=$1
libs=$2

# NYAN GNU/LINUX  DISTRO
printf "-I/nyan/ncurses/current/include/ncurses -I/nyan/ncurses/current/include" >${cflags}
printf "-L/nyan/ncurses/current/lib -lform -lmenu -lpanel -ltic -lncurses -ltinfo -lpthread" >${libs}
exit 0
