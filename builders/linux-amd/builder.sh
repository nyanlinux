# We are heading towards a RCS (Reduced Command Set) hence more verbosity.
. $nyan_root/utils.sh
cleanup()
{
	if test -e $pkg_dir/NYAN/depmod_installed; then
		printf 'NYAN:/sbin/depmod was installed, removing\n'
		rm -f '/sbin/depmod' 
		rmdir /sbin
	fi
	if test -e /bin/bash.HIDDEN; then
		printf 'NYAN:/bin/bash was hidden, unhidding\n'
		mv /bin/bash.HIDDEN /bin/bash
	fi
}
################################################################################

# sadly, the toxic mob from gcc/clang managed to commit too much brain damaged
# (gcc/clang/iso) code into linux, fixing their toxic stuff requires something
# different

pkg_dir=$pkgs_dir_root/$pkg_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
printf 'NYAN:copying linux source tree in ram...\n'
cp -r $src_dir_root/linux-amd $pkg_dir
printf 'NYAN:done\n'
cd $pkg_dir
# create a directory for our stuff
mkdir -p NYAN
################################################################################
PATH_SAVED=$PATH
# must have sh in PATH for some makefile scripts
export PATH="\
/bin:\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/git/current/bin:\
/nyan/make/current/bin:\
/nyan/flex/current/bin:\
/nyan/bison/current/bin:\
$PATH"
if test -e /bin/bash;then
	printf 'NYAN:/bin/bash detected, hidding\n'
	mv /bin/bash /bin/bash.HIDDEN
fi
#-------------------------------------------------------------------------------
# there, the build system is expecting way more than minimal host and target
# compilers, then we need wrappers
mkdir -p $pkg_dir/NYAN/wrappers
export PATH="\
$pkg_dir/NYAN/wrappers:\
$PATH"
sed -r -e "s:HOSTGCC_ABSOLUTE_PATH:/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc:" \
	$nyan_root/builders/$pkg_name/wrappers/hostgcc \
	>$pkg_dir/NYAN/wrappers/hostgcc
sed -r -e "s:TARGETGCC_ABSOLUTE_PATH:/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc:" \
	$nyan_root/builders/$pkg_name/wrappers/crossgcc \
	>$pkg_dir/NYAN/wrappers/gcc
chmod +x $pkg_dir/NYAN/wrappers/hostgcc $pkg_dir/NYAN/wrappers/gcc
#-------------------------------------------------------------------------------
# the config in the linux directory
cp -f $nyan_root/builders/$pkg_name/config $pkg_dir/NYAN/amd_config
sed -r -e "/^CONFIG_INITRAMFS_SOURCE/ c\\
CONFIG_INITRAMFS_SOURCE=\"$pkg_dir/NYAN/amd_ramfs.cpio\"" -i $pkg_dir/NYAN/amd_config
# we don't have modules yet, give an empty one to kbuild for now
rm $pkg_dir/NYAN/amd_ramfs.cpio
touch $pkg_dir/NYAN/amd_ramfs.cpio
#-------------------------------------------------------------------------------
release=$($nyan_root/builders/$pkg_name/release.sh "$nyan_root" "$pkg_dir/NYAN/amd_config" "$pkg_dir")
if test ! "$release"; then
	printf 'NYAN:ERROR:we are missing the kernel release, something is critically wrong, exiting\n'
	exit 1;
fi
printf "NYAN:linux release is \"$release\"\n"
#-------------------------------------------------------------------------------
export KCONFIG_CONFIG=$pkg_dir/NYAN/amd_config
export HOSTCC=hostgcc
export ARCH=x86_64
# linux sdk wants depmod in /sbin
if test ! -e /sbin/depmod; then
	printf 'NYAN:you are missing the depmod utility in /sbin, using the one in PATH\n'
	find_in_path depmod
	if test ! $r; then
		printf 'NYAN:no depmod utility found in PATH, linux modules SDK will fail, exiting\n'
		cleanup
		exit 1
	fi
	mkdir /sbin
	printf 'NYAN:linking %s to /sbin/depmod, will be removed on exiting\n' $r
	ln -s $r /sbin/depmod
	touch $pkg_dir/NYAN/depmod_installed
fi
if test ! -x '/sbin/depmod'; then
	printf 'NYAN:ERROR:/sbin/depmod not executable, linux SDK will fail, exiting\n' >&2
	exit 1
fi
(cd /lib/modules;rm -Rf "$release")
# HOSTCC environment variable seems ignored, overridding makefile variable
make -j $threads_n vmlinux "HOSTCC=$HOSTCC"
make -j $threads_n modules "HOSTCC=$HOSTCC"
make modules "HOSTCC=$HOSTCC"
make -j $threads_n modules_install "HOSTCC=$HOSTCC"
printf "NYAN:linux modules installed:/lib/modules/$release\n"
#-------------------------------------------------------------------------------
# AuthenticAMD.bin firmware building
printf 'NYAN:concatening AMD cpu firmware -- START\n'
firmware_dir=/lib/firmware/amd-ucode
if test ! -d $firmware_dir; then
	printf "NYAN:ERROR:missing AMD cpu firmware \"$firmware_dir\"\n"
	cleanup
	exit 1
fi
cat $(find $firmware_dir -name '*.bin' | sort) >$pkg_dir/NYAN/AuthenticAMD.bin
printf 'NYAN:concatening AMD cpu firmware -- END\n'
#-------------------------------------------------------------------------------
# cpio list generation
printf 'NYAN:cpio list generation for ramfs -- START\n'
cp $pkg_dir/usr/default_cpio_list $pkg_dir/NYAN/amd_ramfs
sed -r	-e "s:AMD_MICROCODE_DIR:$pkg_dir/NYAN:g" \
	-e "s:INIT_DIR:$pkg_dir/NYAN:g" \
	-e "s:LINUX_RELEASE:$release:g" \
	-e "s:BUILDER_DIR:$nyan_root/builders/$pkg_name:g" \
	<$nyan_root/builders/$pkg_name/amd_ramfs.in \
	>>$pkg_dir/NYAN/amd_ramfs
printf 'NYAN:cpio list generation for ramfs -- END\n'
#-------------------------------------------------------------------------------
# init generation, basically craming the root uuid
printf 'NYAN:init generation for ramfs -- START\n'
printf "NYAN:using configuration root UUID '$root_uuid'\n"
sed -r -e "/^initramfs_root_uuid/ c\\
initramfs_root_uuid=$root_uuid" \
	<$nyan_root/builders/$pkg_name/init.in \
	>$pkg_dir/NYAN/init
printf 'NYAN:init generation for ramfs -- END\n'
#-------------------------------------------------------------------------------
# switch from the cpio archive to cpio list
# this is done removing the .cpio file extension, this is how linux kbuild makes
# the difference
rm $pkg_dir/NYAN/amd_ramfs.cpio
sed -r -e "/^CONFIG_INITRAMFS_SOURCE/ c\\
CONFIG_INITRAMFS_SOURCE=\"$pkg_dir/NYAN/amd_ramfs\"" -i $pkg_dir/NYAN/amd_config
#-------------------------------------------------------------------------------
# HOSTCC environment variable seems ignored, overridding makefile variable
make -j $threads_n bzImage "HOSTCC=$HOSTCC"
unset KCONFIG_CONFIG
unset HOSTCC
unset ARCH
unset CROSS_COMPILE
#-------------------------------------------------------------------------------
# install the kernel with the baked initramfs
mkdir /mnt
mkdir /mnt/efi
cp $pkg_dir/arch/x86/boot/bzImage "/mnt/efi/amd-$release.efi"
printf "NYAN:linux kernel installed:/mnt/efi/amd-$release.efi\n"
#-------------------------------------------------------------------------------
export PATH=$PATH_SAVED
cleanup
################################################################################
rm -Rf $pkg_dir
