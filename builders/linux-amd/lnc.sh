#!/bin/sh

builder_dir=$(readlink -f $(dirname $0))
nyan_root=$(readlink -f $builder_dir/../..)
src_name=linux-amd
printf "builder_dir=$builder_dir\n"
printf "nyan_root=$nyan_root\n"
. $nyan_root/conf.sh

# hide bash
if test -e /bin/bash;then
	printf 'NYAN:/bin/bash detected, hidding\n'
	mv /bin/bash /bin/bash.HIDDEN
fi
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
/nyan/flex/current/bin:\
/nyan/bison/current/bin:\
$PATH\
"

build_dir=$builds_dir_root/$src_name-nconfig
mkdir -p $build_dir

# copy our nconf-cfg.sh to configure ncurses for nyanlinux
cp $src_dir_root/$src_name/scripts/kconfig/nconf-cfg.sh $build_dir/nconf-cfg.sh.BK
cp $builder_dir/nconf-cfg.sh $src_dir_root/$src_name/scripts/kconfig/nconf-cfg.sh

export KCONFIG_CONFIG=$builder_dir/config
export HOSTCC=gcc
export 'HOSTCFLAGS=-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include -O2 -pipe -fPIC -static-libgcc'
export 'HOSTLDFLAGS=-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -static-libgcc'
export ARCH=x86_64
make -C	$src_dir_root/$src_name \
	O=$build_dir \
	HOSTCC=$HOSTCC \
	nconfig
if test $? != 0; then
	printf 'NYAN:ERROR:failed to make the ncurses based configurator\n'
fi

# restore 
cp $build_dir/nconf-cfg.sh.BK $src_dir_root/$src_name/scripts/kconfig/nconf-cfg.sh

if test -e /bin/bash.HIDDEN; then
	printf 'NYAN:hidden /bin/bash detected, restoring\n'
	mv /bin/bash.HIDDEN /bin/bash
fi

rm -Rf $build_dir
rmdir $builds_dir_root
