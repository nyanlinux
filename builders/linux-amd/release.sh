#!/bin/sh
# we are heading towards a RCS (Reduced Command Set), expect much more verbosity and simplicity
printf "NYAN:RELEASE.SH -- START\n" >&2
# OUTPUT ON STDOUT THE KERNEL RELEASE
nyan_root=$1
linux_config=$2
patched_linux_dir=$3

if test -z "$nyan_root"; then
	printf 'NYAN:ERROR:missing nyan root\n' >&2
	exit 1
else
	printf "NYAN:using nyan_root \"$nyan_root\"\n" >&2
fi
if test -z "$linux_config"; then
	printf 'NYAN:ERROR:missing linux_config\n' >&2
	exit 1
else
	printf "NYAN:using linux_config \"$linux_config\"\n" >&2
fi
if test ! -d "$patched_linux_dir"; then
	printf 'NYAN:ERROR:missing patched linux directory\n' >&2
	exit 1
else
	printf "NYAN:using patched linux directory \"$patched_linux_dir\"\n" >&2
fi

. $nyan_root/conf.sh
. $nyan_root/utils.sh

cleanup()
{
	if test -e /bin/bash.HIDDEN; then
		printf 'NYAN:detected hidden bash, restoring\n' >&2
		mv /bin/bash.HIDDEN /bin/bash >&2
	fi
	if test $false_installed; then
		printf 'NYAN:detected installed /bin/false, removing\n' >&2
		rm -f /bin/false >&2
	fi
	if test $bracket_installed; then
		printf 'NYAN:detected installed /bin/[, removing\n' >&2
		rm -f '/bin/[' >&2
	fi
	rmdir /bin || true >&2
	printf "NYAN:RELEASE.SH -- END\n" >&2
} 
trap cleanup EXIT
################################################################################
if test -e /bin/bash;then
	printf 'NYAN:detected /bin/bash, hidding\n' >&2
	mv /bin/bash /bin/bash.HIDDEN >&2
fi
mkdir -p /bin >&2
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/make/current/bin:\
/nyan/flex/current/bin:\
/nyan/bison/current/bin:\
$PATH\
"
# linux sdk wants direct sh invocation
if ! command -v sh >/dev/null 2>/dev/null; then
	printf 'NYAN:ERROR:no direct invocation of sh found, linux SDK will fail, exiting\n' >&2
	exit 1;
fi
# linux sdk wants false in /bin
if test ! -e /bin/false; then
	printf 'NYAN:you are missing the false utility in /bin, using the one in PATH\n' >&2
	find_in_path false
	if test ! $r; then
		printf 'NYAN:no false utility found in PATH, linux SDK will fail, exiting\n' >&2
		exit 1
	fi
	printf 'NYAN:linking %s to /bin/false, will be removed on exiting\n' $r >&2
	ln -s $r /bin/false >&2
	false_installed=1
fi
if test ! -x /bin/false; then
	printf 'NYAN:ERROR:/bin/false not executable, linux SDK will fail, exiting\n' >&2
	exit 1
fi
# linux sdk wants [ in /bin
if test ! -e '/bin/['; then
	printf 'NYAN:you are missing the "[" utility in /bin, using "test" in PATH\n' >&2
	find_in_path test
	if test ! $r; then
		printf 'NYAN:ERROR:no test utility found in PATH, linux SDK will fail, exiting\n' >&2
		exit 1
	fi
	printf 'NYAN:linking %s to /bin/[, will be removed on exiting\n' $r >&2
	ln -s $r '/bin/[' >&2
	bracket_installed=1
fi
if test ! -x '/bin/['; then
	printf 'NYAN:ERROR:/bin/[ not executable, linux SDK will fail, exiting\n' >&2
	exit 1
fi
export "KCONFIG_CONFIG=$linux_config"
export "HOSTCC=gcc"
export "HOSTCFLAGS=-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include -O2 -pipe -fPIC -static-libgcc"
export "HOSTLDFLAGS=-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -static-libgcc"
export ARCH=x86_64
cd $patched_linux_dir
# HOSTCC environment variable seems ignored, overridding the makefile variable
make oldconfig "HOSTCC=$HOSTCC" >&2
make -s kernelrelease "HOSTCC=$HOSTCC"
make clean >&2
