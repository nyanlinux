echo "building AMD GCN GPU gallium driver components-->"
. $script_dir/gallium_auxiliary.sh
. $script_dir/gallium_winsys.sh
. $script_dir/gallium_drivers.sh
. $script_dir/gallium_frontends.sh
. $script_dir/gallium_targets.sh
echo "<--AMD GCN GPU gallium driver components built"
