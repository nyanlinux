#===============================================================================
# linux drm uapi
if test "${linux_drm_cpp_flags-unset}" = unset; then
linux_drm_cpp_flags='-I/nyan/glibc/current/include-linux/drm'
external_deps_cpp_flags="$linux_drm_cpp_flags $external_deps_cpp_flags"
fi
#===============================================================================


#===============================================================================
# libdrm
if test "${libdrm_cpp_flags-unset}" = unset; then
libdrm_cpp_flags='-I/nyan/drm/current/include -DHAVE_LIBDRM=1'
external_deps_cpp_flags="$libdrm_cpp_flags $external_deps_cpp_flags"
fi
if test "${libdrm_ld_flags-unset}" = unset; then
libdrm_ld_flags='-L/nyan/drm/current/lib -ldrm'
fi
#===============================================================================


#===============================================================================
# libdrm_amdgpu
if test "${libdrm_amdgpu_cpp_flags-unset}" = unset; then
libdrm_amdgpu_cpp_flags='-I/nyan/drm/current/include/libdrm'
external_deps_cpp_flags="$libdrm_amdgpu_cpp_flags $external_deps_cpp_flags"
fi
if test "${libdrm_amdgpu_ld_flags-unset}" = unset; then
libdrm_amdgpu_ld_flags='-L/nyan/drm/current/lib -ldrm_amdgpu'
fi
#===============================================================================


#===============================================================================
# legacy libdrm_radeon
if test "${libdrm_radeon_cpp_flags-unset}" = unset; then
libdrm_radeon_cpp_flags='-I/nyan/drm/current/include/libdrm'
external_deps_cpp_flags="$libdrm_radeon_cpp_flags $external_deps_cpp_flags"
fi
if test "${libdrm_radeon_ld_flags-unset}" = unset; then
libdrm_radeon_ld_flags='-L/nyan/drm/current/lib -ldrm_radeon'
fi
#===============================================================================


#===============================================================================
# libelf
if test "${libelf_cpp_flags-unset}" = unset; then
libelf_cpp_flags="\
-I/nyan/libelf/current/include/libelf \
-I/nyan/libelf/current/include \
"
external_deps_cpp_flags="$libelf_cpp_flags $external_deps_cpp_flags"
fi
if test "${libelf_static_ld_flags-unset}" = unset; then
libelf_static_ld_flags=/nyan/libelf/current/lib/libelf.a
fi
#===============================================================================


#===============================================================================
# zlib
if test "${zlib_cpp_flags-unset}" = unset; then
zlib_cpp_flags='-I/nyan/zlib/current/include -DHAVE_ZLIB=1'
external_deps_cpp_flags="$zlib_cpp_flags $external_deps_cpp_flags"
fi
if test "${zlib_static_ld_flags-unset}" = unset; then
zlib_static_ld_flags=/nyan/zlib/current/lib/libz.a
fi
#===============================================================================


#===============================================================================
# expat
if test "${expat_cpp_flags-unset}" = unset; then
expat_cpp_flags='-I/nyan/expat/current/include'
external_deps_cpp_flags="$expat_cpp_flags $external_deps_cpp_flags"
fi
if test "${expat_archives-unset}" = unset; then
expat_archives=libexpat.a
external_deps_archives="$expat_archives:$external_deps_archives"
fi
if test "${expat_ld_flags-unset}" = unset; then
expat_ld_flags="/nyan/expat/current/lib/$expat_archives"
external_deps_static_ld_flags="$expat_ld_flags $external_deps_static_ld_flags"
fi
#===============================================================================


#===============================================================================
# xorgproto
if test "${xorgproto_cpp_flags-unset}" = unset; then
xorgproto_cpp_flags='-I/nyan/xorgproto/current/include'
external_deps_cpp_flags="$xorgproto_cpp_flags $external_deps_cpp_flags"
fi
#===============================================================================

  
#===============================================================================
# libx11
if test "${libx11_cpp_flags-unset}" = unset; then
libx11_cpp_flags='-I/nyan/libX11/current/include'
external_deps_cpp_flags="$libx11_cpp_flags $external_deps_cpp_flags"
fi
if test "${libx11_ld_flags-unset}" = unset; then
libx11_ld_flags='-L/nyan/libX11/current/lib -lX11'
external_deps_ld_flags="$external_deps_ld_flags $libx11_ld_flags"
fi
#===============================================================================


#===============================================================================
# libx11-xcb
if test "${libx11_xcb_ld_flags-unset}" = unset; then
libx11_xcb_ld_flags='-L/nyan/libX11/current/include -lX11-xcb'
fi
#===============================================================================


#===============================================================================
# libxext
if test "${libxext_cpp_flags-unset}" = unset; then
libxext_cpp_flags='-I/nyan/libXext/current/include'
external_deps_cpp_flags="$libxext_cpp_flags $external_deps_cpp_flags"
fi
if test "${libxext_ld_flags-unset}" = unset; then
libxext_ld_flags='-L/nyan/libXext/current/lib -lXext'
external_deps_ld_flags="$external_deps_ld_flags $libxext_ld_flags"
fi
#===============================================================================


#===============================================================================
# libxcb
if test "${libxcb_cpp_flags-unset}" = unset; then
libxcb_cpp_flags='-I/nyan/libxcb/current/include'
external_deps_cpp_flags="$libxcb_cpp_flags $external_deps_cpp_flags"
fi
if test "${libxcb_ld_flags-unset}" = unset; then
libxcb_ld_flags="-L/nyan/libxcb/current/lib \
-lxcb \
-lxcb-glx \
-lxcb-present \
-lxcb-dri3 \
-lxcb-dri2 \
-lxcb-sync \
-lxcb-xfixes \
-lxcb-shm \
-lxcb-randr \
"
external_deps_ld_flags="$external_deps_ld_flags $libxcb_ld_flags"
fi
#===============================================================================


#===============================================================================
# libxshmfence
if test "${libxshmfence_cpp_flags-unset}" = unset; then
libxshmfence_cpp_flags='-I/nyan/libxshmfence/current/include'
external_deps_cpp_flags="$libxshmfence_cpp_flags $external_deps_cpp_flags"
fi
if test "${libxshmfence_ld_flags-unset}" = unset; then
libxshmfence_ld_flags="-L/nyan/libxshmfence/current/lib -lxshmfence"
external_deps_ld_flags="$external_deps_ld_flags $libxshmfence_ld_flags"
fi
#===============================================================================


#===============================================================================
# libxxf86vm
if test "${libxxf86vm_cpp_flags-unset}" = unset; then
libxxf86vm_cpp_flags='-I/nyan/libXxf86vm/current/include'
external_deps_cpp_flags="$libxxf86vm_cpp_flags $external_deps_cpp_flags"
fi
if test "${libxxf86vm_ld_flags-unset}" = unset; then
libxxf86vm_ld_flags='-L/nyan/libXxf86vm/current/lib -lXxf86vm'
external_deps_ld_flags="$external_deps_ld_flags $libxxf86vm_ld_flags"
fi
#===============================================================================


#===============================================================================
# libXfixes
if test "${libxfixes_cpp_flags-unset}" = unset; then
libxfixes_cpp_flags='-I/nyan/libXfixes/current/include'
external_deps_cpp_flags="$libxfixes_cpp_flags $external_deps_cpp_flags"
fi
if test "${libxfixes_ld_flags-unset}" = unset; then
libxfixes_ld_flags='-L/nyan/libXfixes/current/lib -lXfixes'
external_deps_ld_flags="$external_deps_ld_flags $libxfixes_ld_flags"
fi
#===============================================================================


#===============================================================================
# libXdamage
if test "${libxdamage_cpp_flags-unset}" = unset; then
libxdamage_cpp_flags='-I/nyan/libXdamage/current/include'
external_deps_cpp_flags="$libxdamage_cpp_flags $external_deps_cpp_flags"
fi
if test "${libxdamage_ld_flags-unset}" = unset; then
libxdamage_ld_flags='/nyan/libXdamage/current/lib/libXdamage.so'
external_deps_ld_flags="$external_deps_ld_flags $libxdamage_ld_flags"
fi
#===============================================================================


#===============================================================================
# libglvnd
if test "${libglvnd_cpp_flags-unset}" = unset; then
libglvnd_cpp_flags='-I/nyan/libglvnd/current/include'
external_deps_cpp_flags="$libglvnd_cpp_flags $external_deps_cpp_flags"
fi
#===============================================================================
