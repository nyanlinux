src_name=openssh
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

#-------------------------------------------------------------------------------
# fix missing prototypes of compat openbsd funcs which are in static libressl
# libs... without that 64 bits values are truncated to 32 bits values...
cd $pkg_dir
cp $nyan_root/builders/$pkg_name/missing-protos.h ./
sed -i -e '$ d' ./includes.h
printf '#include "missing-protos.h"\n#endif /* INCLUDES_H */' >>./includes.h
#-------------------------------------------------------------------------------

build_dir=$builds_dir_root/$src_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
/nyan/pkgconf/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

export "PKG_CONFIG_LIBDIR=/nyan/libressl/current/lib/pkgconfig"
export 'CFLAGS=-O2 -pipe -fPIC'
export "CC=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-rpath-link,/nyan/glibc/current/lib \
	-Wl,-s \
	-static-libgcc"
export AR=$target_gnu_triple-ar
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--without-stackprotect \
	--without-hardening \
	--disable-strip \
	--with-zlib=/nyan/zlib/current \
	--with-ssl-dir=/nyan/libressl/current \
	"--with-libs=$(pkgconf --static --libs libcrypto)"
unset PKG_CONFIG_LIBDIR
unset CFLAGS
unset CC
unset AR

make -j $threads_n
make install

rm -Rf /nyan/$src_name/$slot/share

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
