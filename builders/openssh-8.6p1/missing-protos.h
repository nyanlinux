void *recallocarray(void *ptr, size_t oldnmemb, size_t newnmemb, size_t size);
size_t strlcat(char *dst, const char *src, size_t siz);
void freezero(void *ptr, size_t sz);
int timingsafe_bcmp(const void *b1, const void *b2, size_t n);
size_t strlcpy(char *dst, const char *src, size_t siz);
void arc4random_buf(void *buf, size_t n);
u_int32_t arc4random(void);
u_int32_t arc4random_uniform(u_int32_t upper_bound);
size_t strlcpy(char *dst, const char *src, size_t siz);
