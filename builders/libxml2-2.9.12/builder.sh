src_name=libxml2
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export PKG_CONFIG_LIBDIR=

export "CC=gcc \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include \
-I/nyan/zlib/current/include \
-L/nyan/zlib/current/lib \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
-Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$pkg_dir/configure \
	--prefix=/nyan/$src_name/$slot \
	--disable-shared \
	--enable-static
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/$slot/share/doc
rm -Rf /nyan/$src_name/$slot/share/man
rm -f /nyan/$src_name/$slot/lib/*.la 
find /nyan/$src_name/$slot/lib -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then strip -s $f; fi; done

rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
