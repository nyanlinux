src_name=binutils
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.xz
url0=http://ftpmirror.gnu.org/$src_name/$archive_name

target_arch=riscv64
# linux-gnu counts as 1, gnu as gnu libc, or glibc, could become musl
target_triple=$target_arch-unknown-linux-gnu

pkg_dir=$pkgs_dir_root/rv64/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root/rv64
cp -f $src_dir_root/$archive_name $pkgs_dir_root/rv64
cd $pkgs_dir_root/rv64
tar xf $archive_name

build_dir=$builds_dir_root/rv64/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# Usual autotools/meson/cmake crap.
mkdir $build_dir/bin
cat >$build_dir/bin/makeinfo <<EOF
#!/bin/sh
printf "Makeinfo crap detected!\n"
EOF
chmod +x $build_dir/bin/makeinfo

PATH_SAVED=$PATH
export PATH="\
$build_dir/bin:\
/nyan/make/current/bin:\
$PATH\
"

# We should create a compiler driver wrapper to please libtool/cmake/meson crap.
# --static is a libtool special keyword to force building static binaries, because libtool is whole
# compiler driver by itself.
export "CC=\
/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-I/nyan/zlib/current/include \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-L/nyan/zlib/current/lib \
	-Wl,-s -static-libgcc \
	--static"
export 'CFLAGS=-O2 -pipe -fPIC -ftls-model=global-dynamic'
export AR=/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc-ar
$pkg_dir/configure \
	--target=$target_triple \
	--prefix=/nyan/toolchains/rv64/$src_name/$slot \
	--program-prefix=$target_triple- \
	--enable-gold=no \
	--enable-gprofng=no \
	--with-system-zlib \
  	--with-static-standard-libraries \
	--disable-multilib \
	--disable-plugins \
	--enable-serial-host-configure \
	--enable-serial-build-configure \
	--enable-serial-target-configure \
	--enable-year2038 \
	--with-mmap  \
	--disable-libctf \
	--disable-rpath \
	--disable-nls \
	--disable-checks
unset CC
unset CFLAGS
unset AR

make -j $threads_n
make install 

# cleanup and tidying
export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
