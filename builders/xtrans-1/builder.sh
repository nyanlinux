src_name=xtrans
version=1.4.0
archive_name=$src_name-$version.tar.bz2
url0=http://xorg.freedesktop.org/releases/individual/lib/$archive_name

slot=1

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export "PKG_CONFIG_LIBDIR=/nyan/util-macro/0/share/pkgconfig"
export PKG_CONFIG_LIBDIR=

# need a working compiler because of util-macro
export "CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure \
	--prefix=/nyan/xtrans/$slot \
	--disable-docs

make -j $threads_n
make install

rm -Rf $build_dir $src_dir
