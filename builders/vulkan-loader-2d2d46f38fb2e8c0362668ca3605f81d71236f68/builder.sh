src_name=vulkan-loader
mkdir /nyan/$src_name
git_commit=${pkg_name##*-}
slot=$git_commit
mkdir /nyan/$src_name/$slot

git_url0=https://github.com/khronosgroup/vulkan-loader

pkg_dir=$pkgs_dir_root/$src_name
rm -Rf $pkg_dir
src_dir=$src_dir_root/$src_name
cp -r $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/git/current/bin:\
$PATH\
"

if test "x$git_commit" != "x"; then
	git checkout --force $git_commit
	git reset --hard
fi

# install our canonical build system from the contrib dir
rm -Rf $pkg_dir/contrib
cp -rf $nyan_root/builders/$pkg_name/contrib $pkg_dir/contrib

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir $build_dir
cd $build_dir

$pkg_dir/contrib/x86_64_linux_gnu_xlib_xcb.sh

mkdir /nyan/$src_name/$slot/lib
cp -f $build_dir/libvulkan.so.1.3.299 /nyan/$src_name/$slot/lib/libvulkan.so.1.3.299

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
