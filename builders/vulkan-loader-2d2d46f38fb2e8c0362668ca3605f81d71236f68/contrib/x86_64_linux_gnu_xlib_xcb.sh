#!/bin/sh

# Canonical specialized build scripts on gnu/linux distros.
# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the script at the top of mesa source tree, create somewhere else
# a build directory, cd into it, and call from there this script.
#===============================================================================

set -e

#===============================================================================
# build dir and src dir
build_dir=$(readlink -f .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(readlink -f $(dirname $0)/..)
echo "src_dir=$src_dir"
#===============================================================================


#===============================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===============================================================================


#===============================================================================
if test "${sysconfdir-unset}" = unset; then
sysconfdir='/nyan/vulkan-loader/current/etc'
fi
#===============================================================================


#===============================================================================
if test "${libxcb_cppflags-unset}" = unset; then
libxcb_cppflags='-I/nyan/libxcb/current/include'
fi
#===============================================================================


#===============================================================================
if test "${xorgproto_cppflags-unset}" = unset; then
xorgproto_cppflags='-I/nyan/xorgproto/current/include'
fi
#===============================================================================


#===============================================================================
if test "${libx11_cppflags-unset}" = unset; then
libx11_cppflags='-I/nyan/libX11/current/include'
fi
#===============================================================================


#===============================================================================
if test "${libxrandr_cppflags-unset}" = unset; then
libxrandr_cppflags='-I/nyan/libXrandr/current/include'
fi
#===============================================================================


#===============================================================================
if test "${libxrender_cppflags-unset}" = unset; then
libxrender_cppflags='-I/nyan/libXrender/current/include'
fi
#===============================================================================


#===============================================================================
if test "${vulkan_headers-unset}" = unset; then
vulkan_headers=/nyan/vulkan-headers/current/include
fi
#===============================================================================


#===============================================================================
# build system compiler, _not_  the target compiler if you cross compile
if test "${build_cc-unset}" = unset; then
build_cc="gcc -pipe -O2 -c -static-libgcc \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include"
fi
if test "${build_ccld-unset}" = unset; then
build_ccld="gcc -static-libgcc -pipe \
-B/nyan/glibc/current/lib \
-L/nyan/glibc/current/lib"
fi
#===============================================================================


#===============================================================================
# all symbols are tagged hidden by default, then only the public symbol
# will be tagged public explicitely in the code (with extensions to C)
if test "${cc-unset}" = unset; then
cc="gcc -pipe -fpic -fPIC -O2 -c -static-libgcc -fvisibility=hidden \
-fno-strict-aliasing \
-isystem /nyan/glibc/current/include \
-isystem /nyan/linux-headers/current/include"
fi

if test "${ccas-unset}" = unset; then
ccas="gcc -pipe -c -fvisibility=hidden"
fi
#===============================================================================


#===============================================================================
# generate and do assemble some assembler for fast procedure call dispatch
$build_cc $src_dir/loader/asm_offset.c -o $build_dir/asm_offset.o \
	-I$build_dir \
	-I$src_dir/loader/generated \
	-I$src_dir/loader \
	-I$vulkan_headers
$build_ccld $build_dir/asm_offset.o -o $build_dir/asm_offset

# will generate $build_dir/gen_defines.asm
old_pwd=$PWD
cd $build_dir
$build_dir/asm_offset GAS
cd $old_pwd

$ccas $src_dir/loader/unknown_ext_chain_gas_x86.S \
-o $build_dir/unknown_ext_chain_gas_x86.o \
-I$build_dir &
loader_objs="$loader_objs $build_dir/unknown_ext_chain_gas_x86.o"
#===============================================================================


#===============================================================================
cppflags="\
-I$build_dir \
-I$src_dir/loader/generated \
-I$src_dir/loader \
-I$vulkan_headers \
$libxcb_cppflags \
$libx11_cppflags \
$xorgproto_cppflags \
$libxrandr_cppflags \
$libxrender_cppflags \
-DVK_USE_PLATFORM_XCB_KHR \
-DVK_USE_PLATFORM_XLIB_KHR \
-DVK_USE_PLATFORM_XLIB_XRANDR_EXT \
-DSYSCONFDIR=\"$sysconfdir\" \
-DFALLBACK_DATA_DIRS=\"/usr/local/share:/usr/share\" \
-DFALLBACK_CONFIG_DIRS=\"/etc/xdg\" \
-D_GNU_SOURCE \
-DHAVE_ALLOCA_H \
-DHAVE_SECURE_GETENV \
"

normal_loader_c_files="\
$src_dir/loader/allocation.c \
$src_dir/loader/cJSON.c \
$src_dir/loader/debug_utils.c \
$src_dir/loader/extension_manual.c \
$src_dir/loader/loader_environment.c \
$src_dir/loader/gpa_helper.c \
$src_dir/loader/loader.c \
$src_dir/loader/log.c \
$src_dir/loader/settings.c \
$src_dir/loader/terminator.c \
$src_dir/loader/trampoline.c \
$src_dir/loader/unknown_function_handling.c \
$src_dir/loader/wsi.c \
\
$src_dir/loader/loader_linux.c \
"

opt_loader_base_c_files="\
$src_dir/loader/dev_ext_trampoline.c \
$src_dir/loader/phys_dev_ext.c \
"

for f in $normal_loader_c_files $opt_loader_base_c_files
do
	loader_obj=$build_dir/$(basename $f .c).o
	loader_objs="$loader_objs $loader_obj"

	$cc $cppflags $f -o $loader_obj &
done
#===============================================================================


#===============================================================================
wait

# XXX:the attempt to generalize without kludge the link stage of real program
# build is near a total and complete failure and allows sdk nasty kludges to
# sneak in
if test "${loader_link_cmd-unset}" = unset ; then
loader_link_cmd="gcc -o $build_dir/libvulkan.so.1.3.299 -Wl,-soname=libvulkan.so.1 \
-shared -static-libgcc \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
-Wl,-s \
-Wl,--no-undefined,--gc-sections,-Bsymbolic \
	$loader_objs \
	-Wl,--as-needed \
	-lpthread \
	-ldl \
	-lm \
	-Wl,--no-as-needed"
fi
eval $loader_link_cmd
#===============================================================================
