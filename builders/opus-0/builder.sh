src_name=opus
version=1.2.1
archive_name=$src_name-$version.tar.gz
url0=http://downloads.xiph.org/releases/$src_name/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export 'CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc'
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--prefix=/nyan/opus/0		\
	--enable-static			\
	--disable-shared		\
	--disable-doc 			\
	--disable-extra-programs
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup and tidying
rm -f /nyan/$src_name/0/lib/*.la

rm -Rf $build_dir $src_dir
