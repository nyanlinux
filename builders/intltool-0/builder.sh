src_name=intltool
version=0.51.0
archive_name=$src_name-$version.tar.gz
url0=https://launchpad.net/intltool/trunk/$version/+download/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

# patch this perl shit or xkeyboard-config won't configure
cd $src_dir
cp $nyan_root/builders/$pkg_name/regex.patch ./
patch -i regex.patch

build_dir=$builds_dir_root/$pkg_name-$version
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

export PERL=/nyan/perl/current/bin/perl
$src_dir/configure			\
	--prefix=/nyan/intltool/0
unset PERL

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/0/share/man

rm -Rf $build_dir $src_dir
