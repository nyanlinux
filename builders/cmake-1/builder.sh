src_name=cmake
major=3
minor=18
micro=0
version=$major.$minor.$micro
src_dir=$src_dir_root/$src_name-$version
archive_name=$src_name-$version.tar.gz
url0=http://www.cmake.org/files/v$major.$minor/$archive_name

rm -Rf $src_dir
cd $src_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir

cd $build_dir

export 'CC=gcc'
export "CFLAGS=-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc -O2 -pipe -fPIC"
export 'CXX=g++'
export "CXXFLAGS=-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc -static-libstdc++ -O2 -pipe -fPIC"
$src_dir/configure --prefix=/nyan/cmake/1 --parallel=$threads_n -- -DCMAKE_USE_OPENSSL=OFF
unset CXXFLAGS
unset CXX
unset CFLAGS
unset CC

make -j $threads_n
make install

# cleanup and tidying
rm -Rf /nyan/$src_name/1/doc
strip -s /nyan/$src_name/1/bin/* || true

rm -Rf $build_dir $src_dir
