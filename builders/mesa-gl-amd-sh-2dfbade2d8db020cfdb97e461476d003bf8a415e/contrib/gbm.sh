echo "building GBM (Generic Buffer Manager) components-->"
# only one backend: dri
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/gbm
################################################################################
# first the dri backend
#-------------------------------------------------------------------------------
gbm_dri_c_pathnames="\
$src_dir/src/gbm/backends/dri/gbm_dri.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $gbm_dri_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gbm/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gbm/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/frontends/dri \
		-I$src_dir/src/gallium/frontends/dri \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/gbm/backends/dri \
		-I$src_dir/src/gbm/backends/dri \
		-I$build_dir/src/gbm/main \
		-I$src_dir/src/gbm/main \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $gbm_dri_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gbm/$cpp_filename --> $build_dir/src/gbm/$asm_filename\n"
	$cc_s $build_dir/src/gbm/$cpp_filename -o $build_dir/src/gbm/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $gbm_dri_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	printf "AS $build_dir/src/gbm/$asm_filename --> $build_dir/src/gbm/$o_filename\n"
	$as $build_dir/src/gbm/$asm_filename -o $build_dir/src/gbm/$o_filename &
done
#-------------------------------------------------------------------------------
#===============================================================================
wait
#===============================================================================
mkdir $build_dir/to_install/gbm
#-------------------------------------------------------------------------------
printf "BINUTILS LD $build_dir/to_install/gbm/dri_gbm.so\n"
$binutils_ld -o $build_dir/to_install/gbm/dri_gbm.so \
	-shared \
	-soname dri_gbm.so \
	--no-undefined \
	--gc-sections \
	-s \
	$syslib_ld_slib_start_files \
		$cc_ld_slib_start_files \
			$build_dir/src/gbm/gbm_dri.cpp.c.s.o \
			\
			$build_dir/libloader.a \
			$build_dir/libxmlconfig.a \
			$build_dir/libmesa_util.a \
			\
			--as-needed \
				$build_dir/to_install/libgallium_dri.so  \
				$libdrm_ld_flags \
				$syslibs_libm_ld_flags \
				$syslibs_libdl_ld_flags \
				$syslibs_libpthread_ld_flags \
				$syslibs_libc_ld_flags \
			--no-as-needed \
		$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
################################################################################
# now the gbm itself
gbm_c_pathnames="\
$src_dir/src/gbm/main/backend.c \
$src_dir/src/gbm/main/gbm.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $gbm_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gbm/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gbm/$cpp_filename \
		-DDEFAULT_BACKENDS_PATH=\"$libdir/gbm\" \
		\
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/frontends/dri \
		-I$src_dir/src/gallium/frontends/dri \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/gbm/backends/dri \
		-I$src_dir/src/gbm/backends/dri \
		-I$build_dir/src/gbm/main \
		-I$src_dir/src/gbm/main \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $gbm_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gbm/$cpp_filename --> $build_dir/src/gbm/$asm_filename\n"
	$cc_s $build_dir/src/gbm/$cpp_filename -o $build_dir/src/gbm/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $gbm_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	printf "AS $build_dir/src/gbm/$asm_filename --> $build_dir/src/gbm/$o_filename\n"
	$as $build_dir/src/gbm/$asm_filename -o $build_dir/src/gbm/$o_filename &
done
#-------------------------------------------------------------------------------
#===============================================================================
wait
#===============================================================================
printf "BINUTILS LD $build_dir/to_install/libgbm.so.1.0.0\n"
$binutils_ld -o $build_dir/to_install/libgbm.so.1.0.0  \
	-shared \
	-soname libgbm.so.1 \
	--no-undefined \
	--gc-sections \
	-s \
	$syslib_ld_slib_start_files \
		$cc_ld_slib_start_files \
			$build_dir/src/gbm/backend.cpp.c.s.o \
			$build_dir/src/gbm/gbm.cpp.c.s.o \
			$build_dir/src/gbm/gbm_dri.cpp.c.s.o \
			\
			$build_dir/libloader.a \
			$build_dir/libxmlconfig.a \
			$build_dir/libmesa_util.a \
			\
			--as-needed \
				$build_dir/to_install/libgallium_dri.so  \
				$libdrm_ld_flags \
				$syslibs_libm_ld_flags \
				$syslibs_libdl_ld_flags \
				$syslibs_libpthread_ld_flags \
				$syslibs_libc_ld_flags \
			--no-as-needed \
		$cc_ld_slib_end_files \
	$syslib_ld_slib_end_files
#-------------------------------------------------------------------------------
ln -s libgbm.so.1.0.0 $build_dir/to_install/libgbm.so.1
#===============================================================================
cp $src_dir/src/gbm/main/gbm.h $build_dir/to_install/include/gbm.h
#===============================================================================
echo "<--GBM components built"
