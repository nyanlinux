echo "building AMD GPU related common component-->"
#===============================================================================
# AMD/addrlib... AMD did not get the memo about c++ toxicity.
#------------------------------------------------------------------------------
mkdir $build_dir/src
mkdir $build_dir/src/amd
mkdir $build_dir/src/amd/addrlib
#------------------------------------------------------------------------------
# filenames must be different as we put everything in one directory
libaddrlib_cxx_pathnames="\
$src_dir/src/amd/addrlib/src/addrinterface.cpp \
$src_dir/src/amd/addrlib/src/core/addrelemlib.cpp \
$src_dir/src/amd/addrlib/src/core/addrlib.cpp \
$src_dir/src/amd/addrlib/src/core/addrlib1.cpp \
$src_dir/src/amd/addrlib/src/core/addrlib2.cpp \
$src_dir/src/amd/addrlib/src/core/addrlib3.cpp \
$src_dir/src/amd/addrlib/src/core/addrobject.cpp \
$src_dir/src/amd/addrlib/src/core/addrswizzler.cpp \
$src_dir/src/amd/addrlib/src/core/coord.cpp \
$src_dir/src/amd/addrlib/src/gfx12/gfx12addrlib.cpp \
$src_dir/src/amd/addrlib/src/gfx11/gfx11addrlib.cpp \
$src_dir/src/amd/addrlib/src/gfx10/gfx10addrlib.cpp \
$src_dir/src/amd/addrlib/src/gfx9/gfx9addrlib.cpp \
$src_dir/src/amd/addrlib/src/r800/ciaddrlib.cpp \
$src_dir/src/amd/addrlib/src/r800/egbaddrlib.cpp \
$src_dir/src/amd/addrlib/src/r800/siaddrlib.cpp \
"
for src_pathname in $libaddrlib_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	printf "CXXPP $src_pathname --> $build_dir/src/amd/addrlib/$cxxpp_filename\n"
	$cxxpp $src_pathname -o $build_dir/src/amd/addrlib/$cxxpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$cxx_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-DLITTLEENDIAN_CPU \
		-I$build_dir/src/amd/addrlib/src/chip/r800 \
		-I$src_dir/src/amd/addrlib/src/chip/r800 \
		-I$build_dir/src/amd/addrlib/src/chip/gfx9 \
		-I$src_dir/src/amd/addrlib/src/chip/gfx9 \
		-I$build_dir/src/amd/addrlib/src/chip/gfx10 \
		-I$src_dir/src/amd/addrlib/src/chip/gfx10 \
		-I$build_dir/src/amd/addrlib/src/chip/gfx11 \
		-I$src_dir/src/amd/addrlib/src/chip/gfx11 \
		-I$build_dir/src/amd/addrlib/src/chip/gfx12 \
		-I$src_dir/src/amd/addrlib/src/chip/gfx12 \
		-I$build_dir/src/amd/addrlib/src/core \
		-I$src_dir/src/amd/addrlib/src/core \
		-I$build_dir/src/amd/addrlib/src \
		-I$src_dir/src/amd/addrlib/src \
		-I$build_dir/src/amd/addrlib/inc \
		-I$src_dir/src/amd/addrlib/inc \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libaddrlib_cxx_pathnames
do
	cxxpp_filename=$(basename $src_pathname .cpp).cpp.cxx
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	printf "CXX_S $build_dir/src/amd/addrlib/$cxxpp_filename --> $build_dir/src/amd/addrlib/$asm_filename\n"
	$cxx_s $build_dir/src/amd/addrlib/$cxxpp_filename -o $build_dir/src/amd/addrlib/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libaddrlib_cxx_pathnames
do
	asm_filename=$(basename $src_pathname .cpp).cpp.cxx.s
	o_filename=$(basename $src_pathname .cpp).cpp.cxx.s.o
	os="$os $build_dir/src/amd/addrlib/$o_filename"
	printf "AS $build_dir/src/amd/addrlib/$asm_filename --> $build_dir/src/amd/addrlib/$o_filename\n"
	$as $build_dir/src/amd/addrlib/$asm_filename -o $build_dir/src/amd/addrlib/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libaddrlib.a $os\n"
$ar_rcs $build_dir/libaddrlib.a $os
#===============================================================================
# amd/common
#------------------------------------------------------------------------------
mkdir $build_dir/src/amd/common
mkdir $build_dir/src/amd/common/nir
#------------------------------------------------------------------------------
libamd_common_c_pathnames="\
$src_dir/src/amd/common/nir/ac_nir.c \
$src_dir/src/amd/common/nir/ac_nir_cull.c \
$src_dir/src/amd/common/nir/ac_nir_create_gs_copy_shader.c \
$src_dir/src/amd/common/nir/ac_nir_lower_esgs_io_to_mem.c \
$src_dir/src/amd/common/nir/ac_nir_lower_global_access.c \
$src_dir/src/amd/common/nir/ac_nir_lower_image_opcodes_cdna.c \
$src_dir/src/amd/common/nir/ac_nir_lower_intrinsics_to_args.c \
$src_dir/src/amd/common/nir/ac_nir_lower_legacy_gs.c \
$src_dir/src/amd/common/nir/ac_nir_lower_legacy_vs.c \
$src_dir/src/amd/common/nir/ac_nir_lower_mem_access_bit_sizes.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ngg.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ngg_gs.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ngg_mesh.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ps_early.c \
$src_dir/src/amd/common/nir/ac_nir_lower_ps_late.c \
$src_dir/src/amd/common/nir/ac_nir_lower_resinfo.c \
$src_dir/src/amd/common/nir/ac_nir_lower_sin_cos.c \
$src_dir/src/amd/common/nir/ac_nir_lower_taskmesh_io_to_mem.c \
$src_dir/src/amd/common/nir/ac_nir_lower_tess_io_to_mem.c \
$src_dir/src/amd/common/nir/ac_nir_lower_tex.c \
$src_dir/src/amd/common/nir/ac_nir_meta_cs_blit.c \
$src_dir/src/amd/common/nir/ac_nir_meta_cs_clear_copy_buffer.c \
$src_dir/src/amd/common/nir/ac_nir_meta_ps_resolve.c \
$src_dir/src/amd/common/nir/ac_nir_opt_outputs.c \
$src_dir/src/amd/common/nir/ac_nir_opt_pack_half.c \
$src_dir/src/amd/common/nir/ac_nir_opt_shared_append.c \
$src_dir/src/amd/common/nir/ac_nir_prerast_utils.c \
$src_dir/src/amd/common/nir/ac_nir_surface.c \
$src_dir/src/amd/common/amd_family.c \
$src_dir/src/amd/common/ac_binary.c \
$src_dir/src/amd/common/ac_cmdbuf.c \
$src_dir/src/amd/common/ac_debug.c \
$src_dir/src/amd/common/ac_descriptors.c \
$src_dir/src/amd/common/ac_ib_parser.c \
$src_dir/src/amd/common/ac_formats.c \
$src_dir/src/amd/common/ac_linux_drm.c \
$src_dir/src/amd/common/ac_parse_ib.c \
$src_dir/src/amd/common/ac_perfcounter.c \
$src_dir/src/amd/common/ac_pm4.c \
$src_dir/src/amd/common/ac_shader_util.c \
$src_dir/src/amd/common/ac_shader_args.c \
$src_dir/src/amd/common/ac_shadowed_regs.c \
$src_dir/src/amd/common/ac_gather_context_rolls.c \
$src_dir/src/amd/common/ac_gpu_info.c \
$src_dir/src/amd/common/ac_surface.c \
$src_dir/src/amd/common/ac_surface_meta_address_test.c \
$src_dir/src/amd/common/ac_msgpack.c \
$src_dir/src/amd/common/ac_rtld.c \
$src_dir/src/amd/common/ac_rgp.c \
$src_dir/src/amd/common/ac_rgp_elf_object_pack.c \
$src_dir/src/amd/common/ac_spm.c \
$src_dir/src/amd/common/ac_sqtt.c \
$src_dir/src/amd/common/ac_surface_modifier_test.c \
$src_dir/src/amd/common/ac_vcn_enc.c \
"

for src_pathname in $libamd_common_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/amd/common/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/amd/common/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/amd/common/nir \
		-I$src_dir/src/amd/common/nir \
		-I$build_dir/src/amd/common \
		-I$src_dir/src/amd/common \
		-I$build_dir/src/amd \
		-I$src_dir/src/amd \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libamd_common_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/amd/common/$cpp_filename --> $build_dir/src/amd/common/$asm_filename\n"
	$cc_s $build_dir/src/amd/common/$cpp_filename -o $build_dir/src/amd/common/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libamd_common_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/amd/common/$o_filename"
	printf "AS $build_dir/src/amd/common/$asm_filename --> $build_dir/src/amd/common/$o_filename\n"
	$as $build_dir/src/amd/common/$asm_filename -o $build_dir/src/amd/common/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libamd_common.a $os\n"
$ar_rcs $build_dir/libamd_common.a $os
#===============================================================================
echo "<--AMD GPU related common components built"
