printf "\tbuilding frontends sub-components-->\n"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/gallium
mkdir $build_dir/src/gallium/frontends
mkdir $build_dir/src/gallium/frontends/dri
#-------------------------------------------------------------------------------
# XXX: should merge with libdri.a (below)
libdricommon_c_pathnames="\
$src_dir/src/gallium/frontends/dri/dri_util.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $libdricommon_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/frontends/dri/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/frontends/dri/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/frontends/dri \
		-I$src_dir/src/gallium/frontends/dri \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src/mapi \
		-I$src_dir/src/mapi \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src/ \
		-I$src_dir/src/ \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libdricommon_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/frontends/dri/$cpp_filename --> $build_dir/src/gallium/frontends/dri/$asm_filename\n"
	$cc_s $build_dir/src/gallium/frontends/dri/$cpp_filename -o $build_dir/src/gallium/frontends/dri/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libdricommon_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/frontends/dri/$o_filename"
	printf "AS $build_dir/src/gallium/frontends/dri/$asm_filename --> $build_dir/src/gallium/frontends/dri/$o_filename\n"
	$as $build_dir/src/gallium/frontends/dri/$asm_filename -o $build_dir/src/gallium/frontends/dri/$o_filename &
done
#-------------------------------------------------------------------------------
wait
#-------------------------------------------------------------------------------
printf "AR RCS $build_dir/libdricommon.a $os\n"
$ar_rcs $build_dir/libdricommon.a $os
#===============================================================================
libdri_c_pathnames="\
$src_dir/src/gallium/frontends/dri/dri_context.c \
$src_dir/src/gallium/frontends/dri/dri_drawable.c \
$src_dir/src/gallium/frontends/dri/dri_helpers.c \
$src_dir/src/gallium/frontends/dri/dri_query_renderer.c \
$src_dir/src/gallium/frontends/dri/dri_screen.c \
$src_dir/src/gallium/frontends/dri/kopper_stubs.c \
$src_dir/src/gallium/frontends/dri/dri2.c \
$src_dir/src/gallium/frontends/dri/loader_dri3_helper.c \
$src_dir/src/gallium/frontends/dri/drisw.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $libdri_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/frontends/dri/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/frontends/dri/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/frontends/dri \
		-I$src_dir/src/gallium/frontends/dri \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mesa/drivers/dri/common \
		-I$src_dir/src/mesa/drivers/dri/common \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/mapi \
		-I$src_dir/src/mapi \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libdri_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/frontends/dri/$cpp_filename --> $build_dir/src/gallium/frontends/dri/$asm_filename\n"
	$cc_s $build_dir/src/gallium/frontends/dri/$cpp_filename -o $build_dir/src/gallium/frontends/dri/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libdri_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/frontends/dri/$o_filename"
	printf "AS $build_dir/src/gallium/frontends/dri/$asm_filename --> $build_dir/src/gallium/frontends/dri/$o_filename\n"
	$as $build_dir/src/gallium/frontends/dri/$asm_filename -o $build_dir/src/gallium/frontends/dri/$o_filename &
done
#-------------------------------------------------------------------------------
wait
#-------------------------------------------------------------------------------
printf "AR RCS $build_dir/libdri.a $os\n"
$ar_rcs $build_dir/libdri.a $os
#===============================================================================
printf "\t<--frontends sub-components built\n"
