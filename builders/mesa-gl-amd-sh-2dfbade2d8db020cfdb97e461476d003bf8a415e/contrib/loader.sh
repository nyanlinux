echo "building loader components-->"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/loader
#-------------------------------------------------------------------------------
# XXX: see below
loader_c_pathnames="\
$src_dir/src/loader/loader.c \
$src_dir/src/loader/loader_dri_helper.c \
$src_dir/src/x11/loader_x11.c \
"
#-------------------------------------------------------------------------------
for src_pathname in $loader_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/loader/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/loader/$cpp_filename \
		-DUSE_DRICONF=1 \
		-DDEFAULT_DRIVER_DIR=\"$dri_driver_search_dir\" \
		\
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/frontends/dri \
		-I$src_dir/src/gallium/frontends/dri \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/mesa \
		-I$src_dir/src/mesa \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $loader_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/loader/$cpp_filename --> $build_dir/src/loader/$asm_filename\n"
	$cc_s $build_dir/src/loader/$cpp_filename -o $build_dir/src/loader/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $loader_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	printf "AS $build_dir/src/loader/$asm_filename --> $build_dir/src/loader/$o_filename\n"
	$as $build_dir/src/loader/$asm_filename -o $build_dir/src/loader/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
# this is for a loader on x11/dri platform to go in the generic loader archive
printf "AR RCS $build_dir/libloader.a $build_dir/src/loader/loader.cpp.c.s.o $build_dir/src/loader/loader_dri_helper.cpp.c.s.o\n"
$ar_rcs $build_dir/libloader.a $build_dir/src/loader/loader.cpp.c.s.o $build_dir/src/loader/loader_dri_helper.cpp.c.s.o &
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libloader_x11.a $build_dir/src/loader/loader_x11.cpp.c.s.o\n"
$ar_rcs $build_dir/libloader_x11.a $build_dir/src/loader/loader_x11.cpp.c.s.o
#------------------------------------------------------------------------------
wait
#===============================================================================
echo "<--loader components built"
