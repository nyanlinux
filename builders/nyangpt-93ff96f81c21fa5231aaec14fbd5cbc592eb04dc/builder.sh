src_name=nyangpt
version=${pkg_name##*-}
git_commit=$version
slot=$version
url0=

pkg_dir=$pkgs_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -Hr $src_dir_root/$src_name $pkgs_dir_root

PATH_SAVED=$PATH
export PATH="\
/nyan/git/current/bin:\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

cd $pkg_dir

if test x$git_commit != x; then
	git reset --hard
	git checkout $git_commit
fi

mkdir -p /nyan/$src_name/$slot/bin
ln -sTf $slot /nyan/$src_name/current

$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B /nyan/glibc/current/lib \
	-Wl,-s \
	$pkg_dir/nyangpt.c \
	-o /nyan/$src_name/current/bin/$src_name
	
export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $build_dir $pkg_dir
