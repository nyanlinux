#XXX not yet in a toolchain
src_name=cproc
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=https://git.sr.ht/~mcf/cproc

pkg_dir=/run/pkgs/$src_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p /run/pkgs
cp -r $src_dir $pkg_dir

tcc_path=/nyan/toolchains/binutils-tinycc/current

PATH_SAVED=$PATH
export PATH="\
$tcc_path/bin:\
/nyan/make/current/bin:\
/nyan/git/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d $tcc_path/bin/*-tcc)")
target_gnu_triple=${target_gnu_triple%-tcc}

cd $pkg_dir

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

build_dir=$builds_dir_root/$src_name-$slot
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# XXX we don't run the configure script and genereate config.h and config.mk ourself
# XXX we are trying to make it work with a 0-driver tcc
defines=
cat >config.h <<EOF
static const char target[]               = "$target_gnu_triple";
static const char *const startfiles[]    = {"",""}; /* XXX: EMPTY FOR NOW */
static const char *const endfiles[]      = {"",""}; /* XXX: EMPTY FOR NOW */
static const char *const preprocesscmd[] = {
	"/nyan/cproc/$slot/bin/cpp",

	/* clear preprocessor GNU C version */
	"-U", "__TINYC__",
	"-U", "__TCC_PP__",

	/* we don't yet support these optional features */
	"-D", "__STDC_NO_ATOMICS__",
	"-D", "__STDC_NO_COMPLEX__",
	"-D", "__STDC_NO_VLA__",
	"-U", "__SIZEOF_INT128__",

	/* we don't generate position-independent code */
	"-U", "__PIC__",

	/* ignore attributes and extension markers */
	"-D", "__attribute__(x)=",
	"-D", "__extension__=",
$defines};
static const char *const codegencmd[]    = {"/nyan/qbe/current/bin/qbe"};
static const char *const assemblecmd[]   = {"$tcc_path/bin/$target_gnu_triple-as"};
static const char *const linkcmd[]       = {"$tcc_path/bin/$target_gnu_triple-ld", "-B/nyan/glibc/current/lib --dynamic-linker '/lib64/ld-linux-x86-64.so.2'"};
EOF

# makefile syntax (hence variable syntax)
cat >config.mk <<EOF
PREFIX=/nyan/cproc/$slot
BINDIR=\$(PREFIX)/bin
CC=$target_gnu_triple-tcc
CFLAGS=
LDFLAGS=
EOF

qbe_src_files="\
decl.c \
eval.c \
expr.c \
init.c \
main.c \
map.c \
pp.c \
scan.c \
scope.c \
siphash.c \
stmt.c \
targ.c \
token.c \
tree.c \
type.c \
utf.c \
util.c \
qbe.c \
"

driver_src_files="\
driver.c \
util.c \
"

for f in $driver_src_files $qbe_src_files
do
	cpp=$build_dir/$(basename $f .c).cpp.c

	printf "CPP $pkg_dir/$f-->$cpp\n"
	$target_gnu_triple-tcc \
		-E -nostdinc \
		-I$tcc_path/lib/tcc/include \
		-I/nyan/linux-headers/current/include \
		-I/nyan/glibc/current/include \
\
		-I$build_dir \
		-I$pkg_dir \
		-o $cpp \
		$pkg_dir/$f &
done 

wait

for f in $qbe_src_files
do
	cpp=$build_dir/$(basename $f .c).cpp.c
	o=$build_dir/$(basename $f .c).o
	qbe_os="$qbe_os $o"

	printf "CC $cpp-->$o\n"
	$target_gnu_triple-tcc -c -o $o $cpp &
done 

for f in $driver_src_files
do
	cpp=$build_dir/$(basename $f .c).cpp.c
	o=$build_dir/$(basename $f .c).o
	driver_os="$driver_os $o"

	printf "CC $cpp-->$o\n"
	$target_gnu_triple-tcc -c -o $o $cpp &
done 

wait

mkdir -p /nyan/cproc/$slot/bin

# XXX: too much conflict between gcc/glibc and tcc to build static binaries :(
printf "LDDBIN /nyan/cproc/$slot/bin/cproc\n"
$target_gnu_triple-ld \
	-pie -s \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	/nyan/glibc/current/lib/crt1.o \
	/nyan/glibc/current/lib/crti.o \
	$driver_os \
	-L/nyan/glibc/current/lib \
	-lc \
	/nyan/glibc/current/lib/crtn.o \
	-o /nyan/cproc/$slot/bin/cproc &

# XXX: too much conflict between gcc/glibc and tcc to build static binaries :(
printf "LDDBIN /nyan/cproc/$slot/bin/cproc-qbe\n"
$target_gnu_triple-ld \
	-pie -s \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	/nyan/glibc/current/lib/crt1.o \
	/nyan/glibc/current/lib/crti.o \
	$qbe_os \
	-L/nyan/glibc/current/lib \
	-lc \
	-L$tcc_path/lib/tcc \
	-ltcc1 \
	/nyan/glibc/current/lib/crtn.o \
	-o /nyan/cproc/$slot/bin/cproc-qbe

cat >/nyan/cproc/$slot/bin/cpp <<EOF
#!/bin/sh
exec $tcc_path/bin/$target_gnu_triple-tcc -E "\$@"
EOF
chmod +x /nyan/cproc/$slot/bin/cpp

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -Rf $pkg_dir $build_dir
wait
