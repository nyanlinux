src_name=ninja
git_commit=ca041d88f4d610332aa48c801342edfafb622ccb
git_url0=git://github.com/ninja-build/ninja.git

src_dir=$src_dir_root/$src_name
pkg_dir=$pkgs_dir_root/$pkg_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

cd $pkg_dir

git checkout --force $git_commit
git reset --hard

# fix python paths
find -name '*.py' | xargs -n1 sed -i -e 's:/usr/bin/env python:/nyan/python3/0/bin/python3:'
find -name '*.py' | xargs -n1 sed -i -e 's:/usr/bin/python:/nyan/python3/0/bin/python3:'

export 'CXX=g++'
export "CXXFLAGS=-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-rpath-link,/nyan/glibc/current/lib -static-libgcc -static-libstdc++ -O2 -pipe -fPIC"
./configure.py --bootstrap
unset CXXFLAGS
unset CXX

mkdir -p /nyan/ninja/0/bin
cp -f ./ninja /nyan/ninja/0/bin
strip -s /nyan/ninja/0/bin/ninja
rm -Rf $pkg_dir
