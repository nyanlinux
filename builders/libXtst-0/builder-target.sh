src_name=libXtst
version=1.2.3
archive_name=$src_name-$version.tar.bz2
url0=http://xorg.freedesktop.org/releases/individual/lib/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

export "PKG_CONFIG_LIBDIR=\
$target_sysroot/nyan/libXau/0/lib/pkgconfig:\
$target_sysroot/nyan/libpthread-stubs/0/lib/pkgconfig:\
$target_sysroot/nyan/libxcb/0/lib/pkgconfig:\
$target_sysroot/nyan/libX11/0/lib/pkgconfig:\
$target_sysroot/nyan/libXext/0/lib/pkgconfig:\
$target_sysroot/nyan/libXfixes/0/lib/pkgconfig:\
$target_sysroot/nyan/libXi/0/lib/pkgconfig:\
$target_sysroot/nyan/xorgproto/0/share/pkgconfig:\
$target_sysroot/nyan/util-macro/0/share/pkgconfig"
export PKG_CONFIG_SYSROOT_DIR=$target_sysroot

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure			\
	--build=$build_gnu_triple	\
	--host=$target_gnu_triple	\
	--prefix=/nyan/libXtst/0	\
	--disable-static		\
	--disable-specs			\
	--disable-lint-library		\
	--without-xmlto			\
	--without-fop			\
	--without-xsltproc		\
	--without-lint
unset CFLAGS
unset CC

make -j $threads_n
make install DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/$src_name/0/share
rm -f $target_sysroot/nyan/$src_name/0/lib/*.la 
find $target_sysroot/nyan/$src_name/0/lib -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then $target_gnu_triple-strip -s $f; fi; done

rm -Rf $build_dir $src_dir
OLD_PATH=$PATH
