#!/bin/sh

# Look for "unset", you'll find the values you can override in $1 or
# $build_dir/local_conf.sh file, that in order to tune the script for your
# specific distro/needs.

# Usage, drop the script at the top of mesa source tree, create somewhere else
# a build directory, cd into it, and call from there this script.

# XXX: the defaults are for our custom distro
#===================================================================================================
# build dir and src dir
build_dir=$(realpath .)
echo "build_dir=$build_dir"
# we are in contrib
src_dir=$(realpath $(dirname $0)/..)
echo "src_dir=$src_dir"
#===================================================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset"
# in those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===================================================================================================
if test "${xorgproto_inc_dir-unset}" = unset; then
xorgproto_inc_dir=/nyan/xorgproto/current/include
fi
xorgproto_cppflags="-I$xorgproto_inc_dir"
#===================================================================================================
if test "${libx11_inc_dir-unset}" = unset; then
libx11_inc_dir=/nyan/libX11/current/include
fi
libx11_cppflags="-I$libx11_inc_dir"
#---------------------------------------------------------------------------------------------------
if test "${libx11_lib_dir-unset}" = unset; then
libx11_lib_dir=/nyan/libX11/current/lib
fi
libx11_ldflags="-L$libx11_lib_dir -lX11"
#===================================================================================================
if test "${libsm_inc_dir-unset}" = unset; then
libsm_inc_dir=/nyan/libSM/current/include
fi
libsm_cppflags="-I$libsm_inc_dir"
#---------------------------------------------------------------------------------------------------
if test "${libsm_lib_dir-unset}" = unset; then
libsm_lib_dir=/nyan/libSM/current/lib
fi
libsm_ldflags="-L$libsm_lib_dir -lSM"
#===================================================================================================
if test "${libice_inc_dir-unset}" = unset; then
libice_inc_dir=/nyan/libICE/current/include
fi
libice_cppflags="-I$libice_inc_dir"
#---------------------------------------------------------------------------------------------------
if test "${libice_lib_dir-unset}" = unset; then
libice_lib_dir=/nyan/libICE/current/lib
fi
libice_ldflags="-L$libice_lib_dir -lICE"
#===================================================================================================
if test "${build_cpp-unset}" = unset; then
build_cpp="gcc -E \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
"
fi
#===================================================================================================
if test "${build_cc-unset}" = unset; then
build_cc="gcc -c \
	-std=c11 \
	-pipe -fPIC -O2 -ftls-model=global-dynamic -fpic \
	-static-libgcc"
fi
#===================================================================================================
# using the compiler driver, really a bad idea
if test "${build_exe_ccld-unset}" = unset; then
build_exe_ccld="gcc \
	-static-libgcc \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
"
fi
#===================================================================================================
if test "${cpp-unset}" = unset; then
cpp="gcc -E \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
"
fi
#===================================================================================================
if test "${slib_cc-unset}" = unset; then
slib_cc="gcc -c \
	-std=c11 \
	-pipe -fPIC -O2 -ftls-model=global-dynamic -fpic \
	-static-libgcc"
fi
#===================================================================================================
# we are still using the compiler driver, very bad idea
if test "${slib_ccld-unset}" = unset; then
slib_ccld="gcc \
	-shared \
	-static-libgcc \
	-B/nyan/glibc/current/lib \
	-L/nyan/glibc/current/lib \
	-Wl,-soname=libXt.so.6 \
	-Wl,--no-undefined \
	-Wl,-s \
"
fi
#===================================================================================================
# build code generator
mkdir $build_dir/util
printf "BUILD_CPP $src_dir/util/makestrs.c -> $build_dir/util/makestrs.cpp.c\n"
$build_cpp $src_dir/util/makestrs.c -o $build_dir/util/makestrs.cpp.c \
	-D_GNU_SOURCE \
	-DHAVE_ASPRINTF=1
printf "BUILD_CC $build_dir/util/makestrs.cpp.c -> $build_dir/util/makestrs.o\n"
$build_cc $build_dir/util/makestrs.cpp.c -o $build_dir/util/makestrs.o
printf "BUILD_EXE_CCLD $build_dir/util/makestrs.o -> $build_dir/util/makestrs\n"
$build_exe_ccld $build_dir/util/makestrs.o -o $build_dir/util/makestrs
#===================================================================================================
# generate code using the previously built code generator
mkdir $build_dir/src
mkdir $build_dir/include
mkdir $build_dir/include/X11
$build_dir/util/makestrs -i $src_dir <$src_dir/util/string.list >$build_dir/src/StringDefs.c
cp -f $build_dir/StringDefs.h $build_dir/include/X11/StringDefs.h
rm -f $build_dir/StringDefs.h
cp -f $build_dir/Shell.h $build_dir/include/X11/Shell.h
rm -f $build_dir/Shell.h
#===================================================================================================
glibc_cppflags="\
-D_GNU_SOURCE \
-DHAVE_DLFCN_H=1 \
-DHAVE_INTTYPES_H=1 \
-DHAVE_MALLOC_H=1 \
-DHAVE_MALLOC_USABLE_SIZE=1 \
-DHAVE_REALLOCARRAY=1 \
-DHAVE_STDINT_H=1 \
-DHAVE_STDIO_H=1 \
-DHAVE_STDLIB_H=1 \
-DHAVE_STRINGS_H=1 \
-DHAVE_STRING_H=1 \
-DHAVE_SYS_STAT_H=1 \
-DHAVE_SYS_TYPES_H=1 \
-DHAVE_UNISTD_H=1 \
-DHAVE_WCHAR_H=1 \
-DINCLUDE_ALLOCA_H=1 \
-DUSE_POLL=1 \
"
#===================================================================================================
src_files="\
$build_dir/src/StringDefs.c \
\
$src_dir/src/ActionHook.c \
$src_dir/src/Alloc.c \
$src_dir/src/ArgList.c \
$src_dir/src/Callback.c \
$src_dir/src/ClickTime.c \
$src_dir/src/Composite.c \
$src_dir/src/Constraint.c \
$src_dir/src/Convert.c \
$src_dir/src/Converters.c \
$src_dir/src/Core.c \
$src_dir/src/Create.c \
$src_dir/src/Destroy.c \
$src_dir/src/Display.c \
$src_dir/src/Error.c \
$src_dir/src/Event.c \
$src_dir/src/EventUtil.c \
$src_dir/src/Functions.c \
$src_dir/src/GCManager.c \
$src_dir/src/Geometry.c \
$src_dir/src/GetActKey.c \
$src_dir/src/GetResList.c \
$src_dir/src/GetValues.c \
$src_dir/src/HookObj.c \
$src_dir/src/Hooks.c \
$src_dir/src/Initialize.c \
$src_dir/src/Intrinsic.c \
$src_dir/src/Keyboard.c \
$src_dir/src/Manage.c \
$src_dir/src/NextEvent.c \
$src_dir/src/Object.c \
$src_dir/src/PassivGrab.c \
$src_dir/src/Pointer.c \
$src_dir/src/Popup.c \
$src_dir/src/PopupCB.c \
$src_dir/src/RectObj.c \
$src_dir/src/ResConfig.c \
$src_dir/src/Resources.c \
$src_dir/src/Selection.c \
$src_dir/src/SetSens.c \
$src_dir/src/SetValues.c \
$src_dir/src/SetWMCW.c \
$src_dir/src/Shell.c \
$src_dir/src/TMaction.c \
$src_dir/src/TMgrab.c \
$src_dir/src/TMkey.c \
$src_dir/src/TMparse.c \
$src_dir/src/TMprint.c \
$src_dir/src/TMstate.c \
$src_dir/src/Threads.c \
$src_dir/src/VarCreate.c \
$src_dir/src/VarGet.c \
$src_dir/src/Varargs.c \
$src_dir/src/Vendor.c \
$src_dir/src/sharedlib.c \
"
#===================================================================================================
for f in $src_files
do
	cpp_file=$(basename $f .c).cpp.c
	printf "CPP $f -> $build_dir/$cpp_file\n"
	$cpp -o $build_dir/$cpp_file $f \
		-DLIBXT_COMPILATION=1 \
		-DXKB=1 \
		-D_CONST_X_STRING=1 \
		-I$build_dir/include/X11 \
		-I$src_dir/include/X11 \
		-I$build_dir/include \
		-I$src_dir/include \
		$glibc_cppflags \
		$xorgproto_cppflags \
		$libx11_cppflags \
		$libsm_cppflags \
		$libice_cppflags
done
#===================================================================================================
wait
#===================================================================================================
os=
for f in $src_files
do
	cpp_file=$(basename $f .c).cpp.c
	o_file=$(basename $f .c).o
	os="$os $o_file"
	printf "SLIB_CC $build_dir/$cpp_file -> $build_dir/$o_file\n"
	$slib_cc -o $build_dir/$o_file $build_dir/$cpp_file &
done
#===================================================================================================
wait
#===================================================================================================
# see $archive/src/Makefile.am for the libtool version
printf "SLIB_CCLD $build_dir/libXt.so.6.0.0\n"
$slib_ccld -o $build_dir/libXt.so.6.0.0 $os \
	$libice_ldflags \
	$libsm_ldflags \
	$libx11_ldflags
#===================================================================================================
wait
