src_name=XML-Parser
version=2.44_01
archive_name=$src_name-$version.tar.gz
url0=http://search.cpan.org/CPAN/authors/id/T/TO/TODDR/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

cd $src_dir

# this will augment perl installation with the XML-Parser module only
# needed by intltool
# this module is actually a perl interface to expat

/nyan/perl/current/bin/perl Makefile.PL \
EXPATLIBPATH=/nyan/expat/current/lib \
EXPATINCPATH=/nyan/expat/current/include

make OPTIMIZE=-O2 

make install

# cleanup and tidying
find /nyan/perl/current -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then strip -s $f; fi; done

rm -Rf $src_dir
