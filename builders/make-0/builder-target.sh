src_name=make
git_commit=8a731d1b2cc262d03e0246a4869c704b6c1599ec
git_url0=git://git.savannah.gnu.org/$src_name.git

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -r $src_dir $pkg_dir

#-------------------------------------------------------------------------------
cd $pkg_dir
OLD_PATH=$PATH
export PATH=$sdk_gettext_path/bin:$PATH

git checkout --force $git_commit
git reset --hard

# remove doc
sed -i -e 's:doc/Makefile::' ./configure.ac
sed -i -e '/^SUBDIRS/ cSUBDIRS =       glob config po' ./Makefile.am

autoreconf -i
export PATH=$OLD_PATH
#-------------------------------------------------------------------------------

build_dir=$builds_dir_root/$pkg_name-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

export PKG_CONFIG_LIBDIR=
export PKG_CONFIG_SYSROOT_DIR=$target_sysroot

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$pkg_dir/configure					\
	--build=$build_gnu_triple			\
	--host=$target_gnu_triple			\
	--prefix=/nyan/make/0				\
	--disable-nls
unset CFLAGS
unset CC

make -j $threads_n
make install DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/$src_name/0/share
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/bin/$src_name

rm -Rf $build_dir $pkg_dir
export PATH=$OLD_PATH
