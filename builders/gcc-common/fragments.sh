# YOU MUST READ THIS FIRST:
# there are _TWO_ sysroot:
#	- the build sysroot: cross-compiling a compiler
#	- the sysroot: for any cross-compiler
# there are many bugs related to the configuration of those sysroots

# ALWAYS THINK LIKE YOU ARE CROSS-COMPLING A CROSS-COMPILER ("CANADIAN BUILD")
#                             **** ALWAYS *****

# This is the top level configure for the gcc source tree which actually does
# _global_ configuration and records some specific options you want to pass to
# the configuration stage of the actual gcc and related pieces of software,
# namely to know exactly how the real packages are configured, it is done in
# the top makefile. As of today, you cannot modularize the different
# sub-packages cleanly, coze depending on their configuration, they will use
# some stuff _inside_ the other packages. THIS IS *INSANE BAD DESIGN FOR THE
# CORE OF A GNU/LINUX SYSTEM* since in the middle of all that, *YOU NEED* an
# external component: the glibc!! Proper design: break gcc in ACTUAL modules,
# to make the glibc just one of them, God Damn It!
# native-system-header-dir is relative to sysroot (for a cross compiler)
opts_common="\
	--enable-serial-configure							\
	--disable-bootstrap								\
	--target=$target_gnu_triple							\
	--enable-threads=posix								\
	--disable-decimal-float								\
	--disable-fixed-point								\
	--disable-plugin								\
	--disable-multilib								\
	--disable-multiarch								\
	--disable-vtable-verify								\
	--disable-libquadmath								\
	--disable-libquadmath-support							\
	--disable-libada								\
	--disable-libgomp								\
	--disable-libatomic								\
	--disable-libcilkrts								\
	--disable-libitm								\
	--disable-libsanitizer								\
	--disable-libmpx								\
	--disable-libhsail-rt								\
	--disable-libstdcxx-pch								\
	--disable-lto									\
	--disable-nls									\
	--disable-libssp								\
	--disable-default-ssp								\
	--enable-link-mutex
"

# don't let sysroot empty
# The prefix *MUST* be the same than the binutils one or gcc won't find them at
# runtime.
build_opts_common="\
	--with-gmp=$cross_toolchain_dir_root				\
	--with-mpfr=$cross_toolchain_dir_root				\
	--with-mpc=$cross_toolchain_dir_root				\
	--with-sysroot=$target_sysroot					\
"

cross_static_target_libgcc_gcc_c_configure()
{
export 'CFLAGS=-O2 -pipe -fPIC'
export "CFLAGS_FOR_TARGET=-fPIC -O2 -pipe"
$src_dir/configure										\
	--prefix=$cross_toolchain_static_target_libgcc_dir_root					\
	--with-build-time-tools=$cross_toolchain_static_target_libgcc_dir_root			\
	--with-native-system-header-dir=/nyan/glibc/0/include-static-target-libgcc-linux	\
	--enable-languages=c									\
	--disable-shared									\
	$build_opts_common									\
	$opts_common
unset CFLAGS_FOR_TARGET
unset CFLAGS
}

cross_gcc_compilers_configure()
{
export 'CFLAGS=-O2 -pipe -fPIC'
export 'CXXFLAGS=-O2 -pipe -fPIC'
# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CFLAGS_FOR_TARGET=-O2 -pipe -fPIC -B$target_sysroot/nyan/glibc/0/lib -L$target_sysroot/nyan/glibc/0/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -I$target_sysroot/nyan/glibc/0/include-linux"
export "CXXFLAGS_FOR_TARGET=-O2 -pipe -fPIC -B$target_sysroot/nyan/glibc/0/lib -L$target_sysroot/nyan/glibc/0/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -I$target_sysroot/nyan/glibc/0/include-linux"
$src_dir/configure								\
	--prefix=$cross_toolchain_dir_root					\
	--with-build-time-tools=$cross_toolchain_dir_root			\
	--with-native-system-header-dir=/nyan/glibc/0/include-linux		\
	--enable-languages=c,c++						\
	$build_opts_common							\
	$opts_common
unset CXXFLAGS_FOR_TARGET
unset CFLAGS_FOR_TARGET
unset CXXFLAGS
unset CFLAGS
}

# this is to cross-compile gcc itself
# **** WARNING **** gcc fixinclude usage will _NOT_ honor the build_sysroot
# variable before gcc 8: then when cross-compiling a native compiler, it will
# look in the build system root for the target/native system headers (for gcc
# <8), if this dir is not defined with --with-native-system-header-dir, it will
# default to /usr/include dir (!!)
# this path will be wrongly hardcoded in gcc/cpp include search path list
gcc_compilers_cross_configure()
{
# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/0/lib -L$target_sysroot/nyan/glibc/0/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
export "CXX=$target_gnu_triple-g++ -B$target_sysroot/nyan/glibc/0/lib -L$target_sysroot/nyan/glibc/0/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc -static-libstdc++"
export 'CXXFLAGS=-O2 -pipe -fPIC'
export "CFLAGS_FOR_TARGET=-fPIC -O2 -B$target_sysroot/nyan/glibc/0/lib -L$target_sysroot/nyan/glibc/0/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib"
export "CXXFLAGS_FOR_TARGET=-fPIC -O2 -B$target_sysroot/nyan/glibc/0/lib -L$target_sysroot/nyan/glibc/0/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib"
# **** WARNING ****:the slibdir will be modified with the "multiosdir" from the
# cross compiler, usually, lib64...
$src_dir/configure								\
	--build=$build_gnu_triple						\
	--host=$target_gnu_triple						\
	--enable-languages=c,c++						\
	$opts_common								\
	--prefix=/nyan/toolchains/0						\
	--with-slibdir=/nyan/toolchains/0/lib					\
	--with-native-system-header-dir=/nyan/glibc/current/include-linux	\
	--with-build-sysroot=$target_sysroot					\
	--with-gmp=$target_sysroot/nyan/toolchains/0				\
	--with-mpfr=$target_sysroot/nyan/toolchains/0				\
	--with-mpc=$target_sysroot/nyan/toolchains/0
unset CXXFLAGS_FOR_TARGET
unset CFLAGS_FOR_TARGET
unset CXXFLAGS
unset CFLAGS
}
