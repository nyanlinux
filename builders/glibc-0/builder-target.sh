src_name=glibc
version=2.27
archive_name=$src_name-$version.tar.xz
url0=https://ftpmirror.gnu.org/libc/

. $nyan_root/builders/$src_name-common/fragments.sh

#######################################
#does not install locales or timezones#
#######################################

src_dir=$src_dir_root/$src_name-$version
rm -Rf $src_dir
cd $src_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_static_target_libgcc_dir_root/bin:$PATH

glibc_fix_pre_conf_ssp
glibc_configure
glibc_fix_iconvconfig

make -j $threads_n

make install DESTDIR=$target_sysroot
 
rm -Rf $build_dir $src_dir

# lighten the runtime
cp -f $cross_toolchain_static_target_libgcc_dir_root/bin/$target_gnu_triple-strip /tmp/strip
find $target_sysroot/nyan/glibc/0 -type f | while read f; do if file $f | egrep 'ELF.+(shared|executable)' >/dev/null; then /tmp/strip -s $f; fi; done
rm -f /tmp/strip

export PATH=$OLD_PATH
