# toolchain configuration, default to gcc
#===============================================================================
if test "${cpp-unset}" = unset; then
cpp="/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc \
	-nostdinc \
	-fPIC \
	-fpic \
	-ftls-model=global-dynamic \
	-E \
"
fi
# the geniuses writting compilers may have c++ required hardcoded definitions
# in the c++ preprocessor to be able to compile code...
if test "${cxxpp-unset}" = unset; then
# XXX: you must pass -fno-rtti here to disable __GXX_RTTI and -fno-exceptions to
# disable __EXCEPTIONS
# OMFG... c++...
cxxpp="/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-g++ \
	-fno-rtti \
	-fno-exceptions \
	-fPIC \
	-fpic \
	-ftls-model=global-dynamic \
	-nostdinc \
	-E \
"
fi
# c/c++ compiler specific fixed includes are based on the syslib at compiler build time, COMPILER
# BUILD TIME... oh god... must be found first to override the syslib ones...
if test "${cc_internal_fixed_incdir-unset}" = unset; then
cc_internal_fixed_incdir=/opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include-fixed
fi
# c/c++ compiler specific stdarg.h stddef.h etc...
if test "${cc_internal_incdir-unset}" = unset; then
cc_internal_incdir=/opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include
fi
# c++ compiler specific includes
if test "${cxx_internal_incdir-unset}" = unset; then
cxx_internal_incdir=/opt/toolchains/x64/elf/binutils-gcc/current/include/c++/13.2.0
fi
#===============================================================================
if test "${cc_s-unset}" = unset; then
cc_s="/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc \
	-pipe \
	-std=c99 \
	-O2 -Wno-stringop-overflow \
	-fvisibility=hidden \
	-static-libgcc \
	-fPIC \
	-fpic \
	-ftls-model=global-dynamic \
	-S"
fi
# aco is c++ crap from 2017, did not check AMD addrlib c++ diarrhea
if test "${cxx_s-unset}" = unset; then
cxx_s="/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-g++ \
	-pipe \
	-std=c++17 \
	-fno-rtti \
	-fno-exceptions \
	-O2 -Wno-stringop-overflow \
	-fvisibility=hidden \
	-static-libgcc \
	-static-libstdc++ \
	-fPIC \
	-fpic \
	-ftls-model=global-dynamic \
	-S"
fi
#-------------------------------------------------------------------------------
if test "${binutils_ld-unset}" = unset; then
binutils_ld=/opt/toolchains/x64/elf/binutils-gcc/current/bin/ld
fi
#-------------------------------------------------------------------------------
# Compiler generic support libraries. In the case of gcc it is libgcc, for tinycc
# it is libtcc1, etc. 
# Even with -fno-exceptions, many libstdc++ objects will pull libgcc_eh, OMFG.
# We want a full pathname. With gcc, you can discover the pathnames with:
# "gcc -print-file-name=libgcc.a" and "gcc -print-file-name=libgcc_eh.a"
if test "${cc_ld_support_lib-unset}" = unset; then
eval cc_ld_support_lib=$($cc_s -print-file-name=libgcc.a)
fi
if test "${cc_ld_eh_support_lib-unset}" = unset; then
eval cc_ld_eh_support_lib=$($cc_s -print-file-name=libgcc_eh.a)
fi
#-------------------------------------------------------------------------------
# Compiler c++ runtime.
# We want a full pathname. With gcc, you can discover the pathname with:
# "gcc -print-file-name=libstdc++.a"
if test "${cxx_runtime_lib-unset}" = unset; then
eval cxx_runtime_lib=$($cxx_s -print-file-name=libstdc++.a)
fi
#-------------------------------------------------------------------------------
# Compiler specific start/end files for linking a Shared LIBrary (slib), they
# appear after/before the syslib start/end files while linking. Here, as an
# example, look at the compiler driver $gcc_src/gcc/config/gnu-user.h (13.2.0)
# if you want to understand a bit more in the case of glibc, shared libs do fall
# in the "shared" category.
# With gcc, their pathname can be discovered with:
# "gcc -print-file-name=crtbeginS.o" and "gcc -print-file-name=crtendS.o"
if test "${cc_ld_slib_start_files-unset}" = unset; then
eval cc_ld_slib_start_files=$($cc_s -print-file-name=crtbeginS.o)
fi
if test "${cc_ld_slib_end_files-unset}" = unset; then
eval cc_ld_slib_end_files=$($cc_s -print-file-name=crtendS.o)
fi
#===============================================================================
if test "${as-unset}" = unset; then
as=/opt/toolchains/x64/elf/binutils-gcc/current/bin/as
fi
#===============================================================================
if test "${ar_rcs-unset}" = unset; then
ar_rcs='/opt/toolchains/x64/elf/binutils-gcc/current/bin/ar rcs'
fi
#===============================================================================
# gcc-like built-in
cc_builtins_cpp_flags_defs="\
-DHAVE___BUILTIN_BSWAP32=1 \
-DHAVE___BUILTIN_BSWAP64=1 \
-DHAVE___BUILTIN_CLZ=1 \
-DHAVE___BUILTIN_CLZLL=1 \
-DHAVE___BUILTIN_CTZ=1 \
-DHAVE___BUILTIN_EXPECT=1 \
-DHAVE___BUILTIN_FFS=1 \
-DHAVE___BUILTIN_FFSLL=1 \
-DHAVE___BUILTIN_POPCOUNT=1 \
-DHAVE___BUILTIN_POPCOUNTLL=1 \
-DHAVE___BUILTIN_UNREACHABLE=1 \
-DUSE_GCC_ATOMIC_BUILTINS=1 \
"
#-------------------------------------------------------------------------------
# gcc-like attributes
cc_attributes_cpp_flags_defs="\
-DHAVE_FUNC_ATTRIBUTE_CONST=1 \
-DHAVE_FUNC_ATTRIBUTE_FLATTEN=1 \
-DHAVE_FUNC_ATTRIBUTE_MALLOC=1 \
-DHAVE_FUNC_ATTRIBUTE_PURE=1 \
-DHAVE_FUNC_ATTRIBUTE_UNUSED=1 \
-DHAVE_FUNC_ATTRIBUTE_WARN_UNUSED_RESULT=1 \
-DHAVE_FUNC_ATTRIBUTE_WEAK=1 \
\
-DHAVE_FUNC_ATTRIBUTE_FORMAT=1 \
-DHAVE_FUNC_ATTRIBUTE_PACKED=1 \
-DHAVE_FUNC_ATTRIBUTE_RETURNS_NONNULL=1 \
-DHAVE_FUNC_ATTRIBUTE_VISIBILITY=1 \
-DHAVE_FUNC_ATTRIBUTE_ALIAS=1 \
-DHAVE_FUNC_ATTRIBUTE_NORETURN=1 \
"
