#!/bin/sh
version_0=21
version_1=1
version_2=99
version_3=1
#===================================================================================================
# build dir and src dir
build_dir=$(readlink -f .)
printf "build_dir=$build_dir\n"
# we are in contrib subdir
src_dir=$(readlink -f $(dirname $0)/..)
printf "src_dir=$src_dir\n"
#===================================================================================================
# the current configur-able variables may be individually overridden with the
# content of the file in $1 or $build_dir/local_conf.sh. Look for "unset" in
# those scripts to find what you can override to tune the build.
if test -f "$1"; then
	. "$1"
else
	if test -f $build_dir/local_conf.sh; then
		. $build_dir/local_conf.sh
	fi
fi
#===================================================================================================
if test "${cpp-unset}" = unset; then
cpp=false
fi
#===================================================================================================
if test "${slib_cc-unset}" = unset; then
slib_cc=false
fi
if test "${slib_cc_ld_start_files-unset}" = unset; then
slib_cc_ld_start_files=
fi
if test "${slib_cc_ld_end_files-unset}" = unset; then
slib_cc_ld_end_files=
fi
#===================================================================================================
if test "${slib_binutils_ld-unset}" = unset; then
slib_binutils_ld=false
fi
if test "${readelf-unset}" = unset; then
readelf=
fi
#===================================================================================================
if test "${xorgproto_cppflags-unset}" = unset; then
xorgproto_cppflags=
fi
#===================================================================================================
# It wants pixman, libpciaccess,  headers
if test "${xserver_cppflags-unset}" = unset; then
xserver_cppflags=
fi
#===================================================================================================
if test "${libudev_cppflags-unset}" = unset; then
libudev_cppflags=
fi
if test "${libudev_ldflags-unset}" = unset; then
libudev_ldflags=
fi
#===================================================================================================
if test "${mtdev_cppflags-unset}" = unset; then
mtdev_cppflags=
fi
if test "${mtdev_ldflags-unset}" = unset; then
mtdev_ldflags=
fi
#===================================================================================================
if test "${libevdev_cppflags-unset}" = unset; then
libevdev_cppflags=
fi
if test "${libevdev_ldflags-unset}" = unset; then
libevdev_ldflags=
fi
#===================================================================================================
# usually the glibc
if test "${crt_cppflags-unset}" = unset; then
crt_cppflags=
fi
if test "${crt_ldflags-unset}" = unset; then
crt_ldflags=
fi
if test "${slib_cc_ld_start_files-unset}" = unset; then
crt_ld_start_files=
fi
if test "${slib_cc_ld_end_files-unset}" = unset; then
crt_ld_end_files=
fi
#===================================================================================================
# evdev is linux thing
if test "${linux_uapi_cppflags-unset}" = unset; then
linux_uapi_cppflags=
fi
#===================================================================================================
if test "${pixman_cppflags-unset}" = unset; then
pixman_cppflags=
fi
#===================================================================================================
if test "${libpciaccess_cppflags-unset}" = unset; then
libpciaccess_cppflags=
fi
#===================================================================================================
cppflags="\
-DPACKAGE_VERSION_MAJOR=2 \
-DPACKAGE_VERSION_MINOR=10 \
-DPACKAGE_VERSION_PATCHLEVEL=6 \
-DHAVE_LIBUDEV \
\
$xserver_cppflags \
$pixman_cppflags \
$libpciaccess_cppflags \
$xorgproto_cppflags \
$libudev_cppflags \
$mtdev_cppflags \
$libevdev_cppflags \
$linux_uapi_cppflags \
$crt_cppflags \
\
-I$build_dir/include \
-I$src_dir/include \
\
-I$build_dir/src \
-I$src_dir/src \
"
#===================================================================================================
src_c_pathnames="\
$src_dir/src/emuMB.c \
$src_dir/src/emuThird.c \
$src_dir/src/emuWheel.c \
$src_dir/src/draglock.c \
$src_dir/src/apple.c \
\
$src_dir/src/evdev.c \
"
#===================================================================================================
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c

	printf "CPP $src_c_pathname-->$build_dir/$cpp_filename\n"
	$cpp $cppflags $src_c_pathname -o $build_dir/$cpp_filename &
done
wait
#===================================================================================================
for src_c_pathname in $src_c_pathnames
do
	cpp_filename=$(basename $src_c_pathname .c).cpp.c
	obj_filename=$(basename $src_c_pathname .c).cpp.c.o
	objs="$objs $build_dir/$obj_filename"

	printf "SLIB_CC $build_dir/$cpp_filename-->$build_dir/$obj_filename\n"
	$slib_cc $build_dir/$cpp_filename -o $build_dir/$obj_filename &
done
wait
#===================================================================================================
printf "BINUTILS LD $build_dir/evdev_drv.so\n"
$slib_binutils_ld \
	-shared \
	-o $build_dir/evdev_drv.so \
	-s \
	-soname=evdev_drv.so \
	$crt_ld_start_files \
		$slib_cc_ld_start_files \
			$objs \
			$libmtdev_ldflags \
			$libevdev_ldflags \
			$libudev_ldflags \
			$crt_ldflags \
		$slib_cc_ld_end_files \
	$crt_ld_end_files
#---------------------------------------------------------------------------------------------------
if test "${readelf-unset}" != unset; then
$readelf -a -W $build_dir/evdev_drv.so >$build_dir/evdev_drv.so.re
fi
