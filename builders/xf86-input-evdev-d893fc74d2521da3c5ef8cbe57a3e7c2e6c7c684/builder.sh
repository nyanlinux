src_name=xf86-input-evdev
git_commit=${pkg_name##*-}
slot=$git_commit
git_url0=git://gitlab.freedesktop.org/xorg/driver/$src_name

src_dir=$src_dir_root/$src_name
pkg_dir=$pkgs_dir_root/$pkg_name
mkdir -p $pkgs_dir_root
rm -Rf $pkg_dir
cp -r $src_dir $pkg_dir
#-------------------------------------------------------------------------------
cd $pkg_dir

if test x$git_commit != x; then
	/nyan/git/current/bin/git checkout --force $git_commit
	/nyan/git/current/bin/git reset --hard
fi
# copy the canonical lean build scripts
cp -r $nyan_root/builders/$pkg_name/contrib .
#-------------------------------------------------------------------------------
build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir
#-------------------------------------------------------------------------------
cat >$build_dir/local_conf.sh <<EOF
slib_cc="/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc \
-static-libgcc \
-ftls-model=global-dynamic \
-fvisibility=hidden \
-O2 -pipe -fpic -fPIC -std=c99 \
-c \
"
slib_cc_ld_start_files=/opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/crtbeginS.o
slib_cc_ld_end_files=/opt/toolchains/x64/elf/binutils-gcc/current/lib/gcc/x86_64-pc-linux-gnu/13.2.0/crtendS.o

slib_binutils_ld=/opt/toolchains/x64/elf/binutils-gcc/current/bin/ld
readelf=/opt/toolchains/x64/elf/binutils-gcc/current/bin/readelf

# must be gcc cpp because gcc is trash (and clang is no better).
cpp='/opt/toolchains/x64/elf/binutils-gcc/current/bin/x86_64-pc-linux-gnu-gcc -E -D__pic__=2 -D__PIC__=2'

xorgproto_cppflags=-I/nyan/xorgproto/current/include

libudev_cppflags=-I/nyan/mudev/current/include
libudev_ldflags='-L/nyan/mudev/current/lib -ludev'

mtdev_cppflags=-I/nyan/mtdev/current/include
libmtdev_ldflags='-Bstatic -L/nyan/mtdev/current/lib -lmtdev -Bdynamic'

# libevdev wants linux headers
libevdev_cppflags=-I/nyan/libevdev/current/include/libevdev-1.0
libevdev_ldflags='-Bstatic -L/nyan/libevdev/current/lib -levdev -Bdynamic'

xserver_cppflags=-I/nyan/xserver/current/include/xorg
pixman_cppflags=-I/nyan/pixman/current/include/pixman-1
libpciaccess_cppflags=-I/nyan/libpciaccess/current/include

linux_uapi_cppflags=-I/nyan/linux-headers/current/include

crt_cppflags=-I/nyan/glibc/current/include
crt_ldflags='-Bdynamic -L/nyan/glibc/current/lib -lc'
crt_ld_start_files=/nyan/glibc/current/lib/crti.o
crt_ld_end_files=/nyan/glibc/current/lib/crtn.o
EOF

#-------------------------------------------------------------------------------
$pkg_dir/contrib/linux-glibc.sh
#-------------------------------------------------------------------------------

mkdir -p /nyan/$src_name/$slot
cp -f $build_dir/evdev_drv.so $pkg_dir/10-evdev.conf /nyan/$src_name/$slot

if test ! -d /usr/lib/xorg/modules/input; then
	printf 'ERROR:missing xserver input module directory\n'
	exit 1
fi
# it is "current" not $slot
ln -sTf /nyan/$src_name/current/evdev_drv.so /usr/lib/xorg/modules/input/evdev_drv.so

if test ! -d /usr/share/X11/xorg.conf.d; then
	printf 'ERROR:missing xserver configuration directory\n'
	exit 1
fi
# it is "current" not $slot
ln -sTf /nyan/$src_name/current/10-evdev.conf /usr/share/X11/xorg.conf.d/10-evdev.conf

rm -Rf $build_dir $pkg_dir
