src_name=clzip
version=1.10
archive_name=$src_name-$version.tar.gz
url0=http://download.savannah.gnu.org/releases/lzip/$src_name/$archive_name

src_dir=$src_dir_root/$src_name-$version
cd $src_dir_root
rm -Rf $src_name-$version
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
$src_dir/configure																						\
	--prefix=/nyan/clzip/0																					\
	"CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"	\
	'CFLAGS=-O2 -pipe -fPIC'				

make -j $threads_n
make install-as-lzip DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/$src_name/0/share
$target_gnu_triple-strip -s $target_sysroot/nyan/$src_name/0/bin/$src_name

rm -Rf $build_dir $src_dir
export PATH=$OLD_PATH
