src_name=drm
git_url0=git://anongit.freedesktop.org/mesa/$src_name

src_dir=$src_dir_root/$src_name
pkg_dir=/run/pkgs/$src_name
rm -Rf $pkg_dir
mkdir -p /run/pkgs
cp -Hr $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/git/current/bin:\
/nyan/python/current/bin:\
$PATH\
"
if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi
#-------------------------------------------------------------------------------
# install our canonical build system from the contrib dir
cp -rf $nyan_root/builders/drm-$slot/contrib $pkg_dir

build_dir=/run/builds/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# generate our local configuration file
cat >$build_dir/local_conf.sh <<EOF
runtime_data_dir="/usr/share"
runtime_data_store_dir="/nyan/drm/$slot/share"
runtime_data_store_virtual_dir="/nyan/drm/current/share"

lib_store_dir="/nyan/drm/$slot/lib"
lib_store_virtual_dir="/nyan/drm/current/lib"

runtime_lib_dir="/usr/lib"

inc_store_dir="/nyan/drm/$slot/include"
inc_store_virtual_dir="/nyan/drm/current/include"

slib_gcc="gcc \
	-std=gnu99 -pipe -fPIC -O2 -c -static-libgcc -fvisibility=hidden \
	-ftls-model=global-dynamic -fpic \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include"

libdrm_slib_gccld="gcc \
-o \\\$build_dir/install_root\\\$lib_store_dir/libdrm.so.\\\$libdrm_so_major.\\\$libdrm_so_minor.\\\$libdrm_so_patch \
-Wl,-soname=libdrm.so.\\\$libdrm_so_major \
-pipe -shared -static-libgcc \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
-Wl,-rpath-link,/nyan/glibc/current/lib \
-Wl,--no-undefined,--gc-sections \
-Wl,-s \
\\\$libdrm_so \
-lm"

libdrm_amdgpu_slib_gccld="gcc \
-o \\\$build_dir/install_root\\\$lib_store_dir/libdrm_amdgpu.so.\\\$libdrm_amdgpu_so_major.\\\$libdrm_amdgpu_so_minor.\\\$libdrm_amdgpu_so_patch \
-Wl,-soname=libdrm_amdgpu.so.\\\$libdrm_amdgpu_so_major \
-pipe -shared -static-libgcc \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
-Wl,-rpath-link,/nyan/glibc/current/lib \
-Wl,--no-undefined,--gc-sections \
-Wl,-s \
\\\$libdrm_amdgpu_so \
\\\$build_dir/install_root\\\$lib_store_dir/libdrm.so.\\\$libdrm_so_major.\\\$libdrm_so_minor.\\\$libdrm_so_patch"

libdrm_radeon_slib_gccld="gcc \
-o \\\$build_dir/install_root\\\$lib_store_dir/libdrm_radeon.so.\\\$libdrm_radeon_so_major.\\\$libdrm_radeon_so_minor.\\\$libdrm_radeon_so_patch \
-Wl,-soname=libdrm_radeon.so.\\\$libdrm_radeon_so_major \
-pipe -shared -static-libgcc \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
-Wl,-rpath-link,/nyan/glibc/current/lib \
-Wl,--no-undefined,--gc-sections \
-Wl,-s \
\\\$libdrm_radeon_so \
\\\$build_dir/install_root\\\$lib_store_dir/libdrm.so.\\\$libdrm_so_major.\\\$libdrm_so_minor.\\\$libdrm_so_patch"
EOF

$pkg_dir/contrib/x86_64_amdgpu_linux_gnu_drm_gcc.sh

rm -Rf /nyan/drm/$slot/*
cp -Rf $build_dir/install_root/nyan/drm/$slot/* /nyan/drm/$slot

# we don't use the symbolic link oriented runtime, but we keep it.

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
