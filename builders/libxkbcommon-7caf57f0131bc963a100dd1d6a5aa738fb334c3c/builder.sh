src_name=libxkbcommon
git_commit=${pkg_name##*-}
slot=$git_commit
# version is at the top of the root meson.build file
version=1.6.0
git_url0=git://github.com/xkbcommon/libxkbcommon

pkg_dir=$pkgs_dir_root/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -L -r $src_dir $pkg_dir

cd $pkg_dir

if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

# X11 locale is the "compose" mini input method
cat >config.h <<EOF
#define DFLT_XKB_CONFIG_ROOT		"/nyan/xkeyboard-config/current/share/X11/xkb"
#define DFLT_XKB_CONFIG_EXTRA_PATH	""
#define XLOCALEDIR			"/usr/share/X11/locale"
#define LIBXKBCOMMON_VERSION		"$version"
#define DEFAULT_XKB_RULES		"evdev"
#define DEFAULT_XKB_MODEL		"pc105"
#define DEFAULT_XKB_LAYOUT		"us"
#define DEFAULT_XKB_VARIANT		""
#define DEFAULT_XKB_OPTIONS		""
/* glibc */
#define _GNU_SOURCE		1
#define HAVE__BUILTIN_EXPECT 	1
#define HAVE_EUIDACCESS		1
#define HAVE_MMAP		1
#define HAVE_MKOSTEMP		1
#define HAVE_POSIX_FALLOCATE	1
#define HAVE_STRNDUP		1
#define HAVE_UNISTD_H		1
/* no */
#undef HAVE_ASPRINTF
#undef HAVE_VASPRINTF

#define HAVE_SECURE_GETENV	1
#undef HAVE___SECURE_GETENV
EOF

# DON'T USE BISON EXTENSIONS!
# DON'T USE A YACC PROGRAM, THOSE ARE _BAD_ PARSERS AND IT CREATES NASTY BUILD
# DEPS.
# ALWAYS MANUALLY WRITE YOUR PARSERS!
# JEZUS! FOR GOD SAKE! YOU ANIMALS! PYTHON IS NOT ENOUGH ALREADY!
# TOXIC HUMAN BEINGS!
/nyan/bison/current/bin/bison --defines=parser.h -o $build_dir/parser.c -p _xkbcommon_ \
	$pkg_dir/src/xkbcomp/parser.y
srcs="\
$pkg_dir/src/compose/parser.c \
$pkg_dir/src/compose/paths.c \
$pkg_dir/src/compose/state.c \
$pkg_dir/src/compose/table.c \
$pkg_dir/src/xkbcomp/action.c \
$pkg_dir/src/xkbcomp/ast-build.c \
$pkg_dir/src/xkbcomp/compat.c \
$pkg_dir/src/xkbcomp/expr.c \
$pkg_dir/src/xkbcomp/include.c \
$pkg_dir/src/xkbcomp/keycodes.c \
$pkg_dir/src/xkbcomp/keymap.c \
$pkg_dir/src/xkbcomp/keymap-dump.c \
$pkg_dir/src/xkbcomp/keywords.c \
$build_dir/parser.c \
$pkg_dir/src/xkbcomp/rules.c \
$pkg_dir/src/xkbcomp/scanner.c \
$pkg_dir/src/xkbcomp/symbols.c \
$pkg_dir/src/xkbcomp/types.c \
$pkg_dir/src/xkbcomp/vmod.c \
$pkg_dir/src/xkbcomp/xkbcomp.c \
$pkg_dir/src/atom.c \
$pkg_dir/src/context.c \
$pkg_dir/src/context-priv.c \
$pkg_dir/src/keysym.c \
$pkg_dir/src/keysym-utf.c \
$pkg_dir/src/keymap.c \
$pkg_dir/src/keymap-priv.c \
$pkg_dir/src/state.c \
$pkg_dir/src/text.c \
$pkg_dir/src/utf8.c \
$pkg_dir/src/utils.c \
"
objs=
for f in $srcs
do
	obj=$build_dir/$(dirname $f)/$(basename $f .c).o
	mkdir -p $build_dir/$(dirname $f)
	/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
		-static-libgcc \
		-std=gnu99 \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-D_GNU_SOURCE \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		$f -o $obj &
	objs="$obj $objs"
done
wait
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
	-B/nyan/glibc/current/lib -shared \
	-Wl,--version-script=$pkg_dir/xkbcommon.map \
	-Wl,-soname=libxkbcommon.so.0 \
	-Wl,-s \
	-static-libgcc \
	$objs \
	-o $build_dir/libxkbcommon.so.0.0.0
ln -sTf libxkbcommon.so.0.0.0 $build_dir/libxkbcommon.so.0
ln -sTf libxkbcommon.so.0 $build_dir/libxkbcommon.so

cat >$build_dir/xkbcommon.pc <<EOF
prefix=/nyan/libxkbcommon/current
exec_prefix=/nyan/libxkbcommon/current
libdir=/nyan/libxkbcommon/current/lib
includedir=/nyan/libxkbcommon/current/include

Name: xkbcommon
Description: XKB API common to servers and clients
Version: $version
Cflags: -I\${includedir}
Libs: -L\${libdir} -lxkbcommon
EOF
#-------------------------------------------------------------------------------
# x11 helper lib
srcs="\
$pkg_dir/src/x11/keymap.c \
$pkg_dir/src/x11/state.c \
$pkg_dir/src/x11/util.c \
$pkg_dir/src/context-priv.c \
$pkg_dir/src/keymap-priv.c \
$pkg_dir/src/atom.c \
"
objs=
for f in $srcs
do
	obj=$build_dir/$(dirname $f)/$(basename $f .c).o
	mkdir -p $build_dir/$(dirname $f)
	/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
		-std=gnu99 \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-static-libgcc \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		-I/nyan/libxcb/current/include \
		$f -o $obj &
	objs="$obj $objs"
done
wait
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
	-static-libgcc \
	-B/nyan/glibc/current/lib -shared \
	-Wl,--version-script=$pkg_dir/xkbcommon-x11.map \
	-Wl,-soname=libxkbcommon-x11.so.0 \
	-Wl,-s \
	$objs \
	-L$build_dir -lxkbcommon \
	-L/nyan/libxcb/current/lib -lxcb-xkb \
	-o $build_dir/libxkbcommon-x11.so.0.0.0
ln -sTf libxkbcommon-x11.so.0.0.0 $build_dir/libxkbcommon-x11.so.0
ln -sTf libxkbcommon-x11.so.0 $build_dir/libxkbcommon-x11.so

cat >$build_dir/xkbcommon-x11.pc <<EOF
prefix=/nyan/libxkbcommon/current
exec_prefix=/nyan/libxkbcommon/current
libdir=/nyan/libxkbcommon/current/lib
includedir=/nyan/libxkbcommon/current/include

Name: xkbcommon-x11
Description: XKB API common to servers and clients - X11 support
Version: $version
Requires: xkbcommon
Requires.private: xcb xcb-xkb
Cflags: -I\${includedir}
Libs: -L\${libdir} -lxkbcommon-x11
EOF
#-------------------------------------------------------------------------------
# registry, pulling libxml2... jez... not event support for expat
# some other fanatics will push for json I guess
srcs="\
$pkg_dir/src/registry.c \
$pkg_dir/src/utils.c \
$pkg_dir/src/util-list.c \
"
objs=
for f in $srcs
do
	obj=$build_dir/$(dirname $f)/$(basename $f .c).o
	mkdir -p $build_dir/$(dirname $f)
	/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
		-std=gnu99 \
		-static-libgcc \
		-fvisibility=hidden -fPIC -O2 -pipe -ftls-model=global-dynamic -fpic -c \
		-isystem /nyan/glibc/current/include \
		-isystem /nyan/linux-headers/current/include \
		-I$build_dir \
		-I$pkg_dir/src \
		-I$pkg_dir/include \
		-I$pkg_dir \
		-I/nyan/libxml2/current/include/libxml2 \
		$f -o $obj &
	objs="$obj $objs"
done
wait
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
	-static-libgcc \
	-B/nyan/glibc/current/lib -shared \
	-Wl,-s \
	-Wl,--version-script=$pkg_dir/xkbregistry.map \
	-Wl,-soname=libxkbregistry.so.0 \
	$objs \
	-L/nyan/libxml2/current/lib -lxml2 \
	-L/nyan/zlib/current/lib -lz \
	-lm \
	-o $build_dir/libxkbregistry.so.0.0.0
ln -sTf libxkbregistry.so.0.0.0 $build_dir/libxkbregistry.so.0
ln -sTf libxkbregistry.so.0 $build_dir/libxkbregistry.so

cat >$build_dir/xkbregistry.pc <<EOF
prefix=/nyan/libxkbcommon/current
exec_prefix=/nyan/libxkbcommon/current
libdir=/nyan/libxkbcommon/current/lib
includedir=/nyan/libxkbcommon/current/include

Name: xkbregistry
Description: XKB API to query available rules, models, layouts, variants and options
Version: $version
Requires.private: libxml-2.0
Cflags: -I\${includedir}
Libs: -L\${libdir} -lxkbregistry
EOF
#-------------------------------------------------------------------------------
mkdir -p /nyan/$src_name/$slot/include/xkbcommon
mkdir -p /nyan/$src_name/$slot/lib/pkgconfig
mkdir -p /usr/lib
mv -f	$pkg_dir/include/xkbcommon/xkbcommon.h \
	$pkg_dir/include/xkbcommon/xkbcommon-compat.h \
	$pkg_dir/include/xkbcommon/xkbcommon-compose.h \
	$pkg_dir/include/xkbcommon/xkbcommon-keysyms.h \
	$pkg_dir/include/xkbcommon/xkbcommon-names.h \
	$pkg_dir/include/xkbcommon/xkbcommon-x11.h \
	$pkg_dir/include/xkbcommon/xkbregistry.h \
	/nyan/$src_name/$slot/include/xkbcommon
mv -f $build_dir/*.pc /nyan/$src_name/$slot/lib/pkgconfig
mv -f $build_dir/libxkbcommon.so.0.0.0 /nyan/$src_name/$slot/lib
ln -sTf libxkbcommon.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbcommon.so
# XXX:current, not slot
ln -sTf /nyan/$src_name/current/lib/libxkbcommon.so.0.0.0 /usr/lib/libxkbcommon.so.0
mv -f $build_dir/libxkbcommon-x11.so.0.0.0 /nyan/$src_name/$slot/lib
ln -sTf libxkbcommon-x11.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbcommon-x11.so
# XXX:current, not slot
ln -sTf /nyan/$src_name/current/lib/libxkbcommon-x11.so.0.0.0 /usr/lib/libxkbcommon-x11.so.0
mv -f $build_dir/libxkbregistry.so.0.0.0 /nyan/$src_name/$slot/lib
ln -sTf libxkbregistry.so.0.0.0 /nyan/$src_name/$slot/lib/libxkbregistry.so
# XXX:current, not slot
ln -sTf /nyan/$src_name/current/lib/libxkbregistry.so.0.0.0 /usr/lib/libxkbregistry.so.0

rm -Rf $build_dir $pkg_dir
