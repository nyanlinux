src_name=popt
version=1.16
network_archive_name=$src_name-$version.tar.gz
archive_name=$src_name-$version.tar
# WARNING, it's a bare tar archive, not a gzipped tar archive
url0=http://rpm5.org/files/popt/$network_archive_name


src_dir=$src_dir_root/$src_name-$version
rm -Rf $src_dir
cd $src_dir_root

cp -f $network_archive_name $archive_name
tar xf $archive_name
rm -f $archive_name

build_dir=$builds_dir_root/$pkg_name-$version-$target_gnu_triple
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

OLD_PATH=$PATH
export PATH=$cross_toolchain_dir_root/bin:$PATH

# at link time, the only way to tell gnu ld where to look for shared lib dependencies is to pass the -rpath-link option
export "CC=$target_gnu_triple-gcc -B$target_sysroot/nyan/glibc/current/lib -L$target_sysroot/nyan/glibc/current/lib -Wl,-rpath-link,$target_sysroot/nyan/glibc/current/lib -static-libgcc"
export 'CFLAGS=-O2 -pipe -fPIC'
$src_dir/configure						\
	--build=$build_gnu_triple				\
	--host=$target_gnu_triple				\
	--prefix=/nyan/popt/0					\
	--disable-nls						\
	--disable-shared			
unset CFLAGS
unset CC

make -j $threads_n
make install DESTDIR=$target_sysroot

# cleanup and tidying
rm -Rf $target_sysroot/nyan/$src_name/0/share
rm -Rf $target_sysroot/nyan/$src_name/0/lib/*.la || true

rm -Rf $build_dir $src_dir
export PATH=$OLD_PATH
