src_name=dav1d
git_url0=https://code.videolan.org/videolan/$src_name.git

pkg_dir=/run/pkgs/$pkg_name
src_dir=$src_dir_root/$src_name
rm -Rf $pkg_dir
mkdir -p /run/pkgs
cp -Hr $src_dir $pkg_dir

cd $pkg_dir

PATH_SAVED=$PATH
export PATH="\
/opt/toolchains/x64/elf/binutils-gcc/current/bin:\
/nyan/git/current/bin:\
/nyan/nasm/current/bin:\
$PATH\
"
if test x$git_commit != x; then
	git checkout --force $git_commit
	git reset --hard
fi

# install our canonical build system from the contrib dir
cp -rf $nyan_root/builders/$src_name-$slot/contrib $pkg_dir

build_dir=/run/builds/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

cat >$build_dir/local_conf.sh <<EOF
cc="gcc \
	-c -O2 -std=c99 -fPIC -fpic -pipe \
	-mpreferred-stack-boundary=6 -mincoming-stack-boundary=4 \
	-static-libgcc \
	-fvisibility=hidden \
	-isystem /nyan/linux-headers/current/include \
	-isystem /nyan/glibc/current/include"
ar='ar rcs'
slib_link_cmd="gcc \
	-o \\\$build_dir/libdav1d.so.\\\$version_major.\\\$version_minor.\\\$version_patch \
	\\\$libdav1d_objs \
	-static-libgcc \
	-shared \
	-Wl,-soname,libdav1d.so.\\\$version_major \
	-Wl,-s \
	-B/nyan/glibc/current/lib \
	-lpthread \
	-lm \
	-ldl \
"
EOF
$pkg_dir/contrib/x86_64_linux_glibc_gcc.sh

mkdir -p /nyan/dav1d/$slot/lib
mkdir -p /nyan/dav1d/$slot/include/dav1d

cp -f $build_dir/libdav1d.so.$major.$minor.$patch /nyan/dav1d/$slot/lib
ln -sTf libdav1d.so.$major.$minor.$patch /nyan/dav1d/$slot/lib/libdav1d.so.$major
ln -sTf libdav1d.so.$major /nyan/dav1d/$slot/lib/libdav1d.so

cp -f 	$pkg_dir/include/dav1d/common.h \
	$pkg_dir/include/dav1d/data.h \
	$pkg_dir/include/dav1d/dav1d.h \
	$pkg_dir/include/dav1d/headers.h \
	$pkg_dir/include/dav1d/picture.h \
	$pkg_dir/include/dav1d/version.h \
	/nyan/dav1d/$slot/include/dav1d

export PATH=$PATH_SAVED
unset PATH_SAVED
rm -Rf $build_dir $pkg_dir
