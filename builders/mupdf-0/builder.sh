src_name=mupdf
version=1.12.0
archive_name=$src_name-$version-source.tar.xz
url0=http://mupdf.com/downloads/$archive_name

src_dir=$src_dir_root/$src_name-$version-source
cd $src_dir_root
rm -Rf $src_name-$version-source
tar xf $archive_name

cd $src_dir

# Remove some third party
rm -Rf ./thirdparty/curl
rm -Rf ./thirdparty/freetype
rm -Rf ./thirdparty/harfbuzz
rm -Rf ./thirdparty/zlib
rm -Rf ./thirdparty/freeglut
rm -Rf ./thirdparty/libjpeg
rm -Rf ./thirdparty/lcms2
rm -Rf ./thirdparty/mujs

# fix the linux build which forces glut tools to be build
sed -i -e '/^HAVE_GLUT/ c\HAVE_GLUT := no' ./Makerules
sed -i -e '/^SYS_GLUT_LIBS/ c\SYS_GLUT_LIBS :=' ./Makerules

export "PKG_CONFIG_PATH=\
/nyan/zlib/current/lib/pkgconfig:\
/nyan/libpng/current/lib/pkgconfig:\
/nyan/freetype/current/lib/pkgconfig:\
/nyan/charfbuzz/current/lib/pkgconfig:\
/nyan/libXext/current/lib/pkgconfig:\
/nyan/libXau/current/lib/pkgconfig:\
/nyan/libpthread-stubs/current/lib/pkgconfig:\
/nyan/libxcb/current/lib/pkgconfig:\
/nyan/xorgproto/current/share/pkgconfig:\
/nyan/libX11/current/lib/pkgconfig"

# runs some programs
OLD_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
export "LD_LIBRARY_PATH=\
/nyan/libpng/current/lib:\
/nyan/freetype/current/lib:\
$LD_LIBRARY_PATH"

make											\
	build=release									\
	"CC=gcc -B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -static-libgcc \
-Wl,-rpath-link,\
/nyan/libXau/current/lib:\
/nyan/libxcb/current/lib:\
/nyan/glibc/current/lib"								\
	"XCFLAGS=-O2 -pipe -fPIC \
-I/nyan/zlib/current/include \
-I/nyan/bzip2/current/include \
-I/nyan/libjpeg-turbo/current/include \
-DFZ_ENABLE_XPS=0 \
-DFZ_ENABLE_SVG=0 \
-DFZ_ENABLE_CBZ=0 \
-DFZ_ENABLE_IMG=0 \
-DFZ_ENABLE_TIFF=0 \
-DFZ_ENABLE_HTML=0 \
-DFZ_ENABLE_EPUB=0 \
-DFZ_ENABLE_GPRF=0"									\
	"XLIBS=\
-L/nyan/zlib/current/lib \
-L/nyan/bzip2/current/lib \
-L/nyan/libjpeg-turbo/current/lib"							\
	verbose=yes									\
	prefix=/nyan/mupdf/0

unset PKG_CONFIG_PATH
export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH

make install			\
	build=release		\
	prefix=/nyan/mupdf/0

# the install process forgets mupdf-x11...
cp -f ./build/release/mupdf-x11 /nyan/mupdf/0/bin

# cleanup and tidying
rm -Rf /nyan/$src_name/0/share

rm -Rf $src_dir
