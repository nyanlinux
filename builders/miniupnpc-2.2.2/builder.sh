src_name=miniupnpc
version=${pkg_name##*-}
slot=$version
archive_name=$src_name-$version.tar.gz
url0=http://miniupnp.free.fr/files/download.php?file=$archive_name

pkg_dir=$pkgs_dir_root/$src_name-$version
rm -Rf $pkg_dir
mkdir -p $pkgs_dir_root
cp -f $src_dir_root/$archive_name $pkgs_dir_root
cd $pkgs_dir_root
tar xf $archive_name

cd $pkg_dir
 
PATH_SAVED=$PATH
export PATH="\
/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin:\
/nyan/make/current/bin:\
$PATH\
"
target_gnu_triple=$(basename "$(ls -d /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/*-gcc)")
target_gnu_triple=${target_gnu_triple%-gcc}

sed -i Makefile -e "/^CC ?=/ c\\
CC=$target_gnu_triple-gcc \
	-isystem /nyan/glibc/current/include \
	-isystem /nyan/linux-headers/current/include \
	-B/nyan/glibc/current/lib \
	-Wl,-s"
sed -i Makefile -e "/^CFLAGS ?=/ c\\
CFLAGS ?= -O2 -pipe -fPIC"

make AR=$target_gnu_triple-ar

mkdir -p /nyan/$src_name/$slot/bin
cp -f ./upnpc-static /nyan/$src_name/$slot/bin/upnpc

export PATH=$PATH_SAVED
unset PATH_SAVED
unset target_gnu_triple
rm -f $pkgs_dir_root/$archive_name
rm -Rf $build_dir $pkg_dir
