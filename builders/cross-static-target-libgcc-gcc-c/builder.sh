src_name=gcc
version=7.3.0
archive_name=$src_name-$version.tar.xz
url0=ftp://ftp.lip6.fr/pub/$src_name/releases/$src_name-$version/$archive_name

. $nyan_root/builders/gcc-common/fragments.sh

src_dir=$src_dir_root/$src_name-$version
rm -Rf $src_dir
cd $src_dir_root
tar xf $archive_name

build_dir=$builds_dir_root/$pkg_name
rm -Rf $build_dir
mkdir -p $build_dir
cd $build_dir

cross_static_target_libgcc_gcc_c_configure

make -j $threads_n all-gcc
make install-gcc

make all-target-libgcc
make install-target-libgcc

# clear .la files
find $cross_toolchain_dir_root -name '*.la' | xargs rm -f

rm -Rf $build_dir $src_dir
