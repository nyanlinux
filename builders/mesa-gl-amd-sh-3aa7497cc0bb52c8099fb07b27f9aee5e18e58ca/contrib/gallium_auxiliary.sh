printf "\tbuilding gallium auxiliary sub-components-->\n"
#===============================================================================
mkdir $build_dir/src
mkdir $build_dir/src/gallium
mkdir $build_dir/src/gallium/auxiliary
#-------------------------------------------------------------------------------
# filenames should be different since we build in one directory
libgallium_c_pathnames="\
$src_dir/src/gallium/auxiliary/cso_cache/cso_cache.c \
$src_dir/src/gallium/auxiliary/cso_cache/cso_cache.h \
$src_dir/src/gallium/auxiliary/cso_cache/cso_context.c \
$src_dir/src/gallium/auxiliary/cso_cache/cso_hash.c \
$src_dir/src/gallium/auxiliary/draw/draw_context.c \
$src_dir/src/gallium/auxiliary/draw/draw_fs.c \
$src_dir/src/gallium/auxiliary/draw/draw_gs.c \
$src_dir/src/gallium/auxiliary/draw/draw_mesh.c \
$src_dir/src/gallium/auxiliary/draw/draw_mesh_prim.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_aaline.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_aapoint.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_clip.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_cull.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_flatshade.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_offset.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_pstipple.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_stipple.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_twoside.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_unfilled.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_user_cull.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_util.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_validate.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_vbuf.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_wide_line.c \
$src_dir/src/gallium/auxiliary/draw/draw_pipe_wide_point.c \
$src_dir/src/gallium/auxiliary/draw/draw_prim_assembler.c \
$src_dir/src/gallium/auxiliary/draw/draw_private.h \
$src_dir/src/gallium/auxiliary/draw/draw_pt.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_emit.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_fetch.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_fetch_shade_emit.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_fetch_shade_pipeline.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_mesh_pipeline.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_post_vs.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_so_emit.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_util.c \
$src_dir/src/gallium/auxiliary/draw/draw_pt_vsplit.c \
$src_dir/src/gallium/auxiliary/draw/draw_tess.c \
$src_dir/src/gallium/auxiliary/draw/draw_vertex.c \
$src_dir/src/gallium/auxiliary/draw/draw_vs.c \
$src_dir/src/gallium/auxiliary/draw/draw_vs_exec.c \
$src_dir/src/gallium/auxiliary/draw/draw_vs_variant.c \
$src_dir/src/gallium/auxiliary/driver_ddebug/dd_context.c \
$src_dir/src/gallium/auxiliary/driver_ddebug/dd_draw.c \
$src_dir/src/gallium/auxiliary/driver_ddebug/dd_screen.c \
$src_dir/src/gallium/auxiliary/driver_noop/noop_pipe.c \
$src_dir/src/gallium/auxiliary/driver_noop/noop_state.c \
$src_dir/src/gallium/auxiliary/driver_trace/tr_context.c \
$src_dir/src/gallium/auxiliary/driver_trace/tr_dump.c \
$src_dir/src/gallium/auxiliary/driver_trace/tr_dump_state.c \
$src_dir/src/gallium/auxiliary/driver_trace/tr_public.h \
$src_dir/src/gallium/auxiliary/driver_trace/tr_screen.c \
$src_dir/src/gallium/auxiliary/driver_trace/tr_texture.c \
$src_dir/src/gallium/auxiliary/driver_trace/tr_video.c \
$build_dir/src/gallium/auxiliary/driver_trace/tr_util.c \
$src_dir/src/gallium/auxiliary/hud/font.c \
$src_dir/src/gallium/auxiliary/hud/hud_context.c \
$src_dir/src/gallium/auxiliary/hud/hud_cpu.c \
$src_dir/src/gallium/auxiliary/hud/hud_nic.c \
$src_dir/src/gallium/auxiliary/hud/hud_cpufreq.c \
$src_dir/src/gallium/auxiliary/hud/hud_diskstat.c \
$src_dir/src/gallium/auxiliary/hud/hud_sensors_temp.c \
$src_dir/src/gallium/auxiliary/hud/hud_driver_query.c \
$src_dir/src/gallium/auxiliary/hud/hud_fps.c \
$build_dir/src/gallium/auxiliary/indices/u_indices_gen.c \
$src_dir/src/gallium/auxiliary/indices/u_primconvert.c \
$build_dir/src/gallium/auxiliary/indices/u_unfilled_gen.c \
$src_dir/src/gallium/auxiliary/pipebuffer/pb_buffer_fenced.c \
$src_dir/src/gallium/auxiliary/pipebuffer/pb_bufmgr_cache.c \
$src_dir/src/gallium/auxiliary/pipebuffer/pb_bufmgr_debug.c \
$src_dir/src/gallium/auxiliary/pipebuffer/pb_bufmgr_mm.c \
$src_dir/src/gallium/auxiliary/pipebuffer/pb_bufmgr_slab.c \
$src_dir/src/gallium/auxiliary/pipebuffer/pb_cache.c \
$src_dir/src/gallium/auxiliary/pipebuffer/pb_slab.c \
$src_dir/src/gallium/auxiliary/pipebuffer/pb_validate.c \
$src_dir/src/gallium/auxiliary/postprocess/pp_celshade.c \
$src_dir/src/gallium/auxiliary/postprocess/pp_colors.c \
$src_dir/src/gallium/auxiliary/postprocess/pp_colors.h \
$src_dir/src/gallium/auxiliary/postprocess/pp_init.c \
$src_dir/src/gallium/auxiliary/postprocess/pp_mlaa.c \
$src_dir/src/gallium/auxiliary/postprocess/pp_program.c \
$src_dir/src/gallium/auxiliary/postprocess/pp_run.c \
$src_dir/src/gallium/auxiliary/rtasm/rtasm_execmem.c \
$src_dir/src/gallium/auxiliary/rtasm/rtasm_x86sse.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_aa_point.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_build.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_dump.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_dynamic_indexing.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_exec.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_from_mesa.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_info.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_iterate.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_lowering.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_parse.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_point_sprite.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_sanity.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_scan.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_strings.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_text.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_transform.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_two_side.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_ureg.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_util.c \
$src_dir/src/gallium/auxiliary/tgsi/tgsi_vpos.c \
$src_dir/src/gallium/auxiliary/translate/translate.c \
$src_dir/src/gallium/auxiliary/translate/translate_cache.c \
$src_dir/src/gallium/auxiliary/translate/translate_generic.c \
$src_dir/src/gallium/auxiliary/translate/translate_sse.c \
$src_dir/src/gallium/auxiliary/util/u_async_debug.c \
$src_dir/src/gallium/auxiliary/util/u_bitmask.c \
$src_dir/src/gallium/auxiliary/util/u_blitter.c \
$src_dir/src/gallium/auxiliary/util/u_cache.c \
$src_dir/src/gallium/auxiliary/util/u_compute.c \
$src_dir/src/gallium/auxiliary/util/u_debug_describe.c \
$src_dir/src/gallium/auxiliary/util/u_debug_flush.c \
$src_dir/src/gallium/auxiliary/util/u_debug_image.c \
$src_dir/src/gallium/auxiliary/util/u_debug_refcnt.c \
$src_dir/src/gallium/auxiliary/util/u_draw.c \
$src_dir/src/gallium/auxiliary/util/u_draw_quad.c \
$src_dir/src/gallium/auxiliary/util/u_driconf.c \
$src_dir/src/gallium/auxiliary/util/u_dump_defines.c \
$src_dir/src/gallium/auxiliary/util/u_dump_state.c \
$src_dir/src/gallium/auxiliary/util/u_framebuffer.c \
$src_dir/src/gallium/auxiliary/util/u_gen_mipmap.c \
$src_dir/src/gallium/auxiliary/util/u_handle_table.c \
$src_dir/src/gallium/auxiliary/util/u_helpers.c \
$src_dir/src/gallium/auxiliary/util/u_index_modify.c \
$src_dir/src/gallium/auxiliary/util/u_live_shader_cache.c \
$src_dir/src/gallium/auxiliary/util/u_log.c \
$src_dir/src/gallium/auxiliary/util/u_prim.c \
$src_dir/src/gallium/auxiliary/util/u_prim_restart.c \
$src_dir/src/gallium/auxiliary/util/u_pstipple.c \
$src_dir/src/gallium/auxiliary/util/u_resource.c \
$src_dir/src/gallium/auxiliary/util/u_sample_positions.c \
$src_dir/src/gallium/auxiliary/util/u_sampler.c \
$src_dir/src/gallium/auxiliary/util/u_screen.c \
$src_dir/src/gallium/auxiliary/util/u_simple_shaders.c \
$src_dir/src/gallium/auxiliary/util/u_split_draw.c \
$src_dir/src/gallium/auxiliary/util/u_suballoc.c \
$src_dir/src/gallium/auxiliary/util/u_surface.c \
$src_dir/src/gallium/auxiliary/util/u_tests.c \
$src_dir/src/gallium/auxiliary/util/u_texture.c \
$src_dir/src/gallium/auxiliary/util/u_tile.c \
$src_dir/src/gallium/auxiliary/util/u_transfer.c \
$src_dir/src/gallium/auxiliary/util/u_transfer_helper.c \
$src_dir/src/gallium/auxiliary/util/u_threaded_context.c \
$src_dir/src/gallium/auxiliary/util/u_upload_mgr.c \
$src_dir/src/gallium/auxiliary/util/u_vbuf.c \
$src_dir/src/gallium/auxiliary/util/u_vertex_state_cache.c \
$src_dir/src/gallium/auxiliary/nir/nir_draw_helpers.c \
$src_dir/src/gallium/auxiliary/nir/tgsi_to_nir.c \
$src_dir/src/gallium/auxiliary/nir/nir_to_tgsi.c \
\
$src_dir/src/gallium/auxiliary/renderonly/renderonly.c"
#-------------------------------------------------------------------------------
for src_pathname in $libgallium_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/auxiliary/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/auxiliary/$cpp_filename \
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/src/gallium/auxiliary/driver_trace \
		-I$src_dir/src/gallium/auxiliary/driver_trace \
		-I$build_dir/src/compiler/nir \
		-I$src_dir/src/compiler/nir \
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/gallium/auxiliary/util \
		-I$src_dir/src/gallium/auxiliary/util \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libgallium_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).cpp.c
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	printf "CC_S $build_dir/src/gallium/auxiliary/$cpp_filename --> $build_dir/src/gallium/auxiliary/$asm_filename\n"
	$cc_s $build_dir/src/gallium/auxiliary/$cpp_filename -o $build_dir/src/gallium/auxiliary/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libgallium_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).cpp.c.s
	o_filename=$(basename $src_pathname .c).cpp.c.s.o
	os="$os $build_dir/src/gallium/auxiliary/$o_filename"
	printf "AS $build_dir/src/gallium/auxiliary/$asm_filename --> $build_dir/src/gallium/auxiliary/$o_filename\n"
	$as $build_dir/src/gallium/auxiliary/$asm_filename -o $build_dir/src/gallium/auxiliary/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libgallium.a $os\n"
$ar_rcs $build_dir/libgallium.a $os
#===============================================================================
mkdir $build_dir/src/gallium/auxiliary/pipe-loader
#-------------------------------------------------------------------------------
# pipe-loaders: static and dynamic.
# XXX:right below may be obsolete
# "backends" declare "devices", each linked to a specific winsys (windows
#  system):
#
#     - drm pipe-loader backend: declare a pipe loader device for each drm
#       supported piece of hardware. Each pipe loader device gets linked to the
#       piece of hardware specific winsys. For instance, amdgpu/drm device gets
#       linked to amdgpu/drm winsys (with the legacy radeon/drm winsys 
#       fallback). The drm backend is include only if HAVE_LIBDRM is defined.
#
#     - sw ("software") backend: declare the following devices:
#           - dri device linked to the dri winsys if HAVE_PIPE_LOADER_DRI is
#             defined
#           - kms_dri device linked to the kms_dri winsys if
#             HAVE_PIPE_LOADER_KMS is defined
#           - null device linked to the null winsys if DROP_PIPE_LOADER_MISC
#             is _NOT_ defined
#           - wrapper device linked to the wrapper winsys if
#             DROP_PIPE_LOADER_MISC is _NOT_ defined
libpipe_loader_c_pathnames="\
$src_dir/src/gallium/auxiliary/pipe-loader/pipe_loader.c \
$src_dir/src/gallium/auxiliary/pipe-loader/pipe_loader_drm.c \
$src_dir/src/gallium/auxiliary/pipe-loader/pipe_loader_sw.c \
"
#-------------------------------------------------------------------------------
# static pipe-loader
# XXX: pipe_loader_drm.c is refering to internal drm-uapi header directly
# without "drm-uapi" in their reference pathname, may be an upstream mistake.
for src_pathname in $libpipe_loader_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).static.cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/auxiliary/pipe-loader/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/auxiliary/pipe-loader/$cpp_filename \
		-DGALLIUM_STATIC_TARGETS=1 \
		\
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/include/drm-uapi \
		-I$src_dir/include/drm-uapi \
		\
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src/gallium/auxiliary/pipe-loader \
		-I$src_dir/src/gallium/auxiliary/pipe-loader \
		-I$build_dir/src/gallium/winsys \
		-I$src_dir/src/gallium/winsys \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libpipe_loader_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).static.cpp.c
	asm_filename=$(basename $src_pathname .c).static.cpp.c.s
	printf "CC_S $build_dir/src/gallium/auxiliary/pipe-loader/$cpp_filename --> $build_dir/src/gallium/auxiliary/pipe-loader/$asm_filename\n"
	$cc_s $build_dir/src/gallium/auxiliary/pipe-loader/$cpp_filename -o $build_dir/src/gallium/auxiliary/pipe-loader/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libpipe_loader_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).static.cpp.c.s
	o_filename=$(basename $src_pathname .c).static.cpp.c.s.o
	os="$os $build_dir/src/gallium/auxiliary/pipe-loader/$o_filename"
	printf "AS $build_dir/src/gallium/auxiliary/pipe-loader/$asm_filename --> $build_dir/src/gallium/auxiliary/pipe-loader/$o_filename\n"
	$as $build_dir/src/gallium/auxiliary/pipe-loader/$asm_filename -o $build_dir/src/gallium/auxiliary/pipe-loader/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libpipe_loader_static.a $os\n"
$ar_rcs $build_dir/libpipe_loader_static.a $os
#-------------------------------------------------------------------------------
# dynamic pipe-loader
for src_pathname in $libpipe_loader_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).dynamic.cpp.c
	printf "CPP $src_pathname --> $build_dir/src/gallium/auxiliary/pipe-loader/$cpp_filename\n"
	$cpp $src_pathname -o $build_dir/src/gallium/auxiliary/pipe-loader/$cpp_filename \
		-DPIPE_SEARCH_DIR=\"$pipe_search_dir\" \
		\
		-I$cc_internal_fixed_incdir \
		-I$cc_internal_incdir \
		-I$linux_incdir \
		-I$syslib_incdir \
		\
		-I$build_dir/include/drm-uapi \
		-I$src_dir/include/drm-uapi \
		\
		-I$build_dir/src/compiler \
		-I$src_dir/src/compiler \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src/gallium/auxiliary/pipe-loader \
		-I$src_dir/src/gallium/auxiliary/pipe-loader \
		-I$build_dir/src/gallium/winsys \
		-I$src_dir/src/gallium/winsys \
		-I$build_dir/src/gallium/auxiliary \
		-I$src_dir/src/gallium/auxiliary \
		-I$build_dir/src/gallium/include \
		-I$src_dir/src/gallium/include \
		-I$build_dir/src/util \
		-I$src_dir/src/util \
		-I$build_dir/src/loader \
		-I$src_dir/src/loader \
		-I$build_dir/src \
		-I$src_dir/src \
		-I$build_dir/include \
		-I$src_dir/include \
		\
		$syslib_cpp_flags_defs \
		$linux_cpp_flags_defs \
		$cc_builtins_cpp_flags_defs \
		$cc_attributes_cpp_flags_defs \
		$mesa_cpp_flags_defs \
		\
		$external_deps_cpp_flags &
done
#-------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
for src_pathname in $libpipe_loader_c_pathnames
do
	cpp_filename=$(basename $src_pathname .c).dynamic.cpp.c
	asm_filename=$(basename $src_pathname .c).dynamic.cpp.c.s
	printf "CC_S $build_dir/src/gallium/auxiliary/pipe-loader/$cpp_filename --> $build_dir/src/gallium/auxiliary/pipe-loader/$asm_filename\n"
	$cc_s $build_dir/src/gallium/auxiliary/pipe-loader/$cpp_filename -o $build_dir/src/gallium/auxiliary/pipe-loader/$asm_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
os=
for src_pathname in $libpipe_loader_c_pathnames
do
	asm_filename=$(basename $src_pathname .c).dynamic.cpp.c.s
	o_filename=$(basename $src_pathname .c).dynamic.cpp.c.s.o
	os="$os $build_dir/src/gallium/auxiliary/pipe-loader/$o_filename"
	printf "AS $build_dir/src/gallium/auxiliary/pipe-loader/$asm_filename --> $build_dir/src/gallium/auxiliary/pipe-loader/$o_filename\n"
	$as $build_dir/src/gallium/auxiliary/pipe-loader/$asm_filename -o $build_dir/src/gallium/auxiliary/pipe-loader/$o_filename &
done
#------------------------------------------------------------------------------
wait
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libpipe_loader_dynamic.a $os\n"
$ar_rcs $build_dir/libpipe_loader_dynamic.a $os
#===============================================================================
mkdir $build_dir/src/gallium/auxiliary/vl
#-------------------------------------------------------------------------------
# galliumvl
# vl=Video Layer, where you can find vdpau/vapi hardware accelerated drivers,
# but we do use only the empty stubs
printf "CPP $src_dir/src/gallium/auxiliary/vl/vl_stubs.c --> $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c\n"
$cpp $src_dir/src/gallium/auxiliary/vl/vl_stubs.c -o $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c \
	-I$cc_internal_fixed_incdir \
	-I$cc_internal_incdir \
	-I$linux_incdir \
	-I$syslib_incdir \
	\
	-I$build_dir/src/compiler \
	-I$src_dir/src/compiler \
	-I$build_dir/src/gallium/auxiliary \
	-I$src_dir/src/gallium/auxiliary \
	-I$build_dir/src/gallium/include \
	-I$src_dir/src/gallium/include \
	-I$build_dir/src/util \
	-I$src_dir/src/util \
	-I$build_dir/src \
	-I$src_dir/src \
	-I$build_dir/include \
	-I$src_dir/include \
	\
	$syslib_cpp_flags_defs \
	$linux_cpp_flags_defs \
	$cc_builtins_cpp_flags_defs \
	$cc_attributes_cpp_flags_defs \
	$mesa_cpp_flags_defs \
	\
	$external_deps_cpp_flags
#------------------------------------------------------------------------------
printf "CC_S $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c --> $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c.s\n"
$cc_s $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c -o $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c.s
#------------------------------------------------------------------------------
printf "AS $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c.s --> $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c.s.o\n"
$as $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c.s -o $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c.s.o
#------------------------------------------------------------------------------
printf "AR RCS $build_dir/libgalliumvl.a $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c.s.o\n"
$ar_rcs $build_dir/libgalliumvl.a $build_dir/src/gallium/auxiliary/vl/vl_stubs.cpp.c.s.o &
#===============================================================================
printf "\t<--gallium auxiliary sub-components built\n"
