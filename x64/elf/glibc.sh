#!/bin/sh

# The glibc is lost. With the help of the ELF design failures, their manic abuse of gnu symbol
# versioning, mandating a recent gcc compiler... actually some recent gnu only tools, this is just a
# text-book case of planned obsolescence. There is also the libs still requiring static loading
# because of the ELF static TLS flag (not fixed already?). Some glibc objects which are statically
# linked into binaries which require specific gnu symbol versions in the shared glibc libs.

# In the end, you don't really have more freedom than with msft windows. At least it has the decency
# to cost 0 and nobody should be paid for that work or even worse, pay for it. If anybody is, this
# is a scam.

# Downgraded to obsolete/scam and should be replaced.

# This is not a host != build script.

glibc_version=2.38

set -e
top_dir=$(readlink -f $(dirname $0))
top_src_dir=/root/nyanlinux/src
top_build_dir=/run/x64/elf/glibc
install_dir=/nyan/glibc/$glibc_version
jobs_n=11

printf "TOP_DIR=$top_dir\n"
printf "TOP_SRC_DIR=$top_src_dir\n"
printf "TOP_BUILD_DIR=$top_build_dir\n"
printf "INSTALL_DIR=$install_dir\n"
printf "USING $jobs_n PARALLEL JOBS\n"
#---------------------------------------------------------------------------------------------------
rm -Rf $top_build_dir
mkdir -p $top_build_dir
cd $top_build_dir
#---------------------------------------------------------------------------------------------------
(. $top_dir/glibc/bison.sh) &
(. $top_dir/glibc/gawk.sh) &
(. $top_dir/glibc/grep.sh) &
wait
. $top_dir/glibc/glibc.sh
. $top_dir/glibc/fix_install.sh
