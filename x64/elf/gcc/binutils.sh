printf 'BUILDING BINUTILS\n' 
mkdir -p $top_build_dir
cp -f $top_src_dir/binutils-2.41.tar.xz $top_build_dir
rm -Rf $top_build_dir/binutils-2.41
cd $top_build_dir
tar xf binutils-2.41.tar.xz

# release distribution is broken, as usual.
rm -Rf $top_build_dir/binutils
mkdir -p $top_build_dir/binutils/bin
cat >$top_build_dir/binutils/bin/makeinfo <<EOF
#!/bin/sh
while true
do
	echo \$@
	if test \$# = 0; then
		break
	fi

	case \$1 in
	-o)
		shift
		touch "\$1"
		shift
		;;
	*)
		shift
		;;
	esac
done
EOF
chmod +x $top_build_dir/binutils/bin/makeinfo

cat >$top_build_dir/binutils/bin/cc <<EOF
#!/bin/sh
exec /nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
	-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include \
	-O2 -fPIC -pipe \
	-B/nyan/glibc/current/lib \
	-static-libgcc -static -Wl,-static -Wl,-s \
	"\$@"
EOF
chmod +x $top_build_dir/binutils/bin/cc

cd  $top_build_dir/binutils
PATH_SAVED=$PATH
export PATH=/nyan/make/current/bin:$top_build_dir/binutils/bin:$PATH
export AR=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-ar
$top_build_dir/binutils-2.41/configure \
	--prefix=$install_dir \
	--with-static-standard-libraries \
	--disable-shared \
	--disable-host-shared \
	--enable-gold=no \
	--enable-gprofng=no \
	--disable-checks \
	--with-mmap \
	--disable-libctf \
	--disable-plugins \
	--disable-nls \
	--disable-multilib
unset AR
make -j $jobs_n
make install
cd $top_build_dir
rm -Rf binutils-2.41.tar.xz binutils-2.41 binutils
export PATH=$PATH_SAVED
