printf 'BUILDING MPC\n'
mkdir -p $top_build_dir
cp -f $top_src_dir/mpc-1.3.1.tar.gz $top_build_dir
rm -Rf $top_build_dir/mpc-1.3.1
cd $top_build_dir
tar xf mpc-1.3.1.tar.gz

rm -Rf $top_build_dir/mpc
mkdir -p $top_build_dir/mpc
cd  $top_build_dir/mpc
PATH_SAVED=$PATH
export PATH=/nyan/make/current/bin:$PATH
export CC="\
	/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
	-B/nyan/glibc/current/lib \
	-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include \
	-static-libgcc"
export CFLAGS='-O2 -fPIC -pipe'
export AR=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-ar
$top_build_dir/mpc-1.3.1/configure \
	--prefix=$install_dir \
	--with-gmp=$install_dir \
	--with-mpfr=$install_dir \
	--disable-shared
unset CC
unset CLFLAGS
unset AR
make -j $jobs_n
make install
cd $top_build_dir
rm -Rf mpc-1.3.1.tar.gz mpc-1.3.1 mpc
export PATH=$PATH_SAVED
