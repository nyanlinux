printf 'BUILDING GMP\n'
mkdir -p $top_build_dir
cp -f $top_src_dir/gmp-6.2.1.tar.xz $top_build_dir
rm -Rf $top_build_dir/gmp-6.2.1
cd $top_build_dir
tar xf gmp-6.2.1.tar.xz

rm -Rf $top_build_dir/gmp
mkdir -p $top_build_dir/gmp
cd  $top_build_dir/gmp
PATH_SAVED=$PATH
export PATH=/nyan/make/current/bin:$PATH
export CC="\
	/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
	-B/nyan/glibc/current/lib \
	-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include \
	-static-libgcc"
export CFLAGS='-O2 -fPIC -pipe'
export M4=/nyan/m4/current/bin/m4
export NM=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-nm
export AR=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-ar
$top_build_dir/gmp-6.2.1/configure \
	--prefix=$install_dir \
	--disable-shared
unset CC
unset CLFLAGS
unset M4
unset NM
unset AR
make -j $jobs_n
make install
cd $top_build_dir
rm -Rf gmp-6.2.1.tar.xz gmp-6.2.1 gmp
export PATH=$PATH_SAVED
