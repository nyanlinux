# XXX: gcc 13.2.0 is now requiring a c++11 compiler instead of c++98 (gcc 4.7.4).
# gcc is monkey grade work, no better. This is an insult to humanity, accute toxicity.

printf 'BUILDING GCC\n'

# It is not possible to have "system headers" split in 2 parts, glibc and linux.
# Additionaly, we don't want gcc to know where are our system headers.
# We build temporary native system headers in a random location in ram.
native_sys_hdr=$(mktemp -dt -p /run)
cp -r /nyan/glibc/current/include/* $native_sys_hdr
cp -r /nyan/linux-headers/current/include/* $native_sys_hdr

mkdir -p $top_build_dir
cp -f $top_src_dir/gcc-13.2.0.tar.xz $top_build_dir
rm -Rf $top_build_dir/gcc-13.2.0
cd $top_build_dir
tar xf gcc-13.2.0.tar.xz

#===================================================================================================
rm -Rf $top_build_dir/gcc
mkdir -p $top_build_dir/gcc/bin

cat >$top_build_dir/gcc/bin/cc <<EOF
#!/bin/sh
# We have a "fixed" cc, but if the build system really wants -nostdinc, adapt.
local_cppflags="-nostdinc \
	-isystem /nyan/toolchains/current/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/include \
	-isystem /nyan/toolchains/current/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/include-fixed \
	-isystem $native_sys_hdr"

for arg in "\$@"
do
	if test "\$arg" = "-nostdinc"; then
		local_cppflags=
	fi
done
exec /nyan/toolchains/current/bin/x86_64-nyan-linux-gnu-gcc \$local_cppflags "\$@"
EOF
chmod +x $top_build_dir/gcc/bin/cc

cat >$top_build_dir/gcc/bin/cxx <<EOF
#!/bin/sh
# We have a "fixed" cxx, but if the build system really wants -nostdinc or -nostdinc++, adapt.

for arg in "\$@"
do
	if test "\$arg" = "-nostdinc"; then
		have_nostdinc=yes
	elif test "\$arg" = "-nostdinc++"; then
		have_nostdincpp=yes
	fi
done

if test "\$have_nostdinc" = "yes"; then
	local_cppflags=
elif test "\$have_nostdincpp" = "yes"; then 
	local_cppflags="-nostdinc \
		-isystem /nyan/toolchains/current/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/include \
		-isystem /nyan/toolchains/current/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/include-fixed \
		-isystem $native_sys_hdr"
else
	local_cppflags="-nostdinc \
		-isystem /nyan/toolchains/current/include/c++/7.3.0 \
		-isystem /nyan/toolchains/current/include/c++/7.3.0/x86_64-nyan-linux-gnu \
		-isystem /nyan/toolchains/current/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/include \
		-isystem /nyan/toolchains/current/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/include-fixed \
		-isystem $native_sys_hdr"
fi
exec /nyan/toolchains/current/bin/x86_64-nyan-linux-gnu-g++ \$local_cppflags "\$@"
EOF
chmod +x $top_build_dir/gcc/bin/cxx

cd  $top_build_dir/gcc
PATH_SAVED=$PATH
export PATH=/nyan/make/current/bin:$top_build_dir/gcc/bin:$PATH
export CC=$top_build_dir/gcc/bin/cc
export CFLAGS='-O2 -fPIC -pipe'
export CXX=$top_build_dir/gcc/bin/cxx
export CXXFLAGS='-O2 -fPIC -pipe '
export LDFLAGS='-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -static -Wl,-s'

export CFLAGS_FOR_TARGET='-O2 -pipe -fPIC'
export LDFLAGS_FOR_TARGET="-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib -Wl,-s"
export AR=$install_dir/bin/ar
$top_build_dir/gcc-13.2.0/configure \
	--prefix=$install_dir \
	--disable-libquadmath \
	--disable-libada \
	--disable-libssp \
	--disable-bootstrap \
	--disable-host-shared \
	--disable-vtable-verify \
	--with-gpm=$install_dir \
	--with-mpfr=$install_dir \
	--with-mpc=$install_dir \
	--with-static-standard-libraries \
	--disable-nls \
	--disable-plugins \
	--disable-plugin \
	\
	--enable-decimal-float=no \
	--disable-fixed-point \
	--enable-threads=posix \
	--enable-tls \
	--disable-analyzer \
	--disable-gcov \
	--enable-languages=c,c++ \
	--disable-libssp \
	--disable-default-ssp \
	--enable-link-serialization \
	--disable-host-shared \
	--disable-multilib \
	--enable-explicit-exception-frame-registration \
	--with-native-system-header-dir=$native_sys_hdr
unset CC
unset CFLAGS
unset CXX
unset CXXFLAGS
unset LDFLAGS
unset CFLAGS_FOR_TARGET
unset CXXFLAGS_FOR_TARGET
unset LDFLAGS_FOR_TARGET
unset AR
make -j $jobs_n all-gcc
make install-gcc
# Is libgcc ready for splitting from gcc source tree?
make -j $jobs_n all-target-libgcc
make install-target-libgcc
export PATH=$PATH_SAVED
#===================================================================================================
# Libtool is doing crap as usual, cannot link properly that diarreha of libstdc++ using the top
# gcc build system. Fortunately, libstdc++ is ready for splitting from gcc source tree.

rm -Rf $top_build_dir/libstdcxx
mkdir -p $top_build_dir/libstdcxx/bin

cat >$top_build_dir/libstdcxx/bin/cc <<EOF
#!/bin/sh
# We have a "fixed" cc, but if the build system really wants -nostdinc, adapt.
local_cppflags="-nostdinc \
	-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include \
	-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include-fixed \
	-isystem $native_sys_hdr"

local_link='-B/opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib64 -B/nyan/glibc/current/lib'

for arg in "\$@"
do
	if test "\$arg" = "-nostdinc"; then
		local_cppflags=
	fi
	if test "\$arg" = "-c" -o "\$arg" = '-E'; then
		local_link=
	fi
done
exec /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/bin/x86_64-pc-linux-gnu-gcc \$local_link \$local_cppflags "\$@"
EOF
chmod +x $top_build_dir/libstdcxx/bin/cc

cat >$top_build_dir/libstdcxx/bin/cxx <<EOF
#!/bin/sh
# We have a "fixed" cxx, but if the build system really wants -nostdinc or -nostdinc++, adapt.

local_link='-B/opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib64 -B/nyan/glibc/current/lib'

for arg in "\$@"
do
	if test "\$arg" = "-nostdinc"; then
		have_nostdinc=yes
	elif test "\$arg" = "-nostdinc++"; then
		have_nostdincpp=yes
	fi
	if test "\$arg" = "-c" -o "\$arg" = '-E'; then
		local_link=
	fi
done

if test "\$have_nostdinc" = "yes"; then
	local_cppflags=
elif test "\$have_nostdincpp" = "yes"; then 
	local_cppflags="-nostdinc \
		-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include \
		-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include-fixed \
		-isystem $native_sys_hdr"
else
	local_cppflags="-nostdinc \
		-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/../../../../include/c++/13.2.0 \
		-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/../../../../include/c++/13.2.0/x86_64-pc-linux-gnu \
		-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/../../../../include/c++/13.2.0/backward \
		-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include \
		-isystem /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib/gcc/x86_64-pc-linux-gnu/13.2.0/include-fixed \
		-isystem $native_sys_hdr"
fi
exec /opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/bin/x86_64-pc-linux-gnu-gcc \$local_link \$local_cppflags "\$@"
EOF
chmod +x $top_build_dir/libstdcxx/bin/cxx

# XXX: lol, still need some hardcoded stuff from libgcc build directory, here still need the
# gthr-default.h (libstdc++ threading uses gthread for its non single-threaded-only implementation)
# header.
# We force it here, to allow a "standalone" build of libstdc++.
if ! test -e $top_build_dir/libgcc/gthr-default.h;then
	mkdir -p $top_build_dir/libgcc
	ln -sTf $top_build_dir/gcc-13.2.0/libgcc/gthr-posix.h $top_build_dir/libgcc/gthr-default.h
fi

cd $top_build_dir/libstdcxx
PATH_SAVED=$PATH
export PATH=/nyan/make/current/bin:$top_build_dir/libstdcxx/bin:$PATH
export CC=$top_build_dir/libstdcxx/bin/cc
export CFLAGS='-O2 -fPIC -pipe'
export CXX=$top_build_dir/libstdcxx/bin/cxx
export CXXFLAGS='-O2 -fPIC -pipe '
export LDFLAGS='-L/opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0/lib64 -L/nyan/glibc/current/lib -Wl,-s'
export AR=$install_dir/bin/ar
export RANLIB=$install_dir/bin/ranlib
$top_build_dir/gcc-13.2.0/libstdc++-v3/configure \
	--prefix=$install_dir \
	--disable-multilib \
	--disable-libstdcxx-pch \
	--disable-nls \
	--disable-rpath \
	--enable-linux-futex \
	--enable-libstdcxx-threads \
	--with-gnu-ld
unset CC
unset CFLAGS
unset CXX
unset CXXFLAGS
unset LDFLAGS
unset AR
unset RANLIB
make -j $jobs_n
make install
export PATH=$PATH_SAVED

cd $top_build_dir
rm -Rf $top_build_dir $native_sys_hdr
