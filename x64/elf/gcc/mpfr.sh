printf 'BUILDING MPFR\n'
mkdir -p $top_build_dir
cp -f $top_src_dir/mpfr-4.2.0.tar.xz $top_build_dir
rm -Rf $top_build_dir/mpfr-4.2.0
cd $top_build_dir
tar xf mpfr-4.2.0.tar.xz

rm -Rf $top_build_dir/mpfr
mkdir -p $top_build_dir/mpfr
cd  $top_build_dir/mpfr
PATH_SAVED=$PATH
export PATH=/nyan/make/current/bin:$PATH
export CC="\
	/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-gcc \
	-B/nyan/glibc/current/lib \
	-isystem /nyan/glibc/current/include -isystem /nyan/linux-headers/current/include \
	-static-libgcc"
export CFLAGS='-O2 -fPIC -pipe'
export AR=/nyan/toolchains/binutils-2.36.1-gcc-4.7.4/current/bin/x86_64-nyan2-linux-gnu-ar
$top_build_dir/mpfr-4.2.0/configure \
	--prefix=$install_dir \
	--with-gmp=$install_dir \
	--disable-shared
unset CC
unset CLFLAGS
unset AR
make -j $jobs_n
make install
cd $top_build_dir
rm -Rf mpfr-4.2.0.tar.xz mpfr-4.2.0 mpfr
export PATH=$PATH_SAVED
