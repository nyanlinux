#!/bin/sh

# gcc is lost, like clang/llvm and similar.
# It is now considered as an invisible backdoor injector in source code.
# Mandatory for bootstrapping a modern elf/linux system.
# Downgraded to /opt.

# You must use an ultra complex ISO c++11 compiler to compile this C compiler, probably intended
# for vendor induction-lock-in.
# This is beyond toxic, probably one of the biggest mistakes in open source software, ever.

set -e
top_dir=$(readlink -f $(dirname $0))
top_src_dir=/root/nyanlinux/src
top_build_dir=/run/x64/elf/build
install_dir=/opt/toolchains/x64/elf/binutils-gcc/2.41-13.2.0
jobs_n=11

printf "TOP_DIR=$top_dir\n"
printf "TOP_SRC_DIR=$top_src_dir\n"
printf "TOP_BUILD_DIR=$top_build_dir\n"
printf "INSTALL_DIR=$install_dir\n"
printf "USING $jobs_n PARALLEL JOBS"
. $top_dir/gcc/gmp.sh
. $top_dir/gcc/mpfr.sh
. $top_dir/gcc/mpc.sh
. $top_dir/gcc/binutils.sh
. $top_dir/gcc/gcc.sh
