export PATH="\
$top_build_dir/sdk/bin\
:/nyan/busybox/current/sbin:/nyan/busybox/current/bin\
:/opt/toolchains/x64/elf/binutils-gcc/current/bin\
:/nyan/nyanbison/current/sbin\
:/nyan/python/current/bin\
:/nyan/make/current/bin"

rm -Rf $top_build_dir/target-system-glibc
mkdir $top_build_dir/target-system-glibc
cd $top_build_dir/target-system-glibc
#--------------------------------------------------------------------------------------------------
cp -f $top_src_dir/glibc-$glibc_version.tar.xz ./
tar xf glibc-$glibc_version.tar.xz

mkdir ./build
cd ./build
#---------------------------------------------------------------------------------------------------
export LDFLAGS='-static-libgcc'
$top_build_dir/target-system-glibc/glibc-$glibc_version/configure \
	--prefix=$install_dir \
	--with-headers=/nyan/linux-headers/current/include \
	--enable-stack-protector=no \
	--disable-build-nscd \
	--disable-nscd
unset LDFLAGS	
make -j $jobs_n
make install
