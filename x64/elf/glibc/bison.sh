export PATH="\
:/nyan/busybox/current/sbin:/nyan/busybox/current/bin\
:/opt/toolchains/x64/elf/binutils-gcc/current/bin\
:/nyan/m4/current/bin\
:/nyan/make/current/bin\
"

rm -Rf $top_build_dir/build-system-bison
mkdir $top_build_dir/build-system-bison
cd $top_build_dir/build-system-bison
#--------------------------------------------------------------------------------------------------
cp $top_src_dir/bison-3.8.2.tar.xz ./
tar xf bison-3.8.2.tar.xz

mkdir ./build
cd ./build

# It really wants everything there.
export CC="gcc \
-isystem /nyan/linux-headers/current/include -isystem /nyan/glibc/current/include \
-O2 -pipe -static-libgcc \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
"

$top_build_dir/build-system-bison/bison-3.8.2/configure \
	--prefix=$top_build_dir/sdk \
	--enable-threads=posix \
	--disable-rpath \
	--disable-nls
unset CC
make -j $jobs_n
make install

cd $top_build_dir
