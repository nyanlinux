rm -f $install_dir/bin/ld.so
mkdir -p $install_dir/lib64
mv -f $install_dir/lib/ld-linux-x86-64.so.2 $install_dir/lib64

/opt/toolchains/x64/elf/binutils-gcc/current/bin/strip -s \
$install_dir/bin/gencat \
$install_dir/bin/getconf \
$install_dir/bin/getent \
$install_dir/bin/iconv \
$install_dir/bin/locale \
$install_dir/bin/localedef \
$install_dir/bin/makedb \
$install_dir/bin/pcprofiledump \
$install_dir/bin/pldd \
$install_dir/bin/sprof \
$install_dir/bin/zdump \
\
$install_dir/lib/audit/*.so \
$install_dir/lib/gconv/*.so \
$install_dir/lib/libBrokenLocale.so.1 \
$install_dir/lib/libanl.so.1 \
$install_dir/lib/libc.so.6 \
$install_dir/lib/libdl.so.2 \
$install_dir/lib/libm.so.6 \
$install_dir/lib/libmvec.so.1 \
$install_dir/lib/libnsl.so.1 \
$install_dir/lib/libnss_compat.so.2 \
$install_dir/lib/libnss_db.so.2 \
$install_dir/lib/libnss_dns.so.2 \
$install_dir/lib/libnss_files.so.2 \
$install_dir/lib/libnss_hesiod.so.2 \
$install_dir/lib/libpthread.so.0 \
$install_dir/lib/libresolv.so.2 \
$install_dir/lib/librt.so.1 \
$install_dir/lib/libthread_db.so.1 \
$install_dir/lib/libutil.so.1 \
\
$install_dir/lib64/ld-linux-x86-64.so.2 \
\
$install_dir/libexec/getconf/POSIX_V6_LP64_OFF64 \
$install_dir/libexec/getconf/POSIX_V7_LP64_OFF64 \
$install_dir/libexec/getconf/XBS5_LP64_OFF64 \
\
$install_dir/sbin/iconvconfig \
$install_dir/sbin/ldconfig \
$install_dir/sbin/sln \
$install_dir/sbin/zic
