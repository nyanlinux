export PATH="\
:/nyan/busybox/current/sbin:/nyan/busybox/current/bin\
:/opt/toolchains/x64/elf/binutils-gcc/current/bin\
:/nyan/make/current/bin\
"

rm -Rf $top_build_dir/build-system-gawk
mkdir $top_build_dir/build-system-gawk
cd $top_build_dir/build-system-gawk
#--------------------------------------------------------------------------------------------------
cp $top_src_dir/gawk-5.2.2.tar.xz ./
tar xf gawk-5.2.2.tar.xz

mkdir ./build
cd ./build

# It really wants everything there.
export CC="gcc \
-isystem /nyan/linux-headers/current/include -isystem /nyan/glibc/current/include \
-O2 -pipe -static-libgcc \
-B/nyan/glibc/current/lib -L/nyan/glibc/current/lib \
"
$top_build_dir/build-system-gawk/gawk-5.2.2/configure \
	--prefix=$top_build_dir/sdk \
	--disable-mpfr \
	--disable-pma \
	--enable-threads=posix \
	--disable-rpath \
	--disable-nls \
	--disable-extensions
unset CC
make -j $jobs_n
make install

cd $top_build_dir
