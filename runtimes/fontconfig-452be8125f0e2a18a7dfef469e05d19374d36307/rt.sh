src_name=fontconfig
version=${pkg_name##*-}
slot=$version

rm -Rf /usr/share/fontconfig
cp -r /nyan/$src_name/$slot/fontconfig /usr/share/fontconfig

if test -e /etc/fonts; then
	mv /etc/fonts /etc/fonts.PREV-$(date +%s)
fi
cp -r /nyan/$src_name/$slot/fonts /etc/fonts

cp -f /nyan/$src_name/$slot/lib/libfontconfig.so.1.13.0 /usr/lib/libfontconfig.so.1

mkdir /var
mkdir /var/cache
mkdir /var/cache/fontconfig
