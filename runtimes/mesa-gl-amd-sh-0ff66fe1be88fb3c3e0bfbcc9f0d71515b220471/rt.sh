git_commit=${pkg_name##*-}
slot=$git_commit

cp /nyan/mesa-gl/$slot/libEGL_mesa.so.0.0.0 /usr/lib/libEGL_mesa.so.0
cp /nyan/mesa-gl/$slot/libGLX_mesa.so.0.0.0 /usr/lib/libGLX_mesa.so.0
cp /nyan/mesa-gl/$slot/libgallium_dri.so    /usr/lib/libgallium_dri.so
cp /nyan/mesa-gl/$slot/libgbm.so.1.0.0      /usr/lib/libgbm.so.1

mkdir /usr/lib/gbm
cp /nyan/mesa-gl/$slot/gbm/dri_gbm.so /usr/lib/gbm/dri_gbm.so

mkdir /usr/share/glvnd
mkdir /usr/share/glvnd/egl_vendor.d
cp /nyan/mesa-gl/$slot/glvnd/egl_vendor.d/50_mesa.json /usr/share/glvnd/egl_vendor.d/50_mesa.json

mkdir /usr/share/drirc.d
cp /nyan/mesa-gl/$slot/drirc.d/00-mesa-defaults.conf /usr/share/drirc.d/00-mesa-defaults.conf
