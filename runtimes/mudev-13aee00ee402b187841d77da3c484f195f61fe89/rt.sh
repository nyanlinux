src_name=mudev
git_commit=${pkg_name##*-}
slot=$git_commit

# shared between xorg(libpciaccess) and mudev
mkdir /share
mkdir /share/hwdata
cp -f /nyan/$src_name/$slot/hwdata/pci.ids /share/hwdata/pci.ids
cp -f /nyan/$src_name/$slot/hwdata/usb.ids /share/hwdata/usb.ids

# for video games (libSDL)
cp -f /nyan/$src_name/$slot/lib/libudev.so.0.0.0 /usr/lib/libudev.so.0
