src_name=libxcb
git_commit=${pkg_name##*-}
slot=$git_commit

cp -f /nyan/$src_name/$slot/lib/libxcb.so.1.1.0               /usr/lib/libxcb.so.1
cp -f /nyan/$src_name/$slot/lib/libxcb-composite.so.0.0.0     /usr/lib/libxcb-composite.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-damage.so.0.0.0        /usr/lib/libxcb-damage.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-dbe.so.0.0.0           /usr/lib/libxcb-dbe.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-dpms.so.0.0.0          /usr/lib/libxcb-dpms.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-dri2.so.0.0.0          /usr/lib/libxcb-dri2.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-dri3.so.0.1.0          /usr/lib/libxcb-dri3.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-present.so.0.0.0       /usr/lib/libxcb-present.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-glx.so.0.0.0           /usr/lib/libxcb-glx.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-randr.so.0.1.0         /usr/lib/libxcb-randr.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-record.so.0.0.0        /usr/lib/libxcb-record.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-render.so.0.0.0        /usr/lib/libxcb-render.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-res.so.0.0.0           /usr/lib/libxcb-res.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-screensaver.so.0.0.0   /usr/lib/libxcb-screensaver.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-shape.so.0.0.0         /usr/lib/libxcb-shape.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-shm.so.0.0.0           /usr/lib/libxcb-shm.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-sync.so.1.0.0          /usr/lib/libxcb-sync.so.1
cp -f /nyan/$src_name/$slot/lib/libxcb-xf86dri.so.0.0.0       /usr/lib/libxcb-xf86dri.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-xfixes.so.0.0.0        /usr/lib/libxcb-xfixes.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-xinerama.so.0.0.0      /usr/lib/libxcb-xinerama.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-xinput.so.0.1.0        /usr/lib/libxcb-xinput.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-xkb.so.1.0.0           /usr/lib/libxcb-xkb.so.1
cp -f /nyan/$src_name/$slot/lib/libxcb-xtest.so.0.0.0         /usr/lib/libxcb-xtest.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-xv.so.0.0.0            /usr/lib/libxcb-xv.so.0
cp -f /nyan/$src_name/$slot/lib/libxcb-xvmc.so.0.0.0          /usr/lib/libxcb-xvmc.so.0
