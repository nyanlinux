src_name=mesa
git_commit=${pkg_name##*-}
slot=$git_commit

cp -f /nyan/mesa-vulkan/$slot/libvulkan_radeon.so	 /usr/lib/libvulkan_radeon.so

mkdir /usr/share/vulkan
mkdir /usr/share/vulkan/icd.d

cp -f /nyan/mesa-vulkan/$slot/radeon_icd.x86_64.json	/usr/share/vulkan/icd.d/radeon_icd.x86_64.json
