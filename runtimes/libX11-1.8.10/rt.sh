src_name=libX11
version=${pkg_name##*-}
slot=$version

cp -f /nyan/$src_name/$slot/lib/libX11-xcb.so.1.0.0	/usr/lib/libX11-xcb.so.1
cp -f /nyan/$src_name/$slot/lib/libX11.so.6.4.0		/usr/lib/libX11.so.6

mkdir /usr/share/X11
cp	/nyan/$src_name/$slot/share/X11/XErrorDB	/usr/share/X11/XErrorDB
cp	/nyan/$src_name/$slot/share/X11/Xcms.txt	/usr/share/X11/Xcms.txt
rm -Rf /usr/share/X11/locale
cp -r	/nyan/$src_name/$slot/share/X11/locale		/usr/share/X11/locale
