src_name=drm

mkdir /usr/share/libdrm
cp -f /nyan/$src_name/$slot/share/libdrm/amdgpu.ids	/usr/share/libdrm/amdgpu.ids
cp -f /nyan/$src_name/$slot/lib/libdrm.so.2.4.122	/usr/lib/libdrm.so.2
cp -f /nyan/$src_name/$slot/lib/libdrm_amdgpu.so.1.0.0	/usr/lib/libdrm_amdgpu.so.1
cp -f /nyan/$src_name/$slot/lib/libdrm_radeon.so.1.0.1	/usr/lib/libdrm_radeon.so.1

# We follow the alsa-lib audio scheme.
if ! grep -E -q '^video:' /etc/group; then
	echo 'video::12:' >>/etc/group
fi
