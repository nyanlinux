src_name=alsa-lib
version=${pkg_name##*-}
slot=$version

rm -Rf /usr/share/alsa
cp -r /nyan/$src_name/$slot/share/alsa /usr/share/alsa

cp -f /nyan/$src_name/$slot/lib/libasound.so.2.0.0 /usr/lib/libasound.so.2

# It wants an "audio" group or dmix/dsnoop won't work
if ! grep -E -q '^audio:' /etc/group; then
	echo 'audio::11:' >>/etc/group
fi
